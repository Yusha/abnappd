import React, { Component } from 'react';
import 'typeface-roboto';

import LandingScreen from './pages/LandingScreen';
import ContractsApp from './pages/contracts/ContractsApp';

import BizObjectsApp from './pages/bizobjects/BizObjectsApp';

import CryptoExpressApp from "./pages/cryptoex/CryptoExpressApp";
import FakeProofApp from './pages/fakeproof/FakeProofApp';
import DocuContractApp from './pages/docucontracts/DocuContractsApp';
import MessagingApp from './pages/messaging/MessagingApp';
import ActionsApp from './pages/actions/ActionsApp'
import AccountsApp from './pages/accounts/AccountsApp';
import PIApp from './pages/pi/PIApp';
import DashboardApp from './pages/dashboard/DashboardApp';
import NetworkApp from './pages/network/NetworkApp';
import BoardApp from './pages/collaborate/board/BoardApp';


import PouchDB from 'pouchdb-browser';
import CollaborateApp from "./pages/collaborate/CollaborateApp";

const myDB = new PouchDB('myDB',{auto_compaction: true});




class App extends Component {
  constructor(props) {
    super(props);
  
    this.state = {
      landingScreen: false,
      dashboard: false,
      comm : false,
      activeScreen:0,
      context:{},
      data:""
    };
  
    this.logoutCallback = this.logoutCallback.bind(this);
    this.loginSuccessCallback = this.loginSuccessCallback.bind(this);
    this.setActiveScreen = this.setActiveScreen.bind(this);
    
  }
  
  setActiveScreen(screenId,data) {
    this.setState({activeScreen:screenId,data:data});
    
  }
  
  loginSuccessCallback(ctx) {
    this.setState({activeScreen:1, context:ctx});
    
  }
  
  logoutCallback() {
  
    var that = this;
  
    myDB.get('myHome').then(function(doc) {
      return myDB.remove(doc);
    }).then(function (result) {
      console.log("Logout successfully. Removed myHome");
      that.setActiveScreen(0);
    }).catch(function (err) {
      console.log("Error in logging out " + err);
      that.setActiveScreen(0);
    });
    
  }
  
  
  
  componentDidMount() {
  
  
  }
  
  
  componentWillUnmount() {
  
  }
  
  
  render() {
    return (
      <div>
  
        
        {this.state.activeScreen === 0 &&   <LandingScreen setLoginSuccessCallback={ this.loginSuccessCallback} logoutCallback={this.logoutCallback} /> }
  
        { this.state.activeScreen === 1 && <DashboardApp  setActiveScreenCallback={ this.setActiveScreen}
                                                             context={this.state.context}
                                                             logoutCallback={this.logoutCallback} />
        }
  
        { this.state.activeScreen === 2 && <NetworkApp  setActiveScreenCallback={ this.setActiveScreen} context={this.state.context}
                                                           logoutCallback={this.logoutCallback}/> }
        
        { this.state.activeScreen === 3 && <BizObjectsApp  setActiveScreenCallback={ this.setActiveScreen} context={this.state.context}
                                                           logoutCallback={this.logoutCallback}/> }
        { this.state.activeScreen === 4 && <ContractsApp  setActiveScreenCallback={ this.setActiveScreen} context={this.state.context}
                                                          logoutCallback={this.logoutCallback} /> }
        { this.state.activeScreen === 5 && <AccountsApp  setActiveScreenCallback={ this.setActiveScreen} context={this.state.context}
                                                         logoutCallback={this.logoutCallback} /> }
  
        { this.state.activeScreen === 6 && <CollaborateApp  setActiveScreenCallback={ this.setActiveScreen} context={this.state.context}
                                                         logoutCallback={this.logoutCallback} /> }
                                                         
        { this.state.activeScreen === 7 && <CryptoExpressApp  setActiveScreenCallback={ this.setActiveScreen} context={this.state.context}
                                                              logoutCallback={this.logoutCallback}/> }
        { this.state.activeScreen === 8 && <FakeProofApp  setActiveScreenCallback={ this.setActiveScreen} context={this.state.context}
                                                          logoutCallback={this.logoutCallback}/> }
        { this.state.activeScreen === 9 && <DocuContractApp  setActiveScreenCallback={ this.setActiveScreen} context={this.state.context}
                                                             logoutCallback={this.logoutCallback}/> }
        { this.state.activeScreen === 10 && <MessagingApp  setActiveScreenCallback={ this.setActiveScreen} context={this.state.context}
                                                           logoutCallback={this.logoutCallback}/> }
        { this.state.activeScreen === 11 && <ActionsApp  setActiveScreenCallback={ this.setActiveScreen} context={this.state.context}
                                                         logoutCallback={this.logoutCallback}/> }
        { this.state.activeScreen === 12 && <PIApp  setActiveScreenCallback={ this.setActiveScreen} context={this.state.context}
                                                         logoutCallback={this.logoutCallback}/> }
        { this.state.activeScreen === 13 && <BoardApp  setActiveScreenCallback={ this.setActiveScreen} context={this.state.context}
                                                    logoutCallback={this.logoutCallback} data = {this.state.data}/> }


      </div>
    );
  }
  
}

export default App;
