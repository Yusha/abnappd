
let axios = require('axios');
let crypto = require('crypto');

let EC = require('elliptic').ec;


function createAccountTx1() {
  
  let orgId = "Microsoft";
  let accountId = "admin@microsoft.com";
  
  // create public and private key pair
  
  const ecurve = new EC('secp256k1');
  
  const newkey = ecurve.genKeyPair({entropy:"Microsoft Admin Key1 For Microsoft Admin Key 1 for Microsoft Admin Key 1"});
  
  const publicKey = newkey.getPublic().encode('hex');
  
  const pk = newkey.getPrivate('hex');
  
  const ec = new EC('secp256k1');
  
  const privateKey = ec.keyFromPrivate(pk,'hex');
  
  
  const body = {
    name:"Alex Jonex",
    orgId:orgId,
    email:"alex@microsoft.com",
    phone:"6033222333",
    role:"Admin",
    group:"Finance"
  };
  
  const hash = GetHash(JSON.stringify(body));
  const signatureOf = privateKey.sign(hash).toDER('hex');
  
  let header = {
    fromOrg:orgId,
    fromAccount:accountId,
    publicKey:publicKey,
    type:"Account",
    action:"New",
    hash:hash,
    signature:signatureOf,
  };
  
  let tx = {
    header: header,
    body: body
  };
  
  // send out to the localchain
  
  const url = "http://127.0.0.1:5000/tx/add";
  
  axios.post(url,tx).then ((result) => {
    console.log("Posted Transaction 1... " + result);
    
  }).catch ((error) => {
    console.log("Network Error... " + error );
  });
}

function createAccountTx2() {
  
  let orgId = "Microsoft";
  let accountId = "tom@microsoft.com";
  
  // create public and private key pair
  
  const ecurve = new EC('secp256k1');
  
  const newkey = ecurve.genKeyPair({entropy:"Microsoft Admin Key1 For Microsoft Admin Key 1 for Microsoft Admin Key 1"});
  
  const publicKey = newkey.getPublic().encode('hex');
  
  const pk = newkey.getPrivate('hex');
  
  const ec = new EC('secp256k1');
  
  const privateKey = ec.keyFromPrivate(pk,'hex');
  
  
  const body = {
    name:"Tom Lee",
    orgId:orgId,
    email:"tom@microsoft.com",
    phone:"6033222333",
    role:"Admin",
    group:"HR"
  };
  
  const hash = GetHash(JSON.stringify(body));
  const signatureOf = privateKey.sign(hash).toDER('hex');
  
  let header = {
    fromOrg:orgId,
    fromAccount:accountId,
    publicKey:publicKey,
    type:"Account",
    action:"New",
    hash:hash,
    signature:signatureOf,
  };
  
  let tx = {
    header: header,
    body: body
  };
  
  // send out to the localchain
  
  const url = "http://127.0.0.1:5000/tx/add";
  
  axios.post(url,tx).then ((result) => {
    console.log("Posted Transaction 1... " + result);
    
  }).catch ((error) => {
    console.log("Network Error... " + error );
  });
}

function createRoleTx1() {
  
  let orgId = "Microsoft";
  let accountId = "admin@microsoft.com";
  
  // create public and private key pair
  
  const ecurve = new EC('secp256k1');
  
  const newkey = ecurve.genKeyPair({entropy:"Microsoft Admin Key1 For Microsoft Admin Key 1 for Microsoft Admin Key 1"});
  
  const publicKey = newkey.getPublic().encode('hex');
  
  const pk = newkey.getPrivate('hex');
  
  const ec = new EC('secp256k1');
  
  const privateKey = ec.keyFromPrivate(pk,'hex');
  
  
  const body = {
    name:"Network",
    orgId:orgId,
    description:" Network role.. Allowed to send transactions to Network"
  };
  
  const hash = GetHash(JSON.stringify(body));
  const signatureOf = privateKey.sign(hash).toDER('hex');
  
  let header = {
    fromOrg:orgId,
    fromAccount:accountId,
    publicKey:publicKey,
    type:"Role",
    action:"New",
    hash:hash,
    signature:signatureOf,
  };
  
  let tx = {
    header: header,
    body: body
  };
  
  // send out to the localchain
  
  const url = "http://127.0.0.1:5000/tx/add";
  
  axios.post(url,tx).then ((result) => {
    console.log("Posted Transaction 1... " + result);
    
  }).catch ((error) => {
    console.log("Network Error... " + error );
  });
}

function createRoleTx2() {
  
  let orgId = "Microsoft";
  let accountId = "admin@microsoft.com";
  
  // create public and private key pair
  
  const ecurve = new EC('secp256k1');
  
  const newkey = ecurve.genKeyPair({entropy:"Microsoft Admin Key1 For Microsoft Admin Key 1 for Microsoft Admin Key 1"});
  
  const publicKey = newkey.getPublic().encode('hex');
  
  const pk = newkey.getPrivate('hex');
  
  const ec = new EC('secp256k1');
  
  const privateKey = ec.keyFromPrivate(pk,'hex');
  
  
  const body = {
    name:"Trader",
    orgId:orgId,
    description:" Trader role.. Allowed to Trade"
  };
  
  const hash = GetHash(JSON.stringify(body));
  const signatureOf = privateKey.sign(hash).toDER('hex');
  
  let header = {
    fromOrg:orgId,
    fromAccount:accountId,
    publicKey:publicKey,
    type:"Role",
    action:"New",
    hash:hash,
    signature:signatureOf,
  };
  
  let tx = {
    header: header,
    body: body
  };
  
  // send out to the localchain
  
  const url = "http://127.0.0.1:5000/tx/add";
  
  axios.post(url,tx).then ((result) => {
    console.log("Posted Transaction 1... " + result);
    
  }).catch ((error) => {
    console.log("Network Error... " + error );
  });
}

function createGroupTx1() {
  
  let orgId = "Microsoft";
  let accountId = "admin@microsoft.com";
  
  // create public and private key pair
  
  const ecurve = new EC('secp256k1');
  
  const newkey = ecurve.genKeyPair({entropy:"Microsoft Admin Key1 For Microsoft Admin Key 1 for Microsoft Admin Key 1"});
  
  const publicKey = newkey.getPublic().encode('hex');
  
  const pk = newkey.getPrivate('hex');
  
  const ec = new EC('secp256k1');
  
  const privateKey = ec.keyFromPrivate(pk,'hex');
  
  
  const body = {
    name:"Finance",
    orgId:orgId,
    description:"This is a subgroup in Finance"
  };
  
  const hash = GetHash(JSON.stringify(body));
  const signatureOf = privateKey.sign(hash).toDER('hex');
  
  let header = {
    fromOrg:orgId,
    fromAccount:accountId,
    publicKey:publicKey,
    type:"Group",
    action:"New",
    hash:hash,
    signature:signatureOf,
  };
  
  let tx = {
    header: header,
    body: body
  };
  
  // send out to the localchain
  
  const url = "http://127.0.0.1:5000/tx/add";
  
  axios.post(url,tx).then ((result) => {
    console.log("Posted Transaction 1... " + result);
    
  }).catch ((error) => {
    console.log("Network Error... " + error );
  });
}

function createGroupTx2() {
  
  let orgId = "Microsoft";
  let accountId = "admin@microsoft.com";
  
  // create public and private key pair
  
  const ecurve = new EC('secp256k1');
  
  const newkey = ecurve.genKeyPair({entropy:"Microsoft Admin Key1 For Microsoft Admin Key 1 for Microsoft Admin Key 1"});
  
  const publicKey = newkey.getPublic().encode('hex');
  
  const pk = newkey.getPrivate('hex');
  
  const ec = new EC('secp256k1');
  
  const privateKey = ec.keyFromPrivate(pk,'hex');
  
  
  const body = {
    name:"PR",
    orgId:"Microsoft",
    description:"This is a PR Group of Microsoft"
  };
  
  const hash = GetHash(JSON.stringify(body));
  const signatureOf = privateKey.sign(hash).toDER('hex');
  
  let header = {
    fromOrg:orgId,
    fromAccount:accountId,
    publicKey:publicKey,
    type:"Group",
    action:"New",
    hash:hash,
    signature:signatureOf,
  };
  
  let tx = {
    header: header,
    body: body
  };
  
  // send out to the localchain
  
  const url = "http://127.0.0.1:5000/tx/add";
  
  axios.post(url,tx).then ((result) => {
    console.log("Posted Transaction 1... " + result);
    
  }).catch ((error) => {
    console.log("Network Error... " + error );
  });
}

function testSignature () {
  
  const ecurve = new EC('secp256k1');
  
  const newkey = ecurve.genKeyPair({entropy:"Humd ddkdkdskdskdsksdksdkds sdksdksdkd dskdkdskdsksccn"});
  
  const publicKey = newkey.getPublic().encode('hex');
  
  const privateKey = newkey.getPrivate("hex");
  
  const toSign = "This needs to be signed";
  
  const hashOf = GetHash(toSign);
  
  const ec = new EC('secp256k1');
  
  const pk = ec.keyFromPrivate(privateKey,'hex');
  
  const signatureOf = pk.sign(hashOf).toDER('hex');
  console.log(signatureOf);
  
  const key = ec.keyFromPublic(publicKey, 'hex');
  const verified = key.verify(hashOf, signatureOf);
  console.log(verified);
  
}

function runIt() {
 
 createAccountTx1();
 createAccountTx2();
 createGroupTx1();
 createGroupTx2();
 createRoleTx1();
 createRoleTx2();
}

// runIt();

createGroupTx2();

function GetHash (value) {
  
  const hash = crypto.createHash('sha256');
  
  const toHash = Buffer.from(value,'utf-8');
  hash.update(toHash);
  return hash.digest('hex');
  
}
