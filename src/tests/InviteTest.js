
let axios = require('axios');
let crypto = require('crypto');

let EC = require('elliptic').ec;


function inviteOrg() {
  
  let orgId = "ABN";
  let accountId = "admin@abn.com";
  
  // create public and private key pair
  
  const ecurve = new EC('secp256k1');
  
  const newkey = ecurve.genKeyPair({entropy:"Microsoft Admin Key1 For Microsoft Admin Key 1 for Microsoft Admin Key 1"});
  
  const publicKey = newkey.getPublic().encode('hex');
  
  const pk = newkey.getPrivate('hex');
  
  const ec = new EC('secp256k1');
  
  const privateKey = ec.keyFromPrivate(pk,'hex');
  
  
  
  
  const baseObject = {name:"ABN"};
  const secret = "ABN"
  baseObject.timeStamp = new Date();
  baseObject.secret = GetHash("ABN");
  
  
  const sTx = JSON.stringify(baseObject);
  
  const hashOf = GetHash(sTx);
  
  const signatureOf = privateKey.sign(hashOf).toDER('hex');
  
  const chainTx = {
    homeId: "Microsoft",
    user: "admin@microsoft.com",
    invitee:"admin@abn.com",
    publicKey:publicKey,
    secret: baseObject.secret,
    hash:hashOf,
    signature:signatureOf,
    tx:sTx
  };
  
  const url = "http://127.0.0.1:4000/homeInvite/add";
  
  axios.post(url,chainTx).then (result => {
    console.log("Sent...")
  }).catch (error => {
    console.log("Error in sending the PI tx to the network... " + JSON.stringify(error));
    
  });
  
}


function GetHash (value) {
  
  const hash = crypto.createHash('sha256');
  
  const toHash = Buffer.from(value,'utf-8');
  hash.update(toHash);
  return hash.digest('hex');
  
}

inviteOrg();