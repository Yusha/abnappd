
import React from 'react';

import { makeStyles } from '@material-ui/core/styles';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';


const useStyles = makeStyles(theme => ({
  
  AppBar: {
    marginLeft:10,
  },
  
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  
}));

const onLogout = () => {
  console.log("Logout called");
  
};



export default function DashboardAppBar(props) {
  const classes = useStyles();
  
  return (
    <div className={classes.root}>
      
      <AppBar className={classes.AppBar} position="fixed">
        <Toolbar>
          
          <Typography variant="h6" className={classes.title}>
            ABN Dashboard
          </Typography>
          <Button color="inherit" onClick={onLogout}> Logout</Button>
        </Toolbar>
      </AppBar>
      
    </div>
  
  );
}
