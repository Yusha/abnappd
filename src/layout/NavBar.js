import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    marginTop: 10,
    marginLeft: 10,
    bgcolor:"warning.main"
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
}));



export default function AppNavBar(props) {
  const classes = useStyles();
  const { onRegister, onLogin } = props;
  
  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6" >
            Append Business Blockchain Network
          </Typography>
          <Button color="inherit" style= {{marginLeft:800}}onClick={onLogin}>Login</Button>
          <Button color="inherit" style= {{marginLeft:50}} onClick={onRegister}>Register</Button>
        </Toolbar>
      </AppBar>
    </div>
  );
}