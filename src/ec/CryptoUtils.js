// Libs

import crypto from 'crypto';
import aes256 from 'aes256';
import { ec as EC } from 'elliptic';

import {decryptECIES, encryptECIES} from '../ec/Encryption';


function GetHash (value) {
  
  const hash = crypto.createHash('sha256');
  
  const toHash = Buffer.from(value,'utf-8');
  hash.update(toHash);
  return hash.digest('hex');
  
}

function GetSignature (encryptedKey,keyPassword,hashToSign) {
  
  let decrypted = aes256.decrypt(keyPassword, encryptedKey);
  const ec = new EC('secp256k1');
  const ecSK = ec.keyFromPrivate(decrypted, 'hex');
  const sign = ecSK.sign(hashToSign);
  
  return sign.toDER('hex');
  // return sign;
}

function GetDecryptedPK(encryptedPK, password) {
  let decrypted = aes256.decrypt(password, encryptedPK);
  const ec = new EC('secp256k1');
  const ecSK = ec.keyFromPrivate(decrypted, 'hex');
  return ecSK;
}


function GetEncrypted(publicKey,cleatext) {
  
  return encryptECIES(publicKey,cleatext);
  
}

function GetDecrypted (encryptedPK,password,cypher) {
  
  let privateKey = aes256.decrypt(password, encryptedPK);
  
  return decryptECIES(privateKey,cypher);
}

function validateSignature (pub, data,signature) {
  const ec = new EC('secp256k1');
  const key = ec.keyFromPublic(pub, 'hex');
  const verified = key.verify(data, signature);
  console.log(verified);
  return verified;
}


export {GetHash, GetSignature,GetDecryptedPK, GetDecrypted, GetEncrypted, validateSignature}
