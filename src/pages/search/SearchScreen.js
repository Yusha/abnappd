import React from 'react';

import axios from 'axios';

import { makeStyles } from '@material-ui/core/styles';
import MaterialTable, {MTableToolbar} from 'material-table'

import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import Box from '@material-ui/core/Box';
import Paper from '@material-ui/core/Paper';
import Container from '@material-ui/core/Container';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';

import EmailIcon from '@material-ui/icons/Email';
import PhoneIcon from '@material-ui/icons/Phone';
import HomeIcon from '@material-ui/icons/Home';

import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';


import Dialog from '@material-ui/core/Dialog';

import InputAdornment from '@material-ui/core/InputAdornment';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';

import {Editor} from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { EditorState, convertToRaw } from 'draft-js';
import {GetEncrypted,GetSignature,GetHash} from "../../ec/CryptoUtils";
import tableIcons from '../common/IconDef';




const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
  },
  
  input: {
    marginLeft: theme.spacing(1),
    flex: 1,
  },
  
  tabs: {
    borderRight: `1px solid ${theme.palette.divider}`,
  },
  
  

}));



export default function SearchScreen(props) {
  
  const {context} = props;
  
  const [searchText, setSearchText] = React.useState('');
  const [searchResult, setSearchResult] = React.useState({});
  const [resultSet, setResultSet] = React.useState(false);
  const [exists, setExists] = React.useState(false);
  const [error, setError] = React.useState('');
  
  
  const classes = useStyles();
  
  
  const search = (e) => {
    e.preventDefault();
    console.log("Search the term  :"  + searchText);
  
    const url = "http://localhost:4000/api/search?home=" + searchText ;
  
    axios.get(url).then ((result) => {
      
      if ( !(result.data.profile === undefined)) {
        setExists(true);
        setSearchResult(result.data);
        setSearchText("");
        setResultSet(true);
      } else {
        setSearchText("");
        setResultSet(true);
        setExists(false);
      }
    }).catch ((error) => {
      console.log("Error in search...   " + JSON.stringify(error));
      setError("Search Failed...");
    });
    
  };
  
  return (
  
    <div className={classes.root}>
      
    
      <Container maxWidth="lg" style={{marginTop:20}}>
      
      
        <div className={classes.root} >
    
          <Grid className={classes.root} style={{width:'80%'}} >
  
            <Typography component="div" style={{marginTop:20}}>
          
              <Box fontFamily="Monospace" textAlign="center" letterSpacing={6} fontSize="h3.fontSize" fontWeight="fontWeightMedium" m={2}>
                Search Orgs...
              </Box>
              </Typography>
            <Divider/>
          </Grid>
          
          <div align="center" style={{width:'80%', marginTop:30}}>
  
            <Paper component="form" align="center" onSubmit={search}>
                <IconButton className={classes.iconButton} aria-label="menu">
                <MenuIcon />
                 </IconButton>
              <InputBase
                className={classes.input}
                placeholder="Search ..."
                inputProps={{ 'aria-label': 'Search Homes...' }}
                style={{width:600}}
                onChange={(e)=>{setSearchText(e.target.value)}}
             />
              <IconButton type="submit" className={classes.iconButton} aria-label="search">
                <SearchIcon />
                </IconButton>
            </Paper>
          </div>
          
        </div>
  
        {resultSet &&
          <SearchResult searchResult={{exists: exists, value:searchResult}} context = {context} />
        }
      
      </Container>
    </div>
  );
}



function SearchResult (props) {
  
  const {searchResult, context} = props;
  const [tabValue, setTabValue] = React.useState(0);
  
  const handleTabChange = (event, newValue) => {
    setTabValue(newValue);
  };
  
  
  return (
  
    <div align="center" style={{marginTop:50}}>
  
      {(searchResult.exists)  &&
        <Grid container spacing={0}
            direction="column"
            alignItems="center"
            justify="center">
        <Grid item xs={9}>
      
          <Tabs
            value={tabValue}
            onChange={handleTabChange}
      
          >
            <Tab label="Profile"/>
            <Tab label="Accounts"/>
            <Tab label="Reference Data Objects"/>
          </Tabs>
    
        </Grid>
    
        <Grid item xs={9} style={{marginTop: 70}}>
      
          {(tabValue === 0) && <ProfileTab profile={searchResult.value.profile} context = {context} />}
          {(tabValue === 1) && <AccountsTab accounts={searchResult.value.contacts} profile = {searchResult.value.profile} context = {context} />}
          {(tabValue === 2) && <ObjectsTab objects={searchResult.value.objects} profile = {searchResult.value.profile} context = {context} />}
          {(tabValue === 3) && <PITab PIs={searchResult.value.PIs} profile = {searchResult.value.profile} context = {context} />}
        </Grid>
  
  
      </Grid>
      }
  
  
      {!(searchResult.exists) &&
        <Grid container spacing={0}
            direction="column"
            alignItems="center"
            justify="center">
        <Grid item xs={9} justify="center">
          <Typography variant="h5">
            The home id does not exists..
          </Typography>
    
        </Grid>
  
  
      </Grid>
      }
  
    </div>
  
    
  );
}
function ProfileTab(props) {
  
  const {profile} = props;
  
  return (
    <div>
  
      <Typography align="left" variant="h4" color="primary">
        {profile.businessName}
      </Typography>
  
      <Typography align="left" variant="h6" style={{marginTop:20}} >
        <HomeIcon  style={{color:"#F50057",marginRight:10}} /> {profile.address}
      </Typography>
      <Typography align="left" variant="h6">
        <PhoneIcon style={{color:"#F50057",marginRight:10,marginTop:10}} />  {profile.phone}
      </Typography>
      <Typography align="left" variant="h6">
        <EmailIcon style={{color:"#F50057",marginRight:10,marginTop:10}}/>  {profile.email}
      </Typography>
      
    </div>
    
  );
  
}
function AccountsTab(props) {
  
  const {accounts, profile, context} = props;
  const [rowData, setRowData] = React.useState({});
  const [composeMessage, setComposeMessage] = React.useState(false);
  
  const rowClicked = (e,row) => {
    
    setRowData(row);
    setComposeMessage(true);
    
  };
  
  const composeMessageDialogCallback = (message) => {
    setComposeMessage(false);
  };
  
  const accountData = accounts.map((account, index) => {
    return {name:account.name,group:account.group,role:account.role,email:account.email,phone:account.phone,publicKey:account.publicKey};
  });
  
  return (
    <div>
  
      <div>
        <MaterialTable
          title={profile.businessName}
          icons={tableIcons}
      
          localization={{
        
            header: {
              actions: 'message'
            },
        
          }}
      
      
          columns={[
            {title: 'contact', field: 'name'},
            {title: 'group', field: 'group'},
            {title: 'role', field: 'role'},
            {title: 'email', field: 'email'},
            {title: 'phone', field: 'phone'},
            {title: 'public key', field: 'publicKey', cellStyle: {wordBreak: 'break-all'}},
      
      
          ]}
          data={accountData }
          actions={[
            rowData => ({
              icon: tableIcons.Email,
              tooltip: 'Send Message',
              onClick: rowClicked
            })
          ]}
          options={{
            actionsColumnIndex: -1,
            padding: "dense",
            headerStyle: {
              backgroundColor: '#0091EA',
              color: '#FFF'
            }
          }}
      
        />
        
      </div>
      
      <div>
  
        <SendMessageDialog   dialogCallback={composeMessageDialogCallback} context = {context} open = {composeMessage} rowData = {rowData} homeId={profile.homeId}/>
      
      </div>
    
    </div>
  
  );
}
function ObjectsTab(props) {
  
  const {objects, profile, context} = props;
  
  const [rowData, setRowData] = React.useState({});
  const [readObject, setReadObject] = React.useState(false);
  const [followObject, setFollowObject] = React.useState(false);
  const [objectId, setObjectId] = React.useState("");
  const [isFollowing, setIsFollowing] = React.useState(false);
  
  const readObjectDialogCallback = (action) => {
    setReadObject(false);
    
  };
  
  const rowClicked = (e,row) => {
    setRowData(row);
    setReadObject(true);
  };
  
  const toggleFollow = (e,row) => {
    const oid = objects[e.tableData.id];
    setObjectId(oid.id);
    setIsFollowing(e.follow);
    setFollowObject(true);
  };
  
  const followObjectCallback = () => {
    setFollowObject(false);
  };
  
  const allObjects = objects.map((object,index) => {
    let follow = true;
    return {name:object.name,description:object.description,encrypted:object.encrypted,ownerHome:object.ownerHome,owner:object.owner,follow:follow};
  });
  
  return (
    <div>
      <div>
        <MaterialTable
          title={profile.businessName}
          icons={tableIcons}
      
          localization={{
        
            header: {
              actions: 'Access'
            },
        
          }}
      
      
          columns={[
            {title: 'Object Name', field: 'name'},
            {title: 'Description', field: 'description'},
            {title: 'Follow', field: 'follow',
              render: (data, id) => (
                
                <FormControlLabel
                  control={
                    <Switch
                      checked={data.follow}
                      onChange={(id) => toggleFollow(data, id)}
                      name="Follow"
                      color="secondary"
                    />
                  }
                  label={data.follow ? "Following" : "Not Following"}
                />
              )
            
            },
            
            
          ]}
          data={allObjects }
          actions={[
            rowData => ({
              icon: tableIcons.Unlock,
              tooltip: 'Request Access',
              onClick: rowClicked
            })
          ]}
          options={{
            actionsColumnIndex: -1,
            padding: "dense",
            headerStyle: {
              backgroundColor: '#0091EA',
              color: '#FFF'
            }
          }}

          components={{
            Toolbar: props => (
              <div style={{ backgroundColor: 'white'}}>
                <MTableToolbar {...props} />
              </div>
            ),
          }}
    
        />
  
      </div>
      
      <div>
        {readObject &&
        <ObjectReadRequestDialog dialogCallback={readObjectDialogCallback}
                                 open={readObject} rowData={rowData} homeId={profile.homeId} context={context}/>
        }
        
        {followObject &&

        <FollowerDialog callback={followObjectCallback}
                                 open={followObject} objectId={objectId} follows = {isFollowing} context={context}/>
        }
        
        
        
      </div>
    
    </div>
  
  );
  
}
function PITab(props) {
  
  const {PIs, profile, context} = props;
  
  const [rowData, setRowData] = React.useState({});
  const [readObject, setReadObject] = React.useState(false);
  
  const readObjectDialogCallback = (action) => {
    setReadObject(false);
    
  };
  
  const rowClicked = (e,row) => {
    setRowData(row);
    setReadObject(true);
  };
  
  const allPIs = PIs.map((pi,index) => {
    
    return {name:pi.name,home:pi.homeId,account:pi.account,publicKey:pi.publicKey}
  });
  
  return (
    <div>
      <div>
        <MaterialTable
          title={profile.businessName}
          icons={tableIcons}
          
          localization={{
            
            header: {
              actions: 'Access'
            },
            
          }}
          
          
          columns={[
            {title: 'Name', field: 'name'},
            {title: 'Home Id', field: 'home',cellStyle: {wordBreak: 'break-all'}},
            {title: 'Account Id', field: 'account'},
            {title: 'public Key', field: 'publicKey',cellStyle: {wordBreak: 'break-all'}},
            
          ]}
          data={allPIs }
          actions={[
            rowData => ({
              icon: tableIcons.Unlock,
              tooltip: 'Request Access',
              onClick: rowClicked
            })
          ]}
          options={{
            actionsColumnIndex: -1,
            padding: "dense",
            headerStyle: {
              backgroundColor: '#0091EA',
              color: '#FFF'
            }
          }}
          
          components={{
            Toolbar: props => (
              <div style={{ backgroundColor: 'white'}}>
                <MTableToolbar {...props} />
              </div>
            ),
          }}
        
        />
      
      </div>
      
      <div>
  
        {readObject &&
        <ObjectReadRequestDialog dialogCallback={readObjectDialogCallback}
                                 open={readObject} rowData={rowData} homeId={profile.homeId} context={context}/>
        }
      </div>
    
    </div>
  
  );
  
}



function ObjectReadRequestDialog(props) {
  
  
  const { context, dialogCallback, open, rowData, homeId } = props;
  const classes = dialogStyles();
  
  
  const [password, setPassword] = React.useState("");
  const [editorState, setEditorState] = React.useState(EditorState.createEmpty());
  
  
  const [save, setSave] = React.useState(false);
  
  
  const recipientHomeId = rowData.ownerHome;
  const recipientAccount = rowData.owner;
  const recipientPK = rowData.publicKey;
  
  const accountDetails = rowData.owner?rowData.owner:rowData.account;
  
  const to = homeId + "/" + accountDetails;
  
  const from = context.homeId + "/" + context.user;
  
  const subject = "Object Read Request";
  
  const objectRequested = rowData.name;
  
  const handleClose = (action) => {
    dialogCallback(action);
  };
  
  
  const onEditorStateChange = (s)=> {
    setEditorState(s);
    
  };
  
  const onSave = () => {
    setSave(true);
  };
  
  const cancelThis = (e) => {
    dialogCallback({cancel:true});
  };
  
  const setFieldValue = (e) => {
    
    const id = e.target.id;
    const value = e.target.value;
    
    if (id === "password") {
      setPassword(value);
    }
    else {
      console.log("Wrong id in the form field.. should not hapen...");
    }
  };
  
  
  const onSubmit = () => {
    
    const raw = convertToRaw(editorState.getCurrentContent());
    
    
    const body = {
      senderHomeId:context.homeId,
      senderAccount:context.user,
      recipientHomeId:recipientHomeId,
      recipientAccount:recipientAccount,
      subject: subject,
      content: raw
    };
    
    const sBody = JSON.stringify(body);
    
    const hash = GetHash(sBody);
    
    const signature = GetSignature(context.encryptedPK,password,hash);
    const encryptedBody = GetEncrypted(recipientPK,sBody);
    
    const tx = {
      header: {
        senderHomeId: context.homeId,
        senderAccount:context.user,
        senderPublicKey:context.publicKey,
        recipientHomeId:recipientHomeId,
        recipientAccount:recipientAccount,
        type:"access",
        timeStamp:new Date(),
        hash:hash,
        signature:signature
      },
      body: encryptedBody
    };
    
  
    const url = "http://localhost:4000/message/sendMessage";
  
  
    axios.post(url,tx).then ((result) => {
      
      if (result.data.success) {
        // message posted successfully
        dialogCallback({cancel:false,success:true});
      } else {
        dialogCallback({cancel:false,success:false, error:result.error});
      }
      
    }).catch ((error) => {
      console.log("Error in posting a message");
    })
    
  };
  
  
  
  return (
    <Dialog onClose={handleClose} open={open} fullWidth maxWidth="lg" classes={{paper: classes.dialog }} >
      
      <div className={classes.content} style={{width:800,align:"center",marginLeft:50}}>
        
        <div className={classes.margin}>
          <Typography color="primary" variant="h5" align="center"> Object Read Request...</Typography>
        </div>
        
        
        
        <div className={classes.margin} align="center" style={{marginTop:30}}>
          <TextField
            
            style={{width:500,margin:2}}
            color="primary"
            value = {to}
            id ="to"
            readOnly
            onChange={setFieldValue}
            InputProps={{
              startAdornment: <InputAdornment position="start">@To...</InputAdornment>,
              className:classes.inputColor
            }}
          />
  
          <TextField
    
            style={{width:500,margin:2}}
            color="primary"
            value = {from}
            id ="from"
            readOnly
            onChange={setFieldValue}
            InputProps={{
              startAdornment: <InputAdornment position="start">@From...</InputAdornment>,
              className:classes.inputColor
            }}
          />
  
          <TextField
    
            style={{width:500,margin:2}}
            color="primary"
            value = {objectRequested}
            id ="object"
            readOnly
            onChange={setFieldValue}
            InputProps={{
              startAdornment: <InputAdornment position="start">@Object...</InputAdornment>,
              className:classes.inputColor
            }}
          />
          
          <TextField
            
            style={{width:500,margin:2,marginTop:10}}
            placeholder="Subject"
            color="primary"
            value = {subject}
            id ="subject"
            onChange={setFieldValue}
          
          />
          
          <Editor
            editorState={editorState}
            toolbarClassName="toolbarClassName"
            wrapperClassName="wrapperClassName"
            editorClassName="editorClassName"
            placeholder = "Note for the object owner..."
            onEditorStateChange={onEditorStateChange}
            editorStyle={{backgroundColor:"#FFFFFF",height:200}}
          />
          
          {save &&
          
          <TextField
            
            style={{width: 400, marginTop: 30, backgroundColor: "#FFFFFF", height: 20}}
            placeholder="password for private key"
            value={password}
            id="password"
            onChange={setFieldValue}
            color="secondary"
          />
          }
        
        </div>
        
        <div align="center" style={{marginTop:20, marginBottom:10}}>
          
          {!save &&
          <Button onClick={onSave} className={classes.shape} align="left" variant="contained" color="primary"
                  style={{width: 100, height: 30}}>Save </Button>
          }
          {save &&
          
          <Button onClick={onSubmit} className={classes.shape} align="left" variant="contained" color="primary"
                  style={{width: 100, height: 30}}>Submit </Button>
          }
          <Button onClick = {cancelThis} className={classes.shape}  variant="contained" align = "right" style={{width:100,height:30, marginLeft:20}} color="secondary" > Cancel </Button>
        
        </div>
      </div>
    
    
    </Dialog>
  );
}


function SendMessageDialog(props) {
  
  
  const { context, dialogCallback, open, rowData, homeId } = props;
  const classes = dialogStyles();
  
  const [subject, setSubject] = React.useState("");
  const [password, setPassword] = React.useState("");
  const [save, setSave] = React.useState(false);
  
  const recipientHomeId = homeId;
  const recipientAccount = rowData.email;
  const recipientPK = rowData.publicKey;
  
  const [editorState, setEditorState] = React.useState(EditorState.createEmpty());
  
  const to = homeId + "/" + rowData.email;
  
  
  const handleClose = (action) => {
    dialogCallback(action);
  };
  
  
  const onEditorStateChange = (s)=> {
    setEditorState(s);
    
  };
  
  const onSave = () => {
    setSave(true);
  };
  
  const cancelThis = (e) => {
    dialogCallback({cancel:true});
  };
  
  const setFieldValue = (e) => {
    
    const id = e.target.id;
    const value = e.target.value;
    
    if (id === "subject") {
      setSubject(value);
      
    } else if (id === "password") {
      setPassword(value);
    }
    else {
      console.log("Wrong id in the form field.. should not hapen...");
    }
  };
  
  const onSubmit = () => {
    
     const raw = convertToRaw(editorState.getCurrentContent());
    
    
    const body = {
      senderHomeId:context.homeId,
      senderAccount:context.user,
      recipientHomeId:recipientHomeId,
      recipientAccount:recipientAccount,
      subject: subject,
      content: raw
    };
    
    const sBody = JSON.stringify(body);
    
    const hash = GetHash(sBody);
    
    const signature = GetSignature(context.encryptedPK,password,hash);
    const encryptedBody = GetEncrypted(recipientPK,sBody);
    
    const tx = {
      header: {
        senderHomeId: context.homeId,
        senderAccount:context.user,
        senderPublicKey:context.publicKey,
        recipientHomeId:recipientHomeId,
        recipientAccount:recipientAccount,
        timeStamp:new Date(),
        type:"general",
        hash:hash,
        signature:signature
      },
      body: encryptedBody
    };
    
    const url = "http://localhost:4000/message/sendMessage";
    axios.post(url,tx).then ((result) => {
      
      if (result.data.success) {
        // message posted successfully
        dialogCallback({cancel:false,success:true});
      } else {
        dialogCallback({cancel:false,success:false, error:result.error});
      }
      
    }).catch ((error) => {
      console.log("Error in posting a message");
    })
    
  };
  
  
  
  return (
    <Dialog onClose={handleClose} open={open} fullWidth maxWidth="lg" classes={{paper: classes.dialog }} >
  
      <div className={classes.content} style={{width:800,align:"center",marginLeft:50}}>
    
        <div className={classes.margin}>
          <Typography color="primary" variant="h5" align="center"> Compose New Message</Typography>
        </div>
    
    
    
        <div className={classes.margin} align="center" style={{marginTop:30}}>
          <TextField
        
            style={{width:500,margin:2}}
            color="primary"
            value = {to}
            id ="to"
            readOnly
            onChange={setFieldValue}
            InputProps={{
              startAdornment: <InputAdornment position="start">@To...</InputAdornment>,
              className:classes.inputColor
            }}
          />
      
          <TextField
        
            style={{width:500,margin:2,marginTop:10}}
            placeholder="Subject"
            color="primary"
            value = {subject}
            id ="subject"
            onChange={setFieldValue}
      
          />
      
          <Editor
            editorState={editorState}
            toolbarClassName="toolbarClassName"
            wrapperClassName="wrapperClassName"
            editorClassName="editorClassName"
            onEditorStateChange={onEditorStateChange}
            editorStyle={{backgroundColor:"#FFFFFF",height:200}}
          />
  
          {save &&
  
          <TextField
    
            style={{width: 400, marginTop: 30, backgroundColor: "#FFFFFF", height: 20}}
            placeholder="password for private key"
            value={password}
            id="password"
            onChange={setFieldValue}
            color="secondary"
          />
          }
        
        </div>
  
        <div align="center" style={{marginTop:20, marginBottom:10}}>
    
          {!save &&
          <Button onClick={onSave} className={classes.shape} align="left" variant="contained" color="primary"
                  style={{width: 100, height: 30}}>Save </Button>
          }
          {save &&
    
          <Button onClick={onSubmit} className={classes.shape} align="left" variant="contained" color="primary"
                  style={{width: 100, height: 30}}>Submit </Button>
          }
          <Button onClick = {cancelThis} className={classes.shape}  variant="contained" align = "right" style={{width:100,height:30, marginLeft:20}} color="secondary" > Cancel </Button>
  
        </div>
      </div>
    
      
    </Dialog>
  );
}

function FollowerDialog(props)  {
  
  const {open,callback, objectId, follows} = props;
  const [password, setPassword] = React.useState("");
  
  const finalize = () => {
    console.log("We are in finalize send to network");
    
    
  };
  
  const cancel = () => {
    callback(true);
  };
  
  return (
  
    <Dialog onClose={callback} open={open} fullWidth maxWidth="lg"  >
      
      <div align="center" style={{marginTop:100}}>
        <Typography variant="h3"> {follows?"UnFollow Object" :"Follow Object"} </Typography>
        <Typography variant="h5"> {objectId} </Typography>
      
      </div>
      
      <div align="center" style={{marginTop:100}}>
        
        <TextField
          
          style={{width: 400, marginTop: 30, backgroundColor: "#FFFFFF", height: 20}}
          placeholder="Private Key Password"
          value={password}
          id="password"
          onChange={(e) => {setPassword(e.target.value)}}
          color="secondary"
        />
      
      
      </div>
      
      <div align = "center" style={{marginTop:100, marginBottom:200}}>
        
        <Button onClick={finalize}  variant="contained" align="right"
                style={{width: 250, height: 30}} color="secondary"> Send to Network  </Button>
        
        <Button onClick={cancel}  variant="contained" align="right"
                style={{width: 120, height: 30, marginLeft: 50}} color="secondary"> Cancel </Button>
      
      </div>
    
    </Dialog>
  
  )
}


const dialogStyles = makeStyles({
  dialog: {
    position: 'absolute',
    left: 10,
    top: 50
  }
});

