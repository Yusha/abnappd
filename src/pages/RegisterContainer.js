import React, {useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';

import RegisterHome from './RegisterHome';
import RegisterAccount from './RegisterAccount';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    margin: 'auto',
    maxWidth: 800,
    
  },
  
}));

export default function RegisterContainer(props) {
  const classes = useStyles();
  const {registerCallback} = props;
  
  const [registerHome, setRegisterHome] = React.useState(false);
  const [registerAccount, setRegisterAccount] = React.useState(false);
  
  
  return (
    <div className={classes.root} >
      
      { (!registerHome && !registerAccount) &&
        <div>
          <Paper className={classes.paper} elevation={0}>
            <Grid container spacing={2}>
          
              <Grid item xs={6} sm container style ={{marginTop:25}}>
  
                <Card className={classes.root} variant="outlined">
                  <CardContent>
                    <Typography variant='h5' color="primary" gutterBottom align="center">
                      Network Invitation
                    </Typography>
                    <Typography variant="h6" align="center">
                      Claim Network invitation and create a new home for your organization
                    </Typography>
                  </CardContent>
                  <div align="center">
                    <Button variant="contained" color="primary" align='center' onClick = {()=> setRegisterHome(true)}>
                      Claim
                    </Button>
                  </div>
                </Card>
          
              </Grid>
  
              <Grid item xs={6} sm container style ={{marginTop:25}}>
     
                <Card className={classes.root} variant="outlined">
                  <CardContent>
                    <Typography variant='h5' color="primary" gutterBottom align="center">
                      Account Invitation
                    </Typography>
                    <Typography variant="h6" gutterBottom align="center">
                      Claim an Account Invitation and Register new Account
                    </Typography>
                  </CardContent>
                  <div align="center">
                    <Button variant="contained" color="primary" align='center' onClick = {()=> setRegisterAccount(true)}>
                      Claim
                    </Button>
                  </div>
                </Card>
  
              </Grid>
          
            </Grid>
          </Paper>
        </div>
      }
      
      <div>
        {(registerHome) && <RegisterHome registerCallback={registerCallback} />       }
        {(registerAccount) && <RegisterAccount registerCallback={registerCallback} /> }
      </div>
    </div>
  );
}