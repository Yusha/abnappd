import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import TextField from '@material-ui/core/TextField';
import PhoneIcon from '@material-ui/icons/Phone';
import EmailIcon from '@material-ui/icons/Email';
import BusinessIcon from '@material-ui/icons/Business';


import SupervisorAccountIcon from '@material-ui/icons/SupervisorAccount';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';

import axios from 'axios';
import { ec as EC } from 'elliptic';
import {GetHash} from '../ec/CryptoUtils';

import PouchDB from 'pouchdb-browser';

const myDB = new PouchDB('myDB');

const ecurve = new EC('secp256k1');

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
  },
  backButton: {
    marginRight: theme.spacing(1),
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(2),
    
  },
  margin: {
    margin: theme.spacing(1),
  },
}));


function getSteps() {
  return ['Verify invitation','Choose Additional Properties', 'Generate Keys', 'Finalize Account'];
}


export default function RegisterAccount(props) {
  
  const classes = useStyles();
  const [activeStep, setActiveStep] = React.useState(0);
  const {registerCallback} = props;
  
  const steps = getSteps();
  
  const  handleNext =  () => {
    setActiveStep(prevActiveStep => prevActiveStep + 1);
  };
  
  const handleBack = () => {
    setActiveStep(prevActiveStep => prevActiveStep - 1);
  };
  
  const handleReset = () => {
    setActiveStep(0);
  };
  
  const verifyPropsCallback = () => {
    
    const url = "http://127.0.0.1:4000/home/inviteExists";
    const request = {homeId:profileObject.homeId,invitee:profileObject.email};
    
    axios.post(url,{tx:request}).then((response) => {
      console.log("response is  " + response);
      if (response.data.exists) {
        handleNext();
      } else {
        console.log("Invitation does not exists");
        
      }
      
    }).catch((error) => {
      console.log("Error in connecting to")
    })
  
  
  };
  
  const choosePropsCallback = () => {
    handleNext();
  };
  
  const chooseKeysCallback = () => {
    
    
    handleNext();
    
    
  };
  
  const completeCallback = ()=> {
    
    
    // Tx
    const tx = {
      //
      homeId: profileObject.homeId,
      invitee:profileObject.email,
      name: profileObject.name,
      phone: profileObject.phone,
      publicKey:profileObject.publicKey,
    };
    
    const sTx = JSON.stringify(tx);
    
    const hashOf = GetHash(sTx);
    
    const ec = new EC('secp256k1');
    
    const pk = ec.keyFromPrivate(profileObject.privateKey,'hex');
    
    const signatureOf = pk.sign(hashOf);
    
    const chainTx = {
      _id:hashOf,
      action:"claimInvite",
      homeId: profileObject.homeId,
      invitee:profileObject.email,
      user: profileObject.email,
      hash: hashOf,
      publicKey: profileObject.publicKey,
      signature: signatureOf,
      tx: sTx
    };
    
    const url = "http://127.0.0.1:4000/home/claimInvite";
    
    axios.post(url, {
        tx: chainTx
    }).then (result => {
      
      if (result.data.processed) {
        console.log("Register succeeded");
        registerCallback();
      } else {
        console.log("Registration failed... " + result.data.error);
        
      }
    }).catch((error) => {
      console.log("Received error saving My access Request Objects :" + error);
    });
    
  };
  
  
  
  return (
    <Grid className={classes.root}>
      <Stepper activeStep={activeStep} alternativeLabel>
        {steps.map(label => (
          <Step key={label}>
            <StepLabel>{label}</StepLabel>
          </Step>
        ))}
      </Stepper>
  
      <Grid container justify = "center">
  
  
        <div className={classes.instructions}>
  
          {  activeStep === 0 &&
            <ChooseVerifyProps verifyPropsCallback = {verifyPropsCallback}/>
          }
          
          {activeStep === 1 &&
            
            <ChooseProps backCallback = {handleBack} choosePropsCallback = {choosePropsCallback}/>
          }
    
          {activeStep === 2 &&
            <ChooseKeys backCallback = {handleBack} chooseKeysCallback = {chooseKeysCallback}/>
      
          }
    
          {activeStep === 3 &&
          
            <Finalize backCallback = {handleBack} completeCallback = {completeCallback} />
      
          }
        </div>
        
      </Grid>
  
      <Divider/>
      
    </Grid>
  );
}


export function ChooseVerifyProps(props) {
  
  const {verifyPropsCallback} = props;
  
  const classes = useStyles();
  const [homeId, setHomeId] = React.useState('');
  const [email,setEmail] = React.useState('');
  
  const setValue = (e)=> {
    
    const id = e.target.id;
    const value = e.target.value;
    
    if (id === "homeId") {
      setHomeId(value);
      profileObject.homeId = e.target.value;
      
    } else if (id ==="email") {
      setEmail(value);
      profileObject.email = value;
      
    } else {
      console.log("Unknown Id... should not happen")
    }
  };
  
  
  return (
  
    <div>
      <div className={classes.margin}>
        <Grid container spacing={1} alignItems="flex-end">
          <Grid item>
            <BusinessIcon/>
          </Grid>
          <Grid item>
            <TextField id="homeId" value={homeId} onChange={setValue} label="Org Id"/>
          </Grid>
        </Grid>
      </div>
  
      <div className={classes.margin}>
        <Grid container spacing={1} alignItems="flex-end">
          <Grid item>
            <EmailIcon/>
          </Grid>
          <Grid item>
            <TextField id="email" value={email} onChange={setValue} label="Email"/>
          </Grid>
        </Grid>
      </div>
  
      <div className={classes.margin}>
    
        <Grid container spacing={1} alignItems="flex-end">
      
          <Grid container justify = "center">
        
            <Grid container justify = "center">
          
              <Grid>
                <Button
                  disabled
                >
                  Back
                </Button>
            
                <Button variant="contained" color="primary" onClick={verifyPropsCallback}>
                  Next
                </Button>
              </Grid>
            </Grid>
          </Grid>
    
        </Grid>
  
      </div>
  
      
  
    </div>
    
  );
  
}

function ChooseProps(props) {
  
  const classes = useStyles();
  const {backCallback, choosePropsCallback} = props;
  const [name, setName] = React.useState('');
  const [phone, setPhone] = React.useState('');
  
  const setFieldValue = (e)=> {
    const fieldName = e.target.id;
    const fieldValue = e.target.value;
    
    if (fieldName === "name") {
      profileObject.name = fieldValue;
      setName(fieldValue);
    } else if (fieldName === "phone") {
      profileObject.phone = fieldValue;
      setPhone(fieldValue);
    } else {
      console.log("Unknown Field detected. Should not have happened");
    }
    
  };
  
  return (
    
    <div>
      <div className={classes.margin}>
        <Grid container spacing={1} alignItems="flex-end">
          <Grid item>
            <BusinessIcon/>
          </Grid>
          <Grid item>
            <TextField id="name" value={name} onChange={setFieldValue} label="You full Name"/>
          </Grid>
        </Grid>
      </div>
      
      <div className={classes.margin}>
        <Grid container spacing={1} alignItems="flex-end">
          <Grid item>
            <PhoneIcon/>
          </Grid>
          <Grid item>
            <TextField id="phone" value={phone} onChange={setFieldValue}label=" Your Phone"/>
          </Grid>
        </Grid>
      </div>
      
      <div className= {classes.margin}>
        <Grid container spacing={1} alignItems="flex-end">
    
          <Grid container justify = "center">
      
            <Grid container justify = "center">
        
              <Grid>
                <Button
                  onClick={backCallback}
                  className={classes.backButton}
                >
                  Back
                </Button>
          
                <Button variant="contained" color="primary" onClick={choosePropsCallback}>
                  Next
                </Button>
              </Grid>
            </Grid>
          </Grid>
  
        </Grid>
        
      </div>
      
      
      
    </div>
  
  );
}
  
function ChooseKeys(props) {
    
  const classes = useStyles();
  
  const [passPhrase, setPassPhrase] = React.useState('');
  
  const setFieldValue = (e) => {
    const fieldName = e.target.id;
    const fieldValue = e.target.value;
    
    if (fieldName === "passphrase") {
      profileObject.passPhrase = fieldValue;
      setPassPhrase(fieldValue);
    } else {
        console.log("Unknown Field detected. Should not have happened");
    }
  };
    
    return (
      
      <div>
  
        
        <div style={{marginTop: 20}}>
          
          
          <Grid container spacing={2} alignItems="flex-end">
            <Grid item>
              <SupervisorAccountIcon/>
            </Grid>
            <Grid item>
              <TextareaAutosize style={{width: 300}} aria-label="minimum height" rowsMin={3} id="passphrase" value={passPhrase} onChange={setFieldValue}
                                placeholder="PassPharase to generate cryptographic keys.
                          Only you will have access to the keys. The data you encrypt with the key can only be decrypted by your private key.
                          Please keep you passphare safe. "/>
            </Grid>
          </Grid>
        
        
        </div>
  
        
        <div className= {classes.margin}>
          <Grid container spacing={1} alignItems="flex-end">
      
            <Grid container justify = "center">
        
              <Grid container justify = "center">
          
                <Grid>
                  <Button
                    onClick={props.backCallback}
                    className={classes.backButton}
                  >
                    Back
                  </Button>
            
                  <Button variant="contained" color="primary" onClick={props.chooseKeysCallback}>
                    Next
                  </Button>
                </Grid>
              </Grid>
            </Grid>
    
          </Grid>
  
        </div>
        
        
      </div>
    
    )
}

export function Finalize(props) {
  
  const classes = useStyles();
  
  const newkey = ecurve.genKeyPair({entropy:profileObject.passPhrase});
  
  const adminPublicKey = newkey.getPublic().encode('hex');
  profileObject.publicKey = adminPublicKey;
  
  const adminPrivateKey = newkey.getPrivate("hex");
  
  profileObject.privateKey = adminPrivateKey;
  
  
  
  return (
    
    <div>
  
      <div style={{marginTop: 20, width:600, align:"center"}}>
        <Typography varivariant="h5" gutterBottom color="primary">
          Please note down your passphrase and save private key. You can always regenerate your private key from passphrase.
          Your private key is needed to cryptographically sign and encrypt data.
          Your private key never leaves your computer. Your account is verified using your public key.
        </Typography>
      
      </div>
      
      <div style={{marginTop: 20}}>
  
        
          <Typography color='primary'>
            Passphrase
          </Typography>
        
        <Grid container spacing={2} alignItems="flex-end">
  
          <Grid item>
            <TextareaAutosize style={{width: 600}} aria-label="minimum height" rowsMin={3} id="passphrase" value={profileObject.passPhrase}
                              label="Passphrase"/>
          </Grid>
        </Grid>
      
      
      </div>
  
      <div style={{marginTop: 20}}>
  
        <Typography color='primary'>
          Public Key
        </Typography>
        
        <Grid container spacing={2} alignItems="flex-end">
          <Grid item>
            <TextareaAutosize style={{width: 600}} aria-label="minimum height" rowsMin={3} id="publicKey" value={profileObject.publicKey}
                              label="Public Key"/>
          </Grid>
        </Grid>
      </div>
  
      <div style={{marginTop: 20}}>
    
        <Typography color='primary'>
          Private Key
        </Typography>
    
        <Grid container spacing={2} alignItems="flex-end">
          <Grid item>
            <TextareaAutosize style={{width: 600}} aria-label="minimum height" rowsMin={3} id="privateKey" value={profileObject.privateKey}
                              label="Public Key"/>
          </Grid>
        </Grid>
      </div>
  
      <div className= {classes.margin}>
        <Grid container spacing={1} alignItems="flex-end">
      
          <Grid container justify = "center">
        
            <Grid container justify = "center">
          
              <Grid>
                <Button
                  onClick={props.backCallback}
                  className={classes.backButton}
                >
                  Back
                </Button>
            
                <Button variant="contained" color="primary" onClick={props.completeCallback}>
                  Next
                </Button>
              </Grid>
            </Grid>
          </Grid>
    
        </Grid>
  
      </div>
    </div>
  
  )
}


let profileObject = {
  homeId:"",
  email:"",
  name:"",
  phone:"",
  password:"",
  passPhrase:"",
  publicKey:"",
  encryptedPK:""
  
};
