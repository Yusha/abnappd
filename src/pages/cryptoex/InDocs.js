
import React, { useEffect,useStates } from 'react';
import { makeStyles } from '@material-ui/core/styles';


import MaterialTable from 'material-table';
import { forwardRef } from 'react';

import axios from 'axios';

import DocuDetail from './DocuDetails';
import tableIcons from '../common/IconDef';


export default  function SentDocs(props) {
  
  const { context } = props;
  
  const classes = tableTheme();
  
  const [tableData, setTableData] = React.useState([]);
  const [rowSelected, setRowSelected] = React.useState(-1);
  const [open, setOpen] = React.useState(false);
  
  
  
  useEffect(()=> {
    
    const url = "http://localhost:4000/cryptoxpress/inbox?home=" + context.homeId + "&account=" + context.user;
    
    axios.get(url).then ((result) => {
      setTableData(result.data);
      
    }).catch ((error) => {
      console.log("Error in getting Internal Pending Invitations data  " + JSON.stringify(error));
    })
    
  },[]);
  
  
  
  
  const getDataSet = () => {
    const data = tableData.map((row, index) => {
      return {subject: row.tx.subject, home: row.tx.senderHome,account:row.tx.senderAccount, date:row.tx.timeStamp};
    });
    return data;
  };
  
  const rowClicked = (e,row) => {
    
    const index = tableData[row.tableData.id];
    setRowSelected(index);
    setOpen(true);
  };
  
  const ddCallback = () => {
    setOpen(false);
  };
  
  
  
  return (
    
    <div className={classes.table}>
      <MaterialTable
        title="Documents Received"
        icons={tableIcons}
        
        
        localization={{
          header: {
            actions: 'Actions'
          },
        }}
        
        columns={[
          { title: 'Subject', field: 'subject' },
          { title: 'Sender Home', field: 'home' },
          { title: 'Sender Account', field: 'account' },
          { title: 'Date', field: 'date' },
          
        ]}
        data={getDataSet()}
        
        actions={[
          rowData => ({
            icon: tableIcons.ViewEye,
            tooltip: 'View message',
            onClick: rowClicked
          })
        ]}
        
        options={{
          actionsColumnIndex: -1,
          padding:"dense",
          
          headerStyle: {
            color: 'white',
            background:'#0091EA',
            fontSize:20,
            fontFamily:"Roboto"
          },
          rowStyle: {
            color: 'black'
          }
          
        }}
        
        
      />
  
      {open &&
        <DocuDetail callback={ddCallback} open={open} data={rowSelected} context={context}/>
      }
      
    </div>
  )
}




const tableTheme = makeStyles(theme => ({
  table: {
    '& tbody>.MuiTableRow-root:hover': {
      background: '#EEE',
    }
  },
  body: {
    textColor:"primary"
  }
}));
