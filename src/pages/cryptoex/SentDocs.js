
import React, { useEffect,useStates } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import dateFormat from 'dateformat';


import MaterialTable from 'material-table';
import { forwardRef } from 'react';

import axios from 'axios';
import tableIcons from '../common/IconDef';


export default  function SentDocs(props) {
  
  const { context } = props;
  
  const classes = tableTheme();
  
  const [tableData, setTableData] = React.useState([]);
  const [rowSelected, setRowSelected] = React.useState(-1);
  
  
  
  useEffect(()=> {
    
    const url = "http://localhost:4000/cryptoxpress/sentbox?home=" + context.homeId + "&account=" + context.user;
    
    axios.get(url).then ((result) => {
      setTableData(result.data);
      
    }).catch ((error) => {
      console.log("Error in getting Internal Pending Invitations data  " + JSON.stringify(error));
    })
    
  },[]);
  
  
  
  
  const getDataSet = () => {
    const data = tableData.map((row, index) => {
      let formatDate = new Date(row.timeStamp);
      let newd = dateFormat(formatDate, "DDD, mmmm dS, yyyy, h:MM:ss TT");
      return {subject: row.tx.subject, home: row.tx.recipientHome,account:row.tx.recipientAccount, date:newd};
    });
    return data;
  };
  
  const rowClicked = (e,row) => {
    
    const index = tableData[row.tableData.id];
    setRowSelected(index);
    
    
  };
  
  
  
  return (
    
    <div className={classes.table}>
      <MaterialTable
        title="Sent Documents"
        icons={tableIcons}
        
        
        localization={{
          header: {
            actions: 'View'
          },
        }}
        
        columns={[
          { title: 'Subject', field: 'subject' },
          { title: 'Recipient Home', field: 'home' },
          { title: 'Recipient Account', field: 'account' },
          { title: 'Date', field: 'date' },
          
        ]}
        data={getDataSet()}
        
        actions={[
          rowData => ({
            icon: tableIcons.ViewEye,
            tooltip: 'View message',
            onClick: rowClicked
          })
        ]}
        
        options={{
          actionsColumnIndex: 4,
          padding:"dense",
          
          headerStyle: {
            color: 'white',
            background:'#0091EA',
            fontSize:20,
            fontFamily:"Roboto"
          },
          rowStyle: {
            color: 'black'
          }
          
        }}
        
        components={{
        
        }}
      />
      
    </div>
  )
}


const tableTheme = makeStyles(theme => ({
  table: {
    '& tbody>.MuiTableRow-root:hover': {
      background: '#EEE',
    }
  },
  body: {
    textColor:"primary"
  }
}));
