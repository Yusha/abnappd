import React from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';

import Dialog from '@material-ui/core/Dialog';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';


import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

import {GetDecryptedPK} from '../../ec/CryptoUtils';
import {decryptECIES} from '../../ec/Encryption';


export default function ShowDetailsDialog(props) {
  
  
  const { context, data,callback,open, viewOriginal } = props;
  
  const [password, setPassword] = React.useState("");
  
  
  const classes = dialogStyles();
  
  const handleClose = (action) => {
    callback(action);
  };
  
  
  
  const onDownload = () => {
  
    const docBody = JSON.parse(data.tx.body);
    const encrypteDoc = docBody.doc;
    
    const privateKey = GetDecryptedPK(context.encryptedPK, password);
    const text = decryptECIES(privateKey, encrypteDoc);
    const fileValue = JSON.parse(text);
    
    let fileData = "";
    let fileName = "";
    
    if (viewOriginal) {
      fileData = fileValue.base64;
      fileName = fileValue.name;
      
    } else {
      fileData = fileValue.file.base64;
      fileName = fileValue.file.name;
    }
    
    const downloadLink = document.createElement("a");
    
    downloadLink.href = fileData;
    downloadLink.download = fileName;
    
    downloadLink.click();
  };
  
  
  
  const StyledTableCell = withStyles((theme) => ({
    head: {
      backgroundColor: "#0091EA",
      color: "white",
      fontSize:20
    },
    body: {
      fontSize: 16,
      wordWrap:"break-word",
      maxWidth:200
    },
  }))(TableCell);
  
  const StyledTableRow = withStyles((theme) => ({
    root: {
      '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
      },
    },
  }))(TableRow);
  
  
  
  
  return (
    <Dialog onClose={handleClose} open={open} fullWidth maxWidth="lg" classes={{paper: classes.dialog }} >
      
      <div className={classes.content} style={{width:800,align:"center",marginLeft:50}}>
        
        <div className={classes.margin}>
          <Typography color="primary" variant="h5" align="center"> View Document </Typography>
        </div>
        
        
        
        <div className={classes.margin} align="center" style={{marginTop:30}}>
          
          <TableContainer component={Paper}>
            <Table className={classes.table} aria-label="customized table">
              <TableHead>
                <TableRow>
                  <StyledTableCell>Property </StyledTableCell>
                  <StyledTableCell align="right" >Value </StyledTableCell>
                
                </TableRow>
              </TableHead>
              <TableBody>
                
                <StyledTableRow key="1">
                  <StyledTableCell component="th" scope="row">
                    Name
                  </StyledTableCell>
                  <StyledTableCell align="right">{data.tx.subject}</StyledTableCell>
                </StyledTableRow>
                
                
                <StyledTableRow key="3">
                  <StyledTableCell >
                    Hash
                  </StyledTableCell>
                  <StyledTableCell align="right">{data.tx.hash}</StyledTableCell>
                </StyledTableRow>
                
                <StyledTableRow key="4">
                  <StyledTableCell >
                    Signature
                  </StyledTableCell>
                  <StyledTableCell align="right">{JSON.stringify(data.tx.signature)}</StyledTableCell>
                </StyledTableRow>
  
                <StyledTableRow key="5">
                  <StyledTableCell >
                    BlockId
                  </StyledTableCell>
                  <StyledTableCell align="right">1</StyledTableCell>
                </StyledTableRow>
  
                <StyledTableRow key="6">
                  <StyledTableCell >
                    Created Date
                  </StyledTableCell>
                  <StyledTableCell align="right">{data.tx.timeStamp}</StyledTableCell>
                </StyledTableRow>
                
              </TableBody>
            </Table>
          </TableContainer>
        
        
        </div>
  
        <div align="center" style={{marginTop:20, marginBottom:20}}>
          <Divider />
        </div>
  
  
  
        <div align="center" style={{marginTop: 20, marginBottom: 10}}>
          
          <TextField
            
            style={{width: 400, marginTop: 30, backgroundColor: "#FFFFFF", height: 20}}
            placeholder="password for private key"
            value={password}
            id="password"
            onChange={(v) => {
              setPassword(v.target.value)
            }}
            color="secondary"
          />
  
          
        </div>
  
        <div align="center" style={{marginTop: 20, marginBottom: 30}}>
    
          
          <Button onClick={onDownload} className={classes.shape} align="left" variant="contained" color="primary"
                  style={{width: 100, height: 30}}>Download </Button>
  
          <Button onClick={handleClose} className={classes.shape} align="left" variant="contained" color="secondary"
                  style={{marginLeft:20, width: 100, height: 30}}>Cancel </Button>
  
        </div>
        
        
  
      </div>
    
    
    </Dialog>
  );
}

const dialogStyles = makeStyles({
  dialog: {
    position: 'absolute',
    left: 10,
    top: 50
  },
  table: {
    minWidth: 700,
  },
});