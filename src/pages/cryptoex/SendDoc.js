import React from 'react';


import Dialog from '@material-ui/core/Dialog';

import InputAdornment from '@material-ui/core/InputAdornment';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';

import ListItemText from '@material-ui/core/ListItemText';


import axios from 'axios';

import { makeStyles, withStyles } from '@material-ui/core/styles';
import {GetEncrypted,GetDecryptedPK,GetSignature,GetHash} from '../../ec/CryptoUtils';
import {decryptECIES} from "../../ec/Encryption";


const dialogStyles = makeStyles({
  dialog: {
    position: 'absolute',
    left: 10,
    top: 50
  },
  input: {
    display: 'none',
  },
  list: {
    width: '100%',
    maxWidth: 360,
    
  },
  
});




export default  function SendDoc(props) {
  
  
  const { context, dialogCallback, open, rowData } = props;
  
  const classes = dialogStyles();
  
  
  const [to, setTo] = React.useState("");
  const [subject, setSubject] = React.useState("");
  const [note, setNote] = React.useState("");
  const [fileObject,setFileObject] = React.useState({});
  
  
  const [password, setPassword] = React.useState("");
  const [save, setSave] = React.useState(false);
  
  const [fromMyDocs, setFromMyDocs] = React.useState(false);
  const [myDocs, setMyDocs] = React.useState([]);
  
  const [selectedIndex, setSelectedIndex] = React.useState(0);
  
  
  const onFromMyDocs = () => {
    setFromMyDocs(true);
    // Set all my docs from the chain...
  
    const url = "http://localhost:4000/documents/get?home=" + context.homeId + "&account=" + context.user;
  
    axios.get(url).then ((result) => {
      setMyDocs(result.data);
    
    }).catch ((error) => {
      console.log("Error in getting Internal Pending Invitations data  " + JSON.stringify(error));
    })
    
  };
  
  const handleListClick = (event, index) => {
    setSelectedIndex(index);
  };

  
  const getMyDocList = () => {
    
    return myDocs.map((item,index) => {
     return (
      <ListItem
        button
        selected={selectedIndex === index}
        key={index}
        onClick={(event) => handleListClick(event, index)} >
        
        <ListItemText primary={item.tx.name} />
      </ListItem>

     )
    });
    
    
  };
  
  
  
  
  const handleClose = (action) => {
    dialogCallback(action);
  };
  
  
  
  const onSave = () => {
    setSave(true);
  };
  
  const cancelThis = (e) => {
    dialogCallback("cancel");
  };
  
  const setFieldValue = (e) => {
    
    const id = e.target.id;
    const value = e.target.value;
    
    if (id === "subject") {
      setSubject(value);
    } else if (id ==="to") {
      setTo(value);
    } else if (id ==="note") {
      setNote(value);
    }
    else if (id === "password") {
      setPassword(value);
    }
    else {
      console.log("Wrong id in the form field.. should not hapen...");
    }
  };
  
  const notFound = () => {
    return Promise.resolve("NotFound");
  };
  
  const onSubmit = () => {
  
  
    const index = to.indexOf("/");
  
    const recipientHomeId = to.substring(0,index);
    const recipientAccount = to.substring(index+1,to.length);
    
    let localFileObject = fileObject;
    let fileId = "";
    
    if (fromMyDocs) {
      const docBody = JSON.parse(myDocs[selectedIndex].tx.body);
      fileId = myDocs[selectedIndex]._id;
      const encrypteDoc = docBody.doc;
      const privateKey = GetDecryptedPK(context.encryptedPK, password);
      const decryptedDoc = decryptECIES(privateKey,encrypteDoc);
      const docJson = JSON.parse(decryptedDoc);
      localFileObject = docJson;
    }
    
    // Get Recipient PK from the chain
  
   // const pkURL = "http://localhost:4000/home/account?home=" + recipientHomeId + "&account=" + recipientAccount;
    const url = "http://localhost:4000/api/account?home=" + recipientHomeId + "&account=" +recipientAccount;
    
    axios.get(url).then ((result) => {
      
      const recipientPK = result.data.publicKey;
      
      if (recipientPK) {
  
        const encryptDoc = GetEncrypted(recipientPK,JSON.stringify({notes:note,file:localFileObject}));
  
        const body = {
          senderHomeId:context.homeId,
          senderAccount:context.user,
          recipientHome:recipientHomeId,
          recipientAccount:recipientAccount,
          fileId: fileId,
          subject: subject,
          doc: encryptDoc
        };
  
        const sBody = JSON.stringify(body);
  
        const hash = GetHash(sBody);
  
        const signature = GetSignature(context.encryptedPK,password,hash);
        
        const tx = {
    
          senderHome: context.homeId,
          senderAccount:context.user,
          recipientHome:recipientHomeId,
          recipientAccount:recipientAccount,
          fileId: fileId,
          senderPK:context.publicKey,
          subject: subject,
          timeStamp:new Date(),
          hash:hash,
          signature:signature,
    
          body: sBody
        };
  
        const url = "http://localhost:4000/cryptoxpress/send";
        return axios.post(url,tx)
      
      } else {
        return notFound();
      }
      
    }).then((result) => {
      console.log(result);
      
      if (result !== "NotFound") {
  
        if (result.data.success) {
          // message posted successfully
          dialogCallback({cancel:false,success:true});
        } else {
          dialogCallback({cancel:false,success:false, error:result.error});
        }
  
      }
      
    }).catch ((error) => {
      
      console.log("Error " + error);
      
    });
    
  };
  
  const handleUpload = (e) => {
    
    let file = e.target.files[0];
    
    let reader = new FileReader();
    
    // Convert the file to base64 text
    reader.readAsDataURL(file);
    
    reader.onload = () => {
      
      let fileInfo = {
        fromFile: true,
        name: file.name,
        type: file.type,
        size: Math.round(file.size / 1000) + ' kB',
        base64: reader.result,
      };
      setFileObject(fileInfo);
      
    };
  };
  
  
  
  return (
    <Dialog onClose={handleClose} open={open} fullWidth maxWidth="lg" classes={{paper: classes.dialog }} >
      
      <div className={classes.content} style={{width:800,align:"center",marginLeft:50}}>
        
        <div className={classes.margin}>
          <Typography color="primary" variant="h5" align="center"> Send a Document</Typography>
        </div>
        
        
        
        <div className={classes.margin} align="center" style={{marginTop:30}}>
          <TextField
            
            style={{width:500,margin:2}}
            color="primary"
            value = {to}
            id ="to"
            placeholder="$HomeId/$recipientEmail e.g.... ABN Inc/akbar@abn.com"
            onChange={setFieldValue}
            InputProps={{
              startAdornment: <InputAdornment position="start">@To...</InputAdornment>,
              className:classes.inputColor
            }}
          />
          
          <TextField
            
            style={{width:500,margin:2,marginTop:10}}
            placeholder="Subject"
            color="primary"
            value = {subject}
            id ="subject"
            onChange={setFieldValue}
          
          />
  
          <TextareaAutosize style={{width:500,margin:2,marginTop:20}} rowsMin={5} id="note" value={note} onChange={setFieldValue}
                            placeholder="Note to the recipient...Max. 100 Characters "
          />
  
          {!fromMyDocs &&
  
          <div align="center" style={{marginTop: 30}}>
    
            <input
              accept="*/*"
              className={classes.input}
              id="fileUpload"
              type="file"
              onChange={handleUpload}
            />
            <label htmlFor="fileUpload">
              <Button variant="contained" color="primary" component="span">
                Upload File
              </Button>
            </label>
  
          </div>
    
          }
  
          
          <div align="center" style={{marginTop:30}}>
    
            <Button onClick={onFromMyDocs} className={classes.shape} color="secondary" >
                Choose from My Documents...
            </Button>
            
          </div>
          
          
          <div>
  
           
            { fromMyDocs &&
            <div align="center" style={{marginTop:30, width:400}}>
  
              <List component="nav" >
                {getMyDocList()}
                
              </List>
              
              
              
            </div>
            
            }
            
            
          </div>
          
  
          {save &&
          
          <TextField
    
            style={{width: 400, marginTop: 30, backgroundColor: "#FFFFFF", height: 20}}
            placeholder="password for private key"
            value={password}
            id="password"
            onChange={setFieldValue}
            color="secondary"
          />
          }
          
        
        </div>
        
        <div align="center" style={{marginTop:30, marginBottom:100}}>
          
          {!save &&
          <Button onClick={onSave} className={classes.shape} align="left" variant="contained" color="primary"
                  style={{width: 100, height: 30}}>Save </Button>
          }
          {save &&
          
          <Button onClick={onSubmit} className={classes.shape} align="left" variant="contained" color="primary"
                  style={{width: 100, height: 30}}>Submit </Button>
          }
          <Button onClick = {cancelThis} className={classes.shape}  variant="contained" align = "right" style={{width:100,height:30, marginLeft:20}} color="secondary" > Cancel </Button>
        
        </div>
      </div>
    
    
    </Dialog>
  );
}