
import React, { useEffect,useStates } from 'react';
import { makeStyles } from '@material-ui/core/styles';


import MaterialTable from 'material-table';

import DocuDetails from './DocuDetails';
import tableIcons from '../common/IconDef';
import BlockchainInfo from "./BlockchainInfo";

import axios from 'axios';



export default  function MyDocuments(props) {
  
  const { context } = props;
  
  const classes = tableTheme();
  
  const [tableData, setTableData] = React.useState([]);
  const [indexData, setIndexData] = React.useState({});
  const [openDetailsDialog, setOpenDetailsDialog] = React.useState(false);
  
  const [blockchainInfoDialog, setBlockchainInfoDialog] = React.useState(false);
  
  
  
  useEffect(()=> {
    
    const url = "http://localhost:4000/documents/get?home=" + context.homeId + "&account=" + context.user;
    
    axios.get(url).then ((result) => {
      setTableData(result.data);
      
    }).catch ((error) => {
      console.log("Error in getting Internal Pending Invitations data  " + JSON.stringify(error));
    })
    
  },[]);
  
  
  
  
  const getDataSet = () => {
    const data = tableData.map((row, index) => {
      return {name: row.tx.name, description: row.tx.description,date:row.tx.timeStamp};
    });
    return data;
  };
  
  const viewAndDownload = (row) => {
    
    setIndexData(tableData[row.tableData.id]);
    setOpenDetailsDialog(true);
    
  };
  
  const showBlockchainInfo = (row) => {
    
    setIndexData(tableData[row.tableData.id]);
    setBlockchainInfoDialog(true);
    
  };
  
  const detailsDialogCallback = (f) => {
    setOpenDetailsDialog(false);
  };
  
  const infoDialogCallback = (f) => {
    setBlockchainInfoDialog(false);
  };
  
  
  
  return (
    
    <div className={classes.table}>
      <MaterialTable
        title="My Documents"
        icons={tableIcons}
        
        
        localization={{
          header: {
            actions: 'View'
          },
        }}
        
        columns={[
          { title: 'Document', field: 'name' },
          { title: 'Description', field: 'description' },
          { title: 'Date', field: 'date' },
          
        ]}
        data={getDataSet()}
        
        actions={[
          {
            icon: tableIcons.Detail,
            tooltip: 'Download',
            onClick: (event, rowData) => {viewAndDownload(rowData)}
          },
          {
            icon: tableIcons.Info,
            tooltip: 'Blockchain info',
            onClick: (event, rowData) => {showBlockchainInfo(rowData)}
          }
        ]}
        
        options={{
          actionsColumnIndex: 4,
          padding:"dense",
          
          headerStyle: {
            color: 'white',
            background:'#0091EA',
            fontSize:20,
            fontFamily:"Roboto"
          },
          rowStyle: {
            color: 'black'
          }
          
        }}
        
      />
  
      {openDetailsDialog &&
        <DocuDetails open={openDetailsDialog} context = {context} callback={detailsDialogCallback} data={indexData} viewOriginal = 'true'/>
      }
      
      { blockchainInfoDialog &&
        <BlockchainInfo open={blockchainInfoDialog} context = {context} callback={infoDialogCallback} data={indexData} />
      }
      
    </div>
  )
}



const tableTheme = makeStyles(theme => ({
  table: {
    '& tbody>.MuiTableRow-root:hover': {
      background: '#EEE',
    }
  },
  body: {
    textColor:"primary"
  }
}));
