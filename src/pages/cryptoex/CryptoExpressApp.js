

import React from 'react';


import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import IconButton from '@material-ui/core/IconButton';
import ReferenceIcon from '@material-ui/icons/Dns';
import CollaborateIcon from '@material-ui/icons/GroupWork';

import AddIcon from '@material-ui/icons/Add';
import ListIcon from '@material-ui/icons/List';
import CertifyIcon from '@material-ui/icons/Fingerprint';



import Home from '@material-ui/icons/HomeWork';
import POC from '@material-ui/icons/Gavel';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import BOIcon from '@material-ui/icons/BusinessCenter';
import NetworkIcon from '@material-ui/icons/Business';
import FileIcon from '@material-ui/icons/FileCopy';
import SendIcon from '@material-ui/icons/Send';
import InboxIcon from '@material-ui/icons/Inbox';
import OutboxIcon from '@material-ui/icons/AllOut';

import Grid from '@material-ui/core/Grid';

import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';



import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import SendDoc from "./SendDoc";
import NewDoc from "./NewDoc";
import MyDocuments from "./MyDocuments";
import SentDocs from "./SentDocs";
import InDocs from "./InDocs";




const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    marginTop:50,
    backgroundColor:"white"
  },
  nested: {
    paddingLeft: theme.spacing(11),
  },
  
  toolbar: theme.mixins.toolbar,
}));

export default function CryptoExpressApp(props) {
  
  const {context} = props;
  const classes = useStyles();
  
  const [selectedIndex, setSelectedIndex] = React.useState(-1);
  const [sendDialog,setSendDialog] = React.useState(false);
  const [newDocDialog, setNewDocDialog] = React.useState(false);
  
  const [snackMessage, setSnackMessage] = React.useState("test message");
  const [snackOpen, setSnackOpen] = React.useState(false);
  const [error, setError] = React.useState(false);
  
  const [anchorEl, setAnchorEl] = React.useState(null);
  const anchorOpen = Boolean(anchorEl);
  const [tabValue, setTabValue] = React.useState(1);
  
  const handleTabChange = (event, newValue) => {
  
    if (newValue === 0 ) {
      props.setActiveScreenCallback(1); // Dashboard
    } else if (newValue === 1 ) {
      props.setActiveScreenCallback(7); // files
    } else if (newValue === 2 ) {
      props.setActiveScreenCallback(3); // Reference Objects
    } else if (newValue  === 3) {
      props.setActiveScreenCallback(4); // Contracts
    } else if (newValue  === 4) {
      props.setActiveScreenCallback(6); // Collaborate
    } else if (newValue  === 5) {
      props.setActiveScreenCallback(2); // Network
    }
    setTabValue(newValue);
  };
  
  const onLogout = () => {
    console.log("Logout called");
    props.logoutCallback();
    
  };
  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  
  
  const handleSnackClose = ()=> {
    setSnackOpen(false);
    
  };
  
  const selectTab = (event, index) => {
    setSelectedIndex(index);
    
    if (index === 0) {
      setNewDocDialog(true);
    } else if (index === 2) {
      setSendDialog(true);
    }
  };
  
  
  
  const setSnackbarMessageCallback = (isError,message) => {
    setError(isError);
    setSnackMessage(message);
    setSnackOpen(true);
  };
  
  
  const onSendDocDialogCallback = (type,message) => {
    setSendDialog(false);
  };
  
  const onNewDocDialogCallback = (type,message) => {
  
  
    setNewDocDialog(false);
    setSelectedIndex(-1);
    if (type === "submit") {
    
      setSnackOpen(true);
      setSnackMessage("Successfully sent a new document to the network...");
    }
  };
  
  const onCertify = () => {
    props.setActiveScreenCallback(8);
  
  };
  
  const onBizObjects = () => {
    props.setActiveScreenCallback(3);
    
  };
  
  
  
  return (
    <div className={classes.root}>
      <CssBaseline/>
  
      <AppBar position="fixed" className={classes.appBar} style = {{backgroundColor:"white", color:"#303f9f"}}>
        <Toolbar>
          <Grid direction="row"  container>
  
            <Grid xs={1} item style={{marginTop:10}}>
              <Typography variant="h6" >
                ABBN
              </Typography>
            </Grid>
            
            <Grid xs={9} item>
              <Grid justify={"center"}>
                <Tabs
                  value={tabValue}
                  onChange={handleTabChange}
                  variant="fullWidth"
                  indicatorColor="primary"
                  textColor="primary"
                  centered
                >
                  <Tab icon={<Home fontSize="small"/>} label="Home"/>
                  <Tab icon={<FileIcon  fontSize="small" />} label="Files"/>
                  <Tab icon={<ReferenceIcon  fontSize="small" />} label="Reference Data Objects"/>
                  <Tab icon={<POC  fontSize="small" />} label="Contracts"/>
                  <Tab icon={<CollaborateIcon fontSize="small"/>} label="Collaborate"/>
                  <Tab icon={<NetworkIcon fontSize="small"/>} label="Network"/>

                </Tabs>
              </Grid>
            </Grid>
            <Grid item xs={1} />
        
            <Grid item xs={1} justify={"center"}>
              <Grid justify={"right"}>
            
                <div>
                  <IconButton
                
                    aria-haspopup="true"
                    onClick={handleMenu}
                    color="inherit"
              
                  >
                    <AccountCircle fontSize="large"/>
                  </IconButton>
                  <Menu
                    id="menu-appbar"
                    anchorEl={anchorEl}
                    anchorOrigin={{
                      vertical: 'top',
                      horizontal: 'right',
                    }}
                    keepMounted
                    transformOrigin={{
                      vertical: 'top',
                      horizontal: 'right',
                    }}
                    open={anchorOpen}
                    onClose={handleClose}
                  >
                    <MenuItem >{context.homeId} / {context.user}</MenuItem>
                    <MenuItem onClick={onLogout}>logout</MenuItem>
                  </Menu>
                </div>
          
              </Grid>
            </Grid>
          </Grid>
    
        </Toolbar>
      </AppBar>
      
      <Drawer
        className={classes.drawer}
        variant="permanent"
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <div className={classes.toolbar}/>
  
        <List>
  
          <div style={{marginLeft:20,marginRight:20,width:"90%",fontSize:14,marginTop:30}} align="center">
            {context.homeId}/ {context.user}
          </div>
          
          <div style={{marginLeft:20,marginRight:20,backgroundColor:"#0091EA", color:"white", height:30, width:"90%",fontSize:20,marginTop:30}} align="center">
            Files
          </div>
  
          <ListItem button key="0" onClick={event => selectTab(event, 0)} selected={selectedIndex === 0}>
            <ListItemIcon color="primary">
              <AddIcon/>
            </ListItemIcon>
            <ListItemText primary={<Typography style={{ color: '#303f9f' }}>Add</Typography>}/>
          </ListItem>
  
          <ListItem button key="1" onClick={event => selectTab(event, 1)} selected={selectedIndex === 1}>
            <ListItemIcon color="primary">
              <ListIcon/>
            </ListItemIcon>
            <ListItemText primary={<Typography style={{ color: '#303f9f' }}>All Files</Typography>}/>
          </ListItem>
  
          <Divider/>
          <div style={{marginLeft:20,marginRight:20,backgroundColor:"#0091EA", color:"white", height:30, width:"90%",fontSize:20,marginTop:30}} align="center">
            Send and Receive
          </div>
  
  
          <ListItem button key="2" onClick={event => selectTab(event, 2)} selected={selectedIndex === 2}>
            <ListItemIcon color="primary">
              <SendIcon/>
            </ListItemIcon>
            <ListItemText primary={<Typography style={{ color: '#303f9f' }}>Send</Typography>}/>
          </ListItem>
  
          
          <ListItem button key="3" onClick={event => selectTab(event, 3)} selected={selectedIndex === 3}>
            <ListItemIcon>
              <InboxIcon/>
            </ListItemIcon>
            <ListItemText primary={<Typography style={{ color: '#303f9f' }}>In box</Typography>}/>
          </ListItem>
  
          
          <ListItem button key="4" onClick={event => selectTab(event, 4)} selected={selectedIndex === 4 }>
            <ListItemIcon>
              <OutboxIcon/>
            </ListItemIcon>
            <ListItemText primary={<Typography style={{ color: '#303f9f' }}>Sent Box</Typography>}/>
          </ListItem>
  
          <Divider/>
         
          <div style={{marginLeft:20,marginRight:20,backgroundColor:"#0091EA", color:"white", height:30, width:"90%",fontSize:20,marginTop:30}} align="center">
            Certifications
          </div>
  
          <ListItem button key="6" onClick={onCertify}>
            <ListItemIcon>
              <CertifyIcon/>
            </ListItemIcon>
            <ListItemText primary={<Typography style={{ color: '#303f9f' }}>Certify Assets</Typography>}/>
          </ListItem>



        </List>
        <Divider/>
      </Drawer>
      
      
      <main className={classes.content}>
  
  
        {selectedIndex === -1 &&
        <div>
          <IntoSection context={props.context} setSnackbarMessageCallback={setSnackbarMessageCallback}/>
        </div>
        }
  
        {selectedIndex === 0 &&
        <div>
          <NewDoc context={props.context} open = {newDocDialog} dialogCallback = {onNewDocDialogCallback} />
        </div>
        }
  
        {selectedIndex === 1 &&
        <div>
          <MyDocuments context={props.context}  />
        </div>
        }
  
        {selectedIndex === 2 &&
        <div>
          <SendDoc context={props.context} open = {sendDialog} dialogCallback = {onSendDocDialogCallback}
                   setSnackbarMessageCallback={setSnackbarMessageCallback}/>
        </div>
        }
        
        {selectedIndex === 3 &&
          <div>
            <InDocs context={props.context} />
          </div>
        }
  
        {selectedIndex === 4 &&
        <div>
          <SentDocs context={props.context}   />
        </div>
        }
  
        
        
        <div>
          <Snackbar open={snackOpen} autoHideDuration={6000} onClose={handleSnackClose}>
            { !error &&
              <Alert onClose={handleSnackClose} severity={error?"error":"success"}>{snackMessage}</Alert>
            }
          </Snackbar>
        </div>
      </main>
    </div>
  );
}

function IntoSection (props) {
  
  const {context} = props;
  const classes = useStyles();
  
  return (
  
    <div style={{marginTop:100}}>
  
      <Grid container alignItems="center"
            justify="center" direction="row">
    
        <Grid item xs={4}>
          <Card className={classes.root} variant="outlined"
                style={{width: '100%',height:250,display: 'flex', flexDirection: 'column',align:"center"}}>
            <CardHeader style = {{backgroundColor:"white", color:"#0091EA", textAlign:"center"}}
        
                        title="Store Files on Blockchain"
        
            />
            <CardContent >
              <Typography align="center" color = "primary">
                Encrypted Storage
              </Typography>
              
            </CardContent>
      
          </Card>
    
        </Grid>
        <Grid item xs={4}>
          <Card className={classes.root} variant="outlined"
                style={{width: '100%',height:250,display: 'flex', flexDirection: 'column',align:"center"}}>
            <CardHeader style = {{backgroundColor:"white", color:"#0091EA", textAlign:"center"}}
        
                        title="Send and Receive Files"
        
            />
            <CardContent >
              <Typography align="center" color = "primary">
                Encrypted and Secure
              </Typography>
            </CardContent>
      
          </Card>
    
        </Grid>
        <Grid item xs={4}>
          <Card className={classes.root} variant="outlined"
                style={{width: '100%',height:250,display: 'flex', flexDirection: 'column',align:"center"}}>
            <CardHeader style = {{backgroundColor:"white", color:"#0091EA",textAlign:"center"}}
        
                        title="Cryptographic Certification"
        
            />
            <CardContent >
              <Typography align="center" color = "primary">
                Public Verification
              </Typography>
            </CardContent>
      
          </Card>
    
        </Grid>
        
      </Grid>
      
    </div>
    
  )
  
}


function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}
