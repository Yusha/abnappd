import React from 'react';


import Dialog from '@material-ui/core/Dialog';

import InputAdornment from '@material-ui/core/InputAdornment';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';


import axios from 'axios';

import { makeStyles } from '@material-ui/core/styles';
import {GetEncrypted,GetSignature,GetHash} from '../../ec/CryptoUtils';


const dialogStyles = makeStyles({
  dialog: {
    position: 'absolute',
    left: 10,
    top: 50
  },
  input: {
    display: 'none',
  },
});

export default  function NewDoc(props) {
  
  
  const { context, dialogCallback, open } = props;
  
  const classes = dialogStyles();
  
  
  const [name, setName] = React.useState("");
  const [description, setDescription] = React.useState("");
  
  const [fileObject,setFileObject] = React.useState("");
  
  
  const [password, setPassword] = React.useState("");
  const [save, setSave] = React.useState(false);
  
  
  
  const handleClose = (type, action) => {
    dialogCallback(type, action);
  };
  
  
  
  const onSave = () => {
    setSave(true);
  };
  
  const cancelThis = (e) => {
    dialogCallback("cancel", "");
  };
  
  const setFieldValue = (e) => {
    
    const id = e.target.id;
    const value = e.target.value;
    
    if (id === "name") {
      setName(value);
    } else if (id ==="description") {
      setDescription(value);
    }
    else if (id === "password") {
      setPassword(value);
    }
    else {
      console.log("Wrong id in the form field.. should not hapen...");
    }
  };
  
  const onSubmit = () => {
    
    
    const encryptDoc = GetEncrypted(context.publicKey,JSON.stringify(fileObject));
    
    
    const body = {
      ownerHomeId:context.homeId,
      ownerAccount:context.user,
      name: name,
      description:description,
      doc: encryptDoc
    };
    
    const sBody = JSON.stringify(body);
    
    const hash = GetHash(sBody);
    const signature = GetSignature(context.encryptedPK,password,hash);
    
    const tx = {
      
      ownerHome: context.homeId,
      ownerAccount:context.user,
      publicKey:context.publicKey,
      name: name,
      description: description,
      timeStamp:new Date(),
      hash:hash,
      signature:signature,
      
      body: sBody
    };
    
    const url = "http://localhost:4000/documents/add";
    
    axios.post(url,tx).then ((result) => {
      
      if (result.data.success) {
        // message posted successfully
        handleClose("submit",{cancel:false,success:true});
      } else {
        handleClose("submit", {cancel:false,success:false, error:result.error});
      }
      
    }).catch ((error) => {
      console.log("Error in posting a message");
      
    })
  };
  
  const handleUpload = (e) => {
    
    let file = e.target.files[0];
    
    let reader = new FileReader();
    
    // Convert the file to base64 text
    reader.readAsDataURL(file);
    
    reader.onload = () => {
      
      let fileInfo = {
        fromFile: true,
        name: file.name,
        type: file.type,
        size: Math.round(file.size / 1000) + ' kB',
        base64: reader.result,
      };
      setFileObject(fileInfo);
      
    };
  };
  
  
  
  
  
  return (
    <Dialog onClose={handleClose} open={open} fullWidth maxWidth="lg" classes={{paper: classes.dialog }} >
      
      <div className={classes.content} style={{width:800,align:"center",marginLeft:50}}>
        
        <div className={classes.margin}>
          <Typography color="primary" variant="h5" align="center"> Add a New Document</Typography>
        </div>
        
        
        
        <div className={classes.margin} align="center" style={{marginTop:30}}>
          <TextField
            
            style={{width:500,margin:2}}
            color="primary"
            value = {name}
            id ="name"
            placeholder="Name of the object"
            onChange={setFieldValue}
            InputProps={{
              startAdornment: <InputAdornment position="start">@Name...</InputAdornment>,
              className:classes.inputColor
            }}
          />
          
          <TextField
            
            style={{width:500,margin:2,marginTop:10}}
            placeholder="Description of the object..."
            color="primary"
            value = {description}
            id ="description"
            onChange={setFieldValue}
          
          />
  
          
          
          <div align="center" style={{marginTop:30}}>
  
            <input
              accept="*/*"
              className={classes.input}
              id="fileUpload"
              type="file"
              onChange={handleUpload}
            />
            <label htmlFor="fileUpload">
              <Button variant="contained" color="primary" component="span">
                Upload File
              </Button>
            </label>
          </div>
  
          {save &&
          
          <TextField
    
            style={{width: 400, marginTop: 30, backgroundColor: "#FFFFFF", height: 20}}
            placeholder="password for private key"
            value={password}
            id="password"
            onChange={setFieldValue}
            color="secondary"
          />
          }
          
        
        </div>
        
        <div align="center" style={{marginTop:30, marginBottom:100}}>
          
          {!save &&
          <Button onClick={onSave} className={classes.shape} align="left" variant="contained" color="primary"
                  style={{width: 100, height: 30}}>Save </Button>
          }
          {save &&
          
          <Button onClick={onSubmit} className={classes.shape} align="left" variant="contained" color="primary"
                  style={{width: 100, height: 30}}>Submit </Button>
          }
          <Button onClick = {cancelThis} className={classes.shape}  variant="contained" align = "right" style={{width:100,height:30, marginLeft:20}} color="secondary" > Cancel </Button>
        
        </div>
      </div>
    
    
    </Dialog>
  );
}