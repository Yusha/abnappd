

import React, {useEffect} from 'react';


import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import IconButton from '@material-ui/core/IconButton';
import ApprovalIcon from '@material-ui/icons/ThumbUp';

import GroupIcon from '@material-ui/icons/Group';
import ReferenceIcon from '@material-ui/icons/Dns';


import Grid from '@material-ui/core/Grid';

import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';

import Home from '@material-ui/icons/HomeWork';

import POC from '@material-ui/icons/Gavel';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import MessageIcon from '@material-ui/icons/Message';
import CollaborateIcon from '@material-ui/icons/GroupWork';

import NetworkIcon from '@material-ui/icons/Business';
import FileIcon from '@material-ui/icons/FileCopy';
import Search from '@material-ui/icons/Search';

import SearchSection from '../search/SearchScreen';


const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  nested: {
    paddingLeft: theme.spacing(11),
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    marginTop:100,
  },
  toolbar: theme.mixins.toolbar,
}));

export default function DashboardApp(props) {
  
  const {context} = props;
  
  
  const classes = useStyles();
  
  
  const [listIndex, setListIndex] = React.useState(0);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  
  const [tabValue, setTabValue] = React.useState(0);
  
  const handleTabChange = (event, newValue) => {
  
    if (newValue === 0 ) {
      props.setActiveScreenCallback(1); // Dashboard
    } else if (newValue === 1 ) {
      props.setActiveScreenCallback(7); // files
    } else if (newValue === 2 ) {
      props.setActiveScreenCallback(3); // Reference Objects
    } else if (newValue  === 3) {
      props.setActiveScreenCallback(4); // Contracts
    } else if (newValue  === 4) {
      props.setActiveScreenCallback(6); // Collaborate
    } else if (newValue  === 5) {
      props.setActiveScreenCallback(2); // Network
  }
    setTabValue(newValue);
  };
  
  const onLogout = () => {
    console.log("Logout called");
    props.logoutCallback();
    
  };
  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  
  const handleClick = (type) => {
    
    
    if (type === 1 ) {
      props.setActiveScreenCallback(5);
    } else if (type === 2 ) {
      props.setActiveScreenCallback(10);
  
    } else if (type === 3) {
      props.setActiveScreenCallback(11);
    } else {
      setListIndex(type);
    }
  };
  
  
  
  
  return (
    <div className={classes.root}>
      <CssBaseline/>
  
      <AppBar position="fixed" className={classes.appBar} style = {{backgroundColor:"white", color:"#303f9f"}}>
        <Toolbar>
          <Grid direction="row"  container>
      
            <Grid xs={1} item style={{marginTop:10}}>
              <Typography variant="h6" >
                ABBN
              </Typography>
            </Grid>
            
            <Grid xs={9} item>
              <Grid >
                <Tabs
                  value={tabValue}
                  onChange={handleTabChange}
                  variant="fullWidth"
                  indicatorColor="primary"
                  textColor="primary"
                  centered
                >
                  <Tab icon={<Home fontSize="small"/>} label="Home"/>
                  <Tab icon={<FileIcon  fontSize="small" />} label="Files"/>
                  <Tab icon={<ReferenceIcon  fontSize="small" />} label="Reference Data Objects"/>
                  <Tab icon={<POC  fontSize="small" />} label="Contracts"/>
                  <Tab icon={<CollaborateIcon fontSize="small"/>} label="Collaborate"/>
                  <Tab icon={<NetworkIcon fontSize="small"/>} label="Network"/>
                  
                </Tabs>
              </Grid>
            </Grid>
            <Grid item xs={1} />
      
            <Grid item xs={1} >
              <Grid >
          
                <div>
                  <IconButton
              
                    aria-haspopup="true"
                    onClick={handleMenu}
                    color="inherit"
            
                  >
                    <AccountCircle fontSize="large"/>
                  </IconButton>
                  <Menu
                    id="menu-appbar"
                    anchorEl={anchorEl}
                    anchorOrigin={{
                      vertical: 'top',
                      horizontal: 'right',
                    }}
                    keepMounted
                    transformOrigin={{
                      vertical: 'top',
                      horizontal: 'right',
                    }}
                    open={open}
                    onClose={handleClose}
                  >
                    <MenuItem >{context.homeId} / {context.user}</MenuItem>
                    <MenuItem onClick={onLogout}>logout</MenuItem>
                  </Menu>
                </div>
        
              </Grid>
            </Grid>
          </Grid>
  
        </Toolbar>
      </AppBar>
      
      <Drawer
        className={classes.drawer}
        variant="permanent"
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        
        <div className={classes.toolbar}/>
  
        
        <div style={{marginLeft:20,marginRight:20,width:"90%",fontSize:14,marginTop:30}} align="center">
          {context.homeId}/ {context.user}
        </div>
        
        
        
        <List>
  
          <ListItem button key="search" onClick={()=>{handleClick(0)}} selected={listIndex === 0} style={{marginTop:30}}>
            <ListItemIcon>
              <Search style={{color: '#303f9f',fontSize:30,fontWeight:'bold'}}/>
            </ListItemIcon>
            <ListItemText primary={<Typography style={{ color: '#303f9f' }}>Search</Typography>} />
          </ListItem>
          
       
          <ListItem button key="accounts" onClick={()=>{handleClick(1)}} selected={listIndex === 1} style={{marginTop:10}}>
            <ListItemIcon>
              <GroupIcon style={{color: '#303f9f',fontSize:30,fontWeight:'bold'}}/>
            </ListItemIcon>
            <ListItemText primary={<Typography style={{ color: '#303f9f' }}>Accounts</Typography>} />
          </ListItem>
          
       
          <ListItem button key="roles" onClick={()=>{handleClick(2)} } selected={listIndex === 2} style={{marginTop:10}}>
            <ListItemIcon>
              <MessageIcon style={{color: '#303f9f',fontSize:30,fontWeight:'bold'}}/>
            </ListItemIcon >
            <ListItemText primary={<Typography style={{ color: '#303f9f' }}>Messaging</Typography>} />
          </ListItem>
  
       
          <ListItem button key="actions" onClick={()=>{handleClick(3)} } selected={listIndex === 3} style={{marginTop:10}}>
            <ListItemIcon>
              <ApprovalIcon style={{color: '#303f9f',fontSize:30,fontWeight:'bold'}}/>
            </ListItemIcon>
            <ListItemText primary={<Typography style={{ color: '#303f9f' }}>Approvals</Typography>} />
          </ListItem>
        </List>
        <Divider/>
      </Drawer>
      
      
      <main className={classes.content}>
        
        
        { listIndex === -1 &&
          
          <IntoSection/>
        }
  
        { listIndex === 0 &&
  
          <SearchSection context={context}/>
        }
        
      </main>
    </div>
  );
}




function IntoSection (props) {
  
  const {context} = props;
  const classes = useStyles();
  
  return (
    
    <div style={{marginTop:5}}>
      
      
      <div style={{marginTop:20}}>
        
        <Grid container alignItems="center"
              justify="center" direction="row">
          
          <Grid item xs={8}>
            <Card className={classes.root} variant="outlined"
                  style={{width: '100%',height:100,display: 'flex', flexDirection: 'column',align:"center"}}>
              <CardHeader style = {{backgroundColor:"white", color:"#0091EA", align:"center"}} align="center"
              
                          title="Invite to Network"
              
              />
              <CardContent >
                <Typography align="center" color = "primary">
                  Invite new business to the network
                </Typography>
              </CardContent>
            
            </Card>
          
          </Grid>
          
        
        
        </Grid>
      
      </div>
    </div>
  
  )
  
}
