import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import TextField from '@material-ui/core/TextField';
import EmailIcon from '@material-ui/icons/Email';
import BusinessIcon from '@material-ui/icons/Business';
import CountryIcon from '@material-ui/icons/AssistantPhoto';
import Key from '@material-ui/icons/VpnKey';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';

import ListItemText from '@material-ui/core/ListItemText';


import axios from 'axios';

import {GetHash, GetSignature} from '../../ec/CryptoUtils';


const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
  },
  backButton: {
    marginRight: theme.spacing(1),
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(2),
    
  },
  margin: {
    margin: theme.spacing(1),
  },
}));


function getSteps() {
  return ['Invitee', 'Role','Group', 'Sign and Submit', 'Finalize'];
}



export default function NewInvitation(props) {
  
  const classes = useStyles();
  const {context} = props;
  
  const {callback} = props;
  
  const [activeStep, setActiveStep] = React.useState(0);
  const [propValues, setPropValues] = React.useState({});
  const [role,setRole] = React.useState("");
  const [group, setGroup] = React.useState("");
  
  const [status, setSuccess] = React.useState({});
  
  const steps = getSteps();
  
  const roleCallback = (isCancel,value) => {
    setRole(value);
    handleNext();
  };
  
  const groupCallback = (isCancel,value) => {
    setGroup(value);
    handleNext();
  };
  
  
  
  const saveAndSubmit = (pass)=> {
    
    const password = pass;
    
    propValues['role'] = role;
    propValues['group'] = group;
    
    const sTx = JSON.stringify(propValues);
    
    const hashOf = GetHash(sTx);
    
    const signature = GetSignature(props.context.encryptedPK,password,hashOf);
  
  
    const chainTx = {
      action:"invite",
      homeId: props.context.homeId,
      invitee:propValues.email,
      user: props.context.user,
      publicKey:props.context.publicKey,
      hash:hashOf,
      signature:signature,
      tx:sTx
    };
    
    const url = "http://127.0.0.1:4000/home/invite";
    
      
    axios.post(url, {
        tx: chainTx
      }).then (result => {
        console.log("Transaction sent to the net work" + result);
        setSuccess({status:"Transaction Succeeded", id:hashOf});
        setActiveStep(prevActiveStep => prevActiveStep + 1);
        
      }).catch (error => {
        console.log("Error in sending the tx to the network... " + JSON.stringify(error));
        setSuccess({status:"Transaction Failed", id:hashOf});
        setActiveStep(prevActiveStep => prevActiveStep + 1);
        
    });
  };
  
  const  handleNext = () => {
    
    setActiveStep(prevActiveStep => prevActiveStep + 1);
  };
  
  const handleBack = () => {
    setActiveStep(prevActiveStep => prevActiveStep - 1);
  };
  
  const handleReset = () => {
    setActiveStep(0);
  };
  
  const savePropsAndNext = (propValues) => {
    setPropValues(propValues);
    handleNext();
  };
  
  const complete = () => {
    console.log("Please send me to the parent...");
    callback(false,"New account invitation Sent successfully");
  };
  
  
  return (
    <Grid className={classes.root}>
      <Stepper activeStep={activeStep} alternativeLabel>
        {steps.map(label => (
          <Step key={label}>
            <StepLabel>{label}</StepLabel>
          </Step>
        ))}
      </Stepper>
      
      <Grid container justify = "center">
        <div className={classes.instructions}>
          
          {activeStep === 0 &&
            <ChooseProps savePropsAndNext = {savePropsAndNext}/>
          }
  
          {activeStep === 1 &&
          
            <ChooseRole callback = {roleCallback} context={context}/>
          }
  
          {activeStep === 2 &&
          <ChooseGroup callback = {groupCallback} context={context}/>
          }
  
          {activeStep === 3 &&
            <SignAndSubmit backCallback = {handleBack} signAndSubmitCallback = {saveAndSubmit}/>
            
          }
  
          {activeStep === 4 &&
            <Finalize completeCallback = {complete} status = {status}/>
    
          }
        </div>
      </Grid>
      
      <Grid container justify = "center">
        <Typography className={classes.instructions}>
        
        </Typography>
      
      </Grid>
      <Divider/>
      
    </Grid>
  );
}


 function ChooseProps(props) {
  
  const {savePropsAndNext} = props;
  const classes = useStyles();
  const [email, setEmail] = React.useState('');
  const [message, setMessage] = React.useState('');
  
  
  const setFieldValue = (e)=> {
    const id = e.target.id;
    const value = e.target.value;
    
    if (id === "email") {
      setEmail(value);
    } else if (id === "message") {
      setMessage(value);
    } else {
      console.log("Unknown Field detected. Should not have happened");
    }
    
  };
  
  return (
    
    <div>
      <div className={classes.margin}>
  
        <div className={classes.margin}>
          <Grid container spacing={1} alignItems="flex-end">
            <Grid item>
              <EmailIcon/>
            </Grid>
            <Grid item>
              <TextField id="email" value={email}  onChange={setFieldValue} label="Email of the invitee"/>
            </Grid>
          </Grid>
        </div>
  
        <div className={classes.margin}>
        
          <Grid container spacing={1} alignItems="flex-end">
            <Grid item>
              <BusinessIcon/>
            </Grid>
            <Grid item>
              <TextField id="message" value={message} multiline onChange={setFieldValue} label="Note for the invitee"/>
            </Grid>
          </Grid>
        </div>
      
      
      <div className={classes.margin} style={{marginTop:20}}>
        <Grid container spacing={1} alignItems="flex-end">
  
          <Grid container justify = "center">
            
              <Grid container justify = "center">
        
                <Grid>
                  <Button
                    disabled
                    className={classes.backButton}
                  >
                    Back
                  </Button>
          
                  <Button variant="contained" color="primary" onClick={()=>savePropsAndNext({email:email,message:message})}>
                    Next
                  </Button>
                </Grid>
              </Grid>
            
          </Grid>
          
          
        </Grid>
      </div>
      
    </div>
    </div>
  );
}

 
 
 function ChooseRole(props) {
  
  const {context,callback} = props;
   const classes = useStyles();
  const [roles, setRoles] = React.useState([]);
  const [rolesGotten, setRolesGotten] = React.useState(false);
  const [selectedRole, setSelectedRole] = React.useState("");
  const [selectedIndex, setSelectedIndex] = React.useState(-1);
  
  const onGetRoles = () => {
  
     const url = "http://localhost:4000/home/roles?home=" + context.homeId ;
    
     axios.get(url).then ((result) => {
       setRoles(result.data);
       setRolesGotten(true);
      
     }).catch ((error) => {
       console.log("Error in getting Roles Data from blockchain  " + JSON.stringify(error));
     })
    
  };
  
  const handleListClick = (event, index) => {
     setSelectedRole(event.target.textContent);
     setSelectedIndex(index);
   };
  
  const getRoles = () => {
    
     return roles.map((item,index) => {
       
       const body = JSON.parse(item.tx.tx);
      
       return (
         <ListItem
           button
           selected={selectedIndex === index}
           key={index}
           onClick={(event) => handleListClick(event, index)} >
          
           <ListItemText primary={body.name}  />
         </ListItem>
      
       )
     });
    
   };
  
 const onSelectRole = () => {
     callback(false,selectedRole);
 };
   

 return (
  
    <div>
      
      <div align="center" style={{marginTop:30, width:800}}>
  
        {!rolesGotten &&
        <div align="center" style={{marginTop:100}}>
          <Button onClick={onGetRoles} className={classes.shape} align="left" variant="contained" color="primary"
                  style={{width: 300, height: 30}}>Select from The Roles </Button>
  
        </div>
        }
  
  
        {rolesGotten &&
        <div >
  
          <List component="nav" align="center" style={{align:"center",color:'white',background:"#0091EA"}}>
            {getRoles()}
    
          </List>
        </div>
        }
  
        {rolesGotten &&
        
        <div align="center" style={{marginTop:100}}>
          <Button onClick={onSelectRole} className={classes.shape} align="left" variant="contained" color="primary"
                  style={{width: 100, height: 30}}>Select Role </Button>
          
        </div>
        }
        
      </div>
      
    </div>
    
   )
  
}

function ChooseGroup(props) {
  
  const {context,callback} = props;
  const classes = useStyles();
  const [groups, setGroups] = React.useState([]);
  const [groupsGotten, setGroupsGotten] = React.useState(false);
  const [selectedGroup, setSelectedGroup] = React.useState("");
  const [selectedIndex, setSelectedIndex] = React.useState(-1);
  
  
  const onGetRoles = () => {
    
    const url = "http://localhost:4000/home/groups?home=" + context.homeId ;
    
    axios.get(url).then ((result) => {
      setGroups(result.data);
      setGroupsGotten(true);
      
    }).catch ((error) => {
      console.log("Error in getting Roles Data from blockchain  " + JSON.stringify(error));
    })
    
  };
  
  const handleListClick = (event, index) => {
    setSelectedGroup(event.target.textContent);
    setSelectedIndex(index);
  };
  
  
  const getGroups = () => {
    
    return groups.map((item,index) => {
      
      const body = JSON.parse(item.tx.tx);
      
      return (
        <ListItem
          button
          selected={selectedIndex === index}
          key={index}
          onClick={(event) => handleListClick(event, index)} >
          
          <ListItemText primary={body.name}  />
        </ListItem>
      
      )
    });
    
  };
  
  const onSelectRole = () => {
    callback(false,selectedGroup);
  };
  
  
  return (
    
    <div>
      
      <div align="center" style={{marginTop:30, width:800}}>
        
        {!groupsGotten &&
        <div align="center" style={{marginTop:100}}>
          <Button onClick={onGetRoles} className={classes.shape} align="left" variant="contained" color="primary"
                  style={{width: 300, height: 30}}>Select from The Groups </Button>
        
        </div>
        }
        
        
        {groupsGotten &&
        <div >
          
          <List component="nav" align="center" style={{align:"center",color:'white',background:"#0091EA"}}>
            {getGroups()}
          
          </List>
        </div>
        }
        
        {groupsGotten &&
        
        <div align="center" style={{marginTop:100}}>
          <Button onClick={onSelectRole} className={classes.shape} align="left" variant="contained" color="primary"
                  style={{width: 200, height: 30}}>Select Group </Button>
        
        </div>
        }
      
        
      
      </div>
    
    </div>
  
  )
}


 function SignAndSubmit(props) {
  
  const classes = useStyles();
  const {backCallback, signAndSubmitCallback} = props;
  
  const [password, setPassword] = React.useState('');
  
  const setValue = (e) => {
    setPassword(e.target.value);
  };
  
  
  return (
    
    <div>
      
      <div className={classes.margin}>
        <Grid container spacing={1} alignItems="flex-end">
          <Grid item>
            <Key/>
          </Grid>
          <Grid item>
            <TextField id="password" style ={{width:300}} value={password} onChange={setValue} label="Enter Password for the private key"/>
          </Grid>
        </Grid>
  
        <Grid container spacing={1} alignItems="flex-end" style={{marginTop:30}}>
  
          <Grid container justify = "center">
    
            <Grid container justify = "center">
      
              <Grid>
                <Button
                  onClick={backCallback}
                  className={classes.backButton}
                >
                  Back
                </Button>
        
                <Button variant="contained" color="primary" onClick={()=>signAndSubmitCallback(password)}>
                  Next
                </Button>
              </Grid>
            </Grid>
          </Grid>
          
        </Grid>
        
        
      </div>
    </div>
  
  );
}


 function Finalize(props) {
  
  const {completeCallback} = props;
  
  return (
    
    <div>
      
      <div style={{marginTop: 20, width:600, align:"center"}}>
        <Typography varivariant="h5" gutterBottom color="primary">
          Transaction Status {props.status.status}
        </Typography>
  
        <Grid container spacing={1} alignItems="flex-end">
    
          <Grid container justify = "center">
      
            <Grid container justify = "center">
        
              <Grid>
                <Button variant="contained" color="primary" onClick={completeCallback}>
                  Finish
                </Button>
              </Grid>
            </Grid>
          </Grid>
  
        </Grid>
      </div>
      
    </div>
  
  )
}