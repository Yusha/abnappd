import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import TextField from '@material-ui/core/TextField';
import PhoneIcon from '@material-ui/icons/Phone';
import EmailIcon from '@material-ui/icons/Email';
import BusinessIcon from '@material-ui/icons/Business';
import PostAddIcon from '@material-ui/icons/PostAdd';
import CountryIcon from '@material-ui/icons/AssistantPhoto';

import SupervisorAccountIcon from '@material-ui/icons/SupervisorAccount';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';

import axios from 'axios';
import { ec as EC } from 'elliptic';
import {GetHash} from '../ec/CryptoUtils';

import PouchDB from 'pouchdb-browser';

const myDB = new PouchDB('myDB');

const ecurve = new EC('secp256k1');

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
  },
  backButton: {
    marginRight: theme.spacing(1),
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(2),
    
  },
  margin: {
    margin: theme.spacing(1),
  },
}));


function getSteps() {
  return ['Claim Invite', 'Choose Org Id', 'Set Org Properties', 'Define an Admin','Generate Keys', 'Finalize'];
}

function getStepContent(stepIndex) {
  switch (stepIndex) {
    case 0:
      return <ClaimInvite/>;
    case 1:
      return <ChooseHome/>;
    case 2:
      return <ChooseHomeProps />;
    case 3:
      return <ChooseAdmin />;
    case 4:
      return <ChooseAdminKeys />;
    case 5:
      return <Finalize />;
    default:
      return 'Finished - Home Registration ';
  }
}


export default function RegisterHome(props) {
  
  const classes = useStyles();
  const [activeStep, setActiveStep] = React.useState(0);
  const [error, setError] = React.useState("");
  
  const steps = getSteps();
  
  const saveAndSubmit = ()=> {
    
    // Tx
    const tx = {
      //
      inviterHome: profileObject.inviterHome,
      inviterAccount: profileObject.inviterAccount,
      homeId: profileObject.homeId,
      businessName: profileObject.businessName,
      adminAccounts: profileObject.adminAccount,
      phones: profileObject.phones,
      emails: profileObject.emails,
      addresses: profileObject.addresses,
    };
  
    const sTx = JSON.stringify(tx);
    const hashOf = GetHash(sTx);
  
    const ec = new EC('secp256k1');
    const pk = ec.keyFromPrivate(authObject.privateKey,'hex');
  
    const signatureOf = pk.sign(hashOf);
  
    const chainTx = {
      _id:hashOf,
      action:"newHome",
      homeId: profileObject.homeId,
      hash: hashOf,
      user: profileObject.adminAccount.email,
      publicKey: profileObject.adminAccount.publicKey,
      inviterHome:profileObject.inviterHome,
      inviterAccount:profileObject.inviterAccount,
      seqId:1,
      signature: signatureOf,
      txs: sTx
    };
    
    
    const url = "http://127.0.0.1:4000/home/createHome";
    
     // send chain tx to the network
     axios.post(url, {
        tx: chainTx
     }).then((created) => {
       console.log(" New Home Request sent to the network..." + JSON.stringify(created));
       props.registerCallback();
      
     }).catch((error) => {
       console.log("Received error saving My access Request Objects :" + error);
    });
    
  };
  
  const  handleNext = async () => {
    
    
    
    if (activeStep === 0) {
      
      let url = 'http://localhost:4000/homeInvite/exists';
      
      const secretHash = GetHash(profileObject.inviteSecret);
      
      
      let payload = {
        inviterHome:profileObject.inviterHome,
        inviterAccount:profileObject.inviterAccount,
        invitee:profileObject.inviteId,
        inviteSecret:secretHash
      };
      
      let exists = await axios.post(url,payload);
        console.log("Network Invite exists  " + exists.data.exists);
      
      if (!exists.data.exists) {
        setError("Invitation does not exist");
      } else {
        setActiveStep(prevActiveStep => prevActiveStep + 1);
      }
    } else if (activeStep === 1) {
      let url = 'http://localhost:4000/home/home?'+ profileObject.homeId;
    
      let isHome = await axios.get(url);
      console.log("Home is  " + JSON.stringify(isHome));
    
      if (!isHome.data.length === 0 ) {
        setError("Home is taken. Please choose different home Id");
        console.log(error);
      
      } else {
        setActiveStep(prevActiveStep => prevActiveStep + 1);
      }
    }
    else {
      setActiveStep(prevActiveStep => prevActiveStep + 1);
    }
  };
  
  const handleBack = () => {
    setActiveStep(prevActiveStep => prevActiveStep - 1);
  };
  
  const handleReset = () => {
    setActiveStep(0);
  };
  
  return (
    
    <div>
      
      <div align="center" style ={{marginTop:50}}>
        <Typography variant="h5" color="primary"> Register an Organization Home</Typography>
      </div>
      
      <Grid className={classes.root} style ={{marginTop:50}}>
      
      
      
      <Stepper activeStep={activeStep} alternativeLabel>
        {steps.map(label => (
          <Step key={label}>
            <StepLabel>{label}</StepLabel>
          </Step>
        ))}
      </Stepper>
  
      <Grid container justify = "center">
        <div className={classes.instructions}>{getStepContent(activeStep)}</div>
        
      </Grid>
  
      <Grid container justify = "center">
        <Typography className={classes.instructions}>
        
        </Typography>
  
      </Grid>
      <Divider/>
      
      
      <Grid container justify = "center">
        {activeStep === steps.length ? (
          <Grid container justify = "center">
            <Typography className={classes.instructions}>All steps completed</Typography>
            <Button onClick={handleReset}>Reset</Button>
          </Grid>
        ) : (
          <Grid container justify = "center">
           
            <Grid>
              <Button
                disabled={activeStep === 0}
                onClick={handleBack}
                className={classes.backButton}
              >
                Back
              </Button>
  
              {activeStep === steps.length-1 ? (
                  <Button variant="contained" color="primary" onClick={saveAndSubmit}>
                    Save and Submit to network
                  </Button>
                )
                :
    
                (
                  <Button variant="contained" color="primary" onClick={handleNext}>
                    {'Next'}
                  </Button>
                )
              }
              
              
            </Grid>
          </Grid>
        )}
      </Grid>
    </Grid>
    
    </div>
  );
}

let profileObject = {
  inviteId:"",
  inviteSecret:"",
  homeId:"",
  businessName:"",
  phones:{name:"main",number:""},
  emails:{name:"main",email:""},
  addresses:{name:"main",address:"",country:""},
  adminAccount:{name:"",phone:"", email:"",publicKey:""}
  
};

export function ClaimInvite(props) {
  
  const classes = useStyles();
  const [inviteId, setInviteId] = React.useState('');
  const [inviteSecret, setInviteSecret] = React.useState("");
  const [inviterHome, setInviterHome] = React.useState("");
  const [inviterAccount, setInviterAccount] = React.useState("");
  
  
  const setValue = (e)=> {
    if (e.target.id === "inviteId") {
      profileObject.inviteId = e.target.value;
      setInviteId(e.target.value);
    } else if (e.target.id === "inviteSecret") {
      profileObject.inviteSecret = e.target.value;
      setInviteSecret(e.target.value);
    } else if (e.target.id === "inviterHome") {
      profileObject.inviterHome = e.target.value;
      setInviterHome(e.target.value);
    }else if (e.target.id === "inviterAccount") {
      profileObject.inviterAccount = e.target.value;
      setInviterAccount(e.target.value);
    }
  };
  
  return (
    
    <div>
      
      <form className={classes.root} noValidate autoComplete="off">
        <TextField id="inviteId" value={inviteId} onChange={setValue} label="Invitee email/phone" />
      </form>
  
      <form className={classes.root} noValidate autoComplete="off">
        <TextField id="inviteSecret" value={inviteSecret} onChange={setValue} label="Claim Secret" />
      </form>
  
      <form className={classes.root} noValidate autoComplete="off">
        <TextField id="inviterHome" value={inviterHome} onChange={setValue} label="Inviter Home" />
      </form>
      <form className={classes.root} noValidate autoComplete="off">
        <TextField id="inviterAccount" value={inviterAccount} onChange={setValue} label="Inviter Account" />
      </form>
    </div>
  
  );
  
}

export function ChooseHome(props) {
  
  const classes = useStyles();
  const [homeId, setHomeId] = React.useState('homeId');
  const setValue = (e)=> {
    profileObject.homeId = e.target.value;
    setHomeId(e.target.value);
  };
  
  return (
  
  <div>
    <form className={classes.root} noValidate autoComplete="off">
       <TextField id="homeId" value={homeId} onChange={setValue} label="Pick an Org Id" />
    </form>
  </div>
  
  );
  
}

export function ChooseHomeProps(props) {
  
  const classes = useStyles();
  const [bName, setBName] = React.useState('');
  const [phone, setPhone] = React.useState('');
  const [email, setEmail] = React.useState('');
  const [pAddress, setPAddress] = React.useState('');
  const [country, setCountry] = React.useState('');
  
  const setFieldValue = (e)=> {
    const fieldName = e.target.id;
    const fieldValue = e.target.value;
    
    if (fieldName === "bname") {
      profileObject.businessName = fieldValue;
      setBName(fieldValue);
    } else if (fieldName === "phone") {
      profileObject.phones.number = fieldValue;
      setPhone(fieldValue);
    } else if (fieldName === "email") {
      profileObject.emails.email = fieldValue;
      setEmail(fieldValue);
    } else if (fieldName ==="paddress") {
      profileObject.addresses.address = fieldValue;
      setPAddress(fieldValue);
    } else if (fieldName === "country") {
      profileObject.addresses.country = fieldValue;
      setCountry(fieldValue);
    } else {
      console.log("Unknown Field detected. Should not have happened");
    }
    
  }
  
  return (
    
    <div>
      <div className={classes.margin}>
        <Grid container spacing={1} alignItems="flex-end">
          <Grid item>
            <BusinessIcon/>
          </Grid>
          <Grid item>
            <TextField id="bname" value={bName} onChange={setFieldValue} label="Business Name"/>
          </Grid>
        </Grid>
      </div>
      <div className={classes.margin}>
        <Grid container spacing={1} alignItems="flex-end">
          <Grid item>
            <PhoneIcon/>
          </Grid>
          <Grid item>
            <TextField id="phone" value={phone} onChange={setFieldValue}label="Phone"/>
          </Grid>
        </Grid>
      </div>
      
      <div className={classes.margin}>
        <Grid container spacing={1} alignItems="flex-end">
          <Grid item>
            <EmailIcon/>
          </Grid>
          <Grid item>
            <TextField id="email" value={email} onChange={setFieldValue} label="Email"/>
          </Grid>
        </Grid>
      </div>
      
      <div className={classes.margin}>
        <Grid container spacing={1} alignItems="flex-end">
          <Grid item>
            <PostAddIcon/>
          </Grid>
          <Grid item>
            <TextField id="paddress" value={pAddress} onChange={setFieldValue} label="Postal Address"/>
          </Grid>
        </Grid>
      </div>
      
      
      <div className={classes.margin}>
        <Grid container spacing={1} alignItems="flex-end">
          <Grid item>
            <CountryIcon/>
          </Grid>
          <Grid item>
            <TextField id="country" value={country} onChange={setFieldValue} label="Country"/>
          </Grid>
        </Grid>
      </div>
    </div>
  
  );
}
  
export function ChooseAdmin(props) {
  
  const classes = useStyles();
  
  const [admin, setAdmin] = React.useState('');
  const [phone, setPhone] = React.useState('');
  const [email, setEmail] = React.useState('');
  
  const setFieldValue = (e) => {
    const fieldName = e.target.id;
    const fieldValue = e.target.value;
  
    if (fieldName === "admin") {
      profileObject.adminAccount.name = fieldValue;
      setAdmin(fieldValue);
    } else if (fieldName === "phone") {
      profileObject.adminAccount.phone = fieldValue;
      setPhone(fieldValue);
    } else if (fieldName === "email") {
      profileObject.adminAccount.email = fieldValue;
      setEmail(fieldValue);
    
    } else {
      console.log("Unknown Field detected. Should not have happened");
    }
  };
    
    return (
      
      <div>
        <div className={classes.margin}>
          <Grid container spacing={1} alignItems="flex-end">
            <Grid item>
              <SupervisorAccountIcon/>
            </Grid>
            <Grid item>
              <TextField id="admin" value={admin} onChange={setFieldValue} label="Admin Name"/>
            </Grid>
          </Grid>
        </div>
        <div className={classes.margin}>
          <Grid container spacing={1} alignItems="flex-end">
            <Grid item>
              <PhoneIcon/>
            </Grid>
            <Grid item>
              <TextField id="phone" value={phone} onChange={setFieldValue} label="Admin Phone"/>
            </Grid>
          </Grid>
        </div>
        
        <div className={classes.margin}>
          <Grid container spacing={1} alignItems="flex-end">
            <Grid item>
              <EmailIcon/>
            </Grid>
            <Grid item>
              <TextField id="email" value={email} onChange={setFieldValue} label="Admin Email"/>
            </Grid>
          </Grid>
        </div>
      
      </div>
    
    );
}


const authObject = {
  passPhrase: "",
  privateKey: "",
  
};

export function ChooseAdminKeys(props) {
    
    const classes = useStyles();
  
  const [passPhrase, setPassPhrase] = React.useState('');
  
  const setFieldValue = (e) => {
    const fieldName = e.target.id;
    const fieldValue = e.target.value;
    
    if (fieldName === "passphrase") {
      authObject.passPhrase = fieldValue;
      setPassPhrase(fieldValue);
      
    } else {
        console.log("Unknown Field detected. Should not have happened");
    }
  };
    
    return (
      
      <div>
        <div style={{marginTop: 20}}>
          
          
          <Grid container spacing={2} alignItems="flex-end">
            <Grid item>
              <SupervisorAccountIcon/>
            </Grid>
            <Grid item>
              <TextareaAutosize style={{width: 300}} aria-label="minimum height" rowsMin={3} id="passphrase" value={passPhrase} onChange={setFieldValue}
                                placeholder="PassPharase to generate cryptographic keys.
                          Only you will have access to the keys. The data you encrypt with the key can only be decrypted by your private key.
                          Please keep you passphare safe. "/>
            </Grid>
          </Grid>
        
        
        </div>
      
      
      </div>
    
    )
}

export function Finalize(props) {
  
  const classes = useStyles();
  
  const newkey = ecurve.genKeyPair({entropy:authObject.passPhrase});
  
  const adminPublicKey = newkey.getPublic().encode('hex');
  profileObject.adminAccount.publicKey = adminPublicKey;
  
  const adminPrivateKey = newkey.getPrivate("hex");
  
  authObject.privateKey = adminPrivateKey;
  
  
  
  return (
    
    <div>
  
      <div style={{marginTop: 20, width:600, align:"center"}}>
        <Typography varivariant="h5" gutterBottom color="primary">
          Please note down your passphrase and save private key. You can always regenerate your private key from passphrase.
          Your private key is needed to cryptographically sign and encrypt data.
          Your private key never leaves your computer. Your account is verified using your public key.
        </Typography>
      
      </div>
      
      <div style={{marginTop: 20}}>
  
        
          <Typography color='primary'>
            Passphrase
          </Typography>
        
        <Grid container spacing={2} alignItems="flex-end">
  
          <Grid item>
            <TextareaAutosize style={{width: 600}} aria-label="minimum height" rowsMin={3} id="passphrase" value={authObject.passPhrase}
                              label="Passphrase"/>
          </Grid>
        </Grid>
      
      
      </div>
  
      <div style={{marginTop: 20}}>
  
        <Typography color='primary'>
          Public Key
        </Typography>
        
        <Grid container spacing={2} alignItems="flex-end">
          <Grid item>
            <TextareaAutosize style={{width: 600}} aria-label="minimum height" rowsMin={3} id="publicKey" value={profileObject.adminAccount.publicKey}
                              label="Public Key"/>
          </Grid>
        </Grid>
      </div>
  
      <div style={{marginTop: 20}}>
    
        <Typography color='primary'>
          Private Key
        </Typography>
    
        <Grid container spacing={2} alignItems="flex-end">
          <Grid item>
            <TextareaAutosize style={{width: 600}} aria-label="minimum height" rowsMin={3} id="privateKey" value={adminPrivateKey}
                              label="Public Key"/>
          </Grid>
        </Grid>
      </div>
    </div>
  
  )
}