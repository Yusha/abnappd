
import React from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';

import Dialog from '@material-ui/core/Dialog';
import Typography from '@material-ui/core/Typography';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';


import {Editor} from 'react-draft-wysiwyg';
import { EditorState, convertFromRaw } from 'draft-js';


export default function BlockchainInfo(props) {
  
  const {tx,callback,open } = props;
  
  const data = JSON.parse(tx.tx.tx);
  
  const classes = dialogStyles();
  
  
  const handleClose = (action) => {
    callback(action);
  };
  
  
  
  
  const StyledTableCell = withStyles((theme) => ({
    head: {
      backgroundColor: "#0091EA",
      color: "white",
      fontSize:20
    },
    body: {
      fontSize: 14,
      maxWidth:300,
      wordWrap: 'break-word'
    },
  }))(TableCell);
  
  const StyledTableRow = withStyles((theme) => ({
    root: {
      '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
      },
    },
  }))(TableRow);
  
  
  
  
  return (
    <Dialog onClose={handleClose} open={open} fullWidth maxWidth="lg" classes={{paper: classes.dialog }} style={{marginTop:50, marginLeft:50}}>
      
      <div className={classes.content} style={{width:800,marginLeft:50}}>
        
        <div style={{marginTop:50}}>
          <Typography color="primary" variant="h5" align="center"> Blockchain Info </Typography>
        </div>
        
        <div style={{marginTop:50}}>
          
          <div>
            <Typography variant="h6"> {data.name} </Typography>
          </div>
          
        </div>
        
        <Divider/>
        
        
        <div className={classes.margin} style={{marginTop:30}}>
          
          
          <TableContainer component={Paper}>
            <Table className={classes.table} aria-label="customized table">
              <TableHead>
                <TableRow>
                  <StyledTableCell>Property Name</StyledTableCell>
                  <StyledTableCell align="right">Property Value </StyledTableCell>
                
                </TableRow>
              </TableHead>
              <TableBody>
                
                <StyledTableRow key="x">
                  <StyledTableCell component="th" scope="row">
                    Payment Instruction Object Name
                  </StyledTableCell>
                  <StyledTableCell align="right">{data.name}</StyledTableCell>
                </StyledTableRow>
  
                <StyledTableRow key="hash">
                  <StyledTableCell component="th" scope="row">
                    Hash
                  </StyledTableCell>
                  <StyledTableCell align="right">{tx.tx.hash}</StyledTableCell>
                </StyledTableRow>
  
                <StyledTableRow key="sig">
                  <StyledTableCell component="th" scope="row">
                    Signature
                  </StyledTableCell>
                  <StyledTableCell align="right">{JSON.stringify(tx.tx.signature)}</StyledTableCell>
                </StyledTableRow>
  
                <StyledTableRow key="block">
                  <StyledTableCell component="th" scope="row">
                    Block Id
                  </StyledTableCell>
                  <StyledTableCell align="right">{tx.blockId}</StyledTableCell>
                </StyledTableRow>
                
              </TableBody>
            </Table>
          </TableContainer>
  
          <Divider/>
          
          
          <Divider/>
          
          <div align="center" style={{marginTop:50,marginBottom:50}}>
            
            <Button onClick={handleClose} className={classes.shape} align="left" variant="contained" color="primary"
                    style={{width: 100, height: 30}}>Close</Button>
            
          </div>
        </div>
      </div>
    
    
    </Dialog>
  );
}


const dialogStyles = makeStyles({
  dialog: {
    position: 'absolute',
    left: 50,
    top: 50
  },
  table: {
    minWidth: 700,
  },
});

