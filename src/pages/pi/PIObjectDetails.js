
import React from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';

import Dialog from '@material-ui/core/Dialog';
import Typography from '@material-ui/core/Typography';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';


import {Editor} from 'react-draft-wysiwyg';
import { EditorState, convertFromRaw } from 'draft-js';


export default function PIObjectDetails(props) {
  
  const {data,callback,open } = props;
  const classes = dialogStyles();
  
  
  const handleClose = (action) => {
    callback(action);
  };
  
  
  
  
  const StyledTableCell = withStyles((theme) => ({
    head: {
      backgroundColor: "#0091EA",
      color: "white",
      fontSize:20
    },
    body: {
      fontSize: 14,
    },
  }))(TableCell);
  
  const StyledTableRow = withStyles((theme) => ({
    root: {
      '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
      },
    },
  }))(TableRow);
  
  
  const getProperties = () => {
    
    
    return data.properties.map((item,index) => {
      
      return (
        <StyledTableRow key={index}>
          <StyledTableCell >
            {item.name}
          </StyledTableCell>
          <StyledTableCell align="right">{item.value}</StyledTableCell>
        </StyledTableRow>
      )
    })
  };
  
  const getDescription = () => {
  
    const editorState = EditorState.createWithContent(convertFromRaw(data.description.textObject));
  
    return (
    
      <div className={classes.margin} style={{marginTop:10,width:500}} >
        <Editor
          editorState={editorState}
          toolbarClassName="toolbarClassName"
          wrapperClassName="wrapperClassName"
          editorClassName="editorClassName"
          toolbarHidden
          readOnly
          editorStyle={{backgroundColor:"#FFFFFF",height:"auto"}}
        />
    
      </div>
  
  
  
    )
    
  };
  
  
  
  
  
  return (
    <Dialog onClose={handleClose} open={open} fullWidth maxWidth="lg" classes={{paper: classes.dialog }} style={{marginTop:50, marginLeft:50}}>
      
      <div className={classes.content} style={{width:800,marginLeft:50}}>
        
        <div style={{marginTop:50}}>
          <Typography color="primary" variant="h5" align="center"> {data.name} </Typography>
        </div>
        
        <div style={{marginTop:50}}>
          
          <div>
            <Typography variant="h6"> Payment Instruction Description </Typography>
          </div>
          
         <div>
           
           {getDescription()}
         
         </div>
         
         
        </div>
        
        <Divider/>
        
        
        <div className={classes.margin} style={{marginTop:30}}>
          
          <div>
            <div>
              <Typography variant="h6"> Payment Instruction Properties </Typography>
            </div>
          </div>
          
          <TableContainer component={Paper}>
            <Table className={classes.table} aria-label="customized table">
              <TableHead>
                <TableRow>
                  <StyledTableCell>Property Name</StyledTableCell>
                  <StyledTableCell align="right">PropertyValue </StyledTableCell>
                
                </TableRow>
              </TableHead>
              <TableBody>
                
                <StyledTableRow key="x">
                  <StyledTableCell component="th" scope="row">
                    Payment Instruction Object Name
                  </StyledTableCell>
                  <StyledTableCell align="right">{data.name}</StyledTableCell>
                </StyledTableRow>
                
                {getProperties()}
              </TableBody>
            </Table>
          </TableContainer>
  
          <Divider/>
          
          <div style={{marginTop:30}}>
            <div>
              <Typography variant="h6"> Approving Account </Typography>
            </div>
  
            <div style={{marginTop: 20, marginBottom: 10}}>
    
              <TextField
      
                style={{width: 400, marginTop: 30, backgroundColor: "#FFFFFF", height: 20}}
                
                value={data.approver}
                id="approver"
                readOnly
              />
            </div>
            
          </div>
          <Divider/>
          
          <div align="center" style={{marginTop:50,marginBottom:50}}>
            
            <Button onClick={handleClose} className={classes.shape} align="left" variant="contained" color="primary"
                    style={{width: 100, height: 30}}>Close</Button>
            
          </div>
        </div>
      </div>
    
    
    </Dialog>
  );
}


const dialogStyles = makeStyles({
  dialog: {
    position: 'absolute',
    left: 50,
    top: 50
  },
  table: {
    minWidth: 700,
  },
});

