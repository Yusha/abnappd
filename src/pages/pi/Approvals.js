
import React, { useEffect,useStates } from 'react';
import { makeStyles } from '@material-ui/core/styles';

import dateFormat from 'dateformat';



import MaterialTable from 'material-table';
import { forwardRef } from 'react';



import axios from 'axios';

import MuiAlert from '@material-ui/lab/Alert';
import PIObjectDetails from '../pi/PIObjectDetails';
import ApprovalDialog from "./ApprovalDialog";
import tableIcons from '../common/IconDef';


export default function Approvals(props) {
  
  const { context } = props;
  
  const classes = tableTheme();
  
  const [approved, setApproved] = React.useState(""); // flag to avoid infinite loop in useEffect
  const [tableData, setTableData] = React.useState([]);
  const [rowSelected, setRowSelected] = React.useState(-1);
  
  const [detailsDialog, setDetailsDialog] = React.useState(false);
  const [approveDialog, setApproveDialog] = React.useState(false);
  
  const [piData, setPiData] = React.useState({});
  const [oData, setOdata] = React.useState({});
  
  
  
  useEffect(()=> {
  
    const url = "http://localhost:4000/PI/task/get?homeId=" + context.homeId + "&approver=" + context.user;
    
    axios.get(url).then ((result) => {
      setTableData(result.data);
      
    }).catch ((error) => {
      console.log("Error in getting User Tasks in action center  " + JSON.stringify(error));
    })
    
  },[approved]);
  
  
  
  
  
  const getDataSet = () => {
    const data = tableData.map((row, index) => {
  
      let formatDate = new Date(row.timeStamp);
      let newd = dateFormat(formatDate, "DDD, mmmm dS, yyyy, h:MM:ss TT");
      
      return {rAccount: row.accountId, rType: row.action, timeStamp:newd,approved: row.approved};
    });
    return data;
  };
  
  const showDetail = (e,row) => {
    const rowSelected = tableData[row.tableData.id];
    const id = rowSelected.appReference;
    
    
    const url = "http://localhost:4000/PI/PIO?id=" + id;
    
    axios.get(url).then((result) => {
      if (result) {
        const output = JSON.parse(result.data.tx.tx);
        setPiData(output);
        setOdata(result.data.tx);
        setDetailsDialog(true)
      }
    });
  };
  
  
  
  const detailsDialogCallback = () => {
    setDetailsDialog(false);
  };
  
  const approve = (e,row) => {
  
    const rs = tableData[row.tableData.id];
    setRowSelected(rs);
    const id = rs.appReference;
    const url = "http://localhost:4000/PI/PIO?id=" + id;
    
    axios.get(url).then((result) => {
      if (result) {
        const output = JSON.parse(result.data.tx.tx);
        setPiData(output);
        setOdata(result.data.tx);
        setApproveDialog(true);
      }
    });
  };
  
  
  
  const approveDialogCallback = ()=> {
    setApproveDialog(false);
  };
  
  return (
    
    <div className={classes.table}>
      <MaterialTable
        title="Payment Instructions Approval Tasks"
        icons={tableIcons}
        
        
        localization={{
          header: {
            actions: 'Actions'
          },
        }}
        
        columns={[
          { title: 'Requesing Account', field: 'rAccount' },
          { title: 'Request Type', field: 'rType' },
          { title: 'Date and Time', field: 'timeStamp' },
          { title: 'Approved', field: 'approved' },
        
        ]}
        data={getDataSet()}

        actions={[
          {
            icon: tableIcons.Detail,
            tooltip: 'Details',
            onClick: (event, rowData) => {showDetail(event,rowData)}
          },
          {
            icon: tableIcons.Check,
            tooltip: 'Approve',
            onClick: (event, rowData) => {approve(event,rowData)}
          }
        ]}
        
        options={{
          
          padding:"dense",
          headerStyle: {
            color: 'white',
            background:'#0091EA',
            fontSize:20,
            fontFamily:"Roboto"
          },
          rowStyle: {
            color: 'black'
          }
        }}
        
      />
      
      {detailsDialog &&
        <PIObjectDetails data = {piData} open = {detailsDialog} callback = {detailsDialogCallback}/>
      }
      
      { approveDialog &&
        <ApprovalDialog data = {oData} open = {approveDialog} taskId={rowSelected._id} callback = {approveDialogCallback} context = {context}/>
      }
    
    </div>
  )
}

// TOdo: Make it as common component across all tables



const tableTheme = makeStyles(theme => ({
  table: {
    '& tbody>.MuiTableRow-root:hover': {
      background: '#EEE',
    }
  },
  body: {
    textColor:"primary"
  }
}));
function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}
