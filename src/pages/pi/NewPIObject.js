import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import TextField from '@material-ui/core/TextField';


import CountryIcon from '@material-ui/icons/AssistantPhoto';

import Add from '@material-ui/icons/Add';
import Delete from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';

import {Editor} from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { EditorState, convertToRaw } from 'draft-js';


import axios from 'axios';

import {GetHash, GetSignature} from '../../ec/CryptoUtils';



const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
  },
  backButton: {
    marginRight: theme.spacing(1),
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(2),
    
  },
  button: {
    margin: theme.spacing(1),
  },
  margin: {
    margin: theme.spacing(1),
  },
  input: {
    display: 'none',
  },
}));


function getSteps() {
  return ['Name', "Details",'Properties',"Approver",'Finalize'];
}


export default function NewPIObject(props) {
  
  const classes = useStyles();
  
  const {callback, context } = props;
  
  const [activeStep, setActiveStep] = React.useState(0);
  
  const [name,setName] = React.useState("");
  const [description, setDescription] = React.useState({});
  
  const [properties, setProperties] = React.useState({});
  
  const [approver, setApprover] = React.useState("");
  
  const [status, setStatus] = React.useState({});
  
  const [finished, setFinished] = React.useState(false);
  
  
  
  const steps = getSteps();
  
  const submitCallback = (pass)=> {
    
    const password = pass;
    
    
    
    const baseObject = {
      name:name,
      description:description,
      properties:properties,
      approver:approver,
    };
    
    
    const sTx = JSON.stringify(baseObject);
    
    const hashOf = GetHash(sTx);
    
    const signature = GetSignature(props.context.encryptedPK,password,hashOf);
    
    const chainTx = {
      action:"newPI",
      homeId: context.homeId,
      user: context.user,
      approver:approver,
      publicKey:context.publicKey,
      hash:hashOf,
      signature:signature,
      tx:sTx
    };
    const url = "http://127.0.0.1:4000/PI/add";
    
    axios.post(url,chainTx).then (result => {
      setFinished(true);
      setStatus({status:"PI Tx succeeded", id:hashOf});
      
    }).catch (error => {
      console.log("Error in sending the PI tx to the network... " + JSON.stringify(error));
      setFinished(true);
      setStatus({status:"Transaction Failed", id:hashOf});
      
    });
    
  };
  
  const  handleNext = () => {
    
    setActiveStep(prevActiveStep => prevActiveStep + 1);
  };
  
  const handleBack = () => {
    setActiveStep(prevActiveStep => prevActiveStep - 1);
  };
  
  
  const saveNameCallback = (name) => {
    setName(name);
    handleNext();
  };
  
  const savePropertiesCallback = (values) => {
    setProperties(values);
    handleNext();
  };
  
  const saveDescriptionCallback = (values) => {
    setDescription(values);
    handleNext();
  };
  
  const saveApproverCallback = (value) => {
    setApprover(value);
    handleNext();
    
  };
  
  
  return (
    <Grid className={classes.root}>
      <Stepper activeStep={activeStep} alternativeLabel>
        {steps.map(label => (
          <Step key={label}>
            <StepLabel>{label}</StepLabel>
          </Step>
        ))}
      </Stepper>
  
      
        
        <Grid container justify="center">
          <div className={classes.instructions}>
      
          {activeStep === 0 &&
          <ChooseName saveNameCallback={saveNameCallback}/>
          }
      
          {activeStep === 1 &&
          <ChooseDescription backCallback={handleBack} callback={saveDescriptionCallback}/>
        
          }
      
          {activeStep === 2 &&
          <ChooseProperties backCallback={handleBack} saveMetadataCallback={savePropertiesCallback}/>
        
          }
      
          {activeStep === 3 &&
          <Approver backCallback={handleBack} callback={saveApproverCallback}/>
        
          }
      
      
          {activeStep === 4 &&
          <Finalize backCallback={handleBack} submitCallback={submitCallback}/>
        
          }
    
    
        </div>
        </Grid>
    
      
      
      
      <Divider/>
      
      {finished &&
      <Grid container justify="center" style={{marginTop:50}}>
        <Typography className={classes.instructions} color="red">
          Status of the Transaction is  " {status.status}
        
        </Typography>
        
        <Button onClick = {callback} variant="contained" color="secondary"> Complete </Button>
      
      
      </Grid>
      }
    
    </Grid>
  );
}


function ChooseName(props) {
  
  const {saveNameCallback} = props;
  const classes = useStyles();
  const [name, setName] = React.useState('');
  const [description, setDescription] = React.useState('');
  
  
  const setFieldValue = (e)=> {
    const id = e.target.id;
    const value = e.target.value;
    
    if (id === "name") {
      setName(value);
    }
    
  };
  
  return (
    
    <div>
      <div className={classes.margin}>
        <Grid container spacing={1} alignItems="flex-end">
          
          <Grid item>
            <TextField id="name" value={name} onChange={setFieldValue} style={{width:300}} label="Name"/>
          </Grid>
        </Grid>
      </div>
      
      
      
      <div className={classes.margin} style={{marginTop:30}}>
        <Grid container spacing={1} alignItems="flex-end">
          
          <Grid container justify = "center">
            
            <Grid container justify = "center">
              
              <Grid>
                <Button
                  disabled
                  className={classes.backButton}
                >
                  Back
                </Button>
                
                <Button variant="contained" color="primary" onClick={()=>saveNameCallback(name)}>
                  Next
                </Button>
              </Grid>
            </Grid>
          
          </Grid>
        
        
        </Grid>
      </div>
    
    </div>
  
  );
}

function ChooseDescription(props) {
  
  const {callback, backCallback} = props;
  const classes = useStyles();
  
  const [editorState, setEditorState] = React.useState(EditorState.createEmpty());
  
  
  const submitValues = () => {
      const raw = convertToRaw(editorState.getCurrentContent());
      callback({textObject:raw});
    
  };
  
  const onEditorStateChange = (s)=> {
    setEditorState(s);
  };
  
  
  return (
    
    <div>
      
      
      <div className={classes.margin} style={{marginTop:30}}>
        <Editor
          editorState={editorState}
          toolbarClassName="toolbarClassName"
          wrapperClassName="wrapperClassName"
          editorClassName="editorClassName"
          placeholder = "Payment Instruction notes"
          onEditorStateChange={onEditorStateChange}
          editorStyle={{backgroundColor:"#FFFFFF",height:200}}
        />
      </div>
      
      
      
      <div className={classes.margin} style={{marginTop:30}}>
        <Grid container spacing={1} alignItems="flex-end">
          
          <Grid container justify = "center">
            
            <Grid container justify = "center">
              
              <Grid>
                <Button variant="contained" color="primary" onClick={backCallback}
                        className={classes.backButton}
                >
                  Back
                </Button>
                
                <Button variant="contained" color="primary" onClick={submitValues}>
                  Next
                </Button>
              </Grid>
            </Grid>
          
          </Grid>
        
        
        </Grid>
      </div>
    
    </div>
  
  );
}

function ChooseProperties(props) {
  
  const {saveMetadataCallback, backCallback} = props;
  const classes = useStyles();
  
  const [metadataSet, setMetaDataSet] = React.useState([]);
  
  const [name, setName] = React.useState("");
  const [value, setValue] = React.useState("");
  
  const setMetadataItem = () => {
    setMetaDataSet([...metadataSet,{name:name,value:value}]);
    setName("");
    setValue("");
  };
  
  const deleteItem = (e)=> {
    const id = e.target.id;
    metadataSet.splice(id,1);
    setMetaDataSet([...metadataSet]);
    
  };
  
  const metadataList = () => {
    return metadataSet.map((item,index) => {
      
      return (
        <Grid container spacing={1} alignItems="flex-end" key={index}>
          <Grid item style={{marginLeft:10}}>
            <TextField id="name" value={item.name} readOnly />
          </Grid>
          <Grid item style={{marginLeft:10, width:400}}>
            <TextField id="value" value={item.value} multiline readOnly />
          </Grid>
          <Grid item style={{marginLeft:10}}>
            
            <IconButton size="medium" color="primary" id={index} onClick={deleteItem}>
              <Delete/>
            </IconButton>
          
          
          </Grid>
        
        </Grid>
      
      )
      
    })
    
  };
  
  
  
  
  const setFieldValue = (e)=> {
    const id = e.target.id;
    const value = e.target.value;
    
    if (id === "name") {
      setName(value);
    } else if (id === "value") {
      setValue(value);
    }
    
  };
  
  return (
    
    <div>
      <div className={classes.margin}>
        
        <div>
          
          { metadataList() }
        
        </div>
        
        <div>
          <Grid container spacing={1} alignItems="flex-end">
            <Grid item style={{marginLef:10}}>
              <TextField id="name" value={name} onChange={setFieldValue} style={{width:200}} label="Instruction Property"/>
            </Grid>
            <Grid item style={{marginLeft:10,width:400}}>
              <TextField id="value" value={value} multiline style={{width:400}} onChange={setFieldValue} label="Property Value"/>
            </Grid>
            <Grid item style={{marginLef:10}}>
              <Button
                variant="contained"
                color="secondary"
                className={classes.button}
                size="small"
                startIcon={<Add />}
                onClick = {setMetadataItem}
              
              >
                Add
              </Button>
            </Grid>
          
          </Grid>
        </div>
      
      
      </div>
      
      
      
      
      <div className={classes.margin} style={{marginTop:30}}>
        <Grid container spacing={1} alignItems="flex-end">
          
          <Grid container justify = "center">
            
            <Grid container justify = "center">
              
              <Grid>
                <Button
                  className={classes.backButton}
                  onClick={backCallback}
                >
                  Back
                </Button>
                
                <Button variant="contained" color="primary" onClick={()=>saveMetadataCallback(metadataSet)}>
                  Next
                </Button>
              </Grid>
            </Grid>
          
          </Grid>
        
        
        </Grid>
      </div>
    
    </div>
  
  );
}

function Approver(props) {
  
  const {callback, backCallback} = props;
  const classes = useStyles();
  const [approver, setApprover] = React.useState('');
  
  
  return (
    
    <div>
      <div className={classes.margin}>
        <Grid container spacing={1} alignItems="flex-end">
          
          <Grid item>
            <TextField id="name" value={approver} onChange={(e)=>setApprover(e.target.value)} style={{width:400}} label="Approving Account"/>
          </Grid>
        </Grid>
      </div>
      
      
      
      <div className={classes.margin} style={{marginTop:30}}>
        <Grid container spacing={1} alignItems="flex-end">
          
          <Grid container justify = "center">
            
            <Grid container justify = "center">
              
              <Grid>
                <Button
                  onClick={backCallback}
                  className={classes.backButton}
                >
                  Back
                </Button>
                
                <Button variant="contained" color="primary" onClick={()=>callback(approver)}>
                  Next
                </Button>
              </Grid>
            </Grid>
          
          </Grid>
        
        
        </Grid>
      </div>
    
    </div>
  
  );
}

function Finalize(props) {
  
  const classes = useStyles();
  
  const {backCallback, submitCallback} = props;
  
  const [password, setPassword] = React.useState('');
  
  const setValue = (e) => {
    setPassword(e.target.value);
    
  };
  
  
  return (
    
    <div>
      
      
      <div className={classes.margin}>
        <Grid container spacing={1} alignItems="flex-end">
          <Grid item>
            <CountryIcon/>
          </Grid>
          <Grid item>
            <TextField id="country" multiline value={password} onChange={setValue} style={{width:400}} label="Enter key password..."/>
          </Grid>
        </Grid>
        
        <Grid container spacing={1} alignItems="flex-end" style = {{marginTop:30}}>
          
          <Grid container justify = "center">
            
            <Grid container justify = "center">
              
              <Grid>
                <Button
                  onClick={backCallback}
                  className={classes.backButton}
                >
                  Back
                </Button>
                
                <Button variant="contained" color="primary" onClick={()=>submitCallback(password)}>
                  Sign and Submit
                </Button>
              </Grid>
            </Grid>
          </Grid>
        
        </Grid>
      
      </div>
    </div>
  
  );
}


