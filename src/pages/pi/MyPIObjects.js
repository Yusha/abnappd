
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';


import MaterialTable, {MTableToolbar} from 'material-table'
import { forwardRef } from 'react';

import PIObjectDetails from './PIObjectDetails';


import axios from 'axios';
import BlockchainInfo from "./BlockchainInfo";
import tableIcons from '../common/IconDef';



const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    marginTop:20,
  }
  
  
}));

export default function MyBizObjects(props) {
  
  const {context} = props;
  
  
  const classes = useStyles();
  
  return (
    <div className={classes.root}>
      
      <main className={classes.content}>
        
        <div style={{marginTop:20,marginLeft:20,marginRight:20}}>
          
          
          <PIObjectsTable context = {context} />
          
          
        </div>
        
       
      </main>
    </div>
  );
}


const tableTheme = makeStyles(theme => ({
  table: {
    '& tbody>.MuiTableRow-root:hover': {
      background: '#EEE',
    }
  },
}));


 function PIObjectsTable(props) {
  
  const {context} = props;
  
  
  const classes = tableTheme();
  
  const [bcData,setBCData] = React.useState([]);
  const [currentObject,setCurrentObject] = React.useState({});
  const [showDetailsDialog, setShowDetailsDialog] = React.useState(false);
  
  const [showInfoDialog, setShowInfoDialog] = React.useState(false);
  const [showDeactivateDialog, setShowDeactivateDialog] = React.useState(false);
  
  
  const [currentTx, setCurrentTx] = React.useState({});
  
  
  
  React.useEffect(()=> {
    
    const url = "http://localhost:4000/PI/get?homeId=" + context.homeId + "&accountId=" + context.user;
    axios.get(url).then ((result) => {
      setBCData(result.data);
      
    }).catch ((error) => {
      console.log("Error in getting Accounts   " + JSON.stringify(error));
    })
    
  },[]);
  
  const getTableData = ()=> {
    
    return bcData.map((row,index) => {
      
      let otx = JSON.parse(row.tx.tx);
      
      return {name:otx.name,user:row.tx.user, approver:otx.approver,status:"active"};
    })
  };
  
  
  
  const showDetailsCallback = (data) => {
    setShowDetailsDialog(false);
    
  };
  
  const infoCallback = (data) => {
    setShowInfoDialog(false);
  };
  
  const deactivateDialogCallback = (data) => {
    setShowDeactivateDialog(false);
  };
  
  
  
  const showObjectData = (row) => {
    
    const od = bcData[row.tableData.id];
    setCurrentObject(JSON.parse(od.tx.tx));
    setShowDetailsDialog(true);
  };
  
   const setBlockchainInfo = (row) => {
    
     const od = bcData[row.tableData.id];
     setCurrentTx(od);
     setShowInfoDialog(true);
   };
  
   const setDeactivateDialog = (row) => {
    
     const od = bcData[row.tableData.id];
     setCurrentTx(od);
     setShowDeactivateDialog(true);
   };
  
   
  return (
    
    <div className={classes.table}>
      <MaterialTable
        title="Payment Instructions"
        icons={tableIcons}
        
        
        localization={{
          
          header: {
            actions: 'Actions'
          },
          
        }}
        
        
        
        columns={[
          { title: 'Name', field: 'name' },
          { title: 'User', field: 'user' },
          { title: 'Approver', field: 'approver' },
          { title: 'Status', field: 'status' }
          
        ]}
        data={getTableData()}
        actions={[
          {
            icon: tableIcons.Detail,
            tooltip: 'Details',
            onClick: (event, rowData) => {showObjectData(rowData)}
          },
          {
            icon: tableIcons.Info,
            tooltip: 'Blockchain info',
            onClick: (event, rowData) => {setBlockchainInfo(rowData)}
          },
          {
            icon: tableIcons.Deactivate,
            tooltip: 'retire',
            onClick: (event, rowData) => {setDeactivateDialog(rowData)}
          }
          
        ]}
        options={{
          
          padding:"dense",
          headerStyle: {
            backgroundColor: '#0091EA',
            color: '#FFF'
          }
          
        }}
        
        components={{
          Toolbar: props => (
            <div>
              <MTableToolbar {...props} />
            
            </div>
          ),
        }}
      />
      
      { showDetailsDialog &&
        
        <PIObjectDetails open={showDetailsDialog} callback={showDetailsCallback} data={currentObject} />
      }
  
      { showInfoDialog &&
  
        <BlockchainInfo open={showInfoDialog} callback={infoCallback} tx={currentTx} />
      
      }
    </div>
  )
}

