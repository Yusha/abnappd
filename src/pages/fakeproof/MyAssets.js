
import React, { useEffect,useStates } from 'react';
import { makeStyles } from '@material-ui/core/styles';


import MaterialTable from 'material-table';
import { forwardRef } from 'react';

import AssetDetails from './AssetDetails';
import tableIcons from '../common/IconDef';


import axios from 'axios';


export default  function MyAssets(props) {
  
  const { context } = props;
  
  const classes = tableTheme();
  
  const [tableData, setTableData] = React.useState([]);
  const [indexData, setIndexData] = React.useState({});
  const [openDetailsDialog, setOpenDetailsDialog] = React.useState(false);
  
  
  
  useEffect(()=> {
    
    const url = "http://localhost:4000/fakeproof/assets/get?homeId=" + context.homeId;
    
    axios.get(url).then ((result) => {
      setTableData(result.data);
      
    }).catch ((error) => {
      console.log("Error in getting Internal Pending Invitations data  " + JSON.stringify(error));
    })
    
  },[]);
  
  
  
  
  const getDataSet = () => {
    const data = tableData.map((row, index) => {
      
      return {name: row.tx.body.name, metadata: row.tx.body.description,date:row.tx.timeStamp,fileName:row.tx.body.fileName,fileType:row.tx.body.fileType};
    });
    return data;
  };
  
  const rowClicked = (e,row) => {
    
    setIndexData(tableData[row.tableData.id]);
    
    setOpenDetailsDialog(true);
    
  };
  
  const detailsDialogCallback = (f) => {
    setOpenDetailsDialog(false);
  };
  
  
  
  return (
    
    <div className={classes.table}>
      <MaterialTable
        title="All Asset Certifications"
        icons={tableIcons}
        
        
        localization={{
          header: {
            actions: 'blockchainInfo'
          },
        }}
        
        columns={[
          { title: 'Name', field: 'name' },
          { title: 'Meta Data', field: 'metadata' },
          { title: 'Created ', field: 'date' },
          { title: 'File Name ', field: 'fileName' },
          { title: 'File Type ', field: 'fileType' },
          
        ]}
        data={getDataSet()}
        
        actions={[
          rowData => ({
            icon: tableIcons.ViewEye,
            tooltip: 'View message',
            onClick: rowClicked
          })
        ]}
        
        options={{
          actionsColumnIndex: -1,
          padding:"dense",
          
          headerStyle: {
            color: 'white',
            background:'#0091EA',
            fontSize:20,
            fontFamily:"Roboto"
          },
          rowStyle: {
            color: 'black'
          }
          
        }}
        
        components={{
        
        }}
      />
  
      {openDetailsDialog &&
  
        <AssetDetails open={openDetailsDialog} context = {context} callback={detailsDialogCallback} data={indexData}/>
    
      }
      
    </div>
  )
}




const tableTheme = makeStyles(theme => ({
  table: {
    '& tbody>.MuiTableRow-root:hover': {
      background: '#EEE',
    }
  },
  body: {
    textColor:"primary"
  }
}));
