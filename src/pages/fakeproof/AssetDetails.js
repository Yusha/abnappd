import React from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';

import Dialog from '@material-ui/core/Dialog';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';

import Typography from '@material-ui/core/Typography';


import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';



export default function AssetDetails(props) {
  
  
  const { context, data,callback,open } = props;
  
  
  
  const classes = dialogStyles();
  
  const handleClose = (action) => {
    callback(action);
  };
  
  
  const StyledTableCell = withStyles((theme) => ({
    head: {
      backgroundColor: "#0091EA",
      color: "white",
      fontSize:20
    },
    body: {
      fontSize: 16,
      wordWrap:"break-word",
      maxWidth:200
    },
  }))(TableCell);
  
  const StyledTableRow = withStyles((theme) => ({
    root: {
      '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
      },
    },
  }))(TableRow);
  
  
  
  
  return (
    <Dialog onClose={handleClose} open={open} fullWidth maxWidth="lg" classes={{paper: classes.dialog }} >
      
      <div className={classes.content} style={{width:800,align:"center",marginLeft:50}}>
        
        <div className={classes.margin}>
          <Typography color="primary" variant="h5" align="center"> Asset Certification Details</Typography>
        </div>
        
        
        
        <div className={classes.margin} align="center" style={{marginTop:30}}>
          
          <TableContainer component={Paper}>
            <Table className={classes.table} aria-label="customized table">
              <TableHead>
                <TableRow>
                  <StyledTableCell>Asset Property </StyledTableCell>
                  <StyledTableCell align="right" >Property Value </StyledTableCell>
                
                </TableRow>
              </TableHead>
              <TableBody>
                
                <StyledTableRow key="1">
                  <StyledTableCell component="th" scope="row">
                    Name
                  </StyledTableCell>
                  <StyledTableCell align="right">{data.tx.body.name}</StyledTableCell>
                </StyledTableRow>
                
                <StyledTableRow key="2">
                  <StyledTableCell >
                    Meta Data
                  </StyledTableCell>
                  <StyledTableCell align="right" >{data.tx.body.description}</StyledTableCell>
                </StyledTableRow>
  
                <StyledTableRow key="3">
                  <StyledTableCell >
                    Hash
                  </StyledTableCell>
                  <StyledTableCell align="right">{data.tx.hash}</StyledTableCell>
                </StyledTableRow>
  
                <StyledTableRow key="4">
                  <StyledTableCell >
                    Asset Hash
                  </StyledTableCell>
                  <StyledTableCell align="right">{data.tx.body.assetHash}</StyledTableCell>
                </StyledTableRow>
                
                <StyledTableRow key="5">
                  <StyledTableCell >
                    Certifier Signature
                  </StyledTableCell>
                  <StyledTableCell align="right">{JSON.stringify(data.tx.body.assetSign)}</StyledTableCell>
                </StyledTableRow>
  
                <StyledTableRow key="6">
                  <StyledTableCell >
                    BlockId
                  </StyledTableCell>
                  <StyledTableCell align="right">{data.blockId}</StyledTableCell>
                </StyledTableRow>
  
                <StyledTableRow key="7">
                  <StyledTableCell >
                    Created Date
                  </StyledTableCell>
                  <StyledTableCell align="right">{data.tx.timeStamp}</StyledTableCell>
                </StyledTableRow>
  
                <StyledTableRow key="8">
                  <StyledTableCell >
                    Certifier Home Id
                  </StyledTableCell>
                  <StyledTableCell align="right">{data.tx.homeId}</StyledTableCell>
                </StyledTableRow>
                
              </TableBody>
            </Table>
          </TableContainer>
        
        
        </div>
  
        <div align="center" style={{marginTop:20, marginBottom:20}}>
          <Divider />
        </div>
  
  
  
        
        <div align="center" style={{marginTop: 20, marginBottom: 30}}>
    
          <Button onClick={handleClose} className={classes.shape} align="left" variant="contained" color="secondary"
                  style={{marginLeft:20, width: 100, height: 30}}>Close </Button>
        </div>
        
        
  
      </div>
    
    
    </Dialog>
  );
}

const dialogStyles = makeStyles({
  dialog: {
    position: 'absolute',
    left: 10,
    top: 50
  },
  table: {
    minWidth: 700,
  },
});