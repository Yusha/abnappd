import React from 'react';


import Dialog from '@material-ui/core/Dialog';

import InputAdornment from '@material-ui/core/InputAdornment';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';



import axios from 'axios';

import { makeStyles, withStyles } from '@material-ui/core/styles';
import {GetSignature,GetHash} from '../../ec/CryptoUtils';


const dialogStyles = makeStyles({
  dialog: {
    position: 'absolute',
    left: 20,
    top: 70
  },
  input: {
    display: 'none',
  },
});

export default  function NewAsset(props) {
  
  
  const { context, dialogCallback, open } = props;
  
  const classes = dialogStyles();
  
  
  const [name, setName] = React.useState("");
  const [description, setDescription] = React.useState("");
  
  const [fileObject,setFileObject] = React.useState("");
  
  
  const [password, setPassword] = React.useState("");
  
  const [showCertifyButton, setShowCertifyButton] = React.useState(false);
  
  const [showSubmitButtons, setShowSubmitButtons] = React.useState(false);
  
  const [isUploaded, setIsUploaded] = React.useState(false);
  
  const [hash, setHash] = React.useState("");
  const [sig, setSig] = React.useState("");
  
  
  
  const handleClose = (type, action) => {
    dialogCallback(type, action);
  };
  
  
  
  const onCertify = () => {
    
    setShowSubmitButtons(true);
    setShowCertifyButton(false);
  
    const hashOf = GetHash(fileObject.base64);
    setHash(hashOf);
  
    const signature = GetSignature(context.encryptedPK,password,hashOf);
    
    setSig(signature);
    
  };
  
  const cancelThis = (e) => {
    dialogCallback("cancel", "");
  };
  
  const setFieldValue = (e) => {
    
    const id = e.target.id;
    const value = e.target.value;
    
    if (id === "name") {
      setName(value);
    } else if (id ==="description") {
      setDescription(value);
    }
    else if (id === "password") {
      setPassword(value);
    }
    else {
      console.log("Wrong id in the form field.. should not hapen...");
    }
  };
  
  const onSubmit = () => {
    
    
    
    const body = {
      name: name,
      description:description,
      fileName: fileObject.name,
      fileType: fileObject.type,
      fileSize: fileObject.size,
      assetHash: hash,
      assetSign : sig,
    };
    
    
    
    const txHash = GetHash(JSON.stringify(body));
    
    
    const txSig = GetSignature(context.encryptedPK,password,txHash);
    
    const tx = {
      
      homeId: context.homeId,
      homeAccount:context.user,
      publicKey:context.publicKey,
      timeStamp:new Date(),
      hash:txHash,
      signature:txSig,
      body: body
    };
    
    const url = "http://localhost:4000/fakeproof/assets/add";
    
    axios.post(url,tx).then ((result) => {
      
      if (result.data.success) {
        // message posted successfully
        handleClose("submit",{cancel:false,success:true});
      } else {
        handleClose("submit", {cancel:false,success:false, error:result.error});
      }
      
    }).catch ((error) => {
      console.log("Error in posting a message");
      
    })
  };
  
  const handleUpload = (e) => {
    
    let file = e.target.files[0];
    
    let reader = new FileReader();
    
    // Convert the file to base64 text
    reader.readAsDataURL(file);
    
    reader.onload = () => {
      
      let fileInfo = {
        name: file.name,
        type: file.type,
        size: Math.round(file.size / 1000) + ' kB',
        base64: reader.result,
      };
      setFileObject(fileInfo);
      setShowCertifyButton(true);
      setIsUploaded(true);
      
    };
  };
  
  const StyledTableCell = withStyles((theme) => ({
    head: {
      backgroundColor: "#0091EA",
      color: "white",
      fontSize:20,
    },
    body: {
      fontSize: 14,
    },
  }))(TableCell);
  
  const StyledTableRow = withStyles((theme) => ({
    root: {
      '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
      },
    },
  }))(TableRow);
  
  
  
  const getInfoTable = () => {
    
    return (
  
      <div className={classes.margin} align="center" style={{marginTop:30}}>
    
        <TableContainer component={Paper}>
          <Table className={classes.table} aria-label="customized table">
            <TableHead>
              <TableRow>
                <StyledTableCell>Asset property</StyledTableCell>
                <StyledTableCell align="right">Property Value </StyledTableCell>
          
              </TableRow>
            </TableHead>
            <TableBody>
          
              <StyledTableRow key="1">
                <StyledTableCell component="th" scope="row">
                  Asset Name
                </StyledTableCell>
                <StyledTableCell align="right">{name}</StyledTableCell>
              </StyledTableRow>
          
              <StyledTableRow key="2">
                <StyledTableCell >
                  Object Description
                </StyledTableCell>
                <StyledTableCell align="right">{description}</StyledTableCell>
              </StyledTableRow>
              <StyledTableRow key="3">
                <StyledTableCell >
                  File Name
                </StyledTableCell>
                <StyledTableCell align="right">{fileObject.name}</StyledTableCell>
              </StyledTableRow>
              <StyledTableRow key="4">
                <StyledTableCell >
                  File Type
                </StyledTableCell>
                <StyledTableCell align="right">{fileObject.type}</StyledTableCell>
              </StyledTableRow>
  
              <StyledTableRow key="5">
                <StyledTableCell >
                  File Type
                </StyledTableCell>
                <StyledTableCell align="right" >{fileObject.size}</StyledTableCell>
              </StyledTableRow>
              
              
  
              <StyledTableRow key="6">
                <StyledTableCell >
                  Cryptographic Hash
                </StyledTableCell>
                <StyledTableCell align="right">{hash}</StyledTableCell>
              </StyledTableRow>
  
              <StyledTableRow key="7">
                <StyledTableCell >
                  Cryptographic Signature
                </StyledTableCell>
                <StyledTableCell align="right" style={{wordBreak: 'break-all'}}>{sig}</StyledTableCell>
              </StyledTableRow>
              
            </TableBody>
          </Table>
        </TableContainer>
        
      </div>
      
    )
  }
  
  
  
  
  return (
    <Dialog onClose={handleClose} open={open} fullWidth maxWidth="lg" classes={{paper: classes.dialog }} >
      
      <div className={classes.content} style={{width:800,align:"center",marginLeft:50,height:8000}}>
        
        <div className={classes.margin}>
          <Typography color="primary" variant="h5" align="center"> Certify a Publication </Typography>
        </div>
        
        
        
        <div className={classes.margin} align="center" style={{marginTop:30}}>
          <TextField
            
            style={{width:500,margin:2}}
            color="primary"
            value = {name}
            id ="name"
            placeholder="Name of the asset"
            onChange={setFieldValue}
            InputProps={{
              startAdornment: <InputAdornment position="start">@Name...</InputAdornment>,
              className:classes.inputColor
            }}
          />
          
          <TextField
            
            style={{width:500,margin:2,marginTop:10}}
            placeholder="comma separated metadata of the assets"
            color="primary"
            value = {description}
            multiline
            id ="description"
            onChange={setFieldValue}
          
          />
  
          {!isUploaded &&
  
          <div align="center" style={{marginTop: 30}}>
    
            <input
              accept="*/*"
              className={classes.input}
              id="fileUpload"
              type="file"
              onChange={handleUpload}
            />
            <label htmlFor="fileUpload">
              <Button variant="contained" color="primary" component="span">
                Select File
              </Button>
            </label>
          </div>
          
          }
          
        
        </div>
        
        <div align="center" style={{marginTop:30, marginBottom:100}}>
          
          {isUploaded &&
            getInfoTable()
          }
          
         
          {showCertifyButton &&
            <div  style={{marginTop:50}}>
              <TextField
  
                style={{width: 400, marginTop: 30, backgroundColor: "#FFFFFF", height: 20}}
                placeholder="password for private key"
                value={password}
                id="password"
                onChange={setFieldValue}
                color="secondary"
              />
              
            </div>
          }
          
          
          <div align="center" style={{marginTop:50}}>
  
            {showSubmitButtons &&
  
              <Button onClick={onSubmit} className={classes.shape}  variant="contained" color="primary"
                  style={{width: 100, height: 30}}>Submit </Button>
            }
          
            {showCertifyButton &&
              <Button onClick={onCertify} className={classes.shape} variant="contained" color="primary"
                  style={{width: 100, height: 30}}>Certify </Button>
            }
            
            <Button onClick = {cancelThis} className={classes.shape}  variant="contained" color="secondary"
                    style={{width:100,height:30, marginLeft:20}} > Cancel </Button>
        
          </div>
        </div>
      </div>
    
    
    </Dialog>
  );
}