import React from 'react';

import { forwardRef } from 'react';

import AddBox from '@material-ui/icons/AddBox';
import ArrowUpward from '@material-ui/icons/ArrowUpward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import Unlock from '@material-ui/icons/LockOpen';
import EmailIcon from '@material-ui/icons/Email';
import ViewEye from '@material-ui/icons/VisibilityOutlined';
import FingerPrint from '@material-ui/icons/Fingerprint';
import Detail from '@material-ui/icons/Details';
import AttachmentIcon from '@material-ui/icons/Attachment';
import SignIcon from '@material-ui/icons/Fingerprint';
import Info from '@material-ui/icons/Info';
import Deactivate from '@material-ui/icons/DeleteForever';
import Share from '@material-ui/icons/Share';
import LockIcon from '@material-ui/icons/Lock';
import StorageIcon from '@material-ui/icons/Storage';








export default   {
  Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
  Unlock: forwardRef((props, ref) => <Unlock {...props} ref={ref} />),
  Email: forwardRef((props, ref) => <EmailIcon {...props} ref={ref} />),
  Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
  Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} color="secondary"/>),
  DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
  Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
  Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
  Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
  FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
  LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
  NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
  PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
  ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
  SortArrow: forwardRef((props, ref) => <ArrowUpward {...props} ref={ref} />),
  ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
  ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
  ViewEye: forwardRef((props, ref) => <ViewEye {...props} ref={ref} color="secondary"/>),
  FingerPrint: forwardRef((props, ref) => <FingerPrint {...props} ref={ref} color="secondary"/>),
  Detail: forwardRef((props, ref) => <Detail {...props} ref={ref} color="secondary"/>),
  Sign: forwardRef((props, ref) => <SignIcon {...props} ref={ref} color="secondary"/>),
  Attach: forwardRef((props, ref) => <AttachmentIcon {...props} ref={ref} color="secondary"/>),
  Info: forwardRef((props, ref) => <Info {...props} ref={ref} color="secondary" fontSize="inherit"/>),
  Deactivate: forwardRef((props, ref) => <Deactivate {...props} ref={ref} fontSize="inherit"/>),
  Share: forwardRef((props, ref) => <Share {...props} ref={ref} fontSize="inherit" />),
  Lock: forwardRef((props, ref) => <LockIcon {...props} ref={ref} color="primary"/>),
  Archive:forwardRef((props, ref) => <StorageIcon {...props} ref={ref} color="primary"/>),
  
};
