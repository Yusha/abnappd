
import React, { Component } from 'react';
import NavBar from '../layout/NavBar';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';

import LoginScreen from "./LoginScreen";
import PouchDB from 'pouchdb-browser';

import RegisterContainer from './RegisterContainer';


const myDB = new PouchDB('myDB');


class LandingScreen extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
      isLogin:false,
      isRegister:false,
      registered:false,
      fullLogin:false,
      context:{}
    };
    
    this.loginFunction = this.loginFunction.bind(this);
    this.registerFunction = this.registerFunction.bind(this);
    this.registered = this.registered.bind(this);
    
    
  }
  
  componentDidMount() {
    
    myDB.get("myHome").then((response) => {
      
      const contextObject = {
        user: response.user,
        name: response.name,
        homeId: response.homeId,
        publicKey: response.publicKey,
        encryptedPK: response.encryptedPK
      };
      
      if (isEmpty(response.encryptedPK)) {
        this.setState({fullLogin: true});
        
      } else {
          this.setState({context: contextObject, fullLogin: false})
      }
    }).catch ((error) => {
        console.log("MyHome not set. First time login" + error);
        this.setState({fullLogin:true});
      
    })
  
  }
  
  componentWillUnmount() {
  
  }
  
  loginFunction() {
    
    this.setState({isLogin:true,isRegister:false});
    
  }
  
  registered() {
    this.setState({isLogin:true,isRegister:false, registered:true});
  }
  
  
  registerFunction() {
    this.setState({isRegister:true,isLogin:false});
  }
  
  
  // Render Main Content
  render() {
  
    const classes = makeStyles(theme => ({
      root: {
        flexGrow: 1,
      },
      paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
      },
    }));
    
    return (
      
      <div className={classes.root}>
        
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <NavBar onRegister={this.registerFunction} onLogin={this.loginFunction}/>
  
            {(this.state.isRegister) && <RegisterContainer registerCallback={this.registered}/> }
            
            {(this.state.isLogin) && <LoginScreen setLoginSuccessCallback={this.props.setLoginSuccessCallback}
                                                  registered={this.state.registered}
                                                  fullLogin={this.state.fullLogin}
                                                  context={this.state.context}
                                                  logoutCallback={this.props.logoutCallback}/>}
          </Grid>
        </Grid>
        
        
      </div>
    );
    
  }
  
}

function isEmpty(str) {
  return (!str || 0 === str.length);
}

export default LandingScreen


