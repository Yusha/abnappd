

import React, {useEffect} from 'react';


import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import IconButton from '@material-ui/core/IconButton';
import ReferenceIcon from '@material-ui/icons/Dns';
import CollaborateIcon from '@material-ui/icons/GroupWork';
import PolicyIcon from '@material-ui/icons/Policy';


import Grid from '@material-ui/core/Grid';

import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';

import Home from '@material-ui/icons/HomeWork';

import POC from '@material-ui/icons/Gavel';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import NetworkIcon from '@material-ui/icons/Business';
import FileIcon from '@material-ui/icons/FileCopy';
import AddIcon from '@material-ui/icons/Add';
import ListIcon from '@material-ui/icons/List';
import InboxIcon from '@material-ui/icons/Inbox';
import OutboxIcon from '@material-ui/icons/AllOut';



import NewBizObject from "./NewBizObject";
import MyBizObjects from "./MyBizObjects";
import OutgoingRDOs from './OutgoingRDOs';
import IncomingRDOs from "./IncomingRDOs";
import SignaturePolicy from "./SignaturePolicy";





const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    marginTop:50,
    backgroundColor:"white"
  },
  nested: {
    paddingLeft: theme.spacing(11),
  },
  
  toolbar: theme.mixins.toolbar,
}));

export default function BizObjectsApp(props) {
  
  const {context} = props;
  const classes = useStyles();
  
  const [selectedIndex, setSelectedIndex] = React.useState(-1);
  const [snackMessage, setSnackMessage] = React.useState("test message");
  const [snackOpen, setSnackOpen] = React.useState(false);
  const [error, setError] = React.useState(false);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const anchorOpen = Boolean(anchorEl);
  const [tabValue, setTabValue] = React.useState(2);
  
  const handleTabChange = (event, newValue) => {
  
    if (newValue === 0 ) {
      props.setActiveScreenCallback(1); // Dashboard
    } else if (newValue === 1 ) {
      props.setActiveScreenCallback(7); // files
    } else if (newValue === 2 ) {
      props.setActiveScreenCallback(3); // Reference Objects
    } else if (newValue  === 3) {
      props.setActiveScreenCallback(4); // Contracts
    } else if (newValue  === 4) {
      props.setActiveScreenCallback(6); // Collaborate
    } else if (newValue  === 5) {
      props.setActiveScreenCallback(2); // Network
    }
    setTabValue(newValue);
  };
  
  const onLogout = () => {
    console.log("Logout called");
    props.logoutCallback();
    
  };
  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  
  
  const handleSnackClose = ()=> {
    setSnackOpen(false);
    
  };
  
  const selectTab = (event, index) => {
    setSelectedIndex(index);
  };
  
  
  
  const setSnackbarMessageCallback = (isError,message) => {
    setError(isError);
    setSnackMessage(message);
    setSnackOpen(true);
  };
  
  
  const onNewDocumentDialogCallback = (type,message) => {
    setSnackbarMessageCallback(false,"New Business Object sent to network");
    setSelectedIndex(1);
  };
  
  
  
  
  
  return (
    <div className={classes.root}>
      <CssBaseline/>
  
      <AppBar position="fixed" className={classes.appBar} style = {{backgroundColor:"white", color:"#303f9f"}}>
        <Toolbar>
          <Grid direction="row"  container>
  
            <Grid xs={1} item style={{marginTop:10}}>
              <Typography variant="h6" >
                ABBN
              </Typography>
            </Grid>
            
            <Grid xs={9} item>
              <Grid justify={"center"}>
                <Tabs
                  value={tabValue}
                  onChange={handleTabChange}
                  variant="fullWidth"
                  indicatorColor="primary"
                  textColor="primary"
                  centered
                >
                  <Tab icon={<Home fontSize="small"/>} label="Home"/>
                  <Tab icon={<FileIcon  fontSize="small" />} label="Files"/>
                  <Tab icon={<ReferenceIcon  fontSize="small" />} label="Reference Data Objects"/>
                  <Tab icon={<POC  fontSize="small" />} label="Contracts"/>
                  <Tab icon={<CollaborateIcon fontSize="small"/>} label="Collaborate"/>
                  <Tab icon={<NetworkIcon fontSize="small"/>} label="Network"/>

                </Tabs>
              </Grid>
            </Grid>
            
            <Grid item xs={1} />
        
            <Grid item xs={1} justify={"center"}>
              <Grid justify={"right"}>
            
                <div>
                  <IconButton
                
                    aria-haspopup="true"
                    onClick={handleMenu}
                    color="inherit"
              
                  >
                    <AccountCircle fontSize="large"/>
                  </IconButton>
                  <Menu
                    id="menu-appbar"
                    anchorEl={anchorEl}
                    anchorOrigin={{
                      vertical: 'top',
                      horizontal: 'right',
                    }}
                    keepMounted
                    transformOrigin={{
                      vertical: 'top',
                      horizontal: 'right',
                    }}
                    open={anchorOpen}
                    onClose={handleClose}
                  >
                    <MenuItem >{context.homeId} / {context.user}</MenuItem>
                    <MenuItem onClick={onLogout}>logout</MenuItem>
                  </Menu>
                </div>
          
              </Grid>
            </Grid>
          </Grid>
    
        </Toolbar>
      </AppBar>
      
      <Drawer
        className={classes.drawer}
        variant="permanent"
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <div className={classes.toolbar}/>
        
        
        <List style={{marginTop:40}}>
  
          <ListItem button key="0" onClick={event => selectTab(event, 0)} selected={selectedIndex === 0}>
            <ListItemIcon color="primary">
              <AddIcon/>
            </ListItemIcon>
            <ListItemText primary={<Typography style={{ color: '#303f9f' }}>Add New</Typography>}/>
          </ListItem>
  
          <ListItem button key="1" onClick={event => selectTab(event, 1)} selected={selectedIndex === 1}>
            <ListItemIcon color="primary">
              <ListIcon/>
            </ListItemIcon>
            <ListItemText primary={<Typography style={{ color: '#303f9f' }}>My Reference Data Objects</Typography>}/>
          </ListItem>
          
          <Divider/>
  
          <ListItem button key="2" onClick={event => selectTab(event, 2)} selected={selectedIndex === 2}>
            <ListItemIcon color="primary">
              <InboxIcon/>
            </ListItemIcon>
            <ListItemText primary={<Typography style={{ color: '#303f9f' }}>Received RDOs</Typography>}/>
          </ListItem>
          <Divider/>
  
          <ListItem button key="3" onClick={event => selectTab(event, 3)} selected={selectedIndex === 3}>
            <ListItemIcon color="primary">
              <OutboxIcon/>
            </ListItemIcon>
            <ListItemText primary={<Typography style={{ color: '#303f9f' }}>Created RDOs</Typography>}/>
          </ListItem>
          
          <Divider/>
  
          <ListItem button key="4" onClick={event => selectTab(event, 4)} selected={selectedIndex === 4}>
            <ListItemIcon color="primary">
              <PolicyIcon/>
            </ListItemIcon>
            <ListItemText primary={<Typography style={{ color: '#303f9f' }}>Signature Policy</Typography>}/>
          </ListItem>
  
          <Divider/>
  
        </List>
        <Divider/>
      </Drawer>
      
      
      <main className={classes.content}>
  
  
        {selectedIndex === -1 &&
        <div>
          <IntoSection/>
        </div>
        }
  
        {selectedIndex === 0 &&
        <div>
          <NewBizObject context={props.context}  callback = {onNewDocumentDialogCallback}
                   setSnackbarMessageCallback={setSnackbarMessageCallback}/>
        </div>
        }
        
        
        {selectedIndex === 1 &&
        <div>
          <MyBizObjects context={props.context} />
        </div>
        }
  
        {selectedIndex === 2 &&
        <div>
          <IncomingRDOs context={props.context} />
        </div>
        }
  
        {selectedIndex === 3 &&
        <div>
          <OutgoingRDOs context={props.context} />
        </div>
        }
  
        {selectedIndex === 4 &&
        <div>
          <SignaturePolicy context={props.context} />
        </div>
        }
  
        
        <div>
          <Snackbar open={snackOpen} autoHideDuration={6000} onClose={handleSnackClose}>
            { !error &&
              <Alert onClose={handleSnackClose} severity={error?"error":"success"}>{snackMessage}</Alert>
            }
          </Snackbar>
        </div>
      </main>
    </div>
  );
}

function IntoSection (props) {
  
  const {context} = props;
  const classes = useStyles();
  
  return (
  
    <div align="center" style={{marginTop:50}}>
      <Grid container alignItems="left"
            justify="center" direction="column">
      
        <Grid item xs={8} >
          <Card className={classes.root} variant="outlined"
                style={{width: '100%',height:250,display: 'flex', flexDirection: 'column',align:"center"}}>
            <CardHeader style = {{backgroundColor:"#0091EA", color:"white", textAlign:"center"}}

                        title="Reference Data Objects"
              
            />
            <CardContent >
              <Typography align="center" color = "primary">
                Create a Reference Data Object for your organization
              </Typography>
              <Typography align="center" color = "primary">
                Create a Reference Data Object for some other Organization
              </Typography>
              <Typography align="center" color = "primary">
                Reference Data Accessible all over Network
              </Typography>
              <Typography align="center" color = "primary">
                Cryptographic Audit Trail
              </Typography>
    
            </CardContent>
  
          </Card>
          
        </Grid>
  
        
      
      </Grid>
    </div>
    
  )
  
}

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}
