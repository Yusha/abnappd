
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

import Dialog from '@material-ui/core/Dialog';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';

import aes256 from 'aes256';
import { ec as EC } from 'elliptic';

import axios from 'axios';



export default function RDOSignDialog(props) {
  
  const {callback,open, context, data } = props;
  const classes = dialogStyles();
  const [password, setPassword] = React.useState("");
  const [submitted, setSubmitted] = React.useState(false);
  
  const [error, setError] = React.useState(false);
  
  
  
  const handleClose = (action) => {
    callback(action);
  };
  
  const signAndSubmit = () => {
    
    const encryptedPK = context.encryptedPK;
    
    try {
      let decrypted = aes256.decrypt(password, encryptedPK);
      
      // only Sign the hash of PI Submission.. i,e
      const ec = new EC('secp256k1');
      const ecSK = ec.keyFromPrivate(decrypted, 'hex');
      
      const sign = ecSK.sign(data._id).toDER('hex');
      
      const chainTx = {
        partyHome: context.homeId,
        partyAccount: context.user,
        publicKey:context.publicKey,
        objectId:data._id,
        signature:sign,
      };
      
      const url = "http://127.0.0.1:4000/object/sign";
      
      axios.post(url,chainTx).then ((result) => {
        setSubmitted(true);
        
      }).catch ((error) => {
        console.log("Network Error... " + error );
        setError(true);
      });
    } catch (error) {
      console.log("Error " + error);
    }
  };
  
  
  return (
    <Dialog onClose={handleClose} open={open} fullWidth maxWidth="lg" classes={{paper: classes.dialog }} style={{marginTop:50, marginLeft:50}}>
      
      <div className={classes.content} style={{width:800,marginLeft:50}} align="center">
        
        <div style={{marginTop:50}}>
          <Typography color="primary" variant="h4" align="center">Approve Reference Data Object </Typography>
        </div>
  
        <div style={{marginTop:50}}>
          <Typography color="secondary" variant="h6" align="center">{data.objectId} </Typography>
        </div>
  
        {!submitted &&
        <div style={{marginTop: 50}} align="center">
    
          <TextField id="password" value={password} style={{width:250 }} onChange={(e) => { setPassword(e.target.value)}} label="Key Password"/>
        </div>
    
        }
  
        {!submitted &&
        <div style={{marginTop: 50}} align="center">
    
    
          <Button variant="contained" color="primary" style={{marginRight: 40, borderRadius: 25}}
                  onClick={signAndSubmit}>
            Sign and Approve
          </Button>
    
    
          <Button variant="contained" color="primary" style={{borderRadius: 25, marginLeft: 5}} onClick={handleClose}>
            Cancel
          </Button>
  
  
        </div>
        
        }
        
        <div>
          {submitted &&
            <div align="center">
              <Typography variant="h6"> Approval Submitted to the network  </Typography>
              <Button variant="contained" color="primary" style={{borderRadius: 25, marginTop: 100}} onClick={handleClose}>
                 Close
              </Button>
            </div>
          
          }
          
        </div>
        
        <Divider/>
        
      </div>
    
    </Dialog>
  );
}


const dialogStyles = makeStyles({
  dialog: {
    position: 'absolute',
    left: 50,
    top: 50
  },
  table: {
    minWidth: 700,
  },
});
