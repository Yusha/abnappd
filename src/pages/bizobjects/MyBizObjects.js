

import React from 'react';
import { makeStyles } from '@material-ui/core/styles';


import Dialog from '@material-ui/core/Dialog';
import Button from '@material-ui/core/Button';
import MaterialTable, {MTableToolbar} from 'material-table'
import { forwardRef } from 'react';
import Share from '@material-ui/icons/Share';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';




import BizObjectDetails from './BizObjectDetails';
import tableIcons from '../common/IconDef';


import axios from 'axios';
import InfoDialog from "./InfoDialog";
import ShareDialog from './ShareDialog';
import RDOSignatures from "./RDOSignatures";


const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    marginTop:20,
  }
  
  
}));

export default function MyBizObjects(props) {
  
  const {context} = props;
  
  
  const classes = useStyles();
  
  return (
    <div className={classes.root}>
      
      <main className={classes.content}>
        
        <div style={{marginTop:20,marginLeft:20,marginRight:20}}>
          
          
          <ObjectsTable context = {context} />
          
          
        </div>
        
       
      </main>
    </div>
  );
}


export function ObjectsTable(props) {
  
  const {context} = props;
  
  
  const classes = tableTheme();
  
  const [bcData,setBCData] = React.useState([]);
  const [currentObject,setCurrentObject] = React.useState({});
  const [showDetailsDialog, setShowDetailsDialog] = React.useState(false);
  const [currentTx, setCurrentTx] = React.useState({});
  const [showInfoDialog, setShowInfoDialog] = React.useState(false);
  const [showRetireDialog, setShowRetireDialog] = React.useState(false);
  const [showShareDialog, setShowShareDialog] = React.useState(false);
  const [showSignatureDialog, setShowSignatureDialog] = React.useState(false);
  
  
  React.useEffect(()=> {
    
    const url = "http://localhost:4000/object/getObjects?home=" + context.homeId ;
    axios.get(url).then ((result) => {
      setBCData(result.data);
      
    }).catch ((error) => {
      console.log("Error in getting Accounts   " + JSON.stringify(error));
    })
    
  },[]);
  
  const getTableData = ()=> {
    
    return bcData.map((row,index) => {
      
      let otx = JSON.parse(row.tx.tx);
      
      return {name:otx.objectInfo.name,description:otx.objectInfo.description,encrypted:otx.objectInfo.encrypted,
               originatorHome:row.tx.homeId,originatorAccount:row.tx.user, timeStamp:row.timeStamp};
    })
    
  };
  
  
  
  const showDetailsCallback = (data) => {
    setShowDetailsDialog(false);
    
  };
  
  const infoDialogCallback = (data) => {
    setShowInfoDialog(false);
  };
  
  const retireDialogCallback = (data) => {
    setShowRetireDialog(false);
  };
  
  const shareDialogCallback = (data) => {
    setShowShareDialog(false);
    
  };
  
  const showObjectData = (row) => {
    
    const od = bcData[row.tableData.id];
    setCurrentObject(JSON.parse(od.tx.tx));
    setShowDetailsDialog(true);
  };
  
  const setBlockchainInfo = (row) => {
    const od = bcData[row.tableData.id];
    setCurrentTx(od);
    setShowInfoDialog(true);
  };
  
  const showSignatures = (e,row) => {
    setCurrentTx(bcData[row.tableData.id]);
    setShowSignatureDialog(true);
  };
  
  const RDOSignatureCallback = () => {
    setShowSignatureDialog(false);
  };
  
  const setDeactivateDialog = (row) => {
    
    const od = bcData[row.tableData.id];
    setCurrentTx(od);
    setShowRetireDialog(true);
  };
  
  
  return (
    
    <div className={classes.table}>
      <MaterialTable
        title="Reference Data Objects"
        icons={tableIcons}
        
        
        localization={{
          
          header: {
            actions: 'Actions'
          },
          
        }}
        
        columns={[
          { title: 'Name', field: 'name' },
          { title: 'Description', field: 'description', cellStyle: { wordBreak: 'break-all' }},
          { title: 'Reference Organization', field: 'originatorHome' },
          { title: 'Reference Account', field: 'originatorAccount' },
          { title: 'Encrypted', field: 'encrypted' },
          { title: 'Date and Time', field: 'timeStamp' },
        ]}
        data={getTableData()}
        actions={[
          {
            icon: tableIcons.Detail,
            tooltip: 'Details',
            onClick: (event, rowData) => {showObjectData(rowData)}
          },
          {
            icon: tableIcons.Info,
            tooltip: 'Blockchain info',
            onClick: (event, rowData) => {setBlockchainInfo(rowData)}
          },
          {
            icon: tableIcons.FingerPrint,
            tooltip: 'Signatures',
            onClick: (event, rowData) => {showSignatures(event,rowData)}
          },
  
          {
            icon: tableIcons.Deactivate,
            tooltip: 'Retire',
            onClick: (event, rowData) => {setDeactivateDialog(rowData)}
          }
        ]}
        
        options={{
          padding:"dense",
          headerStyle: {
            backgroundColor: '#0091EA',
            color: '#FFF'
          }
          
        }}
        
        components={{
          Toolbar: props => (
            <div>
              <MTableToolbar {...props} />
            
            </div>
          ),
        }}
      />
      
      
      
      { showDetailsDialog &&
        
        <BizObjectDetails open={showDetailsDialog} callback={showDetailsCallback} data={currentObject}
                         context={context}/>
      }
  
      { showInfoDialog &&
      
        <InfoDialog open={showInfoDialog} callback={infoDialogCallback} data={currentTx}
                        context={context}/>
      }
  
      {showSignatureDialog &&
  
      <RDOSignatures callback = {RDOSignatureCallback} open = {showSignatureDialog} data = {currentTx} />
    
      }
      
      { showShareDialog &&
        
        <ShareDialog open={showShareDialog} callback={shareDialogCallback} data={currentTx}
                     context={context} />
      }
  
      { showRetireDialog &&
  
        <RetireDialog open={showRetireDialog} callback={retireDialogCallback} data={currentTx}
                  context={context}/>
      }
      
      
    </div>
  )
}



const tableTheme = makeStyles(theme => ({
  table: {
    '& tbody>.MuiTableRow-root:hover': {
      background: '#EEE',
    }
  },
}));



export function RetireDialog(props) {
  
  
  const { callback,context, open, data } = props;
  
  const [password, setPassword] = React.useState("");
  const classes = useStyles();
  
  
  
  const handleClose = (action) => {
    callback(action);
  };
  
  const onRetire = () => {
    
    // send retire transaction
  };
  
  
  
  return (
    <Dialog onClose={handleClose} open={open} fullWidth maxWidth="lg" classes={{paper: classes.dialog }} style={{marginTop:50, marginLeft:50}}>
    
      <div align="center" style={{marginTop:100}}>
        <Typography variant="h5" color="secondary" > Retire Object </Typography>
        <Typography> Object Name : {data.objectId} </Typography>
        <Typography> Object Owner : {data.homeId} / {data.user} </Typography>
      </div>
      
      <div align="center" style={{marginTop:30}}>
        
        <TextField id="password" style ={{width:300}} value={password} onChange={(e)=>{setPassword(e.target.value)}} label="Enter Password for the private key"/>
        
      </div>
      
      <div align="center" style={{marginTop:30, marginBottom:200}}>
  
        <Button onClick={onRetire} className={classes.shape} variant="contained" color="primary"
                style={{width: 400, height: 30}}>Retire Object and Finalize </Button>
        <Button onClick={handleClose} className={classes.shape} variant="contained" color="primary"
                style={{width: 100, height: 30,marginLeft:10}}>Cancel </Button>

      </div>
      
    </Dialog>
  );
}
