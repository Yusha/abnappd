
import React from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';

import Dialog from '@material-ui/core/Dialog';
import Typography from '@material-ui/core/Typography';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';

import TextField from '@material-ui/core/TextField';


import {Editor} from 'react-draft-wysiwyg';
import { EditorState, convertFromRaw } from 'draft-js';
import { Document, Page } from 'react-pdf/dist/entry.webpack';

import Button from '@material-ui/core/Button';

import {GetDecryptedPK} from "../../ec/CryptoUtils";
import {decryptECIES} from '../../ec/Encryption';


export default function BizObjectDetails(props) {
  
  
  const { context, data,callback,open } = props;
  
  const [viewData, setViewData] = React.useState(false);
  
  const [displayPDF, setDisplayPDF] = React.useState(false);
  const [password, setPassword] = React.useState("");
  const [decrypted, setDecrypted] = React.useState(false);
  
  const [textValue, setTextValue] = React.useState(data.value);
  
  const [editorState, setEditorState] = React.useState(EditorState.createEmpty());
  
  
  const classes = dialogStyles();
  
  const handleClose = (action) => {
    callback(action);
  };
  
  const onDecrypt = () => {
    
    const privateKey = GetDecryptedPK(context.encryptedPK, password);
    const clearText = decryptECIES(privateKey, data.value);
    const textValue = JSON.parse(clearText);
    setTextValue(textValue);
    setDecrypted(true);
    
  };
  
  const onView = () => {
    
    if (textValue.fromFile) {
      setDisplayPDF(true);
    } else {
      setEditorState(EditorState.createWithContent(convertFromRaw(textValue.textObject)));
      setViewData(true);
    }
  };
  
  const onDownload = () => {
    
    const fileData = textValue.base64;
    const fileName = textValue.name;
    
    const downloadLink = document.createElement("a");
    
    downloadLink.href = fileData;
    downloadLink.download = fileName;
    downloadLink.click();
  };
  
  
  
  const StyledTableCell = withStyles((theme) => ({
    head: {
      backgroundColor: "#0091EA",
      color: "white",
      fontSize:20
    },
    body: {
      fontSize: 14,
    },
  }))(TableCell);
  
  const StyledTableRow = withStyles((theme) => ({
    root: {
      '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
      },
    },
  }))(TableRow);
  
  
  const getMetadata = () => {
    return data.objectInfo.metadata.map((item,index) => {
      
      return (
        <StyledTableRow key={index}>
          <StyledTableCell >
            {item.name}
          </StyledTableCell>
          <StyledTableCell align="right">{item.value}</StyledTableCell>
        </StyledTableRow>
      )
    })
  };
  
  
  
  return (
    <Dialog onClose={handleClose} open={open} fullWidth maxWidth="lg" overflow="scroll" classes={{paper: classes.dialog }} >
      
      <div className={classes.content} style={{width:800,align:"center",marginLeft:50}}>
        
        <div className={classes.margin}>
          <Typography color="primary" variant="h5" align="center"> Object Details</Typography>
        </div>
        
        
        
        <div className={classes.margin} align="center" style={{marginTop:30}}>
          
          <TableContainer component={Paper}>
            <Table className={classes.table} aria-label="customized table">
              <TableHead>
                <TableRow>
                  <StyledTableCell>Metadata Property</StyledTableCell>
                  <StyledTableCell align="right">Value </StyledTableCell>
                
                </TableRow>
              </TableHead>
              <TableBody>
                
                <StyledTableRow key="x">
                  <StyledTableCell component="th" scope="row">
                    Object Name
                  </StyledTableCell>
                  <StyledTableCell align="right">{data.objectInfo.name}</StyledTableCell>
                </StyledTableRow>
                
                <StyledTableRow key="y">
                  <StyledTableCell >
                    Object Description
                  </StyledTableCell>
                  <StyledTableCell align="right">{data.objectInfo.description}</StyledTableCell>
                </StyledTableRow>
                {getMetadata()}
              </TableBody>
            </Table>
          </TableContainer>
        
        
        
        
        </div>
        
        {data.objectInfo.encrypted && !decrypted &&
        <div align="center" style={{marginTop: 20, marginBottom: 10}}>
          
          <TextField
            
            style={{width: 400, marginTop: 30, backgroundColor: "#FFFFFF", height: 20}}
            placeholder="password for private key"
            value={password}
            id="password"
            onChange={(v) => {
              setPassword(v.target.value)
            }}
            color="secondary"
          />
          
          <Button onClick={onDecrypt} className={classes.shape} align="left" variant="contained" color="primary"
                  style={{width: 100, height: 30}}>Decrypt Data </Button>
        
        </div>
        }
        
        { (!data.objectInfo.encrypted || decrypted) &&
        <div align="center" style={{marginTop: 20, marginBottom: 10}}>
          
          
          <Button onClick={onView} className={classes.shape} align="left" variant="contained" color="primary"
                  style={{width: 100, height: 30}}>View </Button>
          
          {textValue.fromFile &&
          <Button onClick={onDownload} className={classes.shape} align="left" variant="contained" color="primary"
                  style={{width: 100, height: 30}}>Download </Button>
          }
          
          <Button onClick={handleClose} className={classes.shape} variant="contained" align="right"
                  style={{width: 100, height: 30, marginLeft: 20}} color="secondary"> Cancel </Button>
        
        </div>
        
        }
        
        
        <div align="center" style={{marginTop:20, marginBottom:20}}>
          <Divider />
        </div>
        
        <div align="center" style={{marginTop:20, marginBottom:10}}>
          
          {displayPDF &&
          <DisplayPDF data = {textValue.base64}/>
          }
          
          {viewData &&
          
          <div className={classes.margin} style={{marginTop:30}} align="center">
            
            <div align="center" style={{marginTop:20, marginBottom:20}}>
              <Typography >
                Object Data
              </Typography>
            </div>
            
            <Editor
              editorState={editorState}
              toolbarClassName="toolbarClassName"
              wrapperClassName="wrapperClassName"
              editorClassName="editorClassName"
              placeholder = "Note for read request..."
              readOnly
              editorStyle={{backgroundColor:"#FFFFFF",height:200}}
            />
          </div>
          
          }
          
        </div>
      
      
      </div>
    
    
    </Dialog>
  );
}


const dialogStyles = makeStyles({
  dialog: {
    position: 'absolute',
    left: 10,
    top: 50
  },
  table: {
    minWidth: 700,
  },
});
function DisplayPDF(props) {
  
  const {data} = props;
  
  
  
  const [numPages,setNumPages] = React.useState(0);
  
  
  const onDocumentLoadSuccess = ( numPages) => {
    setNumPages(numPages);
  };
  
  return (
    <div >
      <Document
        file={data}
        onLoadSuccess={onDocumentLoadSuccess}
      >
        {Array.from(
          new Array(numPages),
          (el, index) => (
            <Page
              key={`page_${index + 1}`}
              pageNumber={index + 1}
            />
          ),
        )}
      </Document>
    </div>
  );
  
}

