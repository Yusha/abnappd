import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import TextField from '@material-ui/core/TextField';


import CountryIcon from '@material-ui/icons/AssistantPhoto';

import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Add from '@material-ui/icons/Add';
import Delete from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';

import {Editor} from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { EditorState, convertToRaw } from 'draft-js';


import axios from 'axios';

import {GetHash, GetSignature} from '../../ec/CryptoUtils';

import {encryptECIES} from "../../ec/Encryption";


const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    marginTop:50,
  },
  backButton: {
    marginRight: theme.spacing(1),
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(2),
    
  },
  button: {
    margin: theme.spacing(1),
  },
  margin: {
    margin: theme.spacing(1),
  },
  input: {
    display: 'none',
  },
}));


function getSteps() {
  return ['Object Name', 'Meta-data', 'Target',' Object Data','Submit'];
}


export default function NewBizObject(props) {
  
  const classes = useStyles();
  
  const {callback, context } = props;
  
  const [activeStep, setActiveStep] = React.useState(0);
  
  const [name,setName] = React.useState({name:"","description":""});
  
  const [metadata, setMetadata] = React.useState({});
  const [target, setTarget] = React.useState({});
  const [value, setValue] = React.useState({isAttachment:"false","value":""});
  const [status, setStatus] = React.useState({});
  const [finished, setFinished] = React.useState(false);
  
  
  
  const steps = getSteps();
  
  const submitCallback = (pass)=> {
    
    const password = pass;
    let valueTx = value;
    
    if (name.encrypted) {
      // encrypt the value object
      valueTx = encryptECIES(context.publicKey,JSON.stringify(value));
    }
    
    
    
    const baseObject = {
      
      objectInfo:{
        name:name.name,
        description:name.description,
        encrypted:name.encrypted,
        metadata:metadata,
      },
      target:target,
      value:valueTx
    };
    
    
    const sTx = JSON.stringify(baseObject);
    const hashOf = GetHash(sTx);
    
    const signature = GetSignature(props.context.encryptedPK,password,hashOf);
    
    const chainTx = {
      action:"newObject",
      homeId: context.homeId,
      objectId:name.name,
      user: context.user,
      targetHome:target.home,
      targetAccount:target.account,
      publicKey:context.publicKey,
      hash:hashOf,
      signature:signature,
      tx:sTx
    };
    
    const url = "http://127.0.0.1:4000/object/createObject";
    
    // TODO: Send as FormData multipart for large files and efficiency.
    
    axios.post(url, {
      tx: chainTx
    }).then (result => {
      console.log("Transaction sent to the net work" + result);
      setFinished(true);
      setStatus({status:"Transaction Succeeded", id:hashOf});
      
      
    }).catch (error => {
      console.log("Error in sending the tx to the network... " + JSON.stringify(error));
      setFinished(true);
      setStatus({status:"Transaction Failed", id:hashOf});
      
    });
    
  };
  
  const  handleNext = () => {
    
    setActiveStep(prevActiveStep => prevActiveStep + 1);
  };
  
  const handleBack = () => {
    setActiveStep(prevActiveStep => prevActiveStep - 1);
  };
  
  
  const saveNameCallback = (name) => {
    setName(name);
    handleNext();
  };
  
  const saveMetadataCallback = (values) => {
    setMetadata(values);
    handleNext();
  };
  
  const saveValueCallback = (values) => {
    setValue(values);
    handleNext();
  };
  
  const saveTargetCallback = (target) => {
    setTarget(target);
    handleNext();
  };
  
  
  
  return (
    <Grid className={classes.root}>
      <Stepper activeStep={activeStep} alternativeLabel>
        {steps.map(label => (
          <Step key={label}>
            <StepLabel>{label}</StepLabel>
          </Step>
        ))}
      </Stepper>
      
      <Grid container justify = "center">
        <div className={classes.instructions}>
          
          {activeStep === 0 &&
            <ChooseName saveNameCallback = {saveNameCallback}/>
          }
          
          {activeStep === 1 &&
            <ChooseMetadata backCallback = {handleBack} saveMetadataCallback = {saveMetadataCallback}/>
          
          }
  
          {activeStep === 2 &&
            <ChooseTarget backCallback = {handleBack} callback = {saveTargetCallback} context={context}/>
    
          }
          
          
          {activeStep === 3 &&
            <ChooseValue backCallback = {handleBack} saveValueCallback = {saveValueCallback}/>
          }
          
          {activeStep === 4 &&
            <Submit  backCallback = {handleBack} submitCallback = {submitCallback}/>
          
          }
        
        
        
        </div>
      </Grid>
      
      
      <Divider/>
      {finished &&
      <Grid container justify="center">
        <Typography className={classes.instructions} color="red">
          Status of the Transaction is  " {status.status}
        
        </Typography>
        
        <Button onClick = {callback} variant="contained" color="secondary"> Complete </Button>
      
      
      
      </Grid>
      }
    
    </Grid>
  );
}


function ChooseName(props) {
  
  const {saveNameCallback} = props;
  const classes = useStyles();
  const [name, setName] = React.useState('');
  const [description, setDescription] = React.useState('');
  const [encrypted, setEncrypted] = React.useState(true);
  
  const setCheckBox = (e) => {
    setEncrypted(!encrypted);
  };
  
  
  const setFieldValue = (e)=> {
    const id = e.target.id;
    const value = e.target.value;
    
    if (id === "name") {
      setName(value);
    } else if (id === "description") {
      setDescription(value);
      
    } else {
      console.log("Unknown Field detected. Should not have happened");
    }
  };
  
  return (
    
    <div>
      <div className={classes.margin}>
        <Grid container spacing={1} alignItems="flex-end">
          
          <Grid item>
            <TextField id="name" value={name} onChange={setFieldValue} label="Object Name"/>
          </Grid>
        </Grid>
      </div>
      
      
      <div className={classes.margin}>
        <Grid container spacing={1} alignItems="flex-end">
          
          <Grid item>
            <TextField id="description" value={description} multiline onChange={setFieldValue} label="Object Description"/>
          </Grid>
        </Grid>
      </div>
      
      <div className={classes.margin}>
        <Grid container spacing={1} alignItems="flex-end">
          <FormControlLabel
            control={
              <Checkbox checked={encrypted} onChange={setCheckBox} />
            }
            label="Encrypted"
          />
        </Grid>
      </div>
      
      
      <div className={classes.margin} style={{marginTop:30}}>
        <Grid container spacing={1} alignItems="flex-end">
          
          <Grid container justify = "center">
            
            <Grid container justify = "center">
              
              <Grid>
                <Button
                  disabled
                  className={classes.backButton}
                >
                  Back
                </Button>
                
                <Button variant="contained" color="primary" onClick={()=>saveNameCallback({name:name,description:description,encrypted:encrypted})}>
                  Next
                </Button>
              </Grid>
            </Grid>
          
          </Grid>
        
        
        </Grid>
      </div>
    
    </div>
  
  );
}

function ChooseMetadata(props) {
  
  const {backCallback,saveMetadataCallback} = props;
  const classes = useStyles();
  
  const [metadataSet, setMetaDataSet] = React.useState([]);
  
  const [name, setName] = React.useState("");
  const [value, setValue] = React.useState("");
  
  const setMetadataItem = () => {
    setMetaDataSet([...metadataSet,{name:name,value:value}]);
    setName("");
    setValue("");
  };
  
  const deleteItem = (e)=> {
    const id = e.target.id;
    metadataSet.splice(id,1);
    setMetaDataSet([...metadataSet]);
    
  };
  
  const metadataList = () => {
    return metadataSet.map((item,index) => {
      
      return (
        <Grid container spacing={1} alignItems="flex-end" key={index}>
          <Grid item style={{marginLef:10}}>
            <TextField id="name" value={item.name} readOnly />
          </Grid>
          <Grid item style={{marginLef:10}}>
            <TextField id="value" value={item.value} readOnly />
          </Grid>
          <Grid item style={{marginLef:10}}>
            
            <IconButton size="medium" color="primary" id={index} onClick={deleteItem}>
              <Delete/>
            </IconButton>
          
          
          </Grid>
        
        </Grid>
      
      )
      
    })
    
  };
  
  
  
  
  const setFieldValue = (e)=> {
    const id = e.target.id;
    const value = e.target.value;
    
    if (id === "name") {
      setName(value);
    } else if (id === "value") {
      setValue(value);
    }
    
  };
  
  return (
    
    <div>
      <div className={classes.margin}>
        
        <div>
          
          { metadataList() }
        
        </div>
        
        <div>
          <Grid container spacing={1} alignItems="flex-end">
            <Grid item style={{marginLef:10}}>
              <TextField id="name" value={name} onChange={setFieldValue} label="Metadata name"/>
            </Grid>
            <Grid item style={{marginLef:10}}>
              <TextField id="value" value={value} onChange={setFieldValue} label="Metadata value"/>
            </Grid>
            <Grid item style={{marginLef:10}}>
              <Button
                variant="contained"
                color="secondary"
                className={classes.button}
                size="small"
                startIcon={<Add />}
                onClick = {setMetadataItem}
              
              >
                Add
              </Button>
            </Grid>
          
          </Grid>
        </div>
      
      
      </div>
      
      
      
      
      <div className={classes.margin} style={{marginTop:30}}>
        <Grid container spacing={1} alignItems="flex-end">
          
          <Grid container justify = "center">
            
            <Grid container justify = "center">
              
              <Grid>
                <Button
                  onClick = {backCallback}
                  className={classes.backButton}
                >
                  Back
                </Button>
                
                <Button variant="contained" color="primary" onClick={()=>saveMetadataCallback(metadataSet)}>
                  Next
                </Button>
              </Grid>
            </Grid>
          
          </Grid>
        
        
        </Grid>
      </div>
    
    </div>
  
  );
}

function ChooseTarget(props) {
  
  const {backCallback, callback, context} = props;
  const classes = useStyles();
  const [home, setHome] = React.useState('');
  const [account, setAccount] = React.useState('');
  const [myOrg, setMyOrg] = React.useState(false);
  
  const setMyOrgFlag = ( ) => {
    setMyOrg(!myOrg);
    if (!myOrg) {
      setHome(context.homeId);
      setAccount(context.user);
    } else {
      setHome("");
      setAccount("");
    }
  };
  
  
  
  return (
    
    <div>
      <div className={classes.margin}>
        <Grid container spacing={1} alignItems="flex-end">
          
          <Grid item>
            <TextField style={{width:400}} value={home} onChange={(e) =>{setHome(e.target.value)}} label="Target Organization Id"/>
          </Grid>
        </Grid>
      </div>
      
      
      <div className={classes.margin}>
        <Grid container spacing={1} alignItems="flex-end">
          
          <Grid item>
            <TextField style={{width:400}} value={account} multiline onChange={(e) => {setAccount(e.target.value)}} label="Target organization Account"/>
          </Grid>
        </Grid>
      </div>
  
      <div className={classes.margin}>
        <Grid container spacing={1} alignItems="flex-end">
          <FormControlLabel
            control={
              <Checkbox checked={myOrg} onChange={setMyOrgFlag} />
            }
            label="Set My Organization"
          />
        </Grid>
      </div>
      
      
      <div className={classes.margin} style={{marginTop:30}}>
        <Grid container spacing={1} alignItems="flex-end">
          
          <Grid container justify = "center">
            
            <Grid container justify = "center">
              
              <Grid>
                <Button
                  onClick = {backCallback}
                  className={classes.backButton}
                >
                  Back
                </Button>
                
                <Button variant="contained" color="primary" onClick={()=>callback({home:home,account:account})}>
                  Next
                </Button>
              </Grid>
            </Grid>
          
          </Grid>
        
        
        </Grid>
      </div>
    
    </div>
  
  );
}

function ChooseValue(props) {
  
  const {saveValueCallback,backCallback} = props;
  const classes = useStyles();
  const [fromFile, setFromFile] = React.useState(false);
  const [fileObject, setFileObject] = React.useState({});
  
  const [editorState, setEditorState] = React.useState(EditorState.createEmpty());
  
  
  const submitValues = () => {
    if (fromFile) {
      saveValueCallback(fileObject);
      
    } else {
      const raw = convertToRaw(editorState.getCurrentContent());
      saveValueCallback({textObject:raw,fromFile:false});
    }
    
  };
  
  const onEditorStateChange = (s)=> {
    setEditorState(s);
    
  };
  
  const handleUpload = (e) => {
    
    let file = e.target.files[0];
    
    let reader = new FileReader();
    
    // Convert the file to base64 text
    reader.readAsDataURL(file);
    
    reader.onload = () => {
      
      let fileInfo = {
        fromFile: true,
        name: file.name,
        type: file.type,
        size: Math.round(file.size / 1000) + ' kB',
        base64: reader.result,
      };
      setFileObject(fileInfo);
      
    };
  };
  
  const setCheckBox = () => {
    setFromFile(!fromFile);
  } ;
  
  
  
  return (
    
    <div>
      
      <div className={classes.margin}>
        <Grid container spacing={1} alignItems="flex-end">
          <Grid container spacing={1} alignItems="flex-end">
            <FormControlLabel
              control={
                <Checkbox checked={fromFile} onChange={setCheckBox} />
              }
              label="Load from File"
            />
          </Grid>
        </Grid>
      </div>
      
      {fromFile &&
      
      <div className={classes.margin} style={{marginTop:30}}>
        <Grid container spacing={1} alignItems="flex-end">
          <input
            accept=".pdf,image/*"
            className={classes.input}
            id="fileUpload"
            type="file"
            onChange={handleUpload}
          />
          <label htmlFor="fileUpload">
            <Button variant="contained" color="primary" component="span">
              Upload
            </Button>
          </label>
        
        
        </Grid>
      </div>
      
      }
      
      {!fromFile &&
      
      <div className={classes.margin} style={{marginTop:30}}>
        <Editor
          editorState={editorState}
          toolbarClassName="toolbarClassName"
          wrapperClassName="wrapperClassName"
          editorClassName="editorClassName"
          placeholder = "Note for read request..."
          onEditorStateChange={onEditorStateChange}
          editorStyle={{backgroundColor:"#FFFFFF",height:200}}
        />
      </div>
      
      }
      
      
      <div className={classes.margin} style={{marginTop:30}}>
        <Grid container spacing={1} alignItems="flex-end">
          
          <Grid container justify = "center">
            
            <Grid container justify = "center">
              
              <Grid>
                <Button variant="contained" color="primary" onClick={backCallback}
                        className={classes.backButton}
                >
                  Back
                </Button>
                
                <Button variant="contained" color="primary" onClick={submitValues}>
                  Next
                </Button>
              </Grid>
            </Grid>
          
          </Grid>
        
        
        </Grid>
      </div>
    
    </div>
  
  );
}

function Submit(props) {
  
  const classes = useStyles();
  
  const {backCallback, submitCallback} = props;
  
  const [password, setPassword] = React.useState('');
  
  const setValue = (e) => {
    setPassword(e.target.value);
    
  };
  
  
  return (
    
    <div>
      
      <div className={classes.margin}>
        <Grid container spacing={1} alignItems="flex-end">
          <Grid item>
            <CountryIcon/>
          </Grid>
          <Grid item>
            <TextField id="country" multiline value={password} onChange={setValue} label="Enter key password..."/>
          </Grid>
        </Grid>
        
        <Grid container spacing={1} alignItems="flex-end" style = {{marginTop:30}}>
          
          <Grid container justify = "center">
            
            <Grid container justify = "center">
              
              <Grid>
                <Button
                  onClick={backCallback}
                  className={classes.backButton}
                >
                  Back
                </Button>
                
                <Button variant="contained" color="primary" onClick={()=>submitCallback(password)}>
                  Sign and Submit
                </Button>
              </Grid>
            </Grid>
          </Grid>
        
        </Grid>
      
      
      </div>
    </div>
  
  );
}


