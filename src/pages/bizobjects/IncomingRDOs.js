

import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import axios from 'axios';


import MaterialTable, {MTableToolbar} from 'material-table'

import BizObjectDetails from './BizObjectDetails';
import tableIcons from '../common/IconDef';
import InfoDialog from "./InfoDialog";
import SignDialog from './RDOSignDialog';


const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    marginTop:20,
  }
  
}));

export default function IncomingRDOs(props) {
  
  const {context} = props;
  
  
  const classes = useStyles();
  
  return (
    <div className={classes.root}>
      
      <main className={classes.content}>
        
        <div style={{marginTop:20,marginLeft:20,marginRight:20}}>
          
          
          <ObjectsTable context = {context} />
          
          
        </div>
        
       
      </main>
    </div>
  );
}


export function ObjectsTable(props) {
  
  const {context} = props;
  
  
  const classes = tableTheme();
  
  const [bcData,setBCData] = React.useState([]);
  const [currentObject,setCurrentObject] = React.useState({});
  const [showDetailsDialog, setShowDetailsDialog] = React.useState(false);
  const [currentTx, setCurrentTx] = React.useState({});
  const [showInfoDialog, setShowInfoDialog] = React.useState(false);
  const [showSignDialog, setShowSignDialog] = React.useState(false);
  const [rowSelected, setRowSelected] = React.useState({});
  
  
  React.useEffect(()=> {
    
    const url = "http://localhost:4000/object/getObjects?home=" + context.homeId ;
    axios.get(url).then ((result) => {
      setBCData(result.data);
      
    }).catch ((error) => {
      console.log("Error in getting Accounts   " + JSON.stringify(error));
    })
    
  },[]);
  
  const getTableData = ()=> {
    
    return bcData.map((row,index) => {
      
      let otx = JSON.parse(row.tx.tx);
      let approved = isApproved(context.homeId,context.user,row.signatures);
      
      return {name:otx.objectInfo.name,description:otx.objectInfo.description,approved:approved,
               originatorHome:row.tx.homeId,originatorAccount:row.tx.user, timeStamp:row.timeStamp};
    })
    
  };
  
  
  
  const showDetailsCallback = (data) => {
    setShowDetailsDialog(false);
    
  };
  
  const infoDialogCallback = (data) => {
    setShowInfoDialog(false);
  };
  
  const signDialogCallback = (data) => {
    setShowSignDialog(false);
    
  };
  
  const showObjectData = (row) => {
    
    const od = bcData[row.tableData.id];
    setCurrentObject(JSON.parse(od.tx.tx));
    setShowDetailsDialog(true);
  };
  
  const setBlockchainInfo = (row) => {
    const od = bcData[row.tableData.id];
    setCurrentTx(od);
    setShowInfoDialog(true);
  };
  
  
  const approve = (row) => {
    
    const rs = bcData[row.tableData.id];
  
    setRowSelected(rs);
    setShowSignDialog(true);
  };
  
  
  
  
  
  return (
    
    <div className={classes.table}>
      <MaterialTable
        title="Incoming Reference Data Objects"
        icons={tableIcons}
        
        
        localization={{
          
          header: {
            actions: 'Actions'
          },
          
        }}
        
        columns={[
          { title: 'Name', field: 'name' },
          { title: 'Description', field: 'description', cellStyle: { wordBreak: 'break-all' }},
          { title: 'Originating Organization', field: 'originatorHome' },
          { title: 'Originating Account', field: 'originatorAccount' },
          { title: 'Approved', field: 'approved' },
          { title: 'Date and Time', field: 'timeStamp' },
        ]}
        data={getTableData()}
        actions={[
          {
            icon: tableIcons.Detail,
            tooltip: 'Details',
            onClick: (event, rowData) => {showObjectData(rowData)}
          },
          {
            icon: tableIcons.Info,
            tooltip: 'Blockchain info',
            onClick: (event, rowData) => {setBlockchainInfo(rowData)}
          },
  
          {
            icon: tableIcons.Sign,
            tooltip: 'Signature',
            onClick: (event, rowData) => {approve(rowData)}
          }
        ]}
        
        options={{
          actionsColumnIndex: -1,
          padding:"dense",
          headerStyle: {
            backgroundColor: '#0091EA',
            color: '#FFF'
          }
        }}
        
        components={{
          Toolbar: props => (
            <div>
              <MTableToolbar {...props} />
            
            </div>
          ),
        }}
      />
      
      
      
      { showDetailsDialog &&
        
        <BizObjectDetails open={showDetailsDialog} callback={showDetailsCallback} data={currentObject}
                         context={context}/>
      }
  
      { showInfoDialog &&
      
        <InfoDialog open={showInfoDialog} callback={infoDialogCallback} data={currentTx}
                        context={context}/>
      }
      
      { showSignDialog &&
        
        <SignDialog open={showSignDialog} callback={signDialogCallback} data={rowSelected}
                     context={context} />
      }
    
    </div>
  )
}



const tableTheme = makeStyles(theme => ({
  table: {
    '& tbody>.MuiTableRow-root:hover': {
      background: '#EEE',
    }
  },
}));

function isApproved (home, account, signatures)  {
  if (!signatures) {
    return false;
  }
  
  for (let i = 0; i <signatures.length; i++) {
    let sign = signatures[i];
    if ((home === sign.tx.partyHome) &&
      (account === sign.tx.partyAccount)) {
      return true;
    }
  }
  return false;
}