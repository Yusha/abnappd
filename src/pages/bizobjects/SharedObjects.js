

import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import dateFormat from 'dateformat';


import MaterialTable, {MTableToolbar} from 'material-table'
import { forwardRef } from 'react';



import BizObjectDetails from './BizObjectDetails';


import axios from 'axios';
import InfoDialog from "./InfoDialog";
import tableIcons from '../common/IconDef';


const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    marginTop:20,
  }
  
  
}));

export default function SharedObjects(props) {
  
  const {context} = props;
  
  
  const classes = useStyles();
  
  return (
    <div className={classes.root}>
      
      <main className={classes.content}>
        
        <div style={{marginTop:20,marginLeft:20,marginRight:20}}>
          
          
          <ObjectsTable context = {context} />
          
          
        </div>
        
       
      </main>
    </div>
  );
}


export function ObjectsTable(props) {
  
  const {context} = props;
  
  const classes = tableTheme();
  
  const [bcData,setBCData] = React.useState([]);
  const [currentObject,setCurrentObject] = React.useState({});
  const [showDetailsDialog, setShowDetailsDialog] = React.useState(false);
  const [currentTx, setCurrentTx] = React.useState({});
  const [showInfoDialog, setShowInfoDialog] = React.useState(false);
  
  
  React.useEffect(()=> {
    
    const url = "http://localhost:4000/object/shared/get?home=" + context.homeId + "&account=" + context.user ;
    axios.get(url).then ((result) => {
      setBCData(result.data);
      
    }).catch ((error) => {
      console.log("Error in getting Accounts   " + JSON.stringify(error));
    })
    
  },[]);
  
  const getTableData = ()=> {
    
    return bcData.map((row,index) => {
      
      let otx = JSON.parse(row.tx.tx);
      let formatDate = new Date(row.timeStamp);
      let newd = dateFormat(formatDate, "DDD, mmmm dS, yyyy, h:MM:ss TT");
      
      return {name:otx.objectInfo.name,home:row.tx.senderHome,encrypted:otx.objectInfo.encrypted,account:row.tx.senderAccount, timeStamp:newd};
    })
    
  };
  
  
  
  const showDetailsCallback = (data) => {
    setShowDetailsDialog(false);
    
  };
  
  const InfoDialogCallback = (data) => {
    setShowInfoDialog(false);
  };
  
  
  
  
  const showObjectData = (row) => {
    
    const od = bcData[row.tableData.id];
    
    setCurrentObject(JSON.parse(od.tx.tx));
    setShowDetailsDialog(true);
  };
  
  const setBlockchainInfo = (row) => {
    
    const od = bcData[row.tableData.id];
    setCurrentTx(od);
    setShowInfoDialog(true);
  };
  
  
  
  
  return (
    
    <div className={classes.table}>
      <MaterialTable
        title="Shared Objects"
        icons={tableIcons}
        
        
        localization={{
          
          header: {
            actions: 'Actions'
          },
          
        }}
        
        
        
        columns={[
          { title: 'Object Name', field: 'name' },
          { title: 'Sender Home', field: 'home' },
          { title: 'Sender Account', field: 'account'},
          { title: 'Encrypted', field: 'encrypted' },
          { title: 'Data and Time', field: 'timeStamp' },
        ]}
        data={getTableData()}
        actions={[
          {
            icon: tableIcons.Detail,
            tooltip: 'Details',
            onClick: (event, rowData) => {showObjectData(rowData)}
          },
          {
            icon: tableIcons.Info,
            tooltip: 'Blockchain info',
            onClick: (event, rowData) => {setBlockchainInfo(rowData)}
          },
          
        ]}
        
        options={{
          actionsColumnIndex: -1,
          padding:"dense",
          headerStyle: {
            backgroundColor: '#0091EA',
            color: '#FFF'
          }
          
        }}
        
        components={{
          Toolbar: props => (
            <div>
              <MTableToolbar {...props} />
            
            </div>
          ),
        }}
      />
      
      
      { showDetailsDialog &&
               <BizObjectDetails open={showDetailsDialog} callback={showDetailsCallback} data={currentObject}
                         context={context}/>
      }
  
      { showInfoDialog &&
      
        <InfoDialog open={showInfoDialog} callback={InfoDialogCallback} data={currentTx}
                        context={context}/>
      }
      
    </div>
  )
}





const tableTheme = makeStyles(theme => ({
  table: {
    '& tbody>.MuiTableRow-root:hover': {
      background: '#EEE',
    }
  },
}));

