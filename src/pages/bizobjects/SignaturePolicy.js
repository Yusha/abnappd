

import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';

import TextField from '@material-ui/core/TextField';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';

const drawerWidth = 240;


const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    marginTop:100,
    align:"center",
    borderRadius:'20%',
    width:'60%',
    display: "inline-block",
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    marginTop:20,
  }
  
}));

export default function SignaturePolicy(props) {
  
  const {context} = props;
  
  const [min, setMin] = React.useState(1);
  const [group, setGroup] = React.useState("Admin");
  const [edit, setEdit] = React.useState(false);
  const [password, setPassword] = React.useState("");
  
  const classes = useStyles();
  
  const signAndSubmit = () => {
  
  
  };
  
  
  return (
    <Box className={classes.root} boxShadow={3}>
      
      <div className={classes.content}>
        
        <div align="center" style={{marginTop:20,marginLeft:20,marginRight:20}}>
          
          <div align="center">
            <Typography variant="h6" color="primary"> Reference Object Signature Policy </Typography>
            
          </div>
  
          <div style={{marginTop:50}}>
            <Grid container direction="row" justify="space-between"
                  alignItems="center">
              
              <Grid xs={4} item >
                
                 Number of Signatures Required
              </Grid>
              <Grid xs={4} item >
                <TextField value={min} type="number" max="5" onChange={(e) => {setMin(e.target.value)}}/>
              </Grid>
              
            </Grid>
  
          </div>
          
          <div style={{marginTop:10}}>
          
            <Grid container direction="row" justify="space-between"
                  alignItems="center">
              <Grid xs={4} item >
                Role Restriction
              </Grid>
              <Grid xs={4} item >
                <TextField value={group} onChange={(e) => {setGroup(e.target.value)}}/>
              </Grid>
            </Grid>
          </div>
          
          <div align="center" style={{marginTop:10, marginBottom:50}}>
  
            {edit &&
            <div style={{marginTop: 50}} align="center">
    
              <TextField id="password" value={password} style={{width:250 }} onChange={(e) => { setPassword(e.target.value)}} label="Key Password"/>
            </div>
    
            }
  
            
            <div style={{marginTop: 50}} align="center">
  
              {!edit &&
              <Button variant="contained" color="primary" style={{marginRight: 40, width:200, borderRadius: 25}}
                      onClick={(e) => {setEdit(true)}}>
                Edit
              </Button>
              }
  
              {edit &&
  
              <Button variant="contained" color="primary" style={{marginRight: 40, borderRadius: 25}}
                      onClick={signAndSubmit}>
                Sign and Approve
              </Button>
              }
              
              {edit &&
  
              < Button variant="contained" color="primary" style={{borderRadius: 25, marginLeft: 5}} onClick={()=>{setEdit(false)}}>
                Cancel
                </Button>
    
              }
  
  
            </div>
    
            
            
            
            
          </div>
          
          
        </div>
      </div>
    </Box>
  );
}
