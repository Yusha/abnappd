import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import TextField from '@material-ui/core/TextField';
import EmailIcon from '@material-ui/icons/Email';
import BusinessIcon from '@material-ui/icons/Business';
import Dialog from '@material-ui/core/Dialog';

import Key from '@material-ui/icons/VpnKey';


import axios from 'axios';

import {GetHash, GetSignature, GetDecrypted, GetEncrypted} from '../../ec/CryptoUtils';



const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
  },
  backButton: {
    marginRight: theme.spacing(1),
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(2),
    
  },
  margin: {
    margin: theme.spacing(1),
  },
}));


function getSteps() {
  return ['Sharee Details', 'Sign and Submit', 'Finalize'];
}



export default function ShareDialog(props) {
  
  const classes = useStyles();
  const {open, context, callback, data} = props;
  
  
  
  
  const [activeStep, setActiveStep] = React.useState(0);
  const [propValues, setPropValues] = React.useState({});
  
  const [status, setSuccess] = React.useState({});
  
  const steps = getSteps();
  
  
  
  const saveAndSubmit = (pass)=> {
    
    const password = pass;
    
    propValues.objectId = data.tx.hash;
    
    let objectTx = JSON.parse(data.tx.tx);
    
    let objectInfo = objectTx.objectInfo;
    propValues.objectInfo = objectInfo;
    
    
    if (objectInfo.encrypted) {
      
      let clearText = GetDecrypted(props.context.encryptedPK,password,objectTx.value);
      propValues.value = GetEncrypted(propValues.shareePK,clearText);
      
    } else {
      propValues.objectInfo = objectInfo;
    }
    
    
    const sTx = JSON.stringify(propValues);
    
    const hashOf = GetHash(sTx);
    
    const signature = GetSignature(props.context.encryptedPK,password,hashOf);
    
    // decrypt Value if object is encrypted and send...
    
    
    
    
    const chainTx = {
      action:"share",
      senderHome: props.context.homeId,
      senderAccount: props.context.user,
      senderPK:props.context.publicKey,
      recipientHome: propValues.homeId,
      recipientAccount: propValues.account,
      recipientPK:propValues.shareePK,
      hash:hashOf,
      signature:signature,
      tx:sTx
    };
    
    const url = "http://127.0.0.1:4000/object/share";
    
    
    axios.post(url,chainTx).then (result => {
      console.log("Share Tx sent to the network" + result);
      setSuccess({status:"Transaction Succeeded", id:hashOf});
      setActiveStep(prevActiveStep => prevActiveStep + 1);
      
    }).catch (error => {
      console.log("Error in sending the Share tx to the Network... " + JSON.stringify(error));
      setSuccess({status:"Transaction Failed", id:hashOf});
      setActiveStep(prevActiveStep => prevActiveStep + 1);
      
    });
  };
  
  const  handleNext = () => {
    
    setActiveStep(prevActiveStep => prevActiveStep + 1);
  };
  
  const handleBack = () => {
    setActiveStep(prevActiveStep => prevActiveStep - 1);
  };
  
  const handleReset = () => {
    setActiveStep(0);
  };
  
  const savePropsAndNext = (propValues) => {
    setPropValues(propValues);
    handleNext();
  };
  
  const complete = () => {
    console.log("Please send me to the parent...");
    callback(false,"New account invitation Sent successfully");
  };
  
  const handleClose = () => {
    callback();
  };
  
  
  return (
  
    <Dialog onClose={handleClose} open={open} fullWidth maxWidth="lg" classes={{paper: classes.dialog }} style={{marginTop:50, marginLeft:50}}>
      
      <div align="center" style={{marginTop:50}}>
        <Typography variant="h5" color="secondary" > Share Object </Typography>
        <Typography> Object Name : {data.objectId} </Typography>
        <Typography> Object Owner : {data.homeId} / {data.user} </Typography>
      </div>
      
      
      <Grid className={classes.root}>
        <Stepper activeStep={activeStep} alternativeLabel>
          {steps.map(label => (
            <Step key={label}>
              <StepLabel>{label}</StepLabel>
            </Step>
          ))}
        </Stepper>
      
        <Grid container justify = "center">
          <div className={classes.instructions}>
          
            {activeStep === 0 &&
            <ChooseProps savePropsAndNext = {savePropsAndNext}/>
            }
          
            
            {activeStep === 1 &&
            <SignAndSubmit backCallback = {handleBack} signAndSubmitCallback = {saveAndSubmit}/>
            
            }
          
            {activeStep === 2 &&
            <Finalize completeCallback = {complete} status = {status}/>
            
            }
          </div>
        </Grid>
      
        <Grid container justify = "center">
          <Typography className={classes.instructions}>
        
          </Typography>
      
        </Grid>
        <Divider/>
    
      </Grid>
    </Dialog>
  );
}


function ChooseProps(props) {
  
  const {savePropsAndNext} = props;
  const classes = useStyles();
  
  const [homeId, setHomeId] = React.useState('');
  const [account, setAccount] = React.useState('');
  const [error, setError] = React.useState("");
  
  
  const save = () => {
    const url = "http://localhost:4000/api/account?home=" + homeId + "&account=" +account  ;
  
    axios.get(url).then ((result) => {
      const pk = result.data.publicKey;
      
      if (pk && pk.length> 16) {
        savePropsAndNext({homeId:homeId,account:account,shareePK:pk});
      } else {
         setError("Either Home or Account does not exist");
         
      }
    
      
    }).catch ((error) => {
      console.log("Error in getting Accounts   " + JSON.stringify(error));
      setError("Either Home or Account does not exist" + error);
    });
    
  };
  
  
  
  
  return (
    
    <div>
      <div className={classes.margin}>
  
        <div className={classes.margin}>
    
          <Grid container spacing={1} alignItems="flex-end">
            <Grid item>
              <BusinessIcon/>
            </Grid>
            <Grid item>
              <TextField id="home" value={homeId} multiline onChange={(e) =>{setHomeId(e.target.value)}} label="Home Id of the Sharee"/>
            </Grid>
          </Grid>
        </div>
  
  
        <div className={classes.margin}>
          <Grid container spacing={1} alignItems="flex-end">
            <Grid item>
              <EmailIcon/>
            </Grid>
            <Grid item>
              <TextField id="account" value={account}  onChange={(e) =>{setAccount(e.target.value)}} label="Account of the Sharee "/>
            </Grid>
          </Grid>
        </div>
  
        <div className={classes.margin}>
          <Typography variant="h6" color="secondary">{error} </Typography>
        </div>
  
  
  
  
        <div className={classes.margin} style={{marginTop:20}}>
          <Grid container spacing={1} alignItems="flex-end">
            
            <Grid container justify = "center">
              
              <Grid container justify = "center">
                
                <Grid>
                  <Button
                    disabled
                    className={classes.backButton}
                  >
                    Back
                  </Button>
                  
                  <Button variant="contained" color="primary" onClick={save}>
                    Next
                  </Button>
                </Grid>
              </Grid>
            
            </Grid>
          
          
          </Grid>
        </div>
      
      </div>
    </div>
  );
}



function SignAndSubmit(props) {
  
  const classes = useStyles();
  const {backCallback, signAndSubmitCallback} = props;
  
  const [password, setPassword] = React.useState('');
  
  const setValue = (e) => {
    setPassword(e.target.value);
  };
  
  
  return (
    
    <div>
      
      <div className={classes.margin}>
        <Grid container spacing={1} alignItems="flex-end">
          <Grid item>
            <Key/>
          </Grid>
          <Grid item>
            <TextField id="password" style ={{width:300}} value={password} onChange={setValue} label="Enter Password for the private key"/>
          </Grid>
        </Grid>
        
        <Grid container spacing={1} alignItems="flex-end" style={{marginTop:30}}>
          
          <Grid container justify = "center">
            
            <Grid container justify = "center">
              
              <Grid>
                <Button
                  onClick={backCallback}
                  className={classes.backButton}
                >
                  Back
                </Button>
                
                <Button variant="contained" color="primary" onClick={()=>signAndSubmitCallback(password)}>
                  Next
                </Button>
              </Grid>
            </Grid>
          </Grid>
        
        </Grid>
      
      
      </div>
    </div>
  
  );
}


function Finalize(props) {
  
  const {completeCallback} = props;
  
  return (
    
    <div>
      
      <div style={{marginTop: 20, width:600, align:"center"}}>
        <Typography varivariant="h5" gutterBottom color="primary">
          Transaction Status {props.status.status}
        </Typography>
        
        <Grid container spacing={1} alignItems="flex-end">
          
          <Grid container justify = "center">
            
            <Grid container justify = "center">
              
              <Grid>
                <Button variant="contained" color="primary" onClick={completeCallback}>
                  Finish
                </Button>
              </Grid>
            </Grid>
          </Grid>
        
        </Grid>
      </div>
    
    </div>
  
  )
}