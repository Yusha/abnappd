
import React, { useEffect,useStates } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import dateFormat from 'dateformat';



import MaterialTable from 'material-table';
import { forwardRef } from 'react';

import axios from 'axios';

import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';

import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';

import tableIcons from '../common/IconDef';






export default function SentBox(props) {
  
  const { context } = props;
  
  const classes = tableTheme();
  
  
  const [displayItem, setDisplayItem] = React.useState(false);
  const [snackOpen, setSnackOpen] = React.useState(false);
  const [password, setPassword] = React.useState("");
  const [error, setError] = React.useState(false);
  const [approved, setApproved] = React.useState(""); // flag to avoid infinite loop in useEffect
  const [tableData, setTableData] = React.useState([]);
  const [rowSelected, setRowSelected] = React.useState(-1);
  const [currentInvitee, setCurrentInvitee] = React.useState("");
  
  
  
  useEffect(()=> {
    
    const url = "http://localhost:4000/message/getMessages?home=" + context.homeId + "&account=" + context.user;
    
    axios.get(url).then ((result) => {
      setTableData(result.data);
      
    }).catch ((error) => {
      console.log("Error in getting Internal Pending Invitations data  " + JSON.stringify(error));
    })
    
  },[approved]);
  
  
  
  const handleSnackClose = ()=> {
    setSnackOpen(false);
  };
  
  
  const getDataSet = () => {
    const data = tableData.map((row, index) => {
      let formatDate = new Date(row.timeStamp);
      let newd = dateFormat(formatDate, "DDD, mmmm dS, yyyy, h:MM:ss TT");
      return {recipientHomeId: row.recipientHomeId, recipientAccount: row.recipientAccount,timeStamp:row.timeStamp, status: "Sent"};
    });
    return data;
  };
  
  const rowClicked = (e,row) => {
    
    const index = tableData[row.tableData.id];
    setRowSelected(index);
    setCurrentInvitee(index.invitee);
    setDisplayItem(true);
    
    
  };
  
  const cancelDislayTask = ()=> {
    setDisplayItem(false);
  };
  
  
  const setValue = (e) => {
    e.preventDefault();
    setPassword(e.target.value);
  };
  
  
  const signAndSubmit = () => {
  
  };
  
  return (
    
    <div className={classes.table}>
      <MaterialTable
        title="Sent Messages"
        icons={tableIcons}
        
        
        localization={{
          header: {
            actions: 'View'
          },
        }}
        
        columns={[
          { title: 'Recipient Home Id', field: 'recipientHomeId' },
          { title: 'Recipient Account', field: 'recipientAccount' },
          { title: 'Time Sent', field: 'timeStamp' },
          { title: 'Status', field: 'status' },
        
        ]}
        data={getDataSet()}
        
        actions={[
          rowData => ({
            icon: tableIcons.Lock,
            tooltip: 'View message',
            onClick: rowClicked
          })
        ]}
        
        options={{
          actionsColumnIndex: 4,
          padding:"dense",
          headerStyle: {
            backgroundColor: '#0091EA',
            color: '#FFF'
          }
          
        }}
        
      />
      
      { displayItem &&
      
      <div align="center" style={{marginTop:20}}>
        
        <Grid item xs={6} sm={6}>
          <Typography variant="h6" color="primary">
            Approve account  : {currentInvitee}
          </Typography>
        </Grid>
        
        <div style={{marginTop:20}}>
          
          <TextField id="homeId" value={password} onChange={setValue} label="Enter your password" />
          
          <Button variant="contained" color="primary" style={{marginRight:40,borderRadius:25}} onClick={signAndSubmit}>
            Sign and Submit
          </Button>
          <Button variant="contained" color="primary" style={{borderRadius:25}} onClick={cancelDislayTask} >
            Cancel
          </Button>/
          
          <Snackbar open={snackOpen} autoHideDuration={6000} onClose={handleSnackClose}>
            <Alert onClose={handleSnackClose} severity={error?"error" : "success"}>
              Transaction submitted successfully....
            </Alert>
          </Snackbar>
        
        </div>
      </div>
      
      }
    
    </div>
  )
}



const tableTheme = makeStyles(theme => ({
  table: {
    '& tbody>.MuiTableRow-root:hover': {
      background: '#EEE',
    }
  },
  body: {
    textColor:"primary"
  }
}));

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}
