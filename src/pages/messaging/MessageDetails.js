import React from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';

import Dialog from '@material-ui/core/Dialog';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';

import {Editor} from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { EditorState, convertFromRaw } from 'draft-js';

import {GetDecryptedPK} from "../../ec/CryptoUtils";
import {decryptECIES} from '../../ec/Encryption';


export default function MessageDetails(props) {
  
  
  const { data,callback,open,context } = props;
  
  const [password, setPassword] = React.useState("");
  
  const [askPassword, setAskPassword] = React.useState(true);
  const [showMessage, setShowMessage] = React.useState(false);
  const [decrypted, setDecrypted] = React.useState({});
  
  
  const classes = dialogStyles();
  
  const handleClose = (action) => {
    callback(action);
  };
  
  const decryptMessage = () => {
  
    const privateKey = GetDecryptedPK(context.encryptedPK, password);
    const clearText = decryptECIES(privateKey, data);
    setDecrypted(JSON.parse(clearText));
    setShowMessage(true);
    setAskPassword(false);
  };
  
  
  const getSubject = () => {
    
    return (
      
      <div>
        <div style={{marginTop:50}}>
          <Typography variant="h6"> Subject </Typography>
        </div>
  
        <div>
    
          <TextField
            style={{width: 500, margin: 2, marginTop: 10}}
      
            inputProps={{
              readOnly: true,
            }}
            value={decrypted.subject}
            
          />
  
        </div>
        
      </div>
      
      
    )
    
  };
  
  const getBody = () => {
  
    const editorState = EditorState.createWithContent(convertFromRaw(decrypted.content));
    
    return (
      
      <div>
        <div className={classes.margin} style={{marginTop:1,width:500}} >
          <Editor
            editorState={editorState}
            toolbarClassName="toolbarClassName"
            wrapperClassName="wrapperClassName"
            editorClassName="editorClassName"
            toolbarHidden
            readOnly
            editorStyle={{backgroundColor:"#FFFFFF",height:"auto"}}
          />
  
        </div>
        
      </div>
    )
    
  };
  
  
  return (
    <Dialog onClose={handleClose} open={open} fullWidth maxWidth="lg" classes={{paper: classes.dialog }} >
      
      <div className={classes.content} style={{width:800,align:"center",marginLeft:50}}>
        
        <div className={classes.margin}>
          <Typography color="primary" variant="h4" align="center"> Message Details</Typography>
        </div>
        
        {askPassword &&
        <div align="center" style={{width: 800, marginTop: 100}}>
          <div>
            <Typography variant="h6"> Encrypted Message. please enter password for your private key to unlock the
              message </Typography>
          </div>
    
          <div>
      
            <TextField
              style={{width: 500, margin: 2, marginTop: 10}}
              value={password}
              onChange={(e) => {
                setPassword(e.target.value)
              }}
              label="Password to decrypt private key"
            />
          </div>
    
    
          <Button onClick={decryptMessage} className={classes.shape} align="left" variant="contained" color="primary"
                  style={{width: 200, height: 30, marginBottom: 100}}>Decrypt Message </Button>
  
        </div>
    
        }
  
        {showMessage &&
        <div>
    
          {getSubject()}
    
          {getBody()}
    
          <div align="center" style={{width: 800, marginTop: 100, marginBottom: 100}}>
            <Button onClick={() => {
              callback(true)
            }} className={classes.shape} align="left" variant="contained" color="primary"
                    style={{width: 100, height: 30, marginBottom: 100}}>Close </Button>
    
    
          </div>
  
        </div>
    
        }
  
        
        
      </div>
    
    
    </Dialog>
  );
}

const dialogStyles = makeStyles({
  dialog: {
    position: 'absolute',
    left: 10,
    top: 50
  },
  table: {
    minWidth: 700,
  },
});