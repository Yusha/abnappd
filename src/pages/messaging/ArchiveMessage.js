import React from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';

import Dialog from '@material-ui/core/Dialog';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';

import {Editor} from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';

import axios from 'axios';
import {GetEncrypted,GetSignature,GetHash} from '../../ec/CryptoUtils';

export default function ArchiveMessage(props) {
  
  
  const { data,callback,open, context } = props;
  
  const [password, setPassword] = React.useState("");
  
  const classes = dialogStyles();
  
  
  const archiveMessage = () => {
  
    const toSign = {
      messageId:data,
    };
  
    const sBody = JSON.stringify(toSign);
  
    const hash = GetHash(sBody);
  
    const signature = GetSignature(context.encryptedPK,password,hash);
    
    const tx = {
      senderHomeId: context.homeId,
      senderName: context.name,
      senderAccount:context.user,
      senderPublicKey:context.publicKey,
      messageId: data,
      timeStamp:new Date(),
      hash:hash,
      signature:signature
    };
    
    const url = "http://localhost:4000/message/archive";
    
    axios.post(url,tx).then ((result) => {
    
      if (result.data.success) {
        // message posted successfully
        callback({cancel:false,success:true});
      } else {
        console.log("Error, hang on...");
        callback();
        // callback({cancel:false,success:false, error:result.error});
      }
    
    }).catch ((error) => {
      console.log("Error in posting a message");
    })
  };
  
  
  
  return (
    <Dialog onClose={()=>{callback()}} open={open} fullWidth maxWidth="lg" classes={{paper: classes.dialog }} >
      
      <div className={classes.content} style={{width:800,align:"center",marginLeft:50, marginTop:100}}>
        
        <div className={classes.margin}>
          <Typography color="primary" variant="h4" align="center"> Archive Message</Typography>
        </div>
        
        
        <div align="center" style={{width: 800, marginTop: 100}}>
          
          <div>
      
            <TextField
              style={{width: 500, margin: 2, marginTop: 10}}
              value={password}
              onChange={(e) => {
                setPassword(e.target.value)
              }}
              label="Password to for your private key"
            />
          </div>
    
        </div>
        
        <div>
    
          <div align="center" style={{width: 800, marginTop: 100, marginBottom: 100}}>
  
            <Button onClick={archiveMessage} className={classes.shape} align="left" variant="contained" color="primary"
                    style={{width: 100, height: 30, marginBottom: 100}}>Archive </Button>
            
            <Button onClick={() => { callback(true)}} className={classes.shape} align="left" variant="contained" color="primary"
                    style={{width: 100, height: 30, marginBottom: 100}}>Close </Button>
            
          </div>
  
        </div>
    
      </div>
    
    </Dialog>
  );
}

const dialogStyles = makeStyles({
  dialog: {
    position: 'absolute',
    left: 10,
    top: 50
  },
  table: {
    minWidth: 700,
  },
});