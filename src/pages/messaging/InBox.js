
import React, { useEffect,useStates } from 'react';
import { makeStyles } from '@material-ui/core/styles';

import dateFormat from 'dateformat';

import MaterialTable from 'material-table';


import axios from 'axios';

import MuiAlert from '@material-ui/lab/Alert';
import MessageDetails from "./MessageDetails";
import tableIcons from '../common/IconDef';
import ArchiveMessage from './ArchiveMessage';



export default function InBox(props) {
  
  const { context } = props;
  
  const classes = tableTheme();
  
  
  
  const [approved, setApproved] = React.useState(""); // flag to avoid infinite loop in useEffect
  const [tableData, setTableData] = React.useState([]);
  
  const [rowBody, setRowBody] = React.useState({});
  
  const [showMessage, setShowMessage] = React.useState(false);
  const [archiveMessage, setArchiveMessage] = React.useState(false);
  
  
  useEffect(()=> {
    
    const url = "http://localhost:4000/message/inbox?home=" + context.homeId + "&account=" + context.user;
    
    axios.get(url).then ((result) => {
      setTableData(result.data);
      
    }).catch ((error) => {
      console.log("Error in getting Internal Pending Invitations data  " + JSON.stringify(error));
    })
    
  },[approved]);
  
  const dialogCallback = () => {
    setShowMessage(false);
    setApproved(rowBody);
  };
  
  
  const getDataSet = () => {
    return tableData.map((row, index) => {
      let formatDate = new Date(row.timeStamp);
      let newd = dateFormat(formatDate, "ddd, mmmm dS, yyyy, h:MM:ss TT");
      return {sender:row.senderName,senderHomeId: row.senderHomeId, senderAccount: row.senderAccount,timeStamp:row.timeStamp, status: "Sent"};
    });
  };
  
  const showDetails = (e,row) => {
    
    const rowData = tableData[row.tableData.id];
    setRowBody(rowData.body);
    setShowMessage(true);
  };
  
  const onArchiveMessage = (e,row) => {
    const rowData = tableData[row.tableData.id];
    setRowBody(rowData._id);
    setArchiveMessage(true);
  };
  
  const archiveMessageCallback = () => {
    setArchiveMessage(false);
  };
  
  
  return (
    
    <div className={classes.table}>
      
      <div>
        <MaterialTable
        title="Incoming Messages"
        icons={tableIcons}
        
        
        localization={{
          header: {
            actions: 'Actions'
          },
        }}
        
        columns={[
          { title: 'Sender Name', field: 'sender' },
          { title: 'Sender Home', field: 'senderHomeId' },
          { title: 'Sender Account', field: 'senderAccount' },
          { title: 'Date', field: 'timeStamp',defaultSort:'desc'  },
          
        ]}
        data={getDataSet()}
        
        actions={[
  
          {
            icon: tableIcons.ViewEye,
            tooltip: 'View Details',
            onClick: (event, rowData) => {showDetails(event,rowData)}
          },
          {
            icon: tableIcons.Archive,
            tooltip: 'Archive',
            onClick: (event, rowData) => {onArchiveMessage(event,rowData)}
          }
          
        ]}
        
        options={{
          
          padding:"dense",
          headerStyle: {
            backgroundColor: '#0091EA',
            color: '#FFF'
          },
          pageSize:10,
          pageSizeOptions:[10,15]
          
        }}
        
      />
      
      </div>
      
      <div>
        {showMessage &&
        <MessageDetails context={context} open={showMessage} data={rowBody} callback={dialogCallback}/>
    
        }
        
        {archiveMessage &&
        <ArchiveMessage context = {context} open = {archiveMessage} data = {rowBody} callback = {archiveMessageCallback }/>
        }
        
      </div>
    
    </div>
  )
}


const tableTheme = makeStyles(theme => ({
  table: {
    '& tbody>.MuiTableRow-root:hover': {
      background: '#EEE',
    }
  },
  body: {
    textColor:"primary"
  }
}));
function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}
