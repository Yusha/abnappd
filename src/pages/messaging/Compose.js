import React from 'react';

import { makeStyles } from '@material-ui/core/styles';

import InputAdornment from '@material-ui/core/InputAdornment';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

import {Editor} from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { EditorState, convertToRaw } from 'draft-js';


import axios from 'axios';
import {GetEncrypted,GetSignature,GetHash} from '../../ec/CryptoUtils';

const drawerWidth = 200;

export default function Compose (props) {
  
  const {context, composeCallback} = props;
  
  
  const [to, setTo] = React.useState("");
  const [subject, setSubject] = React.useState("");
  const [password, setPassword] = React.useState("");
  const [recipientHomeId,setRecipientHomeId] = React.useState("");
  const [recipientAccount,setRecipientAccount] = React.useState("");
  
  const [recipientPK,setRecipientPK] = React.useState("");
  
  const [editorState, setEditorState] = React.useState(EditorState.createEmpty());
  
  
  const [save, setSave] = React.useState(false);
  
  
  
  const classes = useStyles();
  
  const onEditorStateChange = (s)=> {
    setEditorState(s);
    
  };
  
  const onSave = () => {
    
    //Todo: Handle negative cases
    
    const index = to.indexOf("/");
    
    const homeId = to.substring(0,index);
    const account = to.substring(index+1,to.length);
    
    setRecipientHomeId(homeId);
    setRecipientAccount(account);
    
    const url = "http://localhost:4000/api/account?home=" + homeId + "&account=" +account;
    
    axios.get(url).then ((result) => {
      
      setRecipientPK(result.data.publicKey);
      setSave(true);
      
    }).catch ((error) => {
      console.log("Error in getting Accounts   " + JSON.stringify(error));
    });
    setSave(true);
  };
  
  const cancelThis = (e) => {
    composeCallback({cancel:true});
  };
  
  const setFieldValue = (e) => {
    
    const id = e.target.id;
    const value = e.target.value;
    
    if (id === "subject") {
      setSubject(value);
    } else if (id === "to") {
      setTo(value);
      
    } else if (id === "password") {
      setPassword(value);
    }
    else {
      console.log("Wrong id in the form field.. should not hapen...");
    }
  };
  
  const onSubmit = () => {
    
    const raw = convertToRaw(editorState.getCurrentContent());
    
    
    const body = {
      senderHomeId:context.homeId,
      senderAccount:context.user,
      senderName: context.name,
      recipientHomeId:recipientHomeId,
      recipientAccount:recipientAccount,
      subject: subject,
      content: raw
    };
    
    const sBody = JSON.stringify(body);
    
    const hash = GetHash(sBody);
    
    const signature = GetSignature(context.encryptedPK,password,hash);
    
    const encryptedBody = GetEncrypted(recipientPK,sBody);
    
    const tx = {
      header: {
        senderHomeId: context.homeId,
        senderName: context.name,
        senderAccount:context.user,
        senderPublicKey:context.publicKey,
        recipientHomeId:recipientHomeId,
        recipientAccount:recipientAccount,
        timeStamp:new Date(),
        hash:hash,
        signature:signature
      },
      body: encryptedBody
    };
    
    const url = "http://localhost:4000/message/sendMessage";
    axios.post(url,tx).then ((result) => {
      
      if (result.data.success) {
        // message posted successfully
        composeCallback({cancel:false,success:true});
      } else {
        composeCallback({cancel:false,success:false, error:result.error});
      }
      
    }).catch ((error) => {
      console.log("Error in posting a message");
    })
    
  };
  
  
  
  
  return (
    
    
    <div className={classes.content} style={{width:800,align:"center"}}>
      
      <div className={classes.margin}>
        <Typography color="primary" variant="h5" align="center"> Compose New Message</Typography>
      </div>
      
      
      
      <div className={classes.margin} align="center" style={{marginTop:30}}>
        <TextField
          
          style={{width:500,margin:2}}
          value = {to}
          color="primary"
          placeholder="HomeId/accountId.."
          id ="to"
          onChange={setFieldValue}
          InputProps={{
            startAdornment: <InputAdornment position="start">@To...</InputAdornment>,
            
          }}
        />
        
        <TextField
          
          style={{width:500,margin:2,marginTop:10}}
          placeholder="Subject"
          color="primary"
          value = {subject}
          id ="subject"
          onChange={setFieldValue}
        
        />
        
        <Editor
          editorState={editorState}
          toolbarClassName="toolbarClassName"
          wrapperClassName="wrapperClassName"
          editorClassName="editorClassName"
          onEditorStateChange={onEditorStateChange}
          editorStyle={{backgroundColor:"#FFFFFF",height:200}}
        />
        
        {save &&
        <TextField
          
          style={{width:400,margin:2,marginTop:20}}
          placeholder="password for private key"
          value = {password}
          id = "password"
          onChange={setFieldValue}
          color="primary"
        />
        
        }
      
      </div>
      
      <div align="center" style={{marginTop:60}}>
        
        {!save &&
        <Button onClick={onSave} className={classes.shape} align="left" variant="contained" color="primary"
                style={{width: 100, height: 30}}>Save </Button>
        }
        {save &&
        
        <Button onClick={onSubmit} className={classes.shape} align="left" variant="contained" color="primary"
                style={{width: 100, height: 30}}>Submit </Button>
        }
        <Button onClick = {cancelThis} className={classes.shape}  variant="contained" align = "right" style={{width:100,height:30, marginLeft:20}} color="secondary" > Cancel </Button>
      
      </div>
    
    </div>
  );
}



const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  shape: {
    borderRadius: 25,
  },
  small: {
    width: theme.spacing(3),
    height: theme.spacing(3),
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  inputColor:{
    color:'blue'
  },
  
  stickToBottom: {
    width: '60%',
    position: 'fixed',
    bottom: 0,
    align:"center",
    marginRight:'19%'
  },
  drawerPaper: {
    width: drawerWidth,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    marginTop:20,
    flexDirection: 'column',
  },
  nested: {
    paddingLeft: theme.spacing(4),
  },
  
  toolbar: theme.mixins.toolbar,
}));
