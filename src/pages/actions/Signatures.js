
import React, { useEffect,useStates } from 'react';
import { makeStyles } from '@material-ui/core/styles';


import MaterialTable from 'material-table';
import { forwardRef } from 'react';



import axios from 'axios';


import ContractDetails from '../contracts/ContractDetails';
import SignDialog from "./SignDialog";
import tableIcons from '../common/IconDef';



export default function ContractSignatures(props) {
  
  const { context } = props;
  
  const classes = tableTheme();
  
  const [approved, setApproved] = React.useState(""); // flag to avoid infinite loop in useEffect
  const [tableData, setTableData] = React.useState([]);
  const [rowSelected, setRowSelected] = React.useState(-1);
  
  const [detailsDialog, setDetailsDialog] = React.useState(false);
  const [signDialog, setSignDialog] = React.useState(false);
  
  const [contractData, setContractData] = React.useState({});
  const [oData, setOdata] = React.useState({});
  
  
  
  useEffect(()=> {
    
    const url = "http://localhost:4000/actions/signActions?homeId=" + context.homeId +"&accountId=" + context.user;
    
    axios.get(url).then ((result) => {
      setTableData(result.data);
      
    }).catch ((error) => {
      console.log("Error in getting User Tasks in action center  " + JSON.stringify(error));
    })
    
  },[approved]);
  
  const getDataSet = () => {
    const data = tableData.map((row, index) => {
      
      return {name: row.name, originator: row.originator, timeStamp:row.timeStamp,completed: row.completed};
    });
    return data;
  };
  
  const showDetail = (e,row) => {
    const rowSelected = tableData[row.tableData.id];
    const id = rowSelected.contractId;
    
    
    const url = "http://localhost:4000/contracts/contracts/contract?contractId=" + id;
    
    axios.get(url).then((result) => {
      if (result) {
        setContractData(result.data);
        setDetailsDialog(true)
      }
    });
  };
  
  
  
  const detailsDialogCallback = () => {
    setDetailsDialog(false);
  };
  
  const approve = (e,row) => {
    
    const rs = tableData[row.tableData.id];
    setRowSelected(rs);
    const id = rs.contractId;
    const url = "http://localhost:4000/contracts/contracts/contract?contractId=" + id;
    
    axios.get(url).then((result) => {
      if (result) {
        setContractData(result.data);
        setSignDialog(true);
      }
    });
  };
  
  
  
  const signDialogCallback = ()=> {
    setSignDialog(false);
  };
  
  return (
    
    <div className={classes.table}>
      <MaterialTable
        title="Contract Signatures"
        icons={tableIcons}
        
        
        localization={{
          header: {
            actions: 'Actions'
          },
        }}
        
        columns={[
          { title: 'Contract Name', field: 'name' },
          { title: 'Originator', field: 'originator' },
          { title: 'Date and Time', field: 'timeStamp' },
          { title: 'Completed', field: 'completed' },
        
        ]}
        data={getDataSet()}
        
        actions={[
          {
            icon: tableIcons.Attach,
            tooltip: 'View',
            onClick: (event, rowData) => {showDetail(event,rowData)}
          },
          {
            icon: tableIcons.Sign,
            tooltip: 'Signature',
            onClick: (event, rowData) => {approve(event,rowData)}
          }
        ]}
        
        options={{
          
          padding:"dense",
          headerStyle: {
            color: 'white',
            background:'#0091EA',
            fontSize:20,
            fontFamily:"Roboto"
          },
          rowStyle: {
            color: 'black'
          }
          
        }}
      
      />
      
      {detailsDialog &&
      <ContractDetails data = {contractData} open = {detailsDialog} callback = {detailsDialogCallback}/>
      }
      
      { signDialog &&
      <SignDialog open = {signDialog} contractId={rowSelected.contractId} callback = {signDialogCallback} context = {context}/>
      }
    
    </div>
  )
}


const tableTheme = makeStyles(theme => ({
  table: {
    '& tbody>.MuiTableRow-root:hover': {
      background: '#EEE',
    }
  },
  body: {
    textColor:"primary"
  }
}));
