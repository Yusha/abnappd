
import React from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';

import Dialog from '@material-ui/core/Dialog';
import Typography from '@material-ui/core/Typography';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';


export default function InvitationDetail(props) {
  
  const {data,callback,open } = props;
  const classes = dialogStyles();
  
  const claimedData = JSON.parse(data.claimedTx.tx);
  
  
  const handleClose = (action) => {
    callback(action);
  };
  
  
  
  
  const StyledTableCell = withStyles((theme) => ({
    
    
    head: {
      backgroundColor: "#0091EA",
      color: "white",
      fontSize:20,
      wordWrap: 'break-word'
    },
    body: {
      fontSize: 14,
      maxWidth:300,
      wordWrap: 'break-word'
    },
  }))(TableCell);
  
  const StyledTableRow = withStyles((theme) => ({
    root: {
      '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
      },
    },
  }))(TableRow);
  
  
  
  return (
    <Dialog onClose={handleClose} open={open} fullWidth maxWidth="lg" classes={{paper: classes.dialog }} style={{marginTop:50, marginLeft:50}}>
      
      <div className={classes.content} style={{width:800,marginLeft:50}}>
        
        <div style={{marginTop:50}}>
          <Typography color="primary" variant="h5" align="center"> {data.name} </Typography>
        </div>
        
        <div style={{marginTop:50}}>
          
          <div>
            <Typography variant="h6"> Account Approval Task Details </Typography>
          </div>
          
         
         
        </div>
        
        <Divider/>
        
        
        <div className={classes.margin} style={{marginTop:30}}>
          
          
          <TableContainer component={Paper}>
            <Table className={classes.table} aria-label="customized table">
              <TableHead>
                <TableRow>
                  <StyledTableCell>Invitation Property </StyledTableCell>
                  <StyledTableCell align="right">Value </StyledTableCell>
                
                </TableRow>
              </TableHead>
              <TableBody>
                
                <StyledTableRow key="invitee">
                  <StyledTableCell component="th" scope="row">
                    Invitee Email
                  </StyledTableCell>
                  <StyledTableCell align="right">{data.invitee}</StyledTableCell>
                </StyledTableRow>
                <StyledTableRow key="inviter">
                  <StyledTableCell component="th" scope="row">
                    Inviter Email
                  </StyledTableCell>
                  <StyledTableCell align="right">{data.tx.user}</StyledTableCell>
                </StyledTableRow>
                <StyledTableRow key="ipk">
                  <StyledTableCell component="th" scope="row">
                    Inviter Public Key
                  </StyledTableCell>
                  <StyledTableCell align="right">{data.tx.publicKey}</StyledTableCell>
                </StyledTableRow>
  
                <StyledTableRow key="claimed">
                  <StyledTableCell component="th" scope="row">
                    Invitation Claimed
                  </StyledTableCell>
                  <StyledTableCell align="right">True</StyledTableCell>
                </StyledTableRow>
  
                <StyledTableRow key="name">
                  <StyledTableCell component="th" scope="row">
                    Invitee name
                  </StyledTableCell>
                  <StyledTableCell align="right">{claimedData.name}</StyledTableCell>
                </StyledTableRow>
                <StyledTableRow key="phone">
                  <StyledTableCell component="th" scope="row">
                    Invitee Phone
                  </StyledTableCell>
                  <StyledTableCell align="right">{claimedData.phone}</StyledTableCell>
                </StyledTableRow>
  
                <StyledTableRow key="pkk">
                  <StyledTableCell component="th" scope="row">
                    Invitee Phone
                  </StyledTableCell>
                  <StyledTableCell align="right">{claimedData.publickKey}</StyledTableCell>
                </StyledTableRow>



              </TableBody>
            </Table>
          </TableContainer>
  
          <Divider/>
          
          
          
          <div align="center" style={{marginTop:50,marginBottom:50}}>
            
            <Button onClick={handleClose} className={classes.shape} align="left" variant="contained" color="primary"
                    style={{width: 100, height: 30}}>Close</Button>
            
          </div>
        </div>
      </div>
    
    
    </Dialog>
  );
}


const dialogStyles = makeStyles({
  dialog: {
    position: 'absolute',
    left: 50,
    top: 50
  },
  table: {
    minWidth: 700,
  },
});

