
import React, { useEffect,useStates } from 'react';
import { makeStyles } from '@material-ui/core/styles';


import MaterialTable from 'material-table';



import axios from 'axios';

import InvitationDetail from './InvitationDetails';
import AccountApprovalDialog from "./AccountApprovalDialog";
import tableIcons from '../common/IconDef';


export default function AccountsApprovals(props) {
  
  const { context } = props;
  
  const classes = tableTheme();
  
  const [approved, setApproved] = React.useState(""); // flag to avoid infinite loop in useEffect
  const [tableData, setTableData] = React.useState([]);
  const [rowSelected, setRowSelected] = React.useState(-1);
  
  const [detailsDialog, setDetailsDialog] = React.useState(false);
  const [approveDialog, setApproveDialog] = React.useState(false);
  
  const [oData, setOdata] = React.useState({});
  
  
  
  useEffect(()=> {
  
    const url = "http://localhost:4000/home/pendingInvitations?home=" + context.homeId;
    
    axios.get(url).then ((result) => {
      setTableData(result.data);
      
    }).catch ((error) => {
      console.log("Error in getting User Tasks in action center  " + JSON.stringify(error));
    })
    
  },[approved]);
  
  
  
  const getDataSet = () => {
    return tableData.map((row, index) => {
      return {account: row.invitee, type: row.action, timeStamp:row.timeStamp,approved: row.approved};
    });
    
  };
  
  const showDetail = (e,row) => {
    const rowSelected = tableData[row.tableData.id];
    setOdata(rowSelected);
    setDetailsDialog(true);
  };
  
  
  
  const detailsDialogCallback = () => {
    setDetailsDialog(false);
  };
  
  const approve = (e,row) => {
    
    const rs = tableData[row.tableData.id];
    setRowSelected(rs);
    setApproveDialog(true);
  };
  
  
  
  const approveDialogCallback = ()=> {
    setApproveDialog(false);
  };
  
  return (
    
    <div className={classes.table}>
      <MaterialTable
        title="Accounts Approval Tasks"
        icons={tableIcons}
        
        
        localization={{
          header: {
            actions: 'Actions'
          },
        }}
        
        columns={[
          { title: 'Requesing Account', field: 'account' },
          { title: 'Request Type', field: 'type' },
          { title: 'Date and Time', field: 'timeStamp' },
          { title: 'Approved', field: 'approved' },
        
        ]}
        data={getDataSet()}
        
        actions={[
          {
            icon: tableIcons.Detail,
            tooltip: 'Details',
            onClick: (event, rowData) => {showDetail(event,rowData)}
          },
          {
            icon: tableIcons.Check,
            tooltip: 'Approve',
            onClick: (event, rowData) => {approve(event,rowData)}
          }
        ]}
        
        options={{
          
          padding:"dense",
          headerStyle: {
            color: 'white',
            background:'#0091EA',
            fontSize:20,
            fontFamily:"Roboto"
          },
          rowStyle: {
            color: 'black'
          }
          
        }}
      
      />
      
      {detailsDialog &&
      
        <InvitationDetail data = {oData} open = {detailsDialog} callback = {detailsDialogCallback}/>
      
      }
      
      { approveDialog &&
      
        <AccountApprovalDialog data = {oData} open = {approveDialog} taskId={rowSelected._id} callback = {approveDialogCallback} context = {context}/>
      
      }
    
    </div>
  )
}



const tableTheme = makeStyles(theme => ({
  table: {
    '& tbody>.MuiTableRow-root:hover': {
      background: '#EEE',
    }
  },
  body: {
    textColor:"primary"
  }
}));