
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

import Dialog from '@material-ui/core/Dialog';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';

import aes256 from 'aes256';
import { ec as EC } from 'elliptic';

import axios from 'axios';



export default function AccountApprovalDialog(props) {
  
  const {data,callback,open, context } = props;
  const classes = dialogStyles();
  const [password, setPassword] = React.useState("");
  const [submitted, setSubmitted] = React.useState(false);
  
  const [error, setError] = React.useState(false);
  
  
  
  const handleClose = (action) => {
    callback(action);
  };
  
  const signAndSubmit = () => {
    
    const encryptedPK = context.encryptedPK;
    
    try {
      let decrypted = aes256.decrypt(password, encryptedPK);
      
      const txs = data.tx.tx;
      // Sign the txs
      const ec = new EC('secp256k1');
      const ecSK = ec.keyFromPrivate(decrypted, 'hex');
      const genPK = ecSK.getPublic().encode('hex');
      
      console.log(genPK);
      
      
      // only sign hash
      const sign = ecSK.sign(data._id);
      
      const chainTx = {
        action:"approve",
        homeId: data.homeId,
        invitee: data.invitee,
        user: data.user,
        publicKey:data.tx.publicKey,
        approveePK:props.context.publicKey,
        hash:data._id,
        signature:sign,
        tx:txs
      };
      
      
      const url = "http://127.0.0.1:4000/home/approveInvite" ;
      
      axios.post(url, {
        tx: chainTx
      }).then ((result) => {
        console.log("Sent to the network... " + result);
        setSubmitted(true);
        
      }).catch ((error) => {
        
        console.log("Something happened during the posting task to the network... " + error );
        setError(true);
        
        
      });
    } catch (error) {
      console.log("Error " + error);
    }
  };
  
  
  return (
    <Dialog onClose={handleClose} open={open} fullWidth maxWidth="lg" classes={{paper: classes.dialog }} style={{marginTop:200, marginLeft:50}}>
      
      <div className={classes.content} style={{width:800,marginLeft:50}}>
        
        <div style={{marginTop:50}}>
          <Typography color="primary" variant="h5" align="center">Approve Task </Typography>
        </div>
  
        {!submitted &&
        <div style={{marginTop: 50}} align="center">
    
          <TextField id="password" value={password} onChange={(e) => { setPassword(e.target.value)}} style={{width:250}} label="Key Password"/>
        </div>
    
        }
  
        {!submitted &&
        <div style={{marginTop: 50}} align="center" >
    
          <Button variant="contained" color="primary" style={{marginRight: 40, borderRadius: 25}}
                  onClick={signAndSubmit}>
            Sign and Approve
          </Button>
          <Button variant="contained" color="primary" style={{borderRadius: 25, marginLeft: 10}} onClick={handleClose}>
            Cancel
          </Button>/
  
  
        </div>
        }
        
        <Divider/>
  
        <div>
          {submitted &&
          <div align="center" style={{marginTop:100}}>
            <Typography variant="h6"> Approval Submitted to the network  </Typography>
            <Button variant="contained" color="primary" style={{borderRadius: 25, marginTop: 100}} onClick={handleClose}>
              Close
            </Button>
          </div>
      
          }
  
        </div>


      </div>
    
    
    </Dialog>
  );
}


const dialogStyles = makeStyles({
  dialog: {
    position: 'absolute',
    left: 50,
    top: 50
  },
  table: {
    minWidth: 700,
  },
});

