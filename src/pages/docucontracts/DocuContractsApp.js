

import React from 'react';


import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import IconButton from '@material-ui/core/IconButton';

import AdjustIcon from '@material-ui/icons/Adjust';
import ReferenceIcon from '@material-ui/icons/Dns';
import CollaborateIcon from '@material-ui/icons/GroupWork';


import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';

import Home from '@material-ui/icons/HomeWork';
import POC from '@material-ui/icons/Gavel';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';

import NetworkIcon from '@material-ui/icons/Business';
import FileIcon from '@material-ui/icons/FileCopy';


import Collapse from '@material-ui/core/Collapse';


import Grid from '@material-ui/core/Grid';

import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';

import AddIcon from '@material-ui/icons/AddCircleOutlineTwoTone';


import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';


import NewDocument from "./NewDocuContract";
import MyDocuments from "./MyDocuContracts";



const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    marginTop:50,
    backgroundColor:"white"
  },
  nested: {
    paddingLeft: theme.spacing(11),
  },
  
  toolbar: theme.mixins.toolbar,
}));

export default function DocuContractsApp(props) {
  
  const {context} = props;
  const classes = useStyles();
  
  const [selectedIndex, setSelectedIndex] = React.useState(-1);
  const [newDocument,setNewDocument] = React.useState(false);
  
  const [snackMessage, setSnackMessage] = React.useState("test message");
  const [snackOpen, setSnackOpen] = React.useState(false);
  const [error, setError] = React.useState(false);
  
  const [open, setOpen] = React.useState(false); // expand the parent list item
  
  const [anchorEl, setAnchorEl] = React.useState(null);
  const anchorOpen = Boolean(anchorEl);
  const [tabValue, setTabValue] = React.useState(-1);
  
  const handleTabChange = (event, newValue) => {
  
    if (newValue === 0 ) {
      props.setActiveScreenCallback(1); // Dashboard
    } else if (newValue === 1 ) {
      props.setActiveScreenCallback(7); // files
    } else if (newValue  === 2) {
      props.setActiveScreenCallback(4);
    } else if (newValue  === 3) {
      props.setActiveScreenCallback(2);
    }
    setTabValue(newValue);
  };
  
  const onLogout = () => {
    console.log("Logout called");
    props.logoutCallback();
    
  };
  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  
  
  const handleClick = () => {
    setOpen(!open);
  };
  
  
  const handleSnackClose = ()=> {
    setSnackOpen(false);
    
  };
  
  const selectTab = (event, index) => {
    setSelectedIndex(index);
    if (index === 0) {
      setNewDocument(true);
    }
  };
  
  
  
  const setSnackbarMessageCallback = (isError,message) => {
    setError(isError);
    setSnackMessage(message);
    setSnackOpen(true);
  };
  
  
  const onNewDocumentDialogCallback = (type,message) => {
    setNewDocument(false);
  };
  
  
  
  
  return (
    <div className={classes.root}>
      <CssBaseline/>
  
      <AppBar position="fixed" className={classes.appBar} style = {{backgroundColor:"white", color:"#303f9f"}}>
        <Toolbar>
          <Grid direction="row"  container>
  
            <Grid xs={1} item style={{marginTop:10}}>
              <Typography variant="h6" >
                ABBN
              </Typography>
            </Grid>
            
            
            <Grid xs={9} item>
              <Grid justify={"center"}>
                <Tabs
                  value={tabValue}
                  onChange={handleTabChange}
                  variant="fullWidth"
                  indicatorColor="primary"
                  textColor="primary"
                  centered
                >
                  <Tab icon={<Home fontSize="small"/>} label="Home"/>
                  <Tab icon={<FileIcon  fontSize="small" />} label="Files"/>
                  <Tab icon={<ReferenceIcon  fontSize="small" />} label="Reference Data"/>
                  <Tab icon={<POC  fontSize="small" />} label="Contracts"/>
                  <Tab icon={<CollaborateIcon fontSize="small"/>} label="Collaborate"/>
                  <Tab icon={<NetworkIcon fontSize="small"/>} label="Network"/>

                </Tabs>
              </Grid>
            </Grid>
            <Grid item xs={1} />
        
            <Grid item xs={1} justify={"center"}>
              <Grid justify={"right"}>
            
                <div>
                  <IconButton
                
                    aria-haspopup="true"
                    onClick={handleMenu}
                    color="inherit"
              
                  >
                    <AccountCircle fontSize="large"/>
                  </IconButton>
                  <Menu
                    id="menu-appbar"
                    anchorEl={anchorEl}
                    anchorOrigin={{
                      vertical: 'top',
                      horizontal: 'right',
                    }}
                    keepMounted
                    transformOrigin={{
                      vertical: 'top',
                      horizontal: 'right',
                    }}
                    open={anchorOpen}
                    onClose={handleClose}
                  >
                    <MenuItem >{context.homeId} / {context.user}</MenuItem>
                    <MenuItem onClick={onLogout}>logout</MenuItem>
                  </Menu>
                </div>
          
              </Grid>
            </Grid>
          </Grid>
    
        </Toolbar>
      </AppBar>
      
      <Drawer
        className={classes.drawer}
        variant="permanent"
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <div className={classes.toolbar}/>
        
        
        <List>
          <div style={{marginLeft:20,marginRight:20,width:"90%",fontSize:14,marginTop:30, marginBottom:30}} align="center">
            {context.homeId}/ {context.user}
          </div>
  
  
          <ListItem button key="send" onClick={event => selectTab(event, 0)} selected={selectedIndex === 0}>
            <ListItemIcon color="primary">
              <AddIcon/>
            </ListItemIcon>
            <ListItemText primary={<Typography style={{ color: '#303f9f' }}>New Document Contract</Typography>}/>
          </ListItem>
  
          <Divider/>
  
          
          <ListItem button onClick={handleClick}>
            <ListItemIcon>
              <AdjustIcon/>
            </ListItemIcon>
            <ListItemText primary={<Typography style={{ color: '#303f9f' }}>All Document Contracts</Typography>} />
            {open ? <ExpandLess /> : <ExpandMore />}
          </ListItem>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <List component="div" disablePadding>
              <ListItem button className={classes.nested} onClick={event => selectTab(event, 1)} >
                <ListItemText primary={<Typography style={{ color: '#303f9f' }}>All Entries</Typography>} />
              </ListItem>
  
              
            </List>
          </Collapse>
          
        </List>
        <Divider/>
      </Drawer>
      
      
      <main className={classes.content}>
  
  
        {selectedIndex === -1 &&
        <div>
          <IntoSection/>
        </div>
        }
  
        {selectedIndex === 0 &&
        <div>
          <NewDocument context={props.context} open = {newDocument} dialogCallback = {onNewDocumentDialogCallback}
                   setSnackbarMessageCallback={setSnackbarMessageCallback}/>
        </div>
        }
        
        
        {selectedIndex === 1 &&
        <div>
          <MyDocuments context={props.context} />
        </div>
        }
  
        
        <div>
          <Snackbar open={snackOpen} autoHideDuration={6000} onClose={handleSnackClose}>
            { !error &&
              <Alert onClose={handleSnackClose} severity={error?"error":"success"}>{snackMessage}</Alert>
            }
          </Snackbar>
        </div>
      </main>
    </div>
  );
}

function IntoSection (props) {
  
  const {context} = props;
  const classes = useStyles();
  
  return (
  
    <div style={{marginTop:5}}>
      <Grid container alignItems="center"
            justify="center" direction="column">
      
        <Grid item xs={12}>
          <Card className={classes.root} variant="outlined"
                style={{width: '100%',height:250,display: 'flex', flexDirection: 'column',align:"center"}}>
            <CardHeader style = {{backgroundColor:"#0091EA", color:"white"}}

                        title="Digial Signatures for document contracts"
              
            />
            <CardContent >
              <Typography align="center" color = "primary">
                Create New Document Contract
              </Typography>
              <Typography align="center" color = "primary">
                Get cryptographically signed
              </Typography>
              <Typography align="center" color = "primary">
                Keep the document and signatures on the blockchain
              </Typography>
    
            </CardContent>
  
          </Card>
          
        </Grid>
  
        
      
      </Grid>
    </div>
    
  )
  
}




function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}
