
import React, { useEffect,useStates } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import dateFormat from 'dateformat';


import MaterialTable from 'material-table';
import { forwardRef } from 'react';

import DocuContractDetails from './DocuContractDetails';
import tableIcons from '../common/IconDef';


import axios from 'axios';


export default  function MyDocuContracts(props) {
  
  const { context } = props;
  
  const classes = tableTheme();
  
  const [tableData, setTableData] = React.useState([]);
  const [indexData, setIndexData] = React.useState({});
  const [openDetailsDialog, setOpenDetailsDialog] = React.useState(false);
  
  
  
  useEffect(()=> {
    
    const url = "http://localhost:4000/documentcontract/get?originator=" + context.homeId;
    
    axios.get(url).then ((result) => {
      setTableData(result.data);
      
    }).catch ((error) => {
      console.log("Error in getting Internal Pending Invitations data  " + JSON.stringify(error));
    })
    
  },[]);
  
  
  
  
  const getDataSet = () => {
    const data = tableData.map((row, index) => {
      let formatDate = new Date(row.timeStamp);
      let newd = dateFormat(formatDate, "DDD, mmmm dS, yyyy, h:MM:ss TT");
      
      return {originator: row.originator, name: row.header.name,description:row.header.description, parties:row.header.parties.toString(), created:newd};
    });
    return data;
  };
  
  const rowClicked = (e,row) => {
    
    setIndexData(tableData[row.tableData.id]);
    
    setOpenDetailsDialog(true);
    
  };
  
  const detailsDialogCallback = (f) => {
    setOpenDetailsDialog(false);
  };
  
  
  
  return (
    
    <div className={classes.table}>
      <MaterialTable
        title="System Templates"
        icons={tableIcons}
        
        
        localization={{
          header: {
            actions: 'View'
          },
        }}
        
        columns={[
          { title: 'Originator', field: 'originator' },
          { title: 'Name', field: 'name' },
          { title: 'Description', field: 'description' },
          { title: 'Parties', field: 'parties' },
          { title: 'Created At', field: 'created' },
          
          
        ]}
        data={getDataSet()}
        
        actions={[
          rowData => ({
            icon: tableIcons.ViewEye,
            tooltip: 'View Details',
            onClick: rowClicked
          })
        ]}
        
        options={{
          actionsColumnIndex: -1,
          padding:"dense",
          
          headerStyle: {
            color: 'white',
            background:'#0091EA',
            fontSize:20,
            fontFamily:"Roboto"
          },
          rowStyle: {
            color: 'black'
          }
          
        }}
        
        components={{
        
        }}
      />
  
      {openDetailsDialog &&
  
        <DocuContractDetails open={openDetailsDialog} context = {context} callback={detailsDialogCallback} data={indexData}/>
    
      }
      
    </div>
  )
}



const tableTheme = makeStyles(theme => ({
  table: {
    '& tbody>.MuiTableRow-root:hover': {
      background: '#EEE',
    }
  },
  body: {
    textColor:"primary"
  }
}));
