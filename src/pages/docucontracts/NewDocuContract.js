import React from 'react';


import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import MenuItem from '@material-ui/core/MenuItem';
import InputAdornment from '@material-ui/core/InputAdornment';

import {Editor} from 'react-draft-wysiwyg';

import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { EditorState, convertToRaw, convertFromRaw } from 'draft-js';

import EditIcon from '@material-ui/icons/Edit';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';

import Grid from '@material-ui/core/Grid';




import axios from 'axios';

import { makeStyles } from '@material-ui/core/styles';
import {GetSignature,GetHash} from '../../ec/CryptoUtils';


const style = makeStyles((theme)=> ({
  dialog: {
    position: 'absolute',
    left: 10,
    top: 50
  },
  input: {
    display: 'none',
  },
  shape: {
    borderRadius: 25,
  },
  selectRoot: {
    '& .MuiTextField-root': {
      margin: theme.spacing(1),
      width: '40ch',
    },
  },
  
}));

export default  function NewDocuContract(props) {
  
  
  const { context } = props;
  
  const classes = style();
  
  
  
  const [createNew, setCreateNew] = React.useState(false);
  
  const [isTemplate, setTemplate] = React.useState(false);
  const [documentContractData, setDocumentContractData] = React.useState(false);
  const [finalize, setFinalize] = React.useState(false);
  const [result,setResult] = React.useState({set:false});
  
  
  
  const documentContractCallback = (cancel,data) => {
  
    setTemplate(false);
    
    if (!cancel) {
      setTemplate(false);
      
      setDocumentContractData(data);
      setFinalize(true);
    } else {
      setCreateNew(false);
    }
  };
  
  const createNewDocumentContract = () => {
    setCreateNew(true);
    setTemplate(true);
  };
  
  
  
  
  const onFinalize = (cancel,password) => {
    if (cancel) {
      setFinalize(false);
      setTemplate(true);
      return;
    }
    
    // send the template to
    const docuContractS = JSON.stringify(documentContractData);
    const hash = GetHash(docuContractS);
  
    const signature = GetSignature(context.encryptedPK,password,hash);
    const publicKey = context.publicKey;
  
  
    const tx = {
      originator:context.homeId,
      senderAccount:context.user,
      headerData:documentContractData.heading,
      timeStamp: new Date(),
      senderPublicKey: publicKey,
      hash:hash,
      signature:signature,
      body:docuContractS
    };
    
    // Send the request to the server...
    const url = "http://localhost:4000/documentcontract/add";
    
    return axios.post(url,tx).then ((result) => {
      
      if (result.data.success) {
        // message posted successfully
        setResult({set:true,success:true})
      } else {
        setResult({set:true,success:false,error:result.data.error})
      }
      setFinalize(false);
    });
    
  };
  
  const onDone = () => {
    setCreateNew(false);
    setResult(false);
  };
  

  
  return (
    
    <div className={classes.content} >
        
      <div className={classes.margin}>
          <Typography color="primary" variant="h1" align="center">
            
            {!createNew &&

              <Button color="primary" onClick={createNewDocumentContract}> Create a New Document Contract</Button>
            }
            
            </Typography>
      </div>
      
      <div>
        {isTemplate &&
          <DocuContract callback={documentContractCallback}/>
        }
        
        {finalize &&
        
          <Finalize callback = {onFinalize} />
        }
        
        {result.set &&
        
          <Result callback = {onDone} result={result} />
        }
      </div>
  
      
    </div>
    
  );
}



function DocuContract(props) {
  
  const {callback} = props;
  const classes = style();
  
  const [isHeaderSet, setIsHeaderSet] = React.useState(false);
  
  
  const [headerData,setHeaderData] = React.useState({});
  
  const [items, setItems] = React.useState([]);
  const [isInsert, setIsInsert] = React.useState(false);
  const [insertText, setInsertText] = React.useState(false);
  const [insertParty, setInsertParty] = React.useState(false);
  const [insertDate, setInsertDate] = React.useState(false);
  const [insertNumber, setInsertNumber] = React.useState(false);
  const [textEditorState, setTextEditorState] = React.useState(EditorState.createEmpty());
  
  
  // Function variables
  const [variableId, setVariableId] = React.useState("");
  const [variableDescription, setVariableDescription] = React.useState("");
  const [variableType, setVariableType] = React.useState("");
  const [variablePrefix, setVariablePrefix] = React.useState("");
  const [partySelected, setPartySelected] = React.useState("");
  
  
  const saveContract = () => {
    callback(false,{heading:headerData,sections:items});
  };
  
  const cancelTemplate = () => {
    callback(true);
  }
  
  const onInsertText = () => {
    setIsInsert(true);
    setInsertText(true);
  };
  
  const onInsertParty = () => {
    setIsInsert(true);
    setInsertParty(true);
    setVariableType("party");
    
  };
  
  const onInsertDate = () => {
    setIsInsert(true);
    setInsertDate(true);
    setVariableType("date");
    
  };
  
  const onInsertNumber = () => {
    setIsInsert(true);
    setVariableType("number");
    setInsertNumber(true);
    
  };
  
  
  
  const cancelItem = () => {
    setIsInsert(false);
    setInsertParty(false);
    setInsertDate(false);
    setInsertNumber(false);
    
  };
  
  const saveItem = () => {
    if (insertText) {
      const raw = convertToRaw(textEditorState.getCurrentContent());
      
      setTextEditorState(EditorState.createEmpty());
      setItems(items.concat({type:"text",value:raw}));
      setIsInsert(false);
      setInsertText(false);
      
    } else {
      setItems(items => [...items,
        {type:"variable",variableType:variableType, id:variableId,description:variableDescription,prefix:variablePrefix}]);
  
      setIsInsert(false);
      setInsertParty(false);
      setInsertDate(false);
      setInsertNumber(false);
      setVariableType("");
      setVariableDescription("");
      setVariablePrefix("");
  
    }
  };
  
  
  const onTextEditorStateChange = (s) => {
    setTextEditorState(s);
  };
  
  const setFieldValue = (e) => {
    
    const id = e.target.id;
    const value = e.target.value;
    
    if (id ==="vid") {
      setVariableId(value);
    } else if (id === "vdesc") {
      setVariableDescription(value)
      
    } else if (id === "prefix") {
      setVariablePrefix(value);
    }
    else {
      console.log("id  is  " + id);
    }
    
  };
  
  
  const templateHeaderCallback = (cancel,data) => {
    
    if (cancel) {
      setIsHeaderSet(false);
      callback(true);
      
    } else {
      setHeaderData(data);
      setIsHeaderSet(true);
    }
  };
  
  
  const getHeaderParties = () => {
    
    return headerData.parties.map((party, index) => {
      
      return (
  
        <TextField
          style={{width: 500, margin: 2, marginTop: 10}}

          inputProps={{
            readOnly: true,
          }}
          label={party}
        />
      )
    })
  };
  
  const getHeader = () => {
    
    
    
    if (isHeaderSet) {
      return (
        
        <div>
  
          <div className={classes.margin} style={{marginTop: 20, width: 500}}>
            <Typography variant="h4"> {headerData.name} </Typography>
            <Typography variant="body1"> {headerData.description} </Typography>
          </div>
          
          
          <div style={{marginTop:50}}>
            
            <Typography variant="h6"> Contract Parties</Typography>
            
            {getHeaderParties()}
            
          </div>
          
         
        
        </div>
        
      
      
      );

    }
    
  };
  
  const getBody = () => {
    
    return (
      
      <div>
        {isHeaderSet &&
          <div style={{marginTop: 50}}>
            <Typography variant="h6"> Contract Body</Typography>
          </div>
        }
        <div style={{marginTop:50}}>
          
          {getItems()}
        </div>
      
      
      </div>
    )
    
  };
  
  const getItems = () => {
    
    return items.map((item,index) => {
      if (item.type === "text") {
        
        const editorState = EditorState.createWithContent(convertFromRaw(item.value));
        
        return (
  
          <Grid
            container
            direction="row"
            justify="flex-start"
            alignItems="flex-start"
          >
  
            <div className={classes.margin} style={{marginTop:1,width:500}} >
              <Editor
                editorState={editorState}
                toolbarClassName="toolbarClassName"
                wrapperClassName="wrapperClassName"
                editorClassName="editorClassName"
                toolbarHidden
                readOnly
                editorStyle={{backgroundColor:"#FFFFFF",height:"auto"}}
              />
            
            </div>
            
            <div align="center">
              
                <IconButton aria-label="edit" size="small">
                  <EditIcon />
                </IconButton>
                <IconButton aria-label="delete" size="small">
                  <DeleteIcon />
                </IconButton>
              
            </div>
          </Grid>
          
        )
      } else if (item.type === "variable" ) {
  
        if (item.variableType === "party") {
    
          return (
      
            <div>
              <TextField
          
                style={{width: 500, margin: 2, marginTop: 10}}
          
                inputProps={{
                  style: {fontSize: 15}
                }}
                color="secondary"
          
                value={item.description}
                
                InputProps={{
                  startAdornment: <InputAdornment position="start">{item.prefix}  :</InputAdornment>,
                  readOnly: true,
                }}
                
        
              />
              
            </div>
          )
    
    
        } else if ((item.variableType === "date") ||
                  (item.variableType === "number")) {
  
          return (
    
            <TextField
      
              style={{width: 500, margin: 2, marginTop: 10}}
      
              inputProps={{
                style: {fontSize: 15}
              }}
              color="primary"
              
              value={item.description}
              InputProps={{
                startAdornment: <InputAdornment position="start">{item.prefix}  </InputAdornment>,
                readOnly: true,
              }}
      
            />
  
  
          )
        }
  
      }
    })
    
  };
  
  const getPartiesAsMenu = () => {
    
    return headerData.parties.map((party,index) => {
      
      return (
        <MenuItem key="1" value={party}>
          {party}
      </MenuItem>
      )
    })
  };
  
  
  const onSelectPartyFunction = (e) => {
    const vType = e.target.value;
    setPartySelected(vType);
    setVariableDescription(vType);
  };
  
  
  const isVariableReadOnly = variableType === "party";
  
  
  
  return (
  
  <div style={{width: "80%", align: "center", marginLeft: 50}}>
  
  
    {!isHeaderSet &&
    <div>
      <TemplateHeader callback = {templateHeaderCallback}/>
      
    </div>
    
    }
  
    <div>
  
      {getHeader()}
      
      {getBody()}
  
    </div>
  
    <div align="center" style={{marginTop: 100, marginBottom: 100}}>
    
    
      {!isInsert && isHeaderSet &&
      <div>
      
        <Typography variant="h5"> Add Contract Body Items </Typography>
      
        <div style={{marginTop:100}}>
        
          <Button onClick={onInsertText} className={classes.shape} align="left" variant="contained" color="primary"
                  style={{width: 200, height: 30}}>Insert Text </Button>
        
          <Button onClick={onInsertParty} className={classes.shape} variant="contained" align="right"
                  style={{width: 200, height: 30, marginLeft: 20}} color="secondary"> Insert Party </Button>
        
          <Button onClick={onInsertDate} className={classes.shape} variant="contained" align="right"
                  style={{width: 200, height: 30, marginLeft: 20}} color="secondary">  Insert Date </Button>
        
          <Button onClick={onInsertNumber} className={classes.shape} variant="contained" align="right"
                  style={{width: 200, height: 30, marginLeft: 20}} color="secondary"> Insert Number </Button>
        </div>
  
        <div style={{marginTop:100}}>
  
          <Button onClick={saveContract} className={classes.shape} variant="contained" align="right"
                  style={{width: 200, height: 30, marginLeft: 20}} color="secondary"> Save Contract </Button>
  
          <Button onClick={cancelTemplate} className={classes.shape} variant="contained" align="right"
                  style={{width: 120, height: 30, marginLeft: 20}} color="secondary"> Cancel </Button>
          
        </div>
    
      </div>
      
      }
    
      {insertText &&
    
      <div className={classes.margin} style={{marginTop:30,width:800}}>
        <Editor
          editorState={textEditorState}
          toolbarClassName="toolbarClassName"
          wrapperClassName="wrapperClassName"
          editorClassName="editorClassName"
          placeholder = "Note for read request..."
          onEditorStateChange={onTextEditorStateChange}
          editorStyle={{backgroundColor:"#FFFFFF",height:"auto"}}
        />
    
      </div>
      
      }
      
      {insertDate &&


      <div>
  
        <div style={{marginTop:30}} align="center">
    
    
          <TextField
            style={{width: 70, margin: 2, marginTop: 10}}
            value = {variablePrefix}
            id = "prefix"
            onChange={setFieldValue }
            color = "secondary"
      
            label="prefix"
          />
    
          
          <TextField
            style={{width: 400, margin: 2, marginTop: 10}}
            value={variableDescription}
      
            id="vdesc"
            type="date"
            onChange={setFieldValue}
            label="add number value "
    
          />
          
        </div>
      
      </div>
      
      }
      
      
    
      { insertParty &&
      <div>
      
        <div style={{marginTop:30}} align="center">
        
        
          <TextField
            style={{width: 100, margin: 2, marginTop: 10}}
            value = {variablePrefix}
            id = "prefix"
            onChange={setFieldValue }
            color = "secondary"
          
            label="prefix"
          />
  
          <TextField
            style={{width: 300, margin: 2, marginTop: 10, marginLeft:10}}
            value = {variableDescription}
            id = "vdesc"
            onChange={setFieldValue }
            color = "secondary"
            inputProps={{
              readOnly: true,
            }}
    
            label="Party Value"
          />
  
          
          <TextField
            style={{width: 200, margin: 2, marginTop: 10}}
            select
            value={partySelected}
            onChange={onSelectPartyFunction}
            
            helperText="Please select party value"
          >
            {getPartiesAsMenu()}
          </TextField>
          
          
      
      
      
        </div>
      
       
      </div>
      
      }
  
  
  
  
      { insertNumber &&
      <div>
    
        <div style={{marginTop:30}} align="center">
      
      
          <TextField
            style={{width: 70, margin: 2, marginTop: 10}}
            value = {variablePrefix}
            id = "prefix"
            onChange={setFieldValue }
            color = "secondary"
        
            label="prefix"
          />
      
          <TextField
            style={{width: 300, margin: 2, marginTop: 10, marginLeft:10}}
            value = {variableDescription}
            id = "vdesc"
            onChange={setFieldValue }
            type="number"
            color = "secondary"
            
            label="Party Value"
          />
      
        </div>
  
  
      </div>
    
      }
      
      
      
    
      {isInsert &&
      <div>
      
        <Button onClick={saveItem} className={classes.shape} variant="contained" align="right"
                style={{width: 120, height: 30, marginLeft: 20}} color="secondary"> Save </Button>
      
        <Button onClick={cancelItem} className={classes.shape} variant="contained" align="right"
                style={{width: 120, height: 30, marginLeft: 20}} color="secondary"> Cancel </Button>
    
      </div>
      }
  
  
    </div>
    
  
    
    
  </div>
  
  
  );
}

function TemplateHeader(props)  {
  
  const {callback} = props;
  
  const classes = style();
  
  const [name, setName] = React.useState("");
  const [description, setDescription] = React.useState("");
  const [parties, setParties] = React.useState([]);
  const [hasStartDate, setHasStartDate] = React.useState(true);
  const [hasEndDate, setHasEndDate] = React.useState(true);
  const [isBreakable, setIsBreakable] = React.useState(true);
  
  const [isAddParty, setIsAddParty] = React.useState(false);
  
  const [partyName,setPartyName] = React.useState("");
  
  const saveHeader = () => {
    callback(false,{name:name,description:description,parties:parties});
  };
  
  const cancelHeader = () => {
    callback(true);
    
  };
  
  const addParty = () => {
    setIsAddParty(false);
    setParties(parties => [...parties,partyName]);
    
  };
  
  const getParties = () => {
    
    return parties.map((party,index) => {
      
      return (
        
        <TextField
          
          style={{width: 500, margin: 2, marginTop: 10}}
          
          inputProps={{
            style: {fontSize: 15},
            readOnly: true
          }}
          color="primary"
          
          placeholder={party}
          readOnly
        
        />
      
      
      )
      
    });
    
  };
  
  
  
  return (
    
    <div style={{width: "80%", marginLeft: 50}}>
      
      <div>
        
        <div align="center">
          <Typography variant="h5"> Contract Header</Typography>
        
        </div>
        <div className={classes.margin} align="center" style={{marginTop: 30}}>
          <TextField
            
            style={{width: 500, margin: 2, marginTop: 10}}
            
            inputProps={{
              style: {fontSize: 25}
            }}
            color="primary"
            value={name}
            label={
              <Typography variant="h5"> Name of the Document Contract </Typography>
            }
            
            id="name"
            
            fullWidth
            
            onChange={(e)=>{setName(e.target.value)}}
          
          />
          
          <TextField
            
            style={{width: 500, margin: 2, marginTop: 10}}
            
            label="Document Contract Description"
            color="primary"
            value={description}
            id="description"
            onChange={(e)=>{setDescription(e.target.value)}}
            multiline
            rows="2"
            
            inputProps={{
              style: {fontSize: 16, color: "blue"}
            }}
          
          
          />
        </div>
        
        
        <div className={classes.margin} align="center" style={{width:500,marginTop: 10}}>
          <Typography variant="h6"> Contract Parties </Typography>
          
          <div style={{marginTop:10}}>
            
            {getParties()}
          
          </div>
          
          { isAddParty &&
          
          <div style={{marginTop:20}} align="center">
            
            <TextField
              style={{width: 500, margin: 2, marginTop: 10}}
              value = {partyName}
              
              onChange={(e)=>{ setPartyName(e.target.value)}}
              label="Party Name"
            />
          
          </div>
          
          }
        
        </div>
        
        
        
        <div align="center" style={{marginTop:50}}>
          
          {isAddParty &&
          
          <div>
            <Button onClick={addParty} className={classes.shape} align="left" variant="contained" color="primary"
                    style={{width: 100, height: 30}}>Save Party </Button>
            
            <Button onClick={()=>{setIsAddParty(false)}} className={classes.shape} align="left" variant="contained" color="primary"
                    style={{width: 100, height: 30}}>Cancel </Button>
          
          </div>
          
          }
          
          {!isAddParty &&
          
          <div>
            
            <Button onClick={()=>{setIsAddParty(true)}} className={classes.shape} align="left" variant="contained" color="primary"
                    style={{width: 200, height: 30}}>Add party </Button>
            
            <Button onClick={saveHeader} className={classes.shape} align="left" variant="contained" color="primary"
                    style={{width: 200, height: 30}}>Save Header </Button>
            
            
            <Button onClick={cancelHeader} className={classes.shape} align="left" variant="contained" color="secondary"
                    style={{width: 100, height: 30}}>Cancel </Button>
          </div>
          }
        </div>
      
      </div>
    
    
    </div>
  
  
  
  
  );
  
  
  
}

function Finalize(props)  {
  
  const {callback} = props;
  const [password, setPassword] = React.useState("");
  const classes = style();
  
  const finalize = () => {
    
    callback(false,password);
  };
  
  const cancel = () => {
    callback(true);
  };
  
  return (
    
    <div align="center" style={{marginTop:100}}>
      
      <div align="center" style={{marginTop:100}}>
        <Typography variant="h3"> Sign document contract and send to the Network </Typography>
        
      </div>
      
      <div align="center" style={{marginTop:100}}>
  
        <TextField
    
          style={{width: 400, marginTop: 30, backgroundColor: "#FFFFFF", height: 20}}
          placeholder="password for private key"
          value={password}
          id="password"
          onChange={(e) => {setPassword(e.target.value)}}
          color="secondary"
        />
        
        
      </div>
  
      <div align = "center" style={{marginTop:100}}>
    
        <Button onClick={finalize} className={classes.shape} variant="contained" align="right"
                style={{width: 300, height: 30}} color="secondary"> Sign and Send  </Button>
    
        <Button onClick={cancel} className={classes.shape} variant="contained" align="right"
                style={{width: 150, height: 30, marginLeft: 50}} color="secondary"> Cancel </Button>
  
      </div>
      
    </div>
    
  )
}

function Result(props)  {
  
  const {callback, result} = props;
  const classes = style();
  
  const done = () => {
    callback();
  };
  

  
  return (
    
    <div align="center" style={{marginTop:100}}>
  
      {result.success &&
      <div align="center" style={{marginTop:100}}>
        
          <Typography variant="h3"> Transaction Submitted successfully </Typography>
        
      </div>
      
      }
  
      {!result.success &&
  
      <div align="center" style={{marginTop: 100}}>
    
    
        <Typography variant="h3"> Transaction submission failed with error: </Typography>
        <Typography variant="h5"> {result.error} </Typography>
  
  
      </div>
      }
      
      <div align = "center" style={{marginTop:100}}>
        
        <Button onClick={done} className={classes.shape} variant="contained" align="right"
                style={{width: 250, height: 30}} color="secondary"> Done </Button>
        
        
      </div>
    
    </div>
  
  )
}



