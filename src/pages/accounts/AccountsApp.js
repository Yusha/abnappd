

import React, {useEffect} from 'react';


import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import IconButton from '@material-ui/core/IconButton';

import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import AdjustIcon from '@material-ui/icons/Adjust';
import ReferenceIcon from '@material-ui/icons/Dns';
import CollaborateIcon from '@material-ui/icons/GroupWork';

import Home from '@material-ui/icons/HomeWork';
import POC from '@material-ui/icons/Gavel';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';

import NetworkIcon from '@material-ui/icons/Business';
import FileIcon from '@material-ui/icons/FileCopy';


import MaterialTable from 'material-table'

import GroupIcon from '@material-ui/icons/Group';

import Dialog from '@material-ui/core/Dialog';
import Button from '@material-ui/core/Button';

import Grid from '@material-ui/core/Grid';

import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';



import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import Collapse from '@material-ui/core/Collapse';


import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';

import axios from 'axios';
import TextField from '@material-ui/core/TextField';

import aes256 from 'aes256';
import { ec as EC } from 'elliptic';




import NewGroup from "./NewGroup";
import NewInvitation from "./NewInvitation";
import NewRole from './NewRole';
import tableIcons from '../common/IconDef';
import {GetHash} from "../../ec/CryptoUtils";



const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  nested: {
    paddingLeft: theme.spacing(11),
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    marginTop:50,
  },
  toolbar: theme.mixins.toolbar,
}));

export default function AccountsApp(props) {
  
  const {context} = props;
  
  
  const classes = useStyles();
  
  const [selectedIndex, setSelectedIndex] = React.useState(-1);
  const [accountsOpen, setAccountsOpen] = React.useState(false);
  const [groupsOpen, setGroupsOpen] = React.useState(false);
  const [rolesOpen, setRolesOpen] = React.useState(false);
  
  const [error,setError] = React.useState(false);
  
  const [snackMessage, setSnackMessage] = React.useState("test message");
  const [snackOpen, setSnackOpen] = React.useState(false);
  
  const [listIndex, setListIndex] = React.useState(-1);
  
  const [anchorEl, setAnchorEl] = React.useState(null);
  const anchorOpen = Boolean(anchorEl);
  const [tabValue, setTabValue] = React.useState(-1);
  
  const handleTabChange = (event, newValue) => {
  
    if (newValue === 0 ) {
      props.setActiveScreenCallback(1); // Dashboard
    } else if (newValue === 1 ) {
      props.setActiveScreenCallback(7); // files
    } else if (newValue === 2 ) {
      props.setActiveScreenCallback(3); // Reference Objects
    } else if (newValue  === 3) {
      props.setActiveScreenCallback(4); // Contracts
    } else if (newValue  === 4) {
      props.setActiveScreenCallback(6); // Collaborate
    } else if (newValue  === 5) {
      props.setActiveScreenCallback(2); // Network
    }
    setTabValue(newValue);
  };
  
  const onLogout = () => {
    console.log("Logout called");
    props.logoutCallback();
    
  };
  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  
  
  
  const handleClick = (type) => {
    
    setListIndex(type);
    
    if (type === 0) {
      setAccountsOpen(!accountsOpen);
    } else if (type === 1) {
      setGroupsOpen(!groupsOpen);
    } else if (type === 2) {
      setRolesOpen(!rolesOpen);
    } else {
      console.log("Unnown type in activating list in Accouts page");
    }
    
  };
  
  const handleSnackClose = ()=> {
    setSnackOpen(false);
    
  };
  
  const selectTab = (event, index) => {
    setSelectedIndex(index);
  };
  
  const setSnackbarMessageCallback = (isError,message) => {
    setError(isError);
    setSnackMessage(message);
    setSnackOpen(true);
    setSelectedIndex(-1);
    
  };
  
  
  
  
  
  return (
    <div className={classes.root}>
      <CssBaseline/>
  
      <AppBar position="fixed" className={classes.appBar} style = {{backgroundColor:"white", color:"#303f9f"}}>
        <Toolbar>
          <Grid direction="row"  container>
  
            <Grid xs={1} item style={{marginTop:10}}>
              <Typography variant="h6" >
                ABBN
              </Typography>
  
            </Grid>
            <Grid xs={9} item>
              <Grid >
                <Tabs
                  value={tabValue}
                  onChange={handleTabChange}
                  variant="fullWidth"
                  indicatorColor="primary"
                  textColor="primary"
                  centered
                >
                  <Tab icon={<Home fontSize="small"/>} label="Home"/>
                  <Tab icon={<FileIcon  fontSize="small" />} label="Files"/>
                  <Tab icon={<ReferenceIcon  fontSize="small" />} label="Reference Data Objects"/>
                  <Tab icon={<POC  fontSize="small" />} label="Contracts"/>
                  <Tab icon={<CollaborateIcon fontSize="small"/>} label="Collaborate"/>
                  <Tab icon={<NetworkIcon fontSize="small"/>} label="Network"/>
      
                </Tabs>
              </Grid>
            </Grid>
            <Grid item xs={1} />
        
            <Grid item xs={1} justify={"center"}>
              <Grid justify={"right"}>
            
                <div>
                  <IconButton
                
                    aria-haspopup="true"
                    onClick={handleMenu}
                    color="inherit"
              
                  >
                    <AccountCircle fontSize="large"/>
                  </IconButton>
                  <Menu
                    id="menu-appbar"
                    anchorEl={anchorEl}
                    anchorOrigin={{
                      vertical: 'top',
                      horizontal: 'right',
                    }}
                    keepMounted
                    transformOrigin={{
                      vertical: 'top',
                      horizontal: 'right',
                    }}
                    open={anchorOpen}
                    onClose={handleClose}
                  >
                    <MenuItem >{context.homeId} / {context.user}</MenuItem>
                    <MenuItem onClick={onLogout}>logout</MenuItem>
                  </Menu>
                </div>
          
              </Grid>
            </Grid>
          </Grid>
    
        </Toolbar>
      </AppBar>
      
      <Drawer
        className={classes.drawer}
        variant="permanent"
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        
        <div className={classes.toolbar}/>
        <List>
  
          <div style={{marginLeft:20,marginRight:20,width:"90%",fontSize:14,marginTop:30, marginBottom:30}} align="center">
            {context.homeId}/ {context.user}
          </div>
  
  
          <ListItem button key="accounts" onClick={()=>{handleClick(0)}} selected={listIndex === 0}>
            <ListItemIcon>
              <AccountCircleIcon/>
            </ListItemIcon>
            <ListItemText primary={<Typography style={{ color: '#303f9f' }}>Accounts</Typography>} />
            {accountsOpen ? <ExpandLess /> : <ExpandMore />}
          </ListItem>
          <Collapse in={accountsOpen} timeout="auto" unmountOnExit>
            <List component="div" disablePadding>
              <ListItem button key="newcontract" className={classes.nested} onClick={event => selectTab(event, 0)} >
                <ListItemText primary={<Typography style={{ color: '#303f9f' }}>New Account</Typography>} />
              </ListItem>
      
              <ListItem button key="allContracts" className={classes.nested} onClick={event => selectTab(event, 1)}>
                <ListItemText primary={<Typography style={{ color: '#303f9f' }}>All Accounts</Typography>} />
              </ListItem>
    
            </List>
          </Collapse>
  
  
  
          <Divider/>
  
          <ListItem button key="Groups" onClick={()=>{handleClick(1)}} selected={listIndex === 1}>
            <ListItemIcon>
              <GroupIcon/>
            </ListItemIcon>
            <ListItemText primary={<Typography style={{ color: '#303f9f' }}>Groups</Typography>} />
            {groupsOpen ? <ExpandLess /> : <ExpandMore />}
          </ListItem>
          <Collapse in={groupsOpen} timeout="auto" unmountOnExit>
            <List component="div" disablePadding>
              <ListItem button key="newcontract" className={classes.nested} onClick={event => selectTab(event, 2)} >
                <ListItemText primary={<Typography style={{ color: '#303f9f' }}>New Group</Typography>} />
              </ListItem>
      
              <ListItem button key="allContracts" className={classes.nested} onClick={event => selectTab(event, 3)}>
                <ListItemText primary={<Typography style={{ color: '#303f9f' }}>All Groups</Typography>} />
              </ListItem>
    
            </List>
          </Collapse>
  
  
          <Divider/>
  
          <ListItem button key="Roles" onClick={()=>{handleClick(2)} } selected={listIndex === 2}>
            <ListItemIcon>
              <AdjustIcon/>
            </ListItemIcon>
            <ListItemText primary={<Typography style={{ color: '#303f9f' }}>Roles</Typography>} />
            {rolesOpen ? <ExpandLess /> : <ExpandMore />}
          </ListItem>
          <Collapse in={rolesOpen} timeout="auto" unmountOnExit>
            <List component="div" disablePadding>
              <ListItem button key="newcontract" className={classes.nested} onClick={event => selectTab(event, 4)} >
                <ListItemText primary={<Typography style={{ color: '#303f9f' }}>New Role</Typography>} />
              </ListItem>
      
              <ListItem button key="allContracts" className={classes.nested} onClick={event => selectTab(event, 5)}>
                <ListItemText primary={<Typography style={{ color: '#303f9f' }}>All Roles</Typography>} />
              </ListItem>
    
            </List>
          </Collapse>

        </List>
        <Divider/>
      </Drawer>
      
      
      <main className={classes.content}>
        
        
        { selectedIndex === -1 &&
          
          <IntoSection/>
        }
        
        
        {selectedIndex === 0 &&
        
        <div>
          <NewInvitation context={props.context} callback={setSnackbarMessageCallback}/>
        </div>
        }
  
        {selectedIndex === 1 &&
          <div>
            <AccountsTable context={props.context} callback={setSnackbarMessageCallback}/>
          </div>
        }
  
        {selectedIndex === 2 &&
  
        <div>
          <NewGroup context={props.context} callback={setSnackbarMessageCallback}/>
        </div>
        }
        
        
        {selectedIndex === 3 &&
        
        <div>
    
          <GroupsTable context={props.context} callback={setSnackbarMessageCallback}/>
          
        </div>
    
        }
  
        {selectedIndex === 4 &&
  
        <div>
          <NewRole context={props.context} callback={setSnackbarMessageCallback}/>
        </div>
        }
        
  
        {selectedIndex === 5 &&
  
        <div>
    
          <RolesTable context={props.context} setSnackbarMessageCallback={setSnackbarMessageCallback}/>
          
        </div>
    
        }
        
        <div>
          <Snackbar open={snackOpen} autoHideDuration={6000} onClose={handleSnackClose}>
            { !error &&
              <Alert onClose={handleSnackClose} severity={error?"error":"success"}>{snackMessage}</Alert>
            }
          </Snackbar>
        </div>
      </main>
    </div>
  );
}



export function AccountsTable(props) {
  
  
  const {context} = props;
  const [open, setOpen] = React.useState(false);
  const [rowData, setRowData] = React.useState("");
  
  const [invite, setInvite] = React.useState(false);
  const [successSnackback, setSuccessSnackbar] = React.useState(false);
  const [bcData,setBCData] = React.useState([]);
  
  
  useEffect(()=> {
    
    const url = "http://localhost:4000/api/accounts?home=" + context.homeId ;
    axios.get(url).then ((result) => {
      setBCData(result.data);
      
    }).catch ((error) => {
      console.log("Error in getting Accounts   " + JSON.stringify(error));
    })
    
  },[]);
  
  const getTableData = ()=> {
    
    return bcData.map((row,index) => {
      return {name:row.name,email:row.email,role:row.role,group:row.group,approvee:row.approvee,publicKey:row.publicKey};
    })
    
  };
  
  const dialogCallback = (response) => {
    setOpen(false);
    // Handle cancel ot submit here
  };
  
  
  const rowClicked = (e,row) => {
    setRowData(row.email);
    setOpen(true);
  };
  
  
  const handleSnackClose = () => {
    setSuccessSnackbar(false);
  };
  
  
  return (
    
    <div>
  
      {!invite &&
      <div>
    
        <MaterialTable
          title="Accounts Management"
          icons={tableIcons}
      
          localization={{
        
            header: {
              actions: 'Actions'
            },
        
          }}
      
      
          columns={[
            {title: 'Name', field: 'name'},
            {title: 'Email', field: 'email'},
            {title: 'Role', field: 'role'},
            {title: 'Group', field: 'group'},
            {title: 'Approvee', field: 'approvee'},
            {title: 'Public Key', field: 'publicKey', cellStyle: {wordBreak: 'break-all'}},
            
          ]}
          data= {getTableData()}
          
          actions={[
            rowData => ({
              icon: tableIcons.Delete,
              tooltip: 'Retire',
              onClick: rowClicked
            })
          ]}

          options={{
            actionsColumnIndex: -1,
            padding: "dense",
            headerStyle: {
              backgroundColor: '#0091EA',
              color: '#FFF'
            }
          }}
      
          
        />
        {open &&
          <RetireDialog callback={dialogCallback} open={open} data={rowData} type = "Account" context = {context}/>
        }
      </div>
      }
      
      <div>
        <Snackbar open={successSnackback} autoHideDuration={6000} onClose={handleSnackClose}>
          <Alert onClose={handleSnackClose} severity="success">Invitation Sent Successfully</Alert>
        </Snackbar>
      </div>
    </div>
    
  )
    
}

export function GroupsTable(props) {
  
  const {context} = props;
  
  const [open, setOpen] = React.useState(false);
  const [rowData, setRowData] = React.useState("");
  const [bcData,setBCData] = React.useState([]);
  
  const [newGroup, setNewGroup] = React.useState("");
  
  
  useEffect(()=> {
    
    const url = "http://localhost:4000/home/groups?home=" + context.homeId ;
    axios.get(url).then ((result) => {
      setBCData(result.data);
      
    }).catch ((error) => {
      console.log("Error in getting home groups  " + JSON.stringify(error));
    })
    
  },[newGroup]);
  
  const dialogCallback = (data) => {
    setOpen(false);
    // Handle cancel ot submit here
  };
  
  const rowClicked = (e,row) => {
  
    setRowData(row.name);
    setOpen(true);
    
  };
  
  const getTableData = ()=> {
    
    return bcData.map((row,index) => {
      let orginalTx = JSON.parse(row.tx.tx);
      
      return {name:orginalTx.name,admin:orginalTx.admin,description:orginalTx.description};
    })
    
  };
  
  
  
  return (
    <div >
      
      
        <div>
          <MaterialTable
          title="Groups Management "
          icons={tableIcons}
      
          localization={{
        
            header: {
              actions: 'Actions'
            },
        
          }}
      
      
          columns={[
            {title: 'Name', field: 'name'},
            {title: 'Group Admin', field: 'admin'},
            {title: 'Description', field: 'description', cellStyle: {wordBreak: 'break-all'}},
      
      
          ]}
          data={getTableData()}
          actions={[
            rowData => ({
              icon: tableIcons.Delete,
              tooltip: 'Retire a department',
              onClick: rowClicked
            })
          ]}
          options={{
            actionsColumnIndex: -1,
            padding: "dense",
            headerStyle: {
              backgroundColor: '#0091EA',
              color: '#FFF'
            }
          }}
          
        />
          {open &&
            <RetireDialog callback={dialogCallback} open={open} data={rowData} type = "Group" context = {context}/>
    
          }
        </div>
    
      
      
      
    </div>
  )
}

export function RolesTable(props) {
  
  const {context} = props;
  
  const [open, setOpen] = React.useState(false);
  const [rowData, setRowData] = React.useState("");
  
  const [bcData, setBCData] = React.useState([]);
  
  const [newRole,setNewRole] = React.useState(""); // Hash of the newly created role.
  
  const getTableData = ()=> {
    
    return bcData.map((row,index) => {
      let orginalTx = JSON.parse(row.tx.tx);
      
      return {name:orginalTx.name,description:orginalTx.description};
      
    })
  };
  
  
  useEffect(()=> {
    
    const url = "http://localhost:4000/home/roles?home=" + context.homeId ;
    axios.get(url).then ((result) => {
      setBCData(result.data);
      
    }).catch ((error) => {
      console.log("Error in getting Roles for the given home...  " + JSON.stringify(error));
    })
    
  },[newRole]);
  
  
  
  const dialogCallback = (data) => {
    setOpen(false);
    // Handle cancel ot submit here
  };
  
  
  
  const rowClicked = (e,row) => {
    setRowData(row.name);
    setOpen(true);
  };
  
  
  return (
    <div>
      
        <div>
        <MaterialTable
          title="Roles Management"
          icons={tableIcons}
      
          localization={{
        
            header: {
              actions: 'Actions'
            },
        
          }}
      
      
          columns={[
            {title: 'Name', field: 'name'},
            {title: 'description', field: 'description', cellStyle: {wordBreak: 'break-all'}},
      
          ]}
          data={getTableData()}
          actions={[
            rowData => ({
              icon: tableIcons.Delete,
              tooltip: 'Retire a Role',
              onClick: rowClicked
            })
          ]}
          options={{
            actionsColumnIndex: -1,
            padding: "dense",
            headerStyle: {
              backgroundColor: '#0091EA',
              color: '#FFF'
            }
          }}
          
        />
          {open &&
            <RetireDialog callback={dialogCallback} open={open} data={rowData} type = "Role" context = {context} />
          }
          
      </div>
      
      
    </div>
  )
}




function IntoSection (props) {
  
  const {context} = props;
  const classes = useStyles();
  
  return (
    
    <div style={{marginTop:5}}>
      
      <div style={{backgroundColor:"#0091EA", color:"white", height:100, width:"100%",fontSize:24}} align="center">
        Accounts Management
      </div>
      
      <div style={{marginTop:20}}>
        
        <Grid container alignItems="center"
              justify="center" direction="row">
          
          <Grid item xs={4}>
            <Card className={classes.root} variant="outlined"
                  style={{width: '100%',height:250,display: 'flex', flexDirection: 'column',align:"center"}}>
              <CardHeader style = {{backgroundColor:"white", color:"#0091EA", align:"center"}} align="center"
              
                          title="Accounts Management"
              
              />
              <CardContent >
                <Typography align="center" color = "primary">
                  Invite new members as accounts
                </Typography>
                <Typography align="center" color = "primary">
                  Retire an Account
                </Typography>
                <Typography align="center" color = "primary">
                  Verifiable on the blockchain
                </Typography>
              
              </CardContent>
            
            </Card>
          
          </Grid>
          <Grid item xs={4}>
            <Card className={classes.root} variant="outlined"
                  style={{width: '100%',height:250,display: 'flex', flexDirection: 'column',align:"center"}}>
              <CardHeader style = {{backgroundColor:"white", color:"#0091EA",align:"center"}} align="center"
              
                          title="Groups Management"
              
              />
              <CardContent >
                <Typography align="center" color = "primary">
                  Create a new group
                </Typography>
                <Typography align="center" color = "primary">
                  Retire groups
                </Typography>
                <Typography align="center" color = "primary">
                  Verifiable on the blockchain
                </Typography>
              
              </CardContent>
            
            </Card>
          
          </Grid>
          <Grid item xs={4}>
            <Card className={classes.root} variant="outlined"
                  style={{width: '100%',height:250,display: 'flex', flexDirection: 'column',align:"center"}}>
              <CardHeader style = {{backgroundColor:"white", color:"#0091EA", align:"center"}} align="center"
              
                          title="Roles Management"
              
              />
              <CardContent >
                <Typography align="center" color = "primary">
                  Create a new Role
                </Typography>
                <Typography align="center" color = "primary">
                  Retire a Role
                </Typography>
                <Typography align="center" color = "primary">
                  Verifiable on blockchain
                </Typography>
              
              </CardContent>
            
            </Card>
          
          </Grid>
          
        
        
        </Grid>
      
      </div>
    </div>
  
  )
  
}

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

function RetireDialog(props) {
  
  const {data,callback,open, context,taskId, type } = props;
  const classes = dialogStyles();
  const [password, setPassword] = React.useState("");
  const [submitted, setSubmitted] = React.useState(false);
  
  const [error, setError] = React.useState(false);
  
  
  const handleClose = (action) => {
    callback(action);
  };
  
  const signAndSubmit = () => {
    
    const encryptedPK = context.encryptedPK;
    
    try {
      let decrypted = aes256.decrypt(password, encryptedPK);
      
      // only Sign the hash of PI Submission.. i,e
      const ec = new EC('secp256k1');
      const ecSK = ec.keyFromPrivate(decrypted, 'hex');
      const genPK = ecSK.getPublic().encode('hex');
      const txObject = {};
      let action = "";
      
      if (type === "Account") {
        txObject.account = data;
        txObject.action = "retireAccount";
        action = "retireAccount";
      } else if (type === "Role") {
        txObject.role = data;
        txObject.action = "retireRole";
        action = "retireRole";
      } else if (type === "Group") {
        txObject.group = data;
        txObject.action = "retireGroup";
        action = "retireGroup";
      }
      const txs = JSON.stringify(txObject);
      const hash = GetHash(txs);
      
      // only sign hash
      const sign = ecSK.sign(hash).toDER('hex');
      
      const chainTx = {
        action:action,
        homeId: context.homeId,
        user: context.user,
        publicKey:context.publicKey,
        hash:hash,
        signature:sign,
        tx:txs
      };
      
      
      const url = "http://127.0.0.1:4000/home/retireAccount" ;
      
      axios.post(url, {
        tx: chainTx
      }).then ((result) => {
        setSubmitted(true);
        
      }).catch ((error) => {
        console.log("Something happened during the posting task to the network... " + error );
        setError(true);
      });
    } catch (error) {
      console.log("Error " + error);
    }
  };
  
  
  
  
  return (
    <Dialog onClose={handleClose} open={open} fullWidth maxWidth="lg" classes={{paper: classes.dialog }} style={{marginTop:50, marginLeft:50}}>
      
      <div className={classes.content} style={{width:800,marginLeft:50}} align="center">
        
        <div style={{marginTop:50}}>
          <Typography color="primary" variant="h5" align="center">Retire {type}  </Typography>
        </div>
        <div style={{marginTop:5}}>
          <Typography color="secondary" variant="h4" align="center">{data} </Typography>
        </div>
        
        {!submitted &&
        <div style={{marginTop: 50}} align="center">
          
          <TextField id="password" value={password} style={{width:250 }} onChange={(e) => { setPassword(e.target.value)}} label="Key Password"/>
        </div>
        
        }
        
        {!submitted &&
        <div style={{marginTop: 50}} align="center">
          
          
          <Button variant="contained" color="primary" style={{marginRight: 40, borderRadius: 25}}
                  onClick={signAndSubmit}>
            Sign and Approve
          </Button>
          
          
          <Button variant="contained" color="primary" style={{borderRadius: 25, marginLeft: 5}} onClick={handleClose}>
            Cancel
          </Button>
        
        
        </div>
        
        }
        
        <div>
          {submitted &&
          <div align="center">
            <Typography variant="h6"> Approval Submitted to the network  </Typography>
            <Button variant="contained" color="primary" style={{borderRadius: 25, marginTop: 100}} onClick={handleClose}>
              Close
            </Button>
          </div>
          
          }
        
        </div>
        
        <Divider/>
        
      </div>
    
    
    </Dialog>
  );
}


const dialogStyles = makeStyles({
  dialog: {
    position: 'absolute',
    left: 50,
    top: 50
  },
  table: {
    minWidth: 700,
  },
});


