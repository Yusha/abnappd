import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import TextField from '@material-ui/core/TextField';
import PhoneIcon from '@material-ui/icons/Phone';
import EmailIcon from '@material-ui/icons/Email';
import BusinessIcon from '@material-ui/icons/Business';
import CountryIcon from '@material-ui/icons/AssistantPhoto';

import axios from 'axios';

import {GetHash, GetSignature} from '../../ec/CryptoUtils';

import PouchDB from 'pouchdb-browser';

const myDB = new PouchDB('myDB');

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
  },
  backButton: {
    marginRight: theme.spacing(1),
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(2),
    
  },
  margin: {
    margin: theme.spacing(1),
  },
}));


function getSteps() {
  return ['Choose Properties', 'Sign and Submit', ' Finalize'];
}

function getStepContent(stepIndex) {
  switch (stepIndex) {
    case 0:
      return <ChooseProps/>;
    case 1:
      return <SignAndSubmit />;
    case 2:
      return <Finalize />
    default:
      return 'Finish home creation process';
  }
}



export default function NewGroup(props) {
  
  const classes = useStyles();
  
  const {callback} = props;
  
  const [activeStep, setActiveStep] = React.useState(0);
  const [propValues, setPropValues] = React.useState({});
  const [status, setSuccess] = React.useState({});
  
  const steps = getSteps();
  
  const saveAndSubmit = (pass)=> {
    
    const password = pass;
    
    const sTx = JSON.stringify(propValues);
    const hashOf = GetHash(sTx);
    const signature = GetSignature(props.context.encryptedPK,password,hashOf);
  
  
    const chainTx = {
      action:"createGroup",
      homeId: props.context.homeId,
      user: props.context.user,
      publicKey:props.context.publicKey,
      hash:hashOf,
      signature:signature,
      tx:sTx
    };
    
    const url = "http://127.0.0.1:4000/home/createGroup";
    
      
      axios.post(url, {
        tx: chainTx
      }).then (result => {
        console.log("Transaction sent to the net work" + result);
        setSuccess({status:"Transaction Succeeded", id:hashOf});
        setActiveStep(prevActiveStep => prevActiveStep + 1);
        
      }).catch (error => {
        console.log("Error in sending the tx to the network... " + JSON.stringify(error));
        setSuccess({status:"Transaction Failed", id:hashOf});
        setActiveStep(prevActiveStep => prevActiveStep + 1);
        
      });
     
  };
  
  const  handleNext = () => {
    
    setActiveStep(prevActiveStep => prevActiveStep + 1);
  };
  
  const handleBack = () => {
    setActiveStep(prevActiveStep => prevActiveStep - 1);
  };
  
  const handleReset = () => {
    setActiveStep(0);
  };
  
  const savePropsAndNext = (propValues) => {
    setPropValues(propValues);
    handleNext();
  };
  
  const complete = () => {
    console.log("Please send me to the parent...");
    callback(false,"Department created successfully");
  };
  
  
  return (
    <Grid className={classes.root}>
      <Stepper activeStep={activeStep} alternativeLabel>
        {steps.map(label => (
          <Step key={label}>
            <StepLabel>{label}</StepLabel>
          </Step>
        ))}
      </Stepper>
      
      <Grid container justify = "center">
        <div className={classes.instructions}>
          
          {activeStep === 0 &&
            <ChooseProps savePropsAndNext = {savePropsAndNext}/>
          }
  
          {activeStep === 1 &&
            <SignAndSubmit backCallback = {handleBack} signAndSubmitCallback = {saveAndSubmit}/>
            
          }
  
          {activeStep === 2 &&
            <Finalize completeCallback = {complete} status = {status}/>
    
          }
        </div>
      </Grid>
      
      <Grid container justify = "center">
        <Typography className={classes.instructions}>
        
        </Typography>
      
      </Grid>
      <Divider/>
      
    </Grid>
  );
}


export function ChooseProps(props) {
  
  const {savePropsAndNext} = props;
  const classes = useStyles();
  const [name, setName] = React.useState('');
  const [admin, setAdmin] = React.useState('');
  const [description, setDescription] = React.useState('');
  
  
  const setFieldValue = (e)=> {
    const id = e.target.id;
    const value = e.target.value;
    
    if (id === "name") {
      setName(value);
      
    } else if (id === "admin") {
      setAdmin(value);
      
    } else if (id === "description") {
      setDescription(value);
      
    } else {
      console.log("Unknown Field detected. Should not have happened");
    }
    
  };
  
  return (
    
    <div>
      <div className={classes.margin}>
        <Grid container spacing={1} alignItems="flex-end">
          <Grid item>
            <BusinessIcon/>
          </Grid>
          <Grid item>
            <TextField id="name" value={name} onChange={setFieldValue} label="Group Name"/>
          </Grid>
        </Grid>
      </div>
      <div className={classes.margin}>
        <Grid container spacing={1} alignItems="flex-end">
          <Grid item>
            <PhoneIcon/>
          </Grid>
          <Grid item>
            <TextField id="admin" value={admin} onChange={setFieldValue}label="Group Admin"/>
          </Grid>
        </Grid>
      </div>
      
      <div className={classes.margin}>
        <Grid container spacing={1} alignItems="flex-end">
          <Grid item>
            <EmailIcon/>
          </Grid>
          <Grid item>
            <TextField id="description" value={description} multiline onChange={setFieldValue} label="Group Description"/>
          </Grid>
        </Grid>
      </div>
  
  
      <div className={classes.margin} style={{marginTop:50}}>
        <Grid container spacing={1} alignItems="flex-end">
  
          <Grid container justify = "center">
            
              <Grid container justify = "center">
        
                <Grid>
                  <Button
                    disabled
                    className={classes.backButton}
                  >
                    Back
                  </Button>
          
                  <Button variant="contained" color="primary" onClick={()=>savePropsAndNext({name:name,admin:admin,description:description})}>
                    Next
                  </Button>
                </Grid>
              </Grid>
            
          </Grid>
          
          
        </Grid>
      </div>
      
    </div>
    
  );
}


export function SignAndSubmit(props) {
  
  const classes = useStyles();
  const {backCallback, signAndSubmitCallback} = props;
  
  const [password, setPassword] = React.useState('');
  
  const setValue = (e) => {
    setPassword(e.target.value);
    
  };
  
  
  return (
    
    <div>
      
      <div className={classes.margin}>
        <Grid container spacing={1} alignItems="flex-end">
          <Grid item>
            <CountryIcon/>
          </Grid>
          <Grid item>
            <TextField id="country" style={{width:300}} multiline value={password} onChange={setValue} label="Enter Password for the key..."/>
          </Grid>
        </Grid>
  
        <Grid container spacing={1} alignItems="flex-end" style={{marginTop:50}}>
  
          <Grid container justify = "center">
    
            <Grid container justify = "center">
      
              <Grid>
                <Button
                  onClick={backCallback}
                  className={classes.backButton}
                >
                  Back
                </Button>
        
                <Button variant="contained" color="primary" onClick={()=>signAndSubmitCallback(password)}>
                  Next
                </Button>
              </Grid>
            </Grid>
          </Grid>
          
        </Grid>
        
        
      </div>
    </div>
  
  );
}


export function Finalize(props) {
  
  const {completeCallback} = props;
  
  return (
    
    <div>
      
      <div style={{marginTop: 20, width:600, align:"center"}}>
        <Typography varivariant="h5" gutterBottom color="primary">
          Transaction Status {props.status.status}
        </Typography>
  
        <Grid container spacing={1} alignItems="flex-end">
    
          <Grid container justify = "center">
      
            <Grid container justify = "center">
        
              <Grid>
                <Button variant="contained" color="primary" onClick={completeCallback}>
                  Complete
                </Button>
              </Grid>
            </Grid>
          </Grid>
  
        </Grid>
      </div>
      
      
    
    </div>
  
  )
}