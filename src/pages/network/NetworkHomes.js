
import React, { useEffect,useStates } from 'react';

import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';

import axios from 'axios';



import MaterialTable, { MTableToolbar } from 'material-table';
import { forwardRef } from 'react';

import TableIconDef from '../common/IconDef';



export default function NetworkHomes(props) {
  
  const {context, callback} = props;
  
  const [homes,setHomes] = React.useState([]);
  
  const onRemove = (e,row) => {
    console.log("Remove Button clicked");
  };
  
  const onRowClicked = (e, row) => {
    const toShowTx = homes[row.tableData.id];
    const toShow = JSON.parse(toShowTx.tx.tx).name;
    callback(toShow);
  };
  
  
  
  useEffect(()=> {
    
    const url = "http://localhost:4000/network/get?home=" + context.homeId;
    
    axios.get(url).then ((result) => {
      setHomes(result.data);
      
    }).catch ((error) => {
      console.log("Error in getting Internal Pending Invitations data  " + JSON.stringify(error));
    })
    
  },[]);
  
  
  
  const connectionData = homes.map((connection, index) => {
    const tx = JSON.parse(connection.tx.tx);
    return {name:tx.name};
  });
  
  return (
    <div>
      
      
      
      <div style={{marginTop:20}}>
        
        <div align= "center "> Network Connections </div>
  
        <Grid container spacing={1} direction='row'>
  
          <Grid xs={2} item style={{marginTop:10}}>
  
  
          </Grid>
  
          <Grid xs={9} item style={{marginTop:10}}>
          
          <MaterialTable
            title="Our Network"
            icons={TableIconDef}
          
            localization={{
              header: {
                actions: 'Remove'
              },
            
            }}
          
          
            columns={[
              {title: 'Home Id', field: 'name'},
            ]}
            data={connectionData}
            actions={[
              rowData => ({
                icon: TableIconDef.Delete,
                tooltip: 'Remove',
                onClick: onRemove
              })
            ]}
          
            style = {{
              borderStyle:"none"
            }}
          
            options={{
              actionsColumnIndex: -1,
              pageSize:15,
              search:false,
              showTitle:false,
              doubleHorizontalScroll:true,
              padding: "dense",
              paging: false,
              headerStyle: {
                backgroundColor: '#0091EA',
                color: '#FFF'
              }
            }}
            onRowClick={onRowClicked}
          
            components={{
              Toolbar: props => (
                <div align="center" style={{maxHeight:30, color:"primary"}}>
                  <MTableToolbar {...props} />
                </div>
              ),
              Action: props => (
                <Button
                  onClick={(event) => onRemove(event, props.data)}
                  style={{color: 'primary'}}
                  size="small"
                >
                  Remove
                </Button>
              ),
            }}
        
          />
          
          </Grid>
  
          <Grid xs={2} item style={{marginTop:10}}>
          
            
          </Grid>
          
          
        </Grid>
      
      
      </div>
    
    </div>
  
  );
}
