import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import TextField from '@material-ui/core/TextField';

import CountryIcon from '@material-ui/icons/AssistantPhoto';



import axios from 'axios';

import {GetHash, GetSignature} from '../../ec/CryptoUtils';



const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
  },
  backButton: {
    marginRight: theme.spacing(1),
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(2),
    
  },
  button: {
    margin: theme.spacing(1),
  },
  margin: {
    margin: theme.spacing(1),
  },
  input: {
    display: 'none',
  },
}));


function getSteps() {
  return ['Find Home','Finalize and Send to Network'];
}


export default function InviteToNetwork(props) {
  
  const classes = useStyles();
  
  const {callback, context } = props;
  
  const [activeStep, setActiveStep] = React.useState(0);
  
  const [name,setName] = React.useState("");
  
  const [status, setStatus] = React.useState({});
  
  const [finished, setFinished] = React.useState(false);
  
  
  
  const steps = getSteps();
  
  
  const submitCallback = (pass)=> {
    
    const password = pass;
    
    
    
    const baseObject = {
      name:name,
      timeStamp: new Date()
    };
    
    
    const sTx = JSON.stringify(baseObject);
    
    const hashOf = GetHash(sTx);
    
    const signature = GetSignature(props.context.encryptedPK,password,hashOf);
    
    const chainTx = {
      homeId: context.homeId,
      user: context.user,
      publicKey:context.publicKey,
      hash:hashOf,
      signature:signature,
      tx:sTx
    };
    
    const url = "http://127.0.0.1:4000/network/add";
    
    axios.post(url,chainTx).then (result => {
      setFinished(true);
      setStatus({status:"PI Tx succeeded", id:hashOf});
      
    }).catch (error => {
      console.log("Error in sending the PI tx to the network... " + JSON.stringify(error));
      setFinished(true);
      setStatus({status:"Transaction Failed", id:hashOf});
      
    });
    
  };
  
  const  handleNext = () => {
    
    setActiveStep(prevActiveStep => prevActiveStep + 1);
  };
  
  const handleBack = () => {
    setActiveStep(prevActiveStep => prevActiveStep - 1);
  };
  
  
  const saveNameCallback = (name) => {
    setName(name);
    handleNext();
  };
  
  
  
  return (
    <Grid className={classes.root}>
      <Stepper activeStep={activeStep} alternativeLabel>
        {steps.map(label => (
          <Step key={label}>
            <StepLabel>{label}</StepLabel>
          </Step>
        ))}
      </Stepper>
  
      
        
        <Grid container justify="center">
          <div className={classes.instructions}>
      
          {activeStep === 0 &&
          <FindHome saveNameCallback={saveNameCallback} cancelCallback = {callback} />
          }
            
            {activeStep === 1 &&
          <Finalize backCallback={handleBack} submitCallback={submitCallback} cancelCallback = {callback}/>
        
          }
          </div>
          
        </Grid>
      
      <Divider/>
      
      {finished &&
      <Grid container justify="center" style={{marginTop:50}}>
        <Typography className={classes.instructions} color="red">
          Status of the Transaction is  " {status.status}
        
        </Typography>
        
        <Button onClick = {callback} variant="contained" color="secondary"> Complete </Button>
      
      
      </Grid>
      }
    
    </Grid>
  );
}


function FindHome(props) {
  
  const {saveNameCallback, cancelCallback} = props;
  const classes = useStyles();
  const [name, setName] = React.useState('');
  const [isAvailable, setIsAvailable] = React.useState(false);
  
  
  const setFieldValue = (e)=> {
    const id = e.target.id;
    const value = e.target.value;
    
    if (id === "name") {
      setName(value);
    }
    
  };
  
  const checkAvailable = async(e) => {
    let url = 'http://localhost:4000/api/exists?home='+ name;
    let exists = await axios.get(url);
    if (exists.data) {
      setIsAvailable(true);
    } else {
      setIsAvailable(false);
    }
  };
  
  
  return (
    
    <div>
      <div className={classes.margin}>
        <Grid container spacing={1} alignItems="flex-end">
          
          <Grid item>
            <TextField id="name" value={name} onChange={setFieldValue} style={{width:300}} label="Name"/>
          </Grid>
        </Grid>
      </div>
      
      
      
      <div className={classes.margin} style={{marginTop:30}}>
        <Grid container spacing={1} alignItems="flex-end">
          
          <Grid container justify = "center">
            
            <Grid container justify = "center">
              
              <Grid>
  
  
                {isAvailable &&
                  <div>
                    <Typography color="secondary"> Organization Home Exists. Click Next to Continue </Typography>
                  </div>
                }
                
               <div style={{marginTop:20}}>
  
                  {!isAvailable &&
                  <Button variant="contained" color="primary" onClick={checkAvailable}>
                    Check Availability
                  </Button>
                  }
  
                  {isAvailable &&
                  <Button variant="contained" color="primary" onClick={() => saveNameCallback(name)}>
                    Next
                  </Button>
                  }
                  <Button variant="contained" style = {{marginLeft:5}} color="primary" onClick={() => cancelCallback()}>
                    Cancel
                  </Button>
                 
                 </div>
                
              </Grid>
            </Grid>
          
          </Grid>
        
        
        </Grid>
      </div>
    
    </div>
  
  );
}


function Finalize(props) {
  
  const classes = useStyles();
  
  const {backCallback, submitCallback, cancelCallback} = props;
  
  const [password, setPassword] = React.useState('');
  
  const setValue = (e) => {
    setPassword(e.target.value);
  };
  
  return (
    
    <div>
      
      <div className={classes.margin}>
        <Grid container spacing={1} alignItems="flex-end">
          <Grid item>
            <CountryIcon/>
          </Grid>
          <Grid item>
            <TextField id="country" multiline value={password} onChange={setValue} style={{width:400}} label="password to unlock Private Key..."/>
          </Grid>
        </Grid>
        
        <Grid container spacing={1} alignItems="flex-end" style = {{marginTop:30}}>
          
          <Grid container justify = "center">
            
            <Grid container justify = "center">
              
              <Grid>
                <Button
                  onClick={backCallback}
                  className={classes.backButton}
                >
                  Back
                </Button>
                
                <Button variant="contained" color="primary" onClick={()=>submitCallback(password)}>
                  Sign and Submit
                </Button>
  
                <Button variant="contained" color="primary" onClick={() => cancelCallback()}>
                  Cancel
                </Button>
              </Grid>
            </Grid>
          </Grid>
        
        </Grid>
      
      </div>
    </div>
  
  );
}
