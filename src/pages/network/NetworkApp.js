

import React from 'react';


import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import IconButton from '@material-ui/core/IconButton';

import AddIcon from '@material-ui/icons/Add';
import ListIcon from '@material-ui/icons/List';
import ReferenceIcon from '@material-ui/icons/Dns';
import CollaborateIcon from '@material-ui/icons/GroupWork';

import Fab from '@material-ui/core/Fab';

import axios from 'axios';


import MaterialTable, {MTableToolbar} from 'material-table'

import Button from '@material-ui/core/Button';

import EmailIcon from '@material-ui/icons/Email';
import PhoneIcon from '@material-ui/icons/Phone';
import HomeIcon from '@material-ui/icons/Home';
import Home from '@material-ui/icons/HomeWork';
import POC from '@material-ui/icons/Gavel';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import NetworkIcon from '@material-ui/icons/Business';
import FileIcon from '@material-ui/icons/FileCopy';
import InviteIcon from '@material-ui/icons/ThumbUp';

import Grid from '@material-ui/core/Grid';

import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';


import Dialog from '@material-ui/core/Dialog';

import InputAdornment from '@material-ui/core/InputAdornment';
import TextField from '@material-ui/core/TextField';

import {Editor} from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { EditorState, convertToRaw } from 'draft-js';
import {GetEncrypted,GetSignature,GetHash} from "../../ec/CryptoUtils";

import NetworkHomes from './NetworkHomes';
import Invite from "./Invite";
import Add from './Add';
import tableIcons from '../common/IconDef';





const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    marginTop:50,
    backgroundColor:"white"
  },
  nested: {
    paddingLeft: theme.spacing(11),
  },
  
  toolbar: theme.mixins.toolbar,
}));

export default function NetworkApp(props) {
  
  const {context} = props;
  const classes = useStyles();
  
  const [selectedIndex, setSelectedIndex] = React.useState(0);
  
  const [snackMessage, setSnackMessage] = React.useState("test message");
  const [snackOpen, setSnackOpen] = React.useState(false);
  const [error, setError] = React.useState(false);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const anchorOpen = Boolean(anchorEl);
  const [tabValue, setTabValue] = React.useState(5);
  
  const [showHomeDetail, setShowHomeDetail] = React.useState(false);
  const [homeData, setHomeData] = React.useState({});
  
  
  const handleTabChange = (event, newValue) => {
  
    if (newValue === 0 ) {
      props.setActiveScreenCallback(1); // Dashboard
    } else if (newValue === 1 ) {
      props.setActiveScreenCallback(7); // files
    } else if (newValue === 2 ) {
      props.setActiveScreenCallback(3); // Reference Objects
    } else if (newValue  === 3) {
      props.setActiveScreenCallback(4); // Contracts
    } else if (newValue  === 4) {
      props.setActiveScreenCallback(6); // Collaborate
    } else if (newValue  === 5) {
      props.setActiveScreenCallback(2); // Network
    }
    setTabValue(newValue);
  };
  
  const onLogout = () => {
    console.log("Logout called");
    props.logoutCallback();
    
  };
  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  
  const selectTab = (event, index) => {
    setShowHomeDetail(false);
    setSelectedIndex(index);
    
    
  };
  
  
  
  const showHomeInfoCallback = (homeId) => {
    // get the info for the home and set in state
    const url = "http://localhost:4000/api/search?home=" + homeId ;
  
    axios.get(url).then ((result) => {
    
      if ( !(result.data.profile === undefined)) {
        
        setHomeData(result.data);
        setShowHomeDetail(true);
        
      }
    }).catch ((error) => {
      console.log("Error in Data Fetch...   " + JSON.stringify(error));
      setError("Data Fetch Failed...");
    });
    
  };
  
  const inviteCallback = () => {
    setSnackMessage("Invite Sucessfull");
    setSnackOpen(true);
    setSelectedIndex(0);
  };
  
  const homeDetailCallback = () => {
    setShowHomeDetail(false);
  }
  
  return (
    <div className={classes.root}>
      <CssBaseline/>
      
      <AppBar position="fixed" className={classes.appBar} style = {{backgroundColor:"white", color:"#303f9f"}}>
        <Toolbar>
          <Grid direction="row"  container>
  
            <Grid xs={1} item style={{marginTop:10}}>
              <Typography variant="h6" >
                ABBN
              </Typography>
            </Grid>
            
            <Grid xs={9} item>
              <Grid >
                <Tabs
                  value={tabValue}
                  onChange={handleTabChange}
                  variant="fullWidth"
                  indicatorColor="primary"
                  textColor="primary"
                  centered
                >
                  <Tab icon={<Home fontSize="small"/>} label="Home"/>
                  <Tab icon={<FileIcon  fontSize="small" />} label="Files"/>
                  <Tab icon={<ReferenceIcon  fontSize="small" />} label="Reference Data Objects"/>
                  <Tab icon={<POC  fontSize="small" />} label="Contracts"/>
                  <Tab icon={<CollaborateIcon fontSize="small"/>} label="Collaborate"/>
                  <Tab icon={<NetworkIcon fontSize="small"/>} label="Network"/>
                
                </Tabs>
              </Grid>
            </Grid>
            <Grid item xs={1} />
            
            <Grid item xs={1} >
              <Grid >
                
                <div>
                  <IconButton
                    
                    aria-haspopup="true"
                    onClick={handleMenu}
                    color="inherit"
                  
                  >
                    <AccountCircle fontSize="large"/>
                  </IconButton>
                  <Menu
                    id="menu-appbar"
                    anchorEl={anchorEl}
                    anchorOrigin={{
                      vertical: 'top',
                      horizontal: 'right',
                    }}
                    keepMounted
                    transformOrigin={{
                      vertical: 'top',
                      horizontal: 'right',
                    }}
                    open={anchorOpen}
                    onClose={handleClose}
                  >
                    <MenuItem >{context.homeId} / {context.user}</MenuItem>
                    <MenuItem onClick={onLogout}>logout</MenuItem>
                  </Menu>
                </div>
              
              </Grid>
            </Grid>
          </Grid>
        
        </Toolbar>
      </AppBar>
      
      <Drawer
        className={classes.drawer}
        variant="permanent"
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <div className={classes.toolbar}/>
        
        <List>
  
          <div style={{marginLeft:20,marginRight:20,width:"90%",fontSize:14,marginTop:30}} align="center">
            {context.homeId}/ {context.user}
          </div>
  
  
          <ListItem button key="0" style={{marginTop:10}} onClick={event => selectTab(event, 0)} selected={selectedIndex === 0}>
            <ListItemIcon color="primary">
              <AddIcon/>
            </ListItemIcon>
            <ListItemText primary={<Typography style={{ color: '#303f9f' }}>Add</Typography>}/>
          </ListItem>
  
          <ListItem button key="1" style={{marginTop:10}} onClick={event => selectTab(event, 1)} selected={selectedIndex === 1}>
            <ListItemIcon color="primary">
              <InviteIcon/>
            </ListItemIcon>
            <ListItemText primary={<Typography style={{ color: '#303f9f' }}>Invite</Typography>}/>
          </ListItem>
  
  
  
          <ListItem button key="2" style={{marginTop:10}} onClick={event => selectTab(event, 2)} selected={selectedIndex === 2}>
            <ListItemIcon color="primary">
              <ListIcon/>
            </ListItemIcon>
            <ListItemText primary={<Typography style={{ color: '#303f9f' }}>Network</Typography>}/>
          </ListItem>
          
          
        </List>
        <Divider/>
      </Drawer>
    
      
      <main className={classes.content}>
        
        
        { selectedIndex === 0 &&
        <div>
          <AddSection context={props.context} />
        </div>
        }
  
        { selectedIndex === 1 &&
        <div>
          <Invite context={props.context} callback={inviteCallback}/>
        </div>
        }
  
        { selectedIndex === 2 &&
        <div>
          <NetworkHomes context={props.context} callback={showHomeInfoCallback}/>
        </div>
        }
        
        
        
        {showHomeDetail &&
          <HomeDetailDialog searchResult={{value:homeData}} open = {showHomeDetail} context = {context} callback = {homeDetailCallback}/>
        }
        
      </main>
    </div>
  );
}

function AddSection (props) {
  
  const {context} = props;
  const [addOption, setAddOption] = React.useState(false);
  
  const classes = useStyles();
  
  const showAddOption = () => {
    setAddOption(true);
  };
  
  const addOptionCallback = () => {
    setAddOption(false);
  };
  
  
  return (
    
    <div style={{marginTop:100}}>
      
      <Grid container alignItems="center"
            justify="center" direction="row">
        
        <Grid item xs={10}>
          {!addOption &&
          <div style={{backgroundColor: "white", marginTop: 100}} align="center">
    
    
            <Fab color="secondary" onClick={showAddOption}>
              <AddIcon/>
            </Fab>
            <Typography style={{marginTop: 25}}>
              Add A New Organization to the Network
            </Typography>
  
          </div>
          }
          
          {addOption &&
            
            <Add context = {context} callback = {addOptionCallback}/>
          }
          
        </Grid>
        
      </Grid>
    
    </div>
  
  )
  
}




function HomeDetailDialog (props) {
  
  const {searchResult, context, callback, open} = props;
  const [tabValue, setTabValue] = React.useState(0);
  
  const handleTabChange = (event, newValue) => {
    setTabValue(newValue);
  };
  
  
  return (
  
    <Dialog onClose={callback} open={open} fullWidth maxWidth="lg"  >
      
      <ProfileTab profile={searchResult.value.profile} context = {context} />
      
      <Grid container spacing={0}
            direction="column"
            style = {{marginTop:50}}
            alignItems="center"
            justify="center">
        <Grid item xs={9}>
          
          <Tabs
            value={tabValue}
            onChange={handleTabChange}
          
          >
            <Tab label="Accounts"/>
            <Tab label="Reference Objects"/>
            <Tab label="Payment Instructions"/>
          </Tabs>
        
        </Grid>
        
        <Grid item xs={9} style={{marginTop: 70}}>
          
          {(tabValue === 0) && <AccountsTab accounts={searchResult.value.contacts} profile = {searchResult.value.profile} context = {context} />}
          {(tabValue === 1) && <ObjectsTab objects={searchResult.value.objects} profile = {searchResult.value.profile} context = {context} />}
          {(tabValue === 2) && <PITab PIs={searchResult.value.PIs} profile = {searchResult.value.profile} context = {context} />}
        </Grid>
      
      
      </Grid>
      
      
    </Dialog>
  
  
  );
}
function ProfileTab(props) {
  
  const {profile} = props;
  
  return (
    <div >
      
      <Grid container spacing={0}>
  
        <Grid item xs={4} style={{marginTop: 70}}>
        
        </Grid>
  
        <Grid item xs={4} style={{marginTop: 70}}>
        
        
      
          <Typography align="left" variant="h4" color="primary">
            {profile.businessName}
          </Typography>
      
          <Typography align="left" variant="h6" style={{marginTop:20}} >
            <HomeIcon  style={{color:"#F50057",marginRight:10}} /> {profile.address}
          </Typography>
          <Typography align="left" variant="h6">
            <PhoneIcon style={{color:"#F50057",marginRight:10,marginTop:10}} />  {profile.phone}
          </Typography>
          <Typography align="left" variant="h6">
            <EmailIcon style={{color:"#F50057",marginRight:10,marginTop:10}}/>  {profile.email}
          </Typography>
        </Grid>
        
        <Grid item xs={4} style={{marginTop: 70}}>
  
        </Grid>




      </Grid>
    
    </div>
  
  );
  
}
function AccountsTab(props) {
  
  const {accounts, profile, context} = props;
  const [rowData, setRowData] = React.useState({});
  const [composeMessage, setComposeMessage] = React.useState(false);
  
  const rowClicked = (e,row) => {
    
    setRowData(row);
    setComposeMessage(true);
    
  };
  
  const composeMessageDialogCallback = (message) => {
    setComposeMessage(false);
  };
  
  const accountData = accounts.map((account, index) => {
    return {name:account.name,group:account.group,role:account.role,email:account.email,phone:account.phone,publicKey:account.publicKey};
  });
  
  return (
    <div>
      
      <div>
        <MaterialTable
          title={profile.businessName}
          icons={tableIcons}
          
          localization={{
            
            header: {
              actions: 'message'
            },
            
          }}
          
          
          columns={[
            {title: 'contact', field: 'name'},
            {title: 'group', field: 'group'},
            {title: 'role', field: 'role'},
            {title: 'email', field: 'email'},
            {title: 'phone', field: 'phone'},
            {title: 'public key', field: 'publicKey', cellStyle: {wordBreak: 'break-all'}},
          
          
          ]}
          data={accountData }
          actions={[
            rowData => ({
              icon: tableIcons.Email,
              tooltip: 'Send Message',
              onClick: rowClicked
            })
          ]}
          options={{
            actionsColumnIndex: -1,
            padding: "dense",
            headerStyle: {
              backgroundColor: '#0091EA',
              color: '#FFF'
            }
          }}
        
        />
      
      </div>
      
      <div>
        
        <SendMessageDialog   dialogCallback={composeMessageDialogCallback} context = {context} open = {composeMessage} rowData = {rowData} homeId={profile.homeId}/>
      
      </div>
    
    </div>
  
  );
}
function ObjectsTab(props) {
  
  const {objects, profile, context} = props;
  
  const [rowData, setRowData] = React.useState({});
  const [readObject, setReadObject] = React.useState(false);
  
  const readObjectDialogCallback = (action) => {
    setReadObject(false);
    
  };
  
  const rowClicked = (e,row) => {
    setRowData(row);
    setReadObject(true);
  };
  
  const allObjects = objects.map((object,index) => {
    return {name:object.name,description:object.description,encrypted:object.encrypted,ownerHome:object.ownerHome,owner:object.owner,publicKey:object.publicKey};
  });
  
  return (
    <div>
      <div>
        <MaterialTable
          title={profile.businessName}
          icons={tableIcons}
          
          localization={{
            
            header: {
              actions: 'Access'
            },
            
          }}
          
          
          columns={[
            {title: 'Object Name', field: 'name'},
            {title: 'Description', field: 'description',cellStyle: {wordBreak: 'break-all'}},
            {title: 'Encrypted', field: 'encrypted'},
            {title: 'Owner Home', field: 'ownerHome'},
            {title: 'owner', field: 'owner'},
            {title: 'public Key', field: 'publicKey',cellStyle: {wordBreak: 'break-all'}},
          
          ]}
          data={allObjects }
          actions={[
            rowData => ({
              icon: tableIcons.Unlock,
              tooltip: 'Request Access',
              onClick: rowClicked
            })
          ]}
          options={{
            actionsColumnIndex: -1,
            padding: "dense",
            headerStyle: {
              backgroundColor: '#0091EA',
              color: '#FFF'
            }
          }}
          
          components={{
            Toolbar: props => (
              <div style={{ backgroundColor: 'white'}}>
                <MTableToolbar {...props} />
              </div>
            ),
          }}
        
        />
      
      </div>
      
      <div>
        
        <ObjectReadRequestDialog   dialogCallback={readObjectDialogCallback}
                                   open = {readObject} rowData = {rowData} homeId={profile.homeId} context = {context}/>
      
      </div>
    
    </div>
  
  );
  
}
function PITab(props) {
  
  const {PIs, profile, context} = props;
  
  const [rowData, setRowData] = React.useState({});
  const [readObject, setReadObject] = React.useState(false);
  
  const readObjectDialogCallback = (action) => {
    setReadObject(false);
    
  };
  
  const rowClicked = (e,row) => {
    setRowData(row);
    setReadObject(true);
  };
  
  const allPIs = PIs.map((pi,index) => {
    
    return {name:pi.name,home:pi.homeId,account:pi.account,publicKey:pi.publicKey}
  });
  
  return (
    <div>
      <div>
        <MaterialTable
          title={profile.businessName}
          icons={tableIcons}
          
          localization={{
            
            header: {
              actions: 'Access'
            },
            
          }}
          
          
          columns={[
            {title: 'Name', field: 'name'},
            {title: 'Home Id', field: 'home',cellStyle: {wordBreak: 'break-all'}},
            {title: 'Account Id', field: 'account'},
            {title: 'public Key', field: 'publicKey',cellStyle: {wordBreak: 'break-all'}},
          
          ]}
          data={allPIs }
          actions={[
            rowData => ({
              icon: tableIcons.Unlock,
              tooltip: 'Request Access',
              onClick: rowClicked
            })
          ]}
          options={{
            actionsColumnIndex: -1,
            padding: "dense",
            headerStyle: {
              backgroundColor: '#0091EA',
              color: '#FFF'
            }
          }}
          
          components={{
            Toolbar: props => (
              <div style={{ backgroundColor: 'white'}}>
                <MTableToolbar {...props} />
              </div>
            ),
          }}
        
        />
      
      </div>
      
      <div>
        
        {readObject &&
        <ObjectReadRequestDialog dialogCallback={readObjectDialogCallback}
                                 open={readObject} rowData={rowData} homeId={profile.homeId} context={context}/>
        }
      </div>
    
    </div>
  
  );
  
}

const dialogStyles = makeStyles({
  dialog: {
    position: 'absolute',
    left: 10,
    top: 50
  }
});


function ObjectReadRequestDialog(props) {
  
  
  const { context, dialogCallback, open, rowData, homeId } = props;
  const classes = dialogStyles();
  
  
  const [password, setPassword] = React.useState("");
  const [editorState, setEditorState] = React.useState(EditorState.createEmpty());
  
  
  const [save, setSave] = React.useState(false);
  
  
  const recipientHomeId = rowData.ownerHome;
  const recipientAccount = rowData.owner;
  const recipientPK = rowData.publicKey;
  
  const accountDetails = rowData.owner?rowData.owner:rowData.account;
  
  const to = homeId + "/" + accountDetails;
  
  const from = context.homeId + "/" + context.user;
  
  const subject = "Object Read Request";
  
  const objectRequested = rowData.name;
  
  const handleClose = (action) => {
    dialogCallback(action);
  };
  
  
  const onEditorStateChange = (s)=> {
    setEditorState(s);
    
  };
  
  const onSave = () => {
    setSave(true);
  };
  
  const cancelThis = (e) => {
    dialogCallback({cancel:true});
  };
  
  const setFieldValue = (e) => {
    
    const id = e.target.id;
    const value = e.target.value;
    
    if (id === "password") {
      setPassword(value);
    }
    else {
      console.log("Wrong id in the form field.. should not hapen...");
    }
  };
  
  
  const onSubmit = () => {
    
    const raw = convertToRaw(editorState.getCurrentContent());
    
    
    const body = {
      senderHomeId:context.homeId,
      senderAccount:context.user,
      recipientHomeId:recipientHomeId,
      recipientAccount:recipientAccount,
      subject: subject,
      content: raw
    };
    
    const sBody = JSON.stringify(body);
    
    const hash = GetHash(sBody);
    
    const signature = GetSignature(context.encryptedPK,password,hash);
    const encryptedBody = GetEncrypted(recipientPK,sBody);
    
    const tx = {
      header: {
        senderHomeId: context.homeId,
        senderAccount:context.user,
        senderPublicKey:context.publicKey,
        recipientHomeId:recipientHomeId,
        recipientAccount:recipientAccount,
        type:"access",
        timeStamp:new Date(),
        hash:hash,
        signature:signature
      },
      body: encryptedBody
    };
    
    
    const url = "http://localhost:4000/message/sendMessage";
    
    
    axios.post(url,tx).then ((result) => {
      
      if (result.data.success) {
        // message posted successfully
        dialogCallback({cancel:false,success:true});
      } else {
        dialogCallback({cancel:false,success:false, error:result.error});
      }
      
    }).catch ((error) => {
      console.log("Error in posting a message");
    })
    
  };
  
  
  
  return (
    <Dialog onClose={handleClose} open={open} fullWidth maxWidth="lg" classes={{paper: classes.dialog }} >
      
      <div className={classes.content} style={{width:800,align:"center",marginLeft:50}}>
        
        <div className={classes.margin}>
          <Typography color="primary" variant="h5" align="center"> Object Read Request...</Typography>
        </div>
        
        
        
        <div className={classes.margin} align="center" style={{marginTop:30}}>
          <TextField
            
            style={{width:500,margin:2}}
            color="primary"
            value = {to}
            id ="to"
            readOnly
            onChange={setFieldValue}
            InputProps={{
              startAdornment: <InputAdornment position="start">@To...</InputAdornment>,
              className:classes.inputColor
            }}
          />
          
          <TextField
            
            style={{width:500,margin:2}}
            color="primary"
            value = {from}
            id ="from"
            readOnly
            onChange={setFieldValue}
            InputProps={{
              startAdornment: <InputAdornment position="start">@From...</InputAdornment>,
              className:classes.inputColor
            }}
          />
          
          <TextField
            
            style={{width:500,margin:2}}
            color="primary"
            value = {objectRequested}
            id ="object"
            readOnly
            onChange={setFieldValue}
            InputProps={{
              startAdornment: <InputAdornment position="start">@Object...</InputAdornment>,
              className:classes.inputColor
            }}
          />
          
          <TextField
            
            style={{width:500,margin:2,marginTop:10}}
            placeholder="Subject"
            color="primary"
            value = {subject}
            id ="subject"
            onChange={setFieldValue}
          
          />
          
          <Editor
            editorState={editorState}
            toolbarClassName="toolbarClassName"
            wrapperClassName="wrapperClassName"
            editorClassName="editorClassName"
            placeholder = "Note for the object owner..."
            onEditorStateChange={onEditorStateChange}
            editorStyle={{backgroundColor:"#FFFFFF",height:200}}
          />
          
          {save &&
          
          <TextField
            
            style={{width: 400, marginTop: 30, backgroundColor: "#FFFFFF", height: 20}}
            placeholder="password for private key"
            value={password}
            id="password"
            onChange={setFieldValue}
            color="secondary"
          />
          }
        
        </div>
        
        <div align="center" style={{marginTop:20, marginBottom:10}}>
          
          {!save &&
          <Button onClick={onSave} className={classes.shape} align="left" variant="contained" color="primary"
                  style={{width: 100, height: 30}}>Save </Button>
          }
          {save &&
          
          <Button onClick={onSubmit} className={classes.shape} align="left" variant="contained" color="primary"
                  style={{width: 100, height: 30}}>Submit </Button>
          }
          <Button onClick = {cancelThis} className={classes.shape}  variant="contained" align = "right" style={{width:100,height:30, marginLeft:20}} color="secondary" > Cancel </Button>
        
        </div>
      </div>
    
    
    </Dialog>
  );
}
function SendMessageDialog(props) {
  
  
  const { context, dialogCallback, open, rowData, homeId } = props;
  const classes = dialogStyles();
  
  const [subject, setSubject] = React.useState("");
  const [password, setPassword] = React.useState("");
  const [save, setSave] = React.useState(false);
  
  const recipientHomeId = homeId;
  const recipientAccount = rowData.email;
  const recipientPK = rowData.publicKey;
  
  const [editorState, setEditorState] = React.useState(EditorState.createEmpty());
  
  const to = homeId + "/" + rowData.email;
  
  
  const handleClose = (action) => {
    dialogCallback(action);
  };
  
  
  const onEditorStateChange = (s)=> {
    setEditorState(s);
    
  };
  
  const onSave = () => {
    setSave(true);
  };
  
  const cancelThis = (e) => {
    dialogCallback({cancel:true});
  };
  
  const setFieldValue = (e) => {
    
    const id = e.target.id;
    const value = e.target.value;
    
    if (id === "subject") {
      setSubject(value);
      
    } else if (id === "password") {
      setPassword(value);
    }
    else {
      console.log("Wrong id in the form field.. should not hapen...");
    }
  };
  
  const onSubmit = () => {
    
    const raw = convertToRaw(editorState.getCurrentContent());
    
    
    const body = {
      senderHomeId:context.homeId,
      senderAccount:context.user,
      recipientHomeId:recipientHomeId,
      recipientAccount:recipientAccount,
      subject: subject,
      content: raw
    };
    
    const sBody = JSON.stringify(body);
    
    const hash = GetHash(sBody);
    
    const signature = GetSignature(context.encryptedPK,password,hash);
    const encryptedBody = GetEncrypted(recipientPK,sBody);
    
    const tx = {
      header: {
        senderHomeId: context.homeId,
        senderAccount:context.user,
        senderPublicKey:context.publicKey,
        recipientHomeId:recipientHomeId,
        recipientAccount:recipientAccount,
        timeStamp:new Date(),
        type:"general",
        hash:hash,
        signature:signature
      },
      body: encryptedBody
    };
    
    const url = "http://localhost:4000/message/sendMessage";
    axios.post(url,tx).then ((result) => {
      
      if (result.data.success) {
        // message posted successfully
        dialogCallback({cancel:false,success:true});
      } else {
        dialogCallback({cancel:false,success:false, error:result.error});
      }
      
    }).catch ((error) => {
      console.log("Error in posting a message");
    })
    
  };
  
  
  
  return (
    <Dialog onClose={handleClose} open={open} fullWidth maxWidth="lg" classes={{paper: classes.dialog }} >
      
      <div className={classes.content} style={{width:800,align:"center",marginLeft:50}}>
        
        <div className={classes.margin}>
          <Typography color="primary" variant="h5" align="center"> Compose New Message</Typography>
        </div>
        
        
        
        <div className={classes.margin} align="center" style={{marginTop:30}}>
          <TextField
            
            style={{width:500,margin:2}}
            color="primary"
            value = {to}
            id ="to"
            readOnly
            onChange={setFieldValue}
            InputProps={{
              startAdornment: <InputAdornment position="start">@To...</InputAdornment>,
              className:classes.inputColor
            }}
          />
          
          <TextField
            
            style={{width:500,margin:2,marginTop:10}}
            placeholder="Subject"
            color="primary"
            value = {subject}
            id ="subject"
            onChange={setFieldValue}
          
          />
          
          <Editor
            editorState={editorState}
            toolbarClassName="toolbarClassName"
            wrapperClassName="wrapperClassName"
            editorClassName="editorClassName"
            onEditorStateChange={onEditorStateChange}
            editorStyle={{backgroundColor:"#FFFFFF",height:200}}
          />
          
          {save &&
          
          <TextField
            
            style={{width: 400, marginTop: 30, backgroundColor: "#FFFFFF", height: 20}}
            placeholder="password for private key"
            value={password}
            id="password"
            onChange={setFieldValue}
            color="secondary"
          />
          }
        
        </div>
        
        <div align="center" style={{marginTop:20, marginBottom:10}}>
          
          {!save &&
          <Button onClick={onSave} className={classes.shape} align="left" variant="contained" color="primary"
                  style={{width: 100, height: 30}}>Save </Button>
          }
          {save &&
          
          <Button onClick={onSubmit} className={classes.shape} align="left" variant="contained" color="primary"
                  style={{width: 100, height: 30}}>Submit </Button>
          }
          <Button onClick = {cancelThis} className={classes.shape}  variant="contained" align = "right" style={{width:100,height:30, marginLeft:20}} color="secondary" > Cancel </Button>
        
        </div>
      </div>
    
    
    </Dialog>
  );
}
