import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import TextField from '@material-ui/core/TextField';


import CountryIcon from '@material-ui/icons/AssistantPhoto';



import axios from 'axios';

import {GetHash, GetSignature} from '../../ec/CryptoUtils';



const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
  },
  backButton: {
    marginRight: theme.spacing(1),
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(2),
    
  },
  button: {
    margin: theme.spacing(1),
  },
  margin: {
    margin: theme.spacing(1),
  },
  input: {
    display: 'none',
  },
}));


function getSteps() {
  return ['Invitee','Finalize and Send to Network'];
}


export default function Invite(props) {
  
  const classes = useStyles();
  
  const {callback, context } = props;
  
  const [activeStep, setActiveStep] = React.useState(0);
  
  const [name,setName] = React.useState({});
  
  const [status, setStatus] = React.useState({});
  
  const [finished, setFinished] = React.useState(false);
  
  
  const steps = getSteps();
  
  const submitCallback = (pass)=> {
    
    const password = pass;
    
    const baseObject = {name:name.name};
    baseObject.timeStamp = new Date();
    baseObject.secret = GetHash(name.secret);
    
    
    const sTx = JSON.stringify(baseObject);
    
    const hashOf = GetHash(sTx);
    
    const signature = GetSignature(props.context.encryptedPK,password,hashOf);
    
    const chainTx = {
      homeId: context.homeId,
      user: context.user,
      invitee:name.name,
      publicKey:context.publicKey,
      secret: baseObject.secret,
      hash:hashOf,
      signature:signature,
      tx:sTx
    };
    
    const url = "http://127.0.0.1:4000/homeInvite/add";
    
    axios.post(url,chainTx).then (result => {
      setFinished(true);
      setStatus({status:"Success"});
      
    }).catch (error => {
      console.log("Error in sending the PI tx to the network... " + JSON.stringify(error));
      setFinished(true);
      setStatus({status:"Transaction Failed", id:hashOf});
    });
    
  };
  
  const  handleNext = () => {
    
    setActiveStep(prevActiveStep => prevActiveStep + 1);
  };
  
  const handleBack = () => {
    setActiveStep(prevActiveStep => prevActiveStep - 1);
  };
  
  
  const saveNameCallback = (name) => {
    setName(name);
    handleNext();
  };
  
  
  
  return (
    <Grid className={classes.root}>
      <Stepper activeStep={activeStep} alternativeLabel>
        {steps.map(label => (
          <Step key={label}>
            <StepLabel>{label}</StepLabel>
          </Step>
        ))}
      </Stepper>
  
      
        
        <Grid container justify="center">
          <div className={classes.instructions}>
      
          {activeStep === 0 &&
            <Invitee saveNameCallback={saveNameCallback} cancelCallback = {callback} />
          }
            
            {activeStep === 1 &&
          <Finalize backCallback={handleBack} submitCallback={submitCallback} cancelCallback = {callback}/>
        
          }
          </div>
          
        </Grid>
      
      <Divider/>
      
      {finished &&
      
      
      <Grid container justify="center" style={{marginTop:50}}>
  
  
        <Grid container spacing={1} >
  
          <Typography className={classes.instructions} color="red">
            Status of the Transaction is  " {status.status}
          </Typography>
        </Grid>
        <Grid container spacing={1} >
  
          <Grid item>
            <Typography> Please send the claim secret to your invitee. They will need it to claim their invitation </Typography>
          </Grid>
    
        </Grid>
  
        <Grid container spacing={1} >
    
          <Grid item>
            <TextField id="secret" value={name.secret} readOnly style={{width:300}} label="Claim Secret"/>
          </Grid>
        </Grid>
        
        <Button onClick = {callback} variant="contained" color="secondary"> Complete </Button>
      
      </Grid>
      }
    
    </Grid>
  );
}


function Invitee(props) {
  
  const {saveNameCallback, cancelCallback} = props;
  const classes = useStyles();
  const [name, setName] = React.useState('');
  const [secret, setSecret] = React.useState("");
  
  
  const setFieldValue = (e)=> {
    const id = e.target.id;
    const value = e.target.value;
    
    if (id === "name") {
      setName(value);
    } else if (id === "secret") {
      setSecret(value);
    }
  };
  
  const saveAndNext = () => {
    saveNameCallback({name:name,secret:secret});
  };
  
  
  
  return (
    
    <div>
      <div className={classes.margin}>
        <Grid container spacing={1} >
          
          <Grid item>
            <TextField id="name" value={name} onChange={setFieldValue} style={{width:300}} label="Invitee Email/Phone"/>
          </Grid>
        </Grid>
        <Grid container spacing={1} >
    
          <Grid item>
            <TextField id="secret" value={secret} onChange={setFieldValue} style={{width:300}} label="claim secret"/>
          </Grid>
        </Grid>
        
        
      </div>
      
      
      
      <div className={classes.margin} style={{marginTop:30}}>
        <Grid container spacing={1} alignItems="flex-end">
          
          <Grid container justify = "center">
            
            <Grid container justify = "center">
              
              <Grid>
  
                <div style={{marginTop:20}}>
                  
                  <Button variant="contained" color="primary" onClick={saveAndNext}>
                    Next
                  </Button>
                  
                  <Button variant="contained" style = {{marginLeft:5}} color="primary" onClick={() => cancelCallback()}>
                    Cancel
                  </Button>
                 
                 </div>
                
              </Grid>
            </Grid>
          
          </Grid>
        
        
        </Grid>
      </div>
    
    </div>
  
  );
}


function Finalize(props) {
  
  const classes = useStyles();
  
  const {backCallback, submitCallback, cancelCallback} = props;
  
  const [password, setPassword] = React.useState('');
  
  const setValue = (e) => {
    setPassword(e.target.value);
  };
  
  return (
    
    <div>
      
      <div className={classes.margin}>
        <Grid container spacing={1} alignItems="flex-end">
          <Grid item>
            <CountryIcon/>
          </Grid>
          <Grid item>
            <TextField id="country" multiline value={password} onChange={setValue} style={{width:400}} label="password to unlock Private Key..."/>
          </Grid>
        </Grid>
        
        <Grid container spacing={1} alignItems="flex-end" style = {{marginTop:30}}>
          
          <Grid container justify = "center">
            
            <Grid container justify = "center">
              
              <Grid>
                <Button
                  onClick={backCallback}
                  className={classes.backButton}
                >
                  Back
                </Button>
                
                <Button variant="contained" color="primary" onClick={()=>submitCallback(password)}>
                  Sign and Submit
                </Button>
  
                <Button variant="contained" color="primary" onClick={() => cancelCallback()}>
                  Cancel
                </Button>
              </Grid>
            </Grid>
          </Grid>
        
        </Grid>
      
      </div>
    </div>
  
  );
}
