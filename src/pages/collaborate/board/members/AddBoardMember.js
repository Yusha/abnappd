import React from 'react';


import Dialog from '@material-ui/core/Dialog';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import EmailIcon from '@material-ui/icons/Email';
import HomeIcon from '@material-ui/icons/Home';




import axios from 'axios';

import { makeStyles } from '@material-ui/core/styles';
import {GetSignature,GetHash} from '../../../../ec/CryptoUtils';


const dialogStyles = makeStyles({
  dialog: {
    position: 'absolute',
    left: 10,
    top: 50
  },
  input: {
    display: 'none',
  },
});

export default  function AddBoardMember(props) {
  
  
  const { context, callback, boardName, open } = props;
  
  const classes = dialogStyles();
  
  
  const [home, setHome] = React.useState("");
  const [account, setAccount] = React.useState("");
  
  
  
  const [password, setPassword] = React.useState("");
  const [save, setSave] = React.useState(false);
  
  
  
  const handleClose = (type, action) => {
    callback(type, action);
  };
  
  
  
  const onSave = () => {
    setSave(true);
  };
  
  const cancelThis = (e) => {
    callback("cancel", "");
  };
  
  const setFieldValue = (e) => {
    
    const id = e.target.id;
    const value = e.target.value;
    
    if (id === "home") {
      setHome(value);
    } else if (id ==="account") {
      setAccount(value);
    }
    else if (id === "password") {
      setPassword(value);
    }
    else {
      console.log("Wrong id in the form field.. should not hapen...");
    }
  };
  
  const onSubmit = () => {
    
    
    const body = {
      inviterHome:context.homeId,
      inviterAccount:context.user,
      boardName: boardName,
      inviteeHome: home,
      invitee:account,
    };
    
    const sBody = JSON.stringify(body);
    
    const hash = GetHash(sBody);
    const signature = GetSignature(context.encryptedPK,password,hash);
    
    const tx = {
      
      hash:hash,
      signature:signature,
      
      body: sBody
    };
    
    const url = "http://localhost:4000/documents/add";
    
    axios.post(url,tx).then ((result) => {
      
      if (result.data.success) {
        // message posted successfully
        handleClose("submit",{cancel:false,success:true});
      } else {
        handleClose("submit", {cancel:false,success:false, error:result.error});
      }
      
    }).catch ((error) => {
      console.log("Error in posting a message");
      
    })
  };

  
  return (
  
    <Dialog onClose={handleClose} open={open} fullWidth maxWidth="lg" classes={{paper: classes.dialog }} >
      
      <div className={classes.content} style={{width:800,align:"center",marginLeft:50}}>
        
        <div className={classes.margin}>
          <Typography color="primary" variant="h5" align="center"> Add A Member</Typography>
        </div>
        
        
        
        <div className={classes.margin} align="center" style={{marginTop:30}}>
  
  
          <div className={classes.margin}>
            <Grid container spacing={1} justify="center" alignItems="flex-end">
              <Grid item>
                <HomeIcon/>
              </Grid>
              <Grid item>
                <TextField id="home" value={home} onChange={setFieldValue} label="Organization Id"/>
              </Grid>
            </Grid>
          </div>
  
          <div className={classes.margin}>
            <Grid container justify="center" spacing={1} alignItems="flex-end">
              <Grid item>
                <EmailIcon/>
              </Grid>
              <Grid item>
                <TextField id="account" value={account} onChange={setFieldValue} label="Account Email"/>
              </Grid>
            </Grid>
          </div>
          
          
  
          {save &&
          
          <TextField
    
            style={{width: 400, marginTop: 30, backgroundColor: "#FFFFFF", height: 20}}
            placeholder="Enter Private Key Password.."
            value={password}
            id="password"
            onChange={setFieldValue}
            color="secondary"
          />
          }
          
        
        </div>
        
        <div align="center" style={{marginTop:30, marginBottom:100}}>
          
          {!save &&
          <Button onClick={onSave} className={classes.shape} align="left" variant="contained" color="primary"
                  style={{width: 100, height: 30}}>Save </Button>
          }
          {save &&
          
          <Button onClick={onSubmit} className={classes.shape} align="left" variant="contained" color="primary"
                  style={{width: 100, height: 30}}>Submit to Network </Button>
          }
          <Button onClick = {cancelThis} className={classes.shape}  variant="contained" align = "right" style={{width:100,height:30, marginLeft:20}} color="secondary" > Cancel </Button>
        
        </div>
      </div>
    
    
    </Dialog>
  );
}