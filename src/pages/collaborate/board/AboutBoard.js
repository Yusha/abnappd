import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';


const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    marginTop:100,
    backgroundColor:'white',
    align:"center",
    borderRadius:20,
    borderStyle:"solid",
    borderWidth:"0.5px",
    width:'60%',
    display: "inline-block"
  },
  paper: {
    padding: theme.spacing(2),
    margin: 'auto',
    maxWidth: 1000,
    marginTop:100
  }
}));

export default function AboutBoard(props) {
  
  const {context} = props;
  
  const classes = useStyles();
  
  return (
    <div className={classes.root} align="center">
      
      
  
      <Grid justify="center" alignItems="center" container>
        <Grid xs={12} item>
          <div align="center">
            <Typography variant="h4"> About This Group</Typography>
          </div>
          <Divider/>
      
        </Grid>
  
        <Grid xs={12} item style={{marginTop:10}}>
          <Typography gutterBottom variant="body2">
            Owner Organization
          </Typography>
          <Typography gutterBottom>
            {context.homeId}
          </Typography>
          <Divider/>
        </Grid>
        
  
        <Grid xs={12} item style={{marginTop:10}}>
          <Typography gutterBottom variant="body2">
            Owner
          </Typography>
          <Typography gutterBottom>
            {context.user}
          </Typography>
          <Divider/>
        </Grid>
      </Grid>
      
      
    </div>
  );
}
