import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import CommentIcon from '@material-ui/icons/ModeCommentOutlined';
import LikeIcon from '@material-ui/icons/ThumbUpOutlined';
import ReplyIcon from '@material-ui/icons/ReplyRounded';

import PostTopic from './PostTopic';


const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    marginTop:100,
    align:"center",
    borderRadius:20,
    width:'70%',
    display: "inline-block",
    
  },
  paper: {
    padding: theme.spacing(2),
    margin: 'auto',
    maxWidth: 1000,
    marginTop:100
  },
  button: {
    borderRadius:20,
    marginBottom:10
  },
  
}));

export default function BoardDiscussTab(props) {
  
  const {context} = props;
  
  const [isPostTopic, setPostTopic] = React.useState(false);
  
  const classes = useStyles();
  
  
  const getComments = () => {
    
    return (
  
      <div style={{width:"60%"}}  align="center">
        
        <div style={{borderRadius:30,backgroundColor:"#E3F2FD"}}>
          <Grid container direction="row" justify="space-between"
              alignItems="center">
      
            <Grid xs={4} item >
              <div style={{fontSize:12, fontWeight:"bold"}}> Tom @Microsoft </div>
            </Grid>
            <Grid xs={4} item >
      
            </Grid>
      
            <Grid xs={4} item >
              <div style={{fontSize:10}}>
                January 3, 2021,8.50 EST
              </div>
            </Grid>
    
          </Grid>
          <div style={{marginTop:2, fontSize:14}}>
            Good Job Buddy
          </div>
        </div>
        
        <div>
          <Grid container direction="row" justify="space-between"
                alignItems="center">
        
            <Grid xs={4} item >
          
              <Button
                variant="outlines"
                className={classes.button}
                size="small"
                style = {{fontSize:10,fontWeight:"bold"}}
                startIcon={<ReplyIcon /> }
              >
                Reply
              </Button>
              
            </Grid>
            <Grid xs={2} item >
              <Button
                variant="outlines"
                size="small"
                style = {{fontSize:10,fontWeight:"bold"}}
                className={classes.button}
                startIcon={<LikeIcon />} >
                Like
              </Button>
        
            </Grid>
  
            <Grid xs={6} item >
            
            </Grid>
            
          </Grid>
        
          
        </div>
        <div align= "center" style={{width:"50%",borderRadius:20}} >
    
          <TextField placeholder = "Please Reply Here..." id="reply" size="small" margin="none" fullWidth
                     style={{marginLeft:10}}/>
        </div>
        <Divider/>
      </div>
    )
  };
  
  const getCommentField = () => {
    
    return (
      <div align= "center" style={{marginTop:10, width:"60%", backgroundColor:'#E3F2FD',borderRadius:20}} >
    
        <TextField placeholder = "Enter Your Comment Here..." id="comment" size="small" margin="none" fullWidth
                   style={{marginLeft:10}}/>
      </div>

    )
  };
  
  const postDialogCallback = () => {
    setPostTopic(false);
  };
  
  
  
  return (
    <div>
      
      <Box className={classes.root} boxShadow={1} align="center">
      
        <div>
          <Button variant="contained" color="primary" style = {{width:500,borderRadius:30,height:50}} onClick = {()=> {setPostTopic(true)}} >
            
            Start New Discussion Topic </Button>
        </div>
      </Box>
  
      <Box className={classes.root} boxShadow={3} align="center">
        <Grid container direction="row" justify="space-between"
              alignItems="center">
          
          <Grid xs={4} item >
            <Typography color="primary" variant="h6"> Akbar @Microsoft </Typography>
          </Grid>
          <Grid xs={4} item >
          
          </Grid>
          
          <Grid xs={4} variant ="body2" item >
                 January 2, 2021,8.50 EST
          </Grid>
          
        </Grid>
        <Divider/>
        <div style={{marginTop:10, marginBottom:10}}>
          There has been a substitution of the metrics in the calculus causing huge outage all over the map.
          What do you guys think about this in your free time.
          
        </div>
        <Divider/>
        <div style={{marginTop:10, marginBottom:10}}>
          <Grid container direction="row" justify="space-between"
                alignItems="center">
    
            <Grid xs={4} item >
  
              <Button
                variant="outlines"
                className={classes.button}
                size="large"
                startIcon={<CommentIcon />}
              >
                Comment
              </Button>
              
            </Grid>
            <Grid xs={4} item >
    
            </Grid>
  
            <Button
              variant="outlines"
              size="large"
              className={classes.button}
              startIcon={<LikeIcon />}
            >
              Like
            </Button>
          </Grid>
          
        </div>
        <Divider/>
        {getComments()}
        {getComments()}
        {getCommentField()}
        
      </Box>
      
      {isPostTopic &&
        <PostTopic open = {isPostTopic} context = {context} callback = {postDialogCallback}/>
      }
      
    </div>
  );
}
