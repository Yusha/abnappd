import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import TextField from '@material-ui/core/TextField';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';

import CountryIcon from '@material-ui/icons/AssistantPhoto';



import axios from 'axios';

import {GetHash, GetSignature} from '../../ec/CryptoUtils';



const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
  },
  backButton: {
    marginRight: theme.spacing(1),
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(2),
    
  },
  button: {
    margin: theme.spacing(1),
  },
  margin: {
    margin: theme.spacing(1),
  },
  input: {
    display: 'none',
  },
}));


function getSteps() {
  return ['Choose Name','Describe Board', 'Finalize'];
}


export default function AddBoard(props) {
  
  const classes = useStyles();
  
  const {callback, context } = props;
  
  const [activeStep, setActiveStep] = React.useState(0);
  
  const [name,setName] = React.useState("");
  const [description,setDescription] = React.useState("");
  
  const [status, setStatus] = React.useState({});
  
  const [finished, setFinished] = React.useState(false);
  
  
  
  const steps = getSteps();
  
  
  const submitCallback = (pass)=> {
    
    const password = pass;
    
    
    
    const baseObject = {
      name:name,
      description:description,
    };
    
    
    const sTx = JSON.stringify(baseObject);
    
    const hashOf = GetHash(sTx);
    
    const signature = GetSignature(props.context.encryptedPK,password,hashOf);
    
    const chainTx = {
      ownerHome: context.homeId,
      ownerAccount: context.user,
      publicKey:context.publicKey,
      hash:hashOf,
      signature:signature,
      tx:sTx
    };
    
    const url = "http://127.0.0.1:4000/boards/add";
    
    axios.post(url,chainTx).then (result => {
      setFinished(true);
      setStatus({status:"PI Tx succeeded", id:hashOf});
      
    }).catch (error => {
      console.log("Error in sending the PI tx to the network... " + JSON.stringify(error));
      setFinished(true);
      setStatus({status:"Transaction Failed", id:hashOf});
      
    });
    
  };
  
  const  handleNext = () => {
    
    setActiveStep(prevActiveStep => prevActiveStep + 1);
  };
  
  const handleBack = () => {
    setActiveStep(prevActiveStep => prevActiveStep - 1);
  };
  
  
  const saveNameCallback = (name) => {
    setName(name);
    handleNext();
  };
  
  const saveDescriptionCallback = (desc) => {
    setDescription(desc);
    handleNext();
  };
  
  
  
  
  
  return (
    <Grid className={classes.root}>
      <Stepper activeStep={activeStep} alternativeLabel>
        {steps.map(label => (
          <Step key={label}>
            <StepLabel>{label}</StepLabel>
          </Step>
        ))}
      </Stepper>
  
      
        
        <Grid container justify="center">
          <div className={classes.instructions}>
      
          {activeStep === 0 &&
            <GatherName saveNameCallback={saveNameCallback} cancelCallback = {callback} />
          }
  
            {activeStep === 1 &&
              <GatherDescription callback={saveDescriptionCallback} cancelCallback = {callback} />
            }
            
            {activeStep === 2 &&
              <Finalize backCallback={handleBack} submitCallback={submitCallback} cancelCallback = {callback} context = {context}/>
            }
          </div>
          
        </Grid>
      
      <Divider/>
      
      {finished &&
      <Grid container justify="center" style={{marginTop:50}}>
        <Typography className={classes.instructions} color="red">
          Status of the Transaction is  " {status.status}
        
        </Typography>
        
        <Button onClick = {callback} variant="contained" color="secondary"> Complete </Button>
      
      
      </Grid>
      }
    
    </Grid>
  );
}


function GatherName(props) {
  
  const {saveNameCallback, cancelCallback} = props;
  const classes = useStyles();
  const [name, setName] = React.useState('');
  
  
  return (
    
    <div>
      <div className={classes.margin}>
        <Grid container spacing={1} alignItems="flex-end">
          
          <Grid item>
            <TextField id="name" value={name} onChange={(e)=>{setName(e.target.value)}} style={{width:300}} label="Name"/>
          </Grid>
        </Grid>
      </div>
  
      <div style={{marginTop:20}}>
    
        
        
        <Button variant="contained" color="primary" onClick={() => saveNameCallback(name)}>
          Next
        </Button>
        
        <Button variant="contained" style = {{marginLeft:5}} color="primary" onClick={() => cancelCallback()}>
          Cancel
        </Button>
  
      </div>
      
    </div>
  
  );
}

function GatherDescription(props) {
  
  const {callback, cancelCallback} = props;
  const classes = useStyles();
  const [description, setDescription] = React.useState('');
  
  
  return (
    
    <div>
      <div className={classes.margin}>
        <Grid container spacing={1} alignItems="flex-end">
  
          <Grid item style={{marginTop: 20}}>
            <TextareaAutosize style={{width: 500, height: 60}}
                              rowsMin={2} value={description} onChange={(e)=>{setDescription(e.target.value)}}
                              placeholder="Description of the Board "/>
          </Grid>
        </Grid>
      </div>
      
      <div style={{marginTop:20}} align="center">
        
        
        
        <Button variant="contained" color="primary" onClick={() => callback(description)}>
          Next
        </Button>
        
        <Button variant="contained" style = {{marginLeft:5}} color="primary" onClick={() => cancelCallback()}>
          Cancel
        </Button>
      
      </div>
    
    </div>
  
  );
}

function Finalize(props) {
  
  const classes = useStyles();
  
  const {backCallback, submitCallback, cancelCallback} = props;
  
  const [password, setPassword] = React.useState('');
  
  const setValue = (e) => {
    setPassword(e.target.value);
  };
  
  return (
    
    <div>
      
      <div className={classes.margin}>
        <Grid container spacing={1} alignItems="flex-end">
          <Grid item>
            <CountryIcon/>
          </Grid>
          <Grid item>
            <TextField id="country" multiline value={password} onChange={setValue} style={{width:400}} label="password to unlock Private Key..."/>
          </Grid>
        </Grid>
        
        <Grid container spacing={1} alignItems="flex-end" style = {{marginTop:30}}>
          
          <Grid container justify = "center">
            
            <Grid container justify = "center">
              
              <Grid>
                <Button
                  onClick={backCallback}
                  className={classes.backButton}
                >
                  Back
                </Button>
                
                <Button variant="contained" color="primary" onClick={()=>submitCallback(password)}>
                  Sign and Submit
                </Button>
  
                <Button variant="contained" color="primary" onClick={() => cancelCallback()}>
                  Cancel
                </Button>
              </Grid>
            </Grid>
          </Grid>
        
        </Grid>
      
      </div>
    </div>
  
  );
}
