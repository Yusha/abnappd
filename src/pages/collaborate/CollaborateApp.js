

import React, {useEffect} from 'react';


import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';

import ListItemText from '@material-ui/core/ListItemText';

import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import ImageIcon from '@material-ui/icons/Image';
import ReferenceIcon from '@material-ui/icons/Dns';

import IconButton from '@material-ui/core/IconButton';
import CollaborateIcon from '@material-ui/icons/GroupWork';


import axios from 'axios';



import Button from '@material-ui/core/Button';

import Home from '@material-ui/icons/HomeWork';
import POC from '@material-ui/icons/Gavel';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';

import NetworkIcon from '@material-ui/icons/Business';
import FileIcon from '@material-ui/icons/FileCopy';
import Grid from '@material-ui/core/Grid';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

import AddBoard from "./AddBoard";



const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    marginTop:50,
    backgroundColor:"white"
  },
  nested: {
    paddingLeft: theme.spacing(11),
  },
  
  toolbar: theme.mixins.toolbar,
}));

export default function CollaborateApp(props) {
  
  const {context,setActiveScreenCallback, logoutCallback} = props;
  const classes = useStyles();
  
  const [anchorEl, setAnchorEl] = React.useState(null);
  const anchorOpen = Boolean(anchorEl);
  const [tabValue, setTabValue] = React.useState(4);
  const [createNew, setCreateNew] = React.useState(false);
  const [myBoards, setMyBoards] = React.useState([]);
  const [selectedIndex, setSelectedIndex] = React.useState(0);
  
  
  useEffect(()=> {
    
    const url = "http://localhost:4000/boards/get?ownerHome=" + context.homeId + '&ownerAccount=' + context.user;
    
    axios.get(url).then ((result) => {
      setMyBoards(result.data);
      
    }).catch ((error) => {
      console.log("Error in getting My Boards  " + JSON.stringify(error));
    })
    
  },[]);
  
  
  const handleTabChange = (event, newValue) => {
  
    if (newValue === 0 ) {
      props.setActiveScreenCallback(1); // Dashboard
    } else if (newValue === 1 ) {
      props.setActiveScreenCallback(7); // files
    } else if (newValue === 2 ) {
      props.setActiveScreenCallback(3); // Reference Objects
    } else if (newValue  === 3) {
      props.setActiveScreenCallback(4); // Contracts
    } else if (newValue  === 4) {
      props.setActiveScreenCallback(6); // Collaborate
    } else if (newValue  === 5) {
      props.setActiveScreenCallback(2); // Network
    }
    setTabValue(newValue);
  };
  
  const onLogout = () => {
    console.log("Logout called");
    props.logoutCallback();
    
  };
  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  
  
  const onCreateNew = () => {
    setCreateNew(true);
  };
  
  const onCreateNewCallback = () => {
    setCreateNew(false);
  };
  
  const onSelectBoard = (event, index) => {
    setSelectedIndex(index);
    setCreateNew(false);
    setActiveScreenCallback(13,myBoards[selectedIndex]);
  };
  
  
  const getMyBoardsList = () => {
    
    return myBoards.map((board,index) => {
      let thisBoard = JSON.parse(board.tx.tx);
      
      return (
  
        <ListItem button key={index} style={{marginTop:10}} onClick={event => onSelectBoard(event, index)} selected={selectedIndex === {index}}>
          <ListItemAvatar>
            <Avatar>
              <ImageIcon />
            </Avatar>
          </ListItemAvatar>
          <ListItemText primary={<Typography style={{ color: '#303f9f' }}>{thisBoard.name }</Typography>}
            secondary={thisBoard.description}/>
        </ListItem>
      )
      
    });
    
  };
  
  
  return (
    <div className={classes.root}>
      <CssBaseline/>
      
      <AppBar position="fixed" className={classes.appBar} style = {{backgroundColor:"white", color:"#303f9f"}}>
        <Toolbar>
          <Grid direction="row"  container>
  
            <Grid xs={1} item style={{marginTop:10}}>
              <Typography variant="h6" >
                ABBN
              </Typography>
            </Grid>
            
            <Grid xs={9} item>
              <Grid >
                <Tabs
                  value={tabValue}
                  onChange={handleTabChange}
                  variant="fullWidth"
                  indicatorColor="primary"
                  textColor="primary"
                  centered
                >
                  <Tab icon={<Home fontSize="small"/>} label="Home"/>
                  <Tab icon={<FileIcon  fontSize="small" />} label="Files"/>
                  <Tab icon={<ReferenceIcon  fontSize="small" />} label="Reference Data Objects"/>
                  <Tab icon={<POC  fontSize="small" />} label="Contracts"/>
                  <Tab icon={<CollaborateIcon fontSize="small"/>} label="Collaborate"/>
                  <Tab icon={<NetworkIcon fontSize="small"/>} label="Network"/>
                  
                
                </Tabs>
              </Grid>
            </Grid>
            <Grid item xs={1} />
            
            <Grid item xs={1} >
              <Grid >
                
                <div>
                  <IconButton
                    
                    aria-haspopup="true"
                    onClick={handleMenu}
                    color="inherit"
                  
                  >
                    <AccountCircle fontSize="large"/>
                  </IconButton>
                  <Menu
                    id="menu-appbar"
                    anchorEl={anchorEl}
                    anchorOrigin={{
                      vertical: 'top',
                      horizontal: 'right',
                    }}
                    keepMounted
                    transformOrigin={{
                      vertical: 'top',
                      horizontal: 'right',
                    }}
                    open={anchorOpen}
                    onClose={handleClose}
                  >
                    <MenuItem >{context.homeId} / {context.user}</MenuItem>
                    <MenuItem onClick={onLogout}>logout</MenuItem>
                  </Menu>
                </div>
              
              </Grid>
            </Grid>
          </Grid>
        
        </Toolbar>
      </AppBar>
      
      <Drawer
        className={classes.drawer}
        variant="permanent"
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <div className={classes.toolbar}/>
        
        <List>
  
          <div style={{marginLeft:20,marginRight:20,width:"90%",fontSize:20,marginTop:30}} align="center" >
            Groups
          </div>
          
          <div style={{marginLeft:20,marginTop:20,width:"90%",fontSize:14}} >
            <Button onClick={onCreateNew} className={classes.shape} align="left" variant="outlined" color="secondary"
                    style={{width: 200, height: 30}}>Create New Group </Button>
          </div>
          
          <div style={{marginLeft:20, marginRight:20,width:"90%",fontSize:20,marginTop:20}} align="center">
            My Groups
          </div>
          
          {getMyBoardsList()}
          
        </List>
        <Divider/>
        
        <List>
        
        
        </List>
        
      </Drawer>
    
      
      <main className={classes.content}>
        
        {createNew &&
        
          <AddBoard context = {context} callback = {onCreateNewCallback} />
        }
      </main>
    </div>
  );
}