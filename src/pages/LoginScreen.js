import React from 'react';

import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import Box from '@material-ui/core/Box';
import TextField from '@material-ui/core/TextField';

import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import EmailIcon from '@material-ui/icons/Email';
import HomeIcon from '@material-ui/icons/Home';
import LockOpenIcon from '@material-ui/icons/LockOpen';

import { ec as EC } from 'elliptic';
import aes256 from 'aes256';
import PouchDB from 'pouchdb-browser';

import axios from 'axios';

const myDB = new PouchDB('myDB');


const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
  },
  backButton: {
    marginRight: theme.spacing(1),
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(2),
    
  },
  margin: {
    margin: theme.spacing(1),
  },
  
  centerLogo: {
  
  }
  

}));


export default function LoginScreen(props) {
  
  const {context, logoutCallback, fullLogin} = props;
  
  const [fLogin, setFLogin] = React.useState(fullLogin);
  
  const [homeId, setHomeId] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [passPhrase, setPassPhrase] = React.useState('');
  const [email, setEmail] = React.useState('');
  const [error, setError] = React.useState('');
  
  const classes = useStyles();
  
  
  const handleLogin = ()=> {
    
    
    if (props.fullLogin) {
      
      try {
        const ecurve = new EC('secp256k1');
        const key = ecurve.genKeyPair({entropy: passPhrase});
  
        const publicKey = key.getPublic().encode('hex');
        const privateKey = key.getPrivate("hex");
        const encryptedPK = aes256.encrypt(password, privateKey);
        
  
        const url = "http://localhost:4000/api/account?home=" + homeId + "&account=" + email;
  
        axios.get(url).then(result => {
    
          if (publicKey === result.data.publicKey) {
  
            const context = {
              _id: "myHome",
              name: result.data.name,
              homeId: homeId,
              user: email,
              publicKey: publicKey,
              encryptedPK: encryptedPK
            };
      
            myDB.put(context).then((saved) => {
        
              console.log("Context saved for future use  " + saved);
              
              props.setLoginSuccessCallback(context);
        
            }).catch((error) => {
              console.log("Caught error while saving myHome object");
              
              setError("Error Logging " + error);
        
            })
          } else {
            setError("Error Logging " + error);
          }
        }).catch(error => {
          setError("Account does not exists. Error  " + error);
        });
  
      } catch (error ) {
        setError("Error Logging " + error);
      }
  
    } else {
        console.log("Token exists.....");
        props.setLoginSuccessCallback(props.context);
    }
  };
  
  const onNewUser = () => {
    logoutCallback();
    setFLogin(true);
  };
  
  
  const setField = (e) => {
    
    const field = e.target.id;
    const value = e.target.value;
    
    if ("home" === field) {
      setHomeId(value);
    } else if ("passphrase" === field) {
      setPassPhrase(value);
    } else if ("email" === field) {
      setEmail(value);
    } else if ("password" === field) {
      setPassword(value);
    }
    else {
      console.log("Unknown field... must be an error");
    }
  };
  
  
  return (
    <Grid className={classes.root} >
  
  
      <Grid color="primary">
        
        {props.registered &&

        <Typography component="div" color="primary">
  
          <Box fontFamily="Roboto" textAlign="center" fontSize="h5.fontSize" fontWeight="fontWeightMedium" m={2}>
            Home Registration requested submitted to the network. You can login and check the status of the registration in the communication center.
          </Box>
          <Divider/>
        </Typography>
        
        }
      </Grid>
  
  
      <Typography component="div">
        
        <Box fontFamily="Monospace" textAlign="center" letterSpacing={6} fontSize="h3.fontSize" fontWeight="fontWeightMedium" m={2}>
          Login
        </Box>
      </Typography>
      
      <Divider/>
      
      
      <div>
  
        {fLogin &&
          
          <div>
            <div className={classes.margin}>
              <Grid container spacing={1} justify="center" alignItems="flex-end">
                <Grid item>
                  <HomeIcon/>
                </Grid>
                <Grid item>
                <TextField id="home" value={homeId} onChange={setField} label="Home Identifier"/>
                </Grid>
              </Grid>
            </div>
  
            <div className={classes.margin}>
              <Grid container justify="center" spacing={1} alignItems="flex-end">
                <Grid item>
                  <EmailIcon/>
                </Grid>
                <Grid item>
                  <TextField id="email" value={email} onChange={setField} label="Account Email"/>
                </Grid>
              </Grid>
            </div>
  
            <div>
              <Grid container justify="center" spacing={1} alignItems="flex-end">
      
                <Grid item style={{marginTop: 20}}>
                  <TextareaAutosize style={{width: 500, height: 60}}
                                    rowsMin={1} id="passphrase" value={passPhrase} onChange={setField}
                                    placeholder="Passphrase . "/>
                </Grid>
              </Grid>
            </div>
  
            <div className={classes.margin}>
              <Grid container justify = "center" spacing={1} alignItems="flex-end">
                <Grid item>
                  <LockOpenIcon/>
                </Grid>
                <Grid item>
                  <TextField id="password" value={password} style={{width:350 }}onChange={setField} label="Pin to encrypt private key and save locally"/>
                </Grid>
              </Grid>
            </div>
  
            <div style={{marginTop:10}}>
              <Grid container justify = "center" spacing={2} >
                <Typography color="secondary"> {error} </Typography>
              
              </Grid>
            </div>
  
            <div style={{marginTop:20}}>
              <Grid container justify = "center" spacing={2} >
                <Button variant="contained" color="primary" onClick={handleLogin}>
                  Login
                </Button>
              </Grid>
            </div>
            
          </div>
          
        }
  
        {!fLogin &&
          
          <div style={{marginTop:100}} align="center" >
  
            <div>
    
              <Typography variant="h6" color="secondary"> Welcome Back</Typography>
  
            </div>
            
            <div style={{marginTop:5}} align="center">
              
              <Typography variant="h4"> {context.homeId} / {context.user} </Typography>
              
            </div>
            
            <div style={{marginTop:50}} align="center">
  
              <Button variant="contained" color="primary" onClick={handleLogin}>
                Login
              </Button>
            
            </div>
  
            <div style={{marginTop:50}} align="center">
    
              <Button color="secondary" onClick={onNewUser}>
                Login As Different User
              </Button>
  
            </div>
            
          </div>
    
        }
        
      </div>
      
    </Grid>
  );
}
