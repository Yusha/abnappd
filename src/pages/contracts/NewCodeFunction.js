import React from 'react';


import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

import {Editor} from 'react-draft-wysiwyg';

import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { EditorState, convertToRaw, convertFromRaw } from 'draft-js';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';

import ListItemText from '@material-ui/core/ListItemText';




import axios from 'axios';

import { makeStyles } from '@material-ui/core/styles';
import {GetSignature,GetHash} from '../../ec/CryptoUtils';

import CodeFunctionResult from './CodeFunctionResult';


const style = makeStyles((theme)=> ({
  dialog: {
    position: 'absolute',
    left: 10,
    top: 50
  },
  input: {
    display: 'none',
  },
  shape: {
    borderRadius: 25,
  },
  selectRoot: {
    '& .MuiTextField-root': {
      margin: theme.spacing(1),
      width: '40ch',
    },
  },
  
}));

export default  function NewCodeFunction(props) {
  
  
  const { context } = props;
  
  const classes = style();
  
  
  
  const [createNew, setCreateNew] = React.useState(false);
  
  const [chooseTemplate, setChooseTemplate] = React.useState(false);
  const [templateData, setTemplateData] = React.useState(false);
  const [renderFunction, setRenderFunction] = React.useState(false);
  const [functionCode, setFunctionCode] = React.useState({});
  
  const [finalize, setFinalize] = React.useState(false);
  const [result,setResult] = React.useState({set:false});
  
  
  
  
  
  const templateCallback = (cancel,data) => {
  
    setChooseTemplate(false);
    
    if (!cancel) {
      setChooseTemplate(false);
      setTemplateData(data);
      setRenderFunction(true);
    } else {
      setCreateNew(false);
    }
  };
  
  const functionCallback = (cancel,data) => {
  
    setRenderFunction(false);
    if (cancel) {
      setCreateNew(false);
    } else {
      setFunctionCode(data);
      setFinalize(true);
    }
    
  };
  
  const createNewFunction = () => {
    setCreateNew(true);
    setChooseTemplate(true);
  };
  
  const onFinalize = (cancel,password) => {
    if (cancel) {
      setFinalize(false);
      setRenderFunction(true);
      return;
    }
    
    
    const functionCodeS = JSON.stringify(functionCode);
    const hash = GetHash(functionCodeS);
    
    const signature = GetSignature(context.encryptedPK,password,hash);
    const publicKey = context.publicKey;
  
    const body = JSON.parse(templateData.tx.body);
    
    
    const tx = {
      publisher:"System",
      templateId:templateData._id,
      templateName: body.heading.name,
      name: functionCode.name,
      senderHome: context.homeId,
      senderAccount:context.user,
      senderPublicKey: publicKey,
      hash:hash,
      signature:signature,
      body:functionCodeS
    };
    
    // Send the request to the server...
    const url = "http://localhost:4000/contracts/codes/add";
    
    return axios.post(url,tx).then ((result) => {
      
      if (result.data.success) {
        // message posted successfully
        setResult({set:true,success:true})
      } else {
        setResult({set:true,success:false,error:result.data.error})
      }
      setFinalize(false);
      
    });
    
    
  };
  
  const onDone = () => {
    setCreateNew(false);
    setResult(false);
  };
  
  
  
  return (
    
    <div className={classes.content} >
        
      <div className={classes.margin}>
          <Typography color="primary" variant="h1" align="center">
            
            {!createNew &&

              <Button color="primary" onClick={createNewFunction}> Create a New Function</Button>
            }
            
            </Typography>
      </div>
      
      <div>
        {chooseTemplate &&
          <ChooseTemplate callback={templateCallback}/>
        }
        
        {renderFunction &&
          <ThisFunction data = {templateData} callback={ functionCallback }/>
        }
  
        {finalize &&
          <Finalize callback={onFinalize} />
        }
        
        { result.set &&
          
          <Result callback = {onDone} result = {result} />
          
        }
        
        
      </div>
  
      
    </div>
    
  );
}


function ChooseTemplate(props) {
  
  const {callback} = props;
  
  const classes = style();
  
  const [fromSystemT, setFromSystemT] = React.useState(false);
  const [selectedIndex, setSelectedIndex] = React.useState(false);
  const [templates, setTemplates] = React.useState([]);
  
  
  const onSelectT = () => {
    callback(false,templates[selectedIndex]);
  };
  
  const onCancel = () => {
    callback(true,"");
  };
  
  const onSystemT = () => {
    setFromSystemT(true);
    
    const url = "http://localhost:4000/contracts/templates/get?publisher=System";
    
    axios.get(url).then ((result) => {
      setTemplates(result.data);
      
    }).catch ((error) => {
      console.log("Error in getting Internal Pending Invitations data  " + JSON.stringify(error));
    })
    
  };
  
  const handleListClick = (event, index) => {
    setSelectedIndex(index);
    
  };
  
  const getTemplateList = () => {
    
    return templates.map((item,index) => {
      
      const body = JSON.parse(item.tx.body);
      return (
        <ListItem
          button
          selected={selectedIndex === index}
          key={index}
          onClick={(event) => handleListClick(event, index)} >
          
          <ListItemText primary={body.heading.name} color="secondary" />
        </ListItem>
      
      )
    });
    
  };
  
  
  
  
  return (
    
    <div>
      {!fromSystemT &&
      <div align="center" style={{marginTop:100}}>
        <Button onClick={onSystemT} className={classes.shape} align="left" variant="contained" color="primary"
                style={{width: 300, height: 30}}>Select from System Templates </Button>
    
      </div>
      }
  
  
      { fromSystemT &&
      <div align="center" style={{marginTop:30, width:800}}>
    
        <List component="nav" align="center">
          {getTemplateList()}
    
        </List>
  
      </div>
    
      }
  
      {fromSystemT &&
      <div align="center" style={{marginTop:100}}>
        <Button onClick={onSelectT} className={classes.shape} align="left" variant="contained" color="primary"
                style={{width: 100, height: 30}}>Select Template </Button>
        <Button onClick={onCancel} className={classes.shape} align="left" variant="contained" color="primary"
                style={{marginLeft:30, width: 100, height: 30}}>Cancel </Button>
  
      </div>
      }
      
      
    
    </div>
    
  )
  
  
}

function ThisFunction(props) {
  
  
  const { context, data,callback } = props;
  
  const [code, setCode] = React.useState(true);
  const [test, setTest] = React.useState(false);
  const [showResult, setShowResult] = React.useState(false);
  const [testResult, setTestResult] = React.useState("");
  const [testArgs, setTestArgs] = React.useState("");
  
  const [codeFunctionName, setCodeFunctionName] = React.useState("");
  
  const body = JSON.parse(data.tx.body);
  
  let allVariables = "";
  let args = "";
  
  let counter = 1;
  
  if (body.heading.hasStartDate) {
  
    args = args + 'sDate, ';
    allVariables = allVariables + '\n' + "  const startDate = sDate;" ;
  }
  
  if (body.heading.hasEndDate) {
    
    args = args + 'eDate, ';
    allVariables = allVariables + '\n' + "  const endDate = eDate;" ;
  }
  
  let partyArg = 1;
  
  for (let p = 0; p< body.heading.parties.length; p++) {
    args = args + 'party' +  partyArg + ", " ;
    allVariables = allVariables + '\n' + "  const " + body.heading.parties[p]  + " = party" + partyArg + ";" ;
    partyArg++;
  }
  
  
  for (let i = 0; i <body.sections.length; i++) {
    
    let item = body.sections[i];
    
    if (item.variableType ==="party") continue;
    
    if (item.type ==="variable") {
      args = args + 'args' +  counter ;
      // args
      if (i <(body.sections.length - 1)) {
        args = args + ", ";
      }
      // assignments
      allVariables = allVariables + '\n' + "  const " + item.id  + " = args" + counter + ";" ;
      counter++;
    }
  }
  
  
  
  const contentState = convertFromRaw({
    entityMap: {},
    blocks: [
      {
        type: 'normal-block',
        text: 'function Contract(' + args + ") {"  +
        
        '  \n' + allVariables +
        ' \n' +
        ' \n' +
        ' \n' +
        
        
        '};' +
          '\n'
        
      }
    ]
  });
  
  
  const [textEditorState, setTextEditorState] = React.useState(EditorState.createWithContent(contentState));
  
  
  const classes = style();
  
  
  
  const onTextEditorStateChange = (s) => {
    setTextEditorState(s);
  };
  
  
  const onTest = () => {
    setCode(false);
    setTest(true);
  };
  
  const onTestResultCallback = () => {
    setShowResult(false);
  };
  
  
  const onRun = () => {
    
    try {
      const raw = convertToRaw(textEditorState.getCurrentContent());
      let text = "";
      
      raw.blocks.map((block,index) => {
        text = text +  block.text.trim();
      });
      
      const func = new Function("return " + text)();
      const allArrays = testArgs.split(",");
      const result = func(...allArrays);
      
      setTestResult(result);
      setShowResult(true);
    
    } catch (exception) {
      
      setTestResult(exception.toString());
      
    }
  
  };
  
  const save = ()  => {
    callback(false,{name:codeFunctionName,data:convertToRaw(textEditorState.getCurrentContent())});
  };
  
  
  return (
    
      
    <div className={classes.content} style={{width:800,align:"center",marginLeft:50}}>
      
      <div className={classes.margin}>
        <Typography color="primary" variant="h5" align="center"> Please write code for your function </Typography>
      </div>
  
      <div style={{width:800,align:"center",marginLeft:50,marginTop:100}}>
  
        <TextField
          style = {{width:500}}
    
          value={codeFunctionName}
          onChange={(e) => {
            setCodeFunctionName(e.target.value)
          }}
          placeholder="Please enter a name for this code function."
          label= "Code Function Name"
        />
        
      </div>
      
      <div style={{width:800,align:"center",marginLeft:50,marginTop:100}}>
  
        <Editor
          toolbarHidden
          editorState={textEditorState}
          toolbarClassName="toolbarClassName"
          wrapperClassName="wrapperClassName"
          editorClassName="editorClassName"
          placeholder = "Smart contract for a specific template..."
          editorStyle={{backgroundColor:"#c4c4c4",height:"auto", fontFamily:"monospace",fontSize:14}}
          onEditorStateChange={onTextEditorStateChange}
        />
      
      
      </div>
  
      {code &&
      <div align="center" style={{marginTop: 100}}>
    
        <Button onClick={onTest} className={classes.shape} align="left" variant="contained" color="primary"
                style={{width: 200, height: 30}}>Test </Button>
    
        <Button onClick={() => {callback(true)}} className={classes.shape} align="left" variant="contained" color="primary"
                style={{width: 100, height: 30}}>Close </Button>
      </div>
      }
  
  
      {test &&
  
      <div align="center" style={{marginTop: 10, marginBottom:20, width: 800}}>
        
        
        <div>
    
        <TextField
          style = {{width:400}}
      
          value={testArgs}
          onChange={(e) => {
            setTestArgs(e.target.value)
          }}
          placeholder="Please Enter Comma Separated Input Values"
          label= "Contract Input Value"
        />
        </div>
  
  
        { showResult &&
          
          <CodeFunctionResult open = {setShowResult} data = {testResult} callback = {onTestResultCallback}  />
        }
        
        <div align = "center" style={{width: 800, align: "center", marginLeft: 50, marginTop: 100, marginBottom:20}} >
  
          <Button onClick={onRun} className={classes.shape} align="left" variant="contained" color="primary"
                style={{width: 200, height: 30}}>Run</Button>
          <Button onClick={save} className={classes.shape} align="left" variant="contained" color="primary"
                  style={{marginLeft:10,width: 300, height: 30}}>Save </Button>
          <Button onClick={() => {callback(true)}} className={classes.shape} align="left" variant="contained" color="primary"
                  style={{width: 100, height: 30, marginLeft:10}}>Cancel </Button>
          
          
        </div>
      </div>
    
      }
    </div>
  );
}

function Finalize(props)  {
  
  const {callback} = props;
  const [password, setPassword] = React.useState("");
  const classes = style();
  
  const finalize = () => {
    
    callback(false,password);
  };
  
  const cancel = () => {
    callback(true);
  };
  
  return (
    
    <div align="center" style={{marginTop:100}}>
      
      <div align="center" style={{marginTop:100}}>
        <Typography variant="h3"> Finalize Code and Send to the Network </Typography>
      
      </div>
      
      <div align="center" style={{marginTop:100}}>
        
        <TextField
          
          style={{width: 400, marginTop: 30, backgroundColor: "#FFFFFF", height: 20}}
          placeholder="password for private key"
          value={password}
          id="password"
          onChange={(e) => {setPassword(e.target.value)}}
          color="secondary"
        />
      
      
      </div>
      
      <div align = "center" style={{marginTop:100}}>
        
        <Button onClick={finalize} className={classes.shape} variant="contained" align="right"
                style={{width: 250, height: 30}} color="secondary"> Send to Network  </Button>
        
        <Button onClick={cancel} className={classes.shape} variant="contained" align="right"
                style={{width: 120, height: 30, marginLeft: 50}} color="secondary"> Cancel </Button>
      
      </div>
    
    </div>
  
  )
}

function Result(props)  {
  
  const {callback, result} = props;
  const classes = style();
  
  const done = () => {
    callback();
  };
  
  
  
  return (
    
    <div align="center" style={{marginTop:100}}>
      
      {result.success &&
      <div align="center" style={{marginTop:100}}>
        
        <Typography variant="h3"> Transaction Submitted successfully </Typography>
      
      </div>
      
      }
      
      {!result.success &&
      
      <div align="center" style={{marginTop: 100}}>
        
        
        <Typography variant="h3"> Transaction submission failed with error: </Typography>
        <Typography variant="h5"> {result.error} </Typography>
      
      
      </div>
      }
      
      <div align = "center" style={{marginTop:100}}>
        
        <Button onClick={done} className={classes.shape} variant="contained" align="right"
                style={{width: 250, height: 30}} color="secondary"> Done </Button>
      
      </div>
    
    </div>
  
  )
}
