import React,{useEffect} from 'react';

import { makeStyles } from '@material-ui/core/styles';

import Dialog from '@material-ui/core/Dialog';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import InputAdornment from '@material-ui/core/InputAdornment';

import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';

import axios from 'axios';



import {Editor} from 'react-draft-wysiwyg';

import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { EditorState, convertFromRaw } from 'draft-js';


export default function TemplateDetails(props) {
  
  
  const { data,callback,open } = props;
  const [controls, setControls] = React.useState([]);
  const body = JSON.parse(data.tx.body);
  
  
  const classes = dialogStyles();
  
  useEffect(()=> {
    
    const url = "http://localhost:4000/contracts/templates/controls/get?publisher=System" ;
    
    axios.get(url).then ((result) => {
      setControls(result.data);
      
    }).catch ((error) => {
      console.log("Error in getting Template Controls  " + JSON.stringify(error));
    })
    
  },[]);
  
  
  const handleClose = (action) => {
    callback(action);
  };
  
  const getHeaderParties = () => {
    
    return body.heading.parties.map((party, index) => {
      
      return (
        
        <TextField
          style={{width: 500, margin: 2, marginTop: 10}}
          
          inputProps={{
            readOnly: true,
          }}
          label={party}
        />
      )
    })
  };
  
  
  const getHeader = () => {
    
    return (
      
      
      <div>
      
        <div className={classes.margin} style={{marginTop: 1, width: 500}}>
        
          <Typography variant="h5"> {body.heading.name} </Typography>
          <Typography variant="body1"> {body.heading.description} </Typography>
      
        </div>
  
        <div style={{marginTop:50}}>
          <Typography variant="h6"> Contract Schedule </Typography>
        </div>
  
        { body.heading.hasStartDate &&
  
        <div>
    
          <TextField
            style={{width: 500, margin: 2, marginTop: 10}}
      
            inputProps={{
              readOnly: true,
            }}
            label="Contract Execution Date"
          />
  
        </div>
    
    
        }
  
        { body.heading.hasEndDate &&
  
        <div>
    
          <TextField
            style={{width: 500, margin: 2, marginTop: 10}}
      
            inputProps={{
              readOnly: true,
            }}
            label="Contract Termination Date"
          />
  
        </div>
    
        }
        
        <div style={{marginTop:50}}>
    
          <Typography variant="h6"> Contract Parties</Typography>
    
          {getHeaderParties()}
  
        </div>

      </div>
      
      
    )
    
  };
  
  const getBody = () => {
    
    return (
      
      <div>
        <div style={{marginTop:50}}>
        <Typography variant="h6" > Contract Body</Typography>
        </div>
        <div style={{marginTop:50}}>
          
          {getItems()}
        </div>
        
        
      </div>
    )
    
  };
  
  const getItems = () => {
    
    return body.sections.map((item,index) => {
      if (item.type === "text") {
        
        const editorState = EditorState.createWithContent(convertFromRaw(item.value));
        
        return (
          
          
          <div className={classes.margin} style={{marginTop:1,width:500}} >
            <Editor
              editorState={editorState}
              toolbarClassName="toolbarClassName"
              wrapperClassName="wrapperClassName"
              editorClassName="editorClassName"
              toolbarHidden
              readOnly
              editorStyle={{backgroundColor:"#FFFFFF",height:"auto"}}
            />
          
          </div>
          
          
        
        )
      } else if (item.type === "variable" ) {
        
        if ((item.variableType === "text") ) {
          
          return (
            
            <TextField
              
              style={{width: 500, margin: 2, marginTop: 10}}
              
              inputProps={{
                style: {fontSize: 15}
              }}
              color="primary"
              
              placeholder={item.description}
              
              readOnly
              
            
            />
          
          
          )
        } else if (item.variableType === "party") {
          
          return (
            
            <div>
              <TextField
                
                style={{width: 500, margin: 2, marginTop: 10}}
                
                inputProps={{
                  style: {fontSize: 15}
                }}
                color="secondary"
                
                placeholder={item.description}
                
                readOnly
                
              
              />
              
            </div>
          )
          
          
        } else if ((item.variableType === "date") ||
          (item.variableType === "number")) {
          
          return (
            
            <TextField
              
              style={{width: 500, margin: 2, marginTop: 10}}
              
              inputProps={{
                style: {fontSize: 15}
              }}
              color="primary"
              
              placeholder={item.description}
              InputProps={{
                startAdornment: <InputAdornment position="start">{item.prefix}  :</InputAdornment>,
              }}
              
              readOnly
              
            
            />
          
          
          )
        }
  
      } else if (item.type ==="control") {
  
        return (
          <div>
            {getControlBody(item.name)}
    
          </div>
  
        )
  
  
      } else if (item.type === "controlFooter") {
        
          return (
            <div align="center" style={{marginTop:30, width:800}}>
        
              <Button className={classes.shape} align="left" variant="contained" color="primary"
                  style={{width: 200, height: 30, marginRight:30}}>Add Control </Button>
        
              <Button className={classes.shape} align="left" variant="contained" color="primary"
                  style={{width: 200, height: 30}}>Delete Control </Button>
      
            </div>
          )
    
      }
    })
    
  };
  
  
  
  const getControlBody = (cid) => {
    
    let controlObject = "";
    let isSet = false;
    for (let i =0; i <controls.length; i++) {
      if (controls[i]._id === cid) {
        controlObject = controls[i];
        isSet = true;
        break;
      }
    }
    
    if (!isSet) {
      return ;
    }
    
    let items = JSON.parse(controlObject.tx.body).sections;
    
    
    return (
      
      <div style={{marginTop:100, borderStyle:"solid", borderColor:"black", width:800}}>
        <div style={{marginTop:50}}>
          <Typography variant="h6" > </Typography>
        </div>
        <div style={{marginLeft:50,marginTop:50}}>
          
          {getControlItems(items)}
        </div>
      
      
      </div>
    )
    
  };
  const getControlItems = (items) => {
    
    return items.map((item,index) => {
      if (item.type === "text") {
        
        const editorState = EditorState.createWithContent(convertFromRaw(item.value));
        
        return (
          
          
          <div className={classes.margin} style={{marginTop:1,width:500}} >
            <Editor
              editorState={editorState}
              toolbarClassName="toolbarClassName"
              wrapperClassName="wrapperClassName"
              editorClassName="editorClassName"
              toolbarHidden
              readOnly
              editorStyle={{backgroundColor:"#FFFFFF",height:"auto"}}
            />
          
          </div>
        
        
        
        )
      } else if (item.type === "variable" ) {
        
        if ((item.variableType === "text") ) {
          
          return (
            
            <TextField
              
              style={{width: 500, margin: 2, marginTop: 10}}
              
              inputProps={{
                style: {fontSize: 15},
                readOnly: true,
              }}
              color="primary"
              label={item.label}
              
              helperText={item.description}
              
              readOnly
            
            
            />
          
          
          )
        } else if (item.variableType === "party") {
          
          return (
            
            <div>
              <TextField
                
                style={{width: 500, margin: 2, marginTop: 10}}
                
                inputProps={{
                  style: {fontSize: 15}
                }}
                color="secondary"
                
                placeholder={item.description}
                
                readOnly
              
              
              />
            
            </div>
          )
          
          
        } else if ((item.variableType === "date") ||
          (item.variableType === "number")) {
          
          return (
            
            <TextField
              
              style={{width: 500, margin: 2, marginTop: 10}}
              
              inputProps={{
                style: {fontSize: 15}
              }}
              color="primary"
              
              placeholder={item.description}
              InputProps={{
                startAdornment: <InputAdornment position="start">{item.prefix}  :</InputAdornment>,
              }}
              
              readOnly
            
            
            />
          
          
          )
        }else if (item.variableType === "signature") {
          
          return (
            
            <div style={{marginTop:50}}>
              <Typography variant="h5" color="secondary">
                Signature
              </Typography>
              
              <div >
                <TextField
                  
                  style={{width: 300, margin: 2, marginTop: 10}}
                  
                  inputProps={{
                    style: {fontSize: 15}
                  }}
                  color="secondary"
                  
                  label = "Signatory Name"
                
                
                />
                <TextField
                  
                  style={{marginLeft:30,width: 300, margin: 2, marginTop: 10}}
                  
                  inputProps={{
                    style: {fontSize: 15}
                  }}
                  color="secondary"
                  
                  label= "Signatory Title"
                
                />
              
              
              </div>
              
              <div >
                <TextField
                  
                  style={{width: 300, margin: 2, marginTop: 10}}
                  
                  inputProps={{
                    style: {fontSize: 15}
                  }}
                  color="secondary"
                  
                  label = "Signatory Home"
                
                
                />
                <TextField
                  
                  style={{marginLeft:10, width: 300, margin: 2, marginTop: 10}}
                  
                  inputProps={{
                    style: {fontSize: 15}
                  }}
                  color="secondary"
                  
                  label= "Signatory Account"
                
                />
              
              
              </div>
              
              <div style={{marginTop:30}}>
                <Typography> Signatory Role</Typography>
                
                <FormGroup row>
                  {getControlRoles(item.roles)}
                
                </FormGroup>
              
              
              
              </div>
            
            </div>
          )
          
        }
        
      }
    })
    
  };
  const getControlRoles = (signerRoles) => {
    
    return signerRoles.map((role,index) => {
      return (
        
        <FormControlLabel
          control={
            <Checkbox  />
          }
          label= {role}
        />
      
      )
      
    })
    
  };
  
  
  
  return (
    <Dialog onClose={handleClose} open={open} fullWidth maxWidth="lg" classes={{paper: classes.dialog }} >
      
      <div className={classes.content} style={{width:800,align:"center",marginLeft:50}}>
        
        <div className={classes.margin}>
          <Typography color="primary" variant="h5" align="center"> Template Details</Typography>
        </div>
        
        <div>
          
          {getHeader()}
          
          {getBody()}
        
        </div>
  
        <div align="center" style={{width:800,marginTop:100, marginBottom:100}}>
          <Button onClick={()=>{callback(true)}} className={classes.shape} align="left" variant="contained" color="primary"
                  style={{width: 100, height: 30, marginBottom:100}}>Close </Button>
    
          
        </div>
        
      </div>
    
    
    </Dialog>
  );
}

const dialogStyles = makeStyles({
  dialog: {
    position: 'absolute',
    left: 10,
    top: 50
  },
  table: {
    minWidth: 700,
  },
});