import React,{useEffect} from 'react';


import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import InputAdornment from '@material-ui/core/InputAdornment';
import Dialog from '@material-ui/core/Dialog';

import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';



import {Editor} from 'react-draft-wysiwyg';

import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { EditorState, convertFromRaw } from 'draft-js';

import Grid from '@material-ui/core/Grid';


import axios from 'axios';

import {GetSignature,GetHash} from '../../ec/CryptoUtils';
import { makeStyles } from '@material-ui/core/styles';
import CodeFunctionResult from './CodeFunctionResult';
import ControlEntryReadOnly from './ControlEntryReadOnly';





const style = makeStyles((theme)=> ({
  dialog: {
    position: 'absolute',
    left: 10,
    top: 50
  },
  input: {
    display: 'none',
  },
  shape: {
    borderRadius: 25,
  },
  selectRoot: {
    '& .MuiTextField-root': {
      margin: theme.spacing(1),
      width: '40ch',
    },
  },
  
}));

export default  function ContractDetails(props) {
  
  
  const { context, data, callback, open } = props;
  
  const classes = style();
  
  const [showDetails, setShowDetails] = React.useState(true);
  
  const [templateBody, setTemplateBody] = React.useState({});
  const [codeBody, setCodeBody] = React.useState({});
  const [signatures, setSignatures] = React.useState({});
  const [showContract, setShowContract] = React.useState(false);
  
  const [controlObject, setControlObject] = React.useState([]);
  
  useEffect(()=> {
    
    const url = "http://localhost:4000/api/contractDetails?contractId=" + data._id;
    
    axios.get(url).then ((result) => {
      setControlObject(result.data);
      
    }).catch ((error) => {
      console.log("Error in getting Template Controls  " + JSON.stringify(error));
    })
    
  },[]);
  
  
  const contractCallback = () => {
    setShowContract(false);
    callback();
    
  };
  
  const onShowDetails = () => {
  
    const url = "http://localhost:4000/contracts/contracts/contractdata?templateId=" + data.header.templateId +
      "&codeId=" + data.header.codeId + "&contractId =" + data._id;
  
    axios.get(url).then ((result) => {
      setTemplateBody(result.data.templateData);
      setCodeBody(result.data.codeData);
      setSignatures(result.data.signatures);
      setShowDetails(false);
      setShowContract(true);
      
    }).catch ((error) => {
      console.log("Error in getting Internal Pending Invitations data  " + JSON.stringify(error));
    });
  };
  
  
  return (
  
    <Dialog onClose={callback} open={open} fullWidth maxWidth="lg" classes={{paper: classes.dialog }} style={{marginTop:50, marginLeft:50}}>
        
      <div className={classes.margin}>
          <Typography color="primary" variant="h1" align="center">
            
            {showDetails &&

              <Button color="primary" onClick={onShowDetails}> Show Contract Details </Button>
            }
            
            </Typography>
      </div>
      
      <div>
        
        {showContract &&
          <Contract data = {data} templateBody = {templateBody} codeBody = {codeBody}
                    callback={ contractCallback} controls = {controlObject} context = {context} />
        }
  
      </div>
  
      
    </Dialog>
    
  );
}


function Contract(props) {
  
  
  const { context,data,templateBody, codeBody, callback, controls } = props;
  
  const [showResultBox, setShowResultBox] = React.useState(false);
  const [testResultData, setTestResultData] = React.useState({});
  
  const [addControl, setAddControl] = React.useState(false);
  const [currentControl, setCurrentControl] = React.useState({});
  
  
  const contractBody = JSON.parse(data.tx.body);
  
  const functionVariables = contractBody.variableData;
  
  const partiesData = contractBody.headerData.partiesData;
  
  
  
  const classes = style();
  
  const getArgs = () => {
    
    var result = [];
    
    result.push(contractBody.headerData.startDate);
    
    result.push(contractBody.headerData.endDate);
    
    for (var p=0; p<partiesData.length;p++) {
      result.push(partiesData[p]);
    }
    
    
    for (var i =0; i < templateBody.sections.length; i++) {
      const item = templateBody.sections[i];
      if (item.variableType ==="party") continue;
      if (item.type === "variable") {
        result.push(functionVariables[i]);
      }
      
    }
    console.log("Arg Values  " + result);
    return result;
  };
  
  
  const onTest = () => {
    
    
    try {
      const raw = codeBody.data;
      let text = "";
      
      raw.blocks.map((block,index) => {
        text = text +  block.text.trim();
      });
      
      const func = new Function("return " + text)();
      const args = getArgs();
      const result = func(...args);
    //  setTestResultData(JSON.stringify(result));
      setTestResultData(result);
      setShowResultBox(true);
  
    } catch (exception) {
      
      setTestResultData(exception.toString());
      
    }
  };
  
  const showDialogCallback = () => {
    setShowResultBox(false);
  };
  
  const onClose = () => {
    callback();
  };
  
  const getHeaderParties = () => {
    
    return templateBody.heading.parties.map((party, index) => {
      
      return (
        
        <div>
        
          <TextField
            style={{width: 250, margin: 2, marginTop: 10}}
            id = {party}
            key ={index}
            label={party}
            value = {partiesData[index].name}
          
          />
          <TextField
            style={{width: 150, marginLeft: 5, marginTop: 10}}
            id = {party}
            label = "homeId"
            value = {partiesData[index].homeId}
          />
          <TextField
            style={{width: 250, marginLeft: 5, marginTop: 10}}
            id = {party}
            label="accountId"
            value = {partiesData[index].accountId}
          />
          
        </div>
        
      )
    })
  };
  
  const getHeader = () => {
    
    return (
      
      <div>
        <div className={classes.margin} style={{marginTop: 1, width: 500}}>
          <Typography variant="h5"> {templateBody.heading.name} </Typography>
          <Typography variant="body1"> {templateBody.heading.description} </Typography>
        </div>
  
        <div style={{marginTop:50}}>
          <Typography variant="h6"> Contract Schedule </Typography>
        </div>
  
        
  
        <div>
    
          <TextField
            style={{width: 500, margin: 2, marginTop: 10}}
      
            label="Contract Execution Date"
            value = {contractBody.headerData.startDate}
            type="date"
            
            InputLabelProps={{
              shrink: true,
            }}
            
          />
  
        </div>
        
  
        <div>
    
          <TextField
            style={{width: 500, margin: 2, marginTop: 10}}
      
            label="Contract Termination Date"
            value = {contractBody.headerData.endDate}
            type="date"
            
            InputLabelProps={{
              shrink: true,
            }}
            
          />
  
        </div>
    
        
  
        <div style={{marginTop:50}}>
    
          <Typography variant="h6"> Contract Parties</Typography>
    
          {getHeaderParties()}
  
        </div>
        
        
      </div>
    )
  };
  
  
  
  const getBody = () => {
    
    return (
      
      <div>
        <div style={{marginTop:50}}>
          <Typography variant="h6" > Contract Body</Typography>
        </div>
        <div style={{marginTop:50}}>
          
          {getItems()}
        </div>
      
      
      </div>
    )
  };
  
  const getControlTemplate = (cid) => {
    
    let controlObject = "";
    let localControls = controls.controls;
    
    for (let i =0; i <localControls.length; i++) {
      if (localControls[i]._id === cid) {
        controlObject = localControls[i];
        break;
      }
    }
    return controlObject;
    
  };
  
  const getItems = () => {
    
    return templateBody.sections.map((item,index) => {
      if (item.type === "text") {
        
        const editorState = EditorState.createWithContent(convertFromRaw(item.value));
        
        return (
          
          
          <div className={classes.margin} style={{marginTop:1,width:500}} >
            <Editor
              editorState={editorState}
              toolbarClassName="toolbarClassName"
              wrapperClassName="wrapperClassName"
              editorClassName="editorClassName"
              toolbarHidden
              readOnly
              editorStyle={{backgroundColor:"#FFFFFF",height:"auto"}}
            />
          
          </div>
        )
      } else if (item.type === "variable" ) {
        
        if ((item.variableType === "text") ) {
  
          return (
            
            <TextField
              
              style={{width: 500, margin: 2, marginTop: 10}}
              
              inputProps={{
                style: {fontSize: 15}
              }}
              color="primary"
              
              placeholder={item.description}
              
              value = {functionVariables[index]}
              
            />
          )
        } else if (item.variableType === "party") {
  
          
          return (
            
            <div>
              <TextField
                
                style={{width: 500, margin: 2, marginTop: 10}}
                
                inputProps={{
                  style: {fontSize: 15,color:"red"}
                }}
                color="secondary"
                placeholder={item.description}
                
                value = {functionVariables[index]}
                
              />
              
            </div>
          )
        
        } else if (item.variableType === "number") {
          
          return (
            
            <TextField
              
              style={{width: 500, margin: 2, marginTop: 10}}
              type="number"
              
              inputProps={{
                style: {fontSize: 15}
              }}
              color="primary"
              
              placeholder={item.description}
              InputProps={{
                startAdornment: <InputAdornment position="start">{item.prefix}  :</InputAdornment>,
              }}

              value = {functionVariables[index]}
              
            />
          
          )
        } else if (item.variableType === "date" ) {
          
          return (
            
            <div>
  
              <TextField
                id="date"
                label={item.description}
                type="date"
                
                InputLabelProps={{
                  shrink: true,
                }}

                value = {functionVariables[index]}
                
              />
            </div>
          )
        }
        
      } else if (item.type ==="control") {
    
        let control = getControlTemplate(item.name);
      
        let fv = functionVariables[index];
      
        if (control) {
          return (
        
          <ControlEntryReadOnly functionVariables={fv} template={control} key = {index} />
        ) } else {
          return "";
        }
      } else if (item.type === "controlFooter") {
    
          return (
          
            <div style={{marginTop:30, width:800}}>
            
              <div style={{marginTop:20}}>
              
              {getAdditionalControls(item.name)}
              
              </div>
  
              <div style={{marginTop:20}}>
    
                <Button onClick = {()=>{addNewControl(item.name)}} className={classes.shape}  align="left" variant="contained" color="primary"
                        style={{width: 200, height: 30, marginRight:30}}>Add Control </Button>
    
                <Button onClick = {()=>{deactivateControls(item.name)}} className={classes.shape} align="left" variant="contained" color="primary"
                        style={{width: 200, height: 30}}>Delete Control </Button>
              </div>
      
            </div>
          )
      }
    })
  };
  
  
  const getAdditionalControls = (cid) => {
    
    let aControls = controls.controlAdds;
    let template = getControlTemplate(cid);
    
    return aControls.map((control, index) => {
      let fv = JSON.parse(control.tx.body).values;
      
      return (
        
        <ControlEntryReadOnly template = {template} functionVariables = {fv} />
      )
      
    });
    
    
  };
  
  const addNewControl = (templateId) => {
    
    let templateControl = getControlTemplate(templateId);
    
    
    let newFV = [];
  
    let body = JSON.parse(templateControl.tx.body);
  
    body.sections.map((item, index) => {
      if (item.type === "variable") {
        if (item.variableType === "signature") {
          newFV[index] = {};
          newFV[index].roles  = [];
        } else {
          newFV[index] = "";
        }
      }
    });
    setCurrentControl({control:templateControl,fv:newFV,controlId:templateId, contractId:data._id});
    
    setAddControl(true);
    
  };
  
  const addNewControlCallback = () => {
    setAddControl(false);
  };
  
  const deactivateControls = (templateId) => {
  
  };
  
  const getCodeFunctionComponent = () => {
    
    const editorState = EditorState.createWithContent(convertFromRaw(codeBody.data));
    
    return (
      
      <div align="center">
        
        
        <div align="center" className={classes.margin} style={{marginTop: 30, width: 800}}>
          <Editor
            editorState={editorState}
            toolbarClassName="toolbarClassName"
            wrapperClassName="wrapperClassName"
            editorClassName="editorClassName"
            toolbarHidden
            readOnly
            editorStyle={{backgroundColor: "#c4c4c4", height: "auto", fontFamily: "monospace", fontSize: 14}}
          />
        
        </div>
      </div>
    )
    
  };
  
  
  return (
    
    
    <div className={classes.content} style={{width:800,align:"center",marginLeft:50}}>
      
      <div>
        
        {getHeader()}
        
        {getBody()}
      
      </div>
  
      {codeBody &&
        <div style={{marginTop: 30}}>
    
        <Typography variant="h5"> Contract Code</Typography>
    
    
        <div style={{marginTop: 20}}>
          <Grid
            container
            direction="row"
            justify="flex-start"
            alignItems="flex-start"
          >
        
            <div className={classes.margin} align="center">
              <Typography variant="h6" color="secondary"> {codeBody.name}  </Typography>
            </div>
      
          </Grid>
    
        </div>
    
        <div style={{marginTop: 30}}>
          {getCodeFunctionComponent()}
        </div>
    
        {showResultBox &&
    
        <CodeFunctionResult open={showResultBox} data={testResultData} callback={showDialogCallback}/>
      
        }
    
    
        <div align="center" style={{marginTop: 100}}>
      
          <Button onClick={onTest} className={classes.shape} align="left" variant="contained" color="primary"
                  style={{width: 100, height: 30}}>Test Contract</Button>
          <Button onClick={onClose} className={classes.shape} align="left" variant="contained" color="primary"
                  style={{marginLeft: 10, width: 100, height: 30}}>Close </Button>
    
        </div>
  
      </div>
      }
      
      {!codeBody &&

      <div align="center" style={{marginTop: 100, marginBottom:200}}>
  
        <Button onClick={onClose} className={classes.shape} align="left" variant="contained" color="primary"
                style={{marginLeft: 10, width: 100, height: 30}}>Close </Button>

      </div>
      
      }
      
      {addControl &&
        <AdditionalControl context = {context} controlTemplate = {currentControl} callback = {addNewControlCallback} open = {addControl}/>
      }
      
    </div>
    
  );
}

function AdditionalControl(props) {
  
  const { context,controlTemplate, open, callback } = props;
  
  const [functionVariables, setFunctionVariables] = React.useState(controlTemplate.fv);
  
  const [save, setSave] = React.useState(false);
  const [password, setPassword] = React.useState("");
  const [result, setResult] = React.useState({});
  
  
  
  
  const setVariableValue = (e,index) =>  {
    
    const newValue = functionVariables;
    newValue[index] = e.target.value;
    setFunctionVariables(newValue);
    
  };
  
  const setSignatoryName = (e,index) =>  {
    const newValue = functionVariables;
    let indexValue = newValue[index];
    
    if (!indexValue) {
      indexValue = {};
      indexValue.roles = [];
    }
    indexValue.name = e.target.value;
    
    newValue[index] = indexValue;
    setFunctionVariables(newValue);
  };
  
  const setSignatoryTitle = (e,index) =>  {
    const newValue = functionVariables;
    let indexValue = newValue[index];
    
    if (!indexValue) {
      indexValue = {};
      indexValue.roles = [];
    }
    indexValue.title = e.target.value;
    
    newValue[index] = indexValue;
    setFunctionVariables(newValue);
  };
  
  const setSignatoryRole = (e,indexRole, index) =>  {
    const newValue = functionVariables;
    let indexValue = newValue[indexRole];
    
    if (!indexValue) {
      indexValue = {};
      indexValue.roles = [];
    }
    
    indexValue.roles[index] = e.target.checked;
    newValue[indexRole] = indexValue;
    setFunctionVariables(newValue);
    
  };
  
  const setSignatoryHome = (e,index) =>  {
    const newValue = functionVariables;
    let indexValue = newValue[index];
    
    if (!indexValue) {
      indexValue = {};
    }
    indexValue.home = e.target.value;
    
    newValue[index] = indexValue;
    setFunctionVariables(newValue);
  };
  
  const setSignatoryAccount = (e,index) =>  {
    const newValue = functionVariables;
    let indexValue = newValue[index];
    
    if (!indexValue) {
      indexValue = {};
    }
    indexValue.account = e.target.value;
    
    newValue[index] = indexValue;
    setFunctionVariables(newValue);
  };
  
  
  const getControlBody = (controlTemplate) => {
    
    if (!controlTemplate) {
      return;
    }
    
    
    let items = JSON.parse(controlTemplate.control.tx.body).sections;
    
    
    return (
      
      <div align="center" style={{marginTop:100, borderStyle:"solid", borderColor:"black", width:800}}>
        <div style={{marginTop:50}}>
          <Typography variant="h6" > </Typography>
        </div>
        <div style={{marginLeft:50,marginTop:50}}>
          
          {getControlItems(items)}
        </div>
      
      
      </div>
    )
    
  };
  
  
  const getControlItems = (items) => {
    
    return items.map((item,index) => {
      
      
      if (item.type === "text") {
        
        const editorState = EditorState.createWithContent(convertFromRaw(item.value));
        
        return (
          
          
          <div  style={{marginTop:1,width:500}} >
            <Editor
              editorState={editorState}
              toolbarClassName="toolbarClassName"
              wrapperClassName="wrapperClassName"
              editorClassName="editorClassName"
              toolbarHidden
              editorStyle={{backgroundColor:"#FFFFFF",height:"auto"}}
            />
          
          </div>
        )
      } else if (item.type === "variable" ) {
        
        if ((item.variableType === "text") ) {
          
          return (
            
            <TextField
              
              style={{width: 500, margin: 2, marginTop: 10}}
              
              inputProps={{
                style: {fontSize: 15},
              }}
              color="primary"
              label={item.label}
              value = {functionVariables.index}
              
              helperText={item.description}
              onChange={(e)=>setVariableValue(e,index)}
              
            />
          
          
          )
        } else if (item.variableType === "party") {
          
          return (
            
            <div>
              <TextField
                
                style={{width: 500, margin: 2, marginTop: 10}}
                
                inputProps={{
                  style: {fontSize: 15}
                }}
                color="secondary"
                
                placeholder={item.description}
                value = {functionVariables.index}
                onChange={(e)=>setVariableValue(e,index)}
                
              />
            
            </div>
          )
          
          
        } else if ((item.variableType === "date") ||
          (item.variableType === "number")) {
          
          return (
            
            <TextField
              
              style={{width: 500, margin: 2, marginTop: 10}}
              
              inputProps={{
                style: {fontSize: 15}
              }}
              color="primary"
              
              placeholder={item.description}
              InputProps={{
                startAdornment: <InputAdornment position="start">{item.prefix}  :</InputAdornment>,
              }}
              
              value = {functionVariables.index}
              onChange={(e)=>setVariableValue(e,index)}
              
            />
          
          
          )
        }else if (item.variableType === "signature") {
          
          return (
            
            <div style={{marginTop:50}}>
              <Typography variant="h5" color="secondary">
                Signature
              </Typography>
              
              <div >
                <TextField
                  
                  style={{width: 300, margin: 2, marginTop: 10}}
                  
                  inputProps={{
                    style: {fontSize: 15}
                  }}
                  color="secondary"
                  
                  label = "Signatory Name"
                  value = {functionVariables[index].name}
                  onChange={(e)=>setSignatoryName(e,index)}
                
                
                />
                <TextField
                  
                  style={{marginLeft:30,width: 300, margin: 2, marginTop: 10}}
                  
                  inputProps={{
                    style: {fontSize: 15}
                  }}
                  color="secondary"
                  
                  label= "Signatory Title"
                  value = {functionVariables[index].title}
                  onChange={(e)=>setSignatoryTitle(e,index)}
                
                />
              
              
              </div>
              
              <div >
                <TextField
                  
                  style={{width: 300, margin: 2, marginTop: 10}}
                  
                  inputProps={{
                    style: {fontSize: 15}
                  }}
                  color="secondary"
                  
                  label = "Signatory Home"
                  value = {functionVariables[index].home}
                  onChange={(e)=>setSignatoryHome(e,index)}
                
                
                />
                <TextField
                  
                  style={{marginLeft:10, width: 300, margin: 2, marginTop: 10}}
                  
                  inputProps={{
                    style: {fontSize: 15}
                  }}
                  color="secondary"
                  
                  label= "Signatory Account"
                  value = {functionVariables[index].account}
                  onChange={(e)=>setSignatoryAccount(e,index)}
                
                />
              
              
              </div>
              
              <div style={{marginTop:30}}>
                <Typography> Signatory Role</Typography>
                
                <FormGroup row>
                  {getControlRoles(item.roles, index)}
                
                </FormGroup>
              
              
              
              </div>
            
            </div>
          )
          
        }
        
      }
    })
    
  };
  
  const getControlRoles = (signerRoles, indexRole) => {
    
    
    return signerRoles.map((role,index) => {
      return (
        
        <FormControlLabel
          control={
            <Checkbox checked= {functionVariables[indexRole].roles[index]} onChange={(e)=>setSignatoryRole(e,indexRole, index)} name = {role} />
          }
          label= {role}
          key = {index}
        
        
        />
      
      )
      
    })
    
  };
  
  
  
  
  const onSave = () => {
    setSave(true);
  };
  
  const OnSaveAndSubmit = () => {
    
    const controlData = {
      contractId:controlTemplate.contractId,
      controlId: controlTemplate.controlId,
      values : functionVariables,
    };
    
  
    const controlDataS = JSON.stringify(controlData);
    const hash = GetHash(controlDataS);
  
    const signature = GetSignature(context.encryptedPK, password,hash);
    
    const tx = {
      originator:context.homeId,
      senderAccount:context.user,
      senderPublicKey: context.publicKey,
      contractId:controlData.contractId,
      controlId: controlData.controlId,
      hash:hash,
      signature:signature,
      body:controlDataS
    };
  
    // Send the request to the server...
    const url = "http://localhost:4000/contracts/controladd/add";
    
    return axios.post(url, tx).then((result) => {
    
      if (result.data.success) {
        // message posted successfully
        setResult({set: true, success: true});
        callback(result);
      } else {
        setResult({set: true, success: false, error: result.data.error});
        callback(result);
      }
    }).catch (ex => {
       console.log("Error in sending to add control  " + ex);
       
    });
  };
  
  
  return (
  
     <Dialog onClose={callback} open={open} fullWidth maxWidth="lg"  disableEscapeKeyDown>
       
       <div align="center" style={{marginTop:100}}>
         
         <Typography variant="h5"> Add a New Control</Typography>
       </div>
       
       <div align="center" style={{marginTop:10}}>
         {getControlBody(controlTemplate)}
       </div>
  
       <div align="center" style={{marginTop: 20, marginBotton:20 }}>
         <Button onClick={onSave} align="center" variant="contained" color="primary"
                 style={{marginLeft: 10, width: 100, height: 30}}>Save </Button>
       </div>
  
  
       {save &&
       <div align="center" style={{marginTop: 10}}>
    
         <div>
      
           <TextField
        
             style={{width: 400, marginTop: 30, backgroundColor: "#FFFFFF", height: 20}}
             
             label = "Pin fpr Private Key"
             value={password}
             onChange={(e) => {
               setPassword(e.target.value)
             }}
             color="secondary"
           />
         </div>
    
         <div align="center" style={{marginTop: 100, marginBottom:100}}>
      
           <Button onClick={OnSaveAndSubmit} variant="contained" align="right"
                   style={{width: 250, height: 30}} color="secondary"> Submit to Network </Button>
      
           <Button onClick={() => {
             callback()
           }}  variant="contained" align="right"
                   style={{width: 120, height: 30, marginLeft: 50}} color="secondary"> Cancel </Button>
         </div>
  
  
       </div>
    
       }
     </Dialog>
  )
  
}