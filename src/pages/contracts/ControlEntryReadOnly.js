import React from 'react';

import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';


import {Editor} from 'react-draft-wysiwyg';

import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { EditorState, convertFromRaw } from 'draft-js';


export default function ControlEntryReadOnly(props) {
  
  
  const {template,functionVariables } = props;
  
  const body = JSON.parse(template.tx.body);
  
  
  
  
  
  const getBody = () => {
    
    return (
      
      <div>
        
        <div style={{marginTop:50}}>
          
          {getItems()}
        </div>
        
        
      </div>
    )
    
  };
  
  
  const getRoles = (signerRoles, indexRole) => {
    
    
    return signerRoles.map((role,index) => {
      return (
        
        <FormControlLabel
          control={
            <Checkbox checked= {functionVariables[indexRole].roles[index]}  />
          }
          label= {role}
          key = {index}
          
          
        />
      
      )
      
    })
    
  };
  
  const getItems = () => {
    
    return body.sections.map((item,index) => {
      if (item.type === "text") {
        
        const editorState = EditorState.createWithContent(convertFromRaw(item.value));
        
        return (
          <div style={{marginTop:1,width:500}} >
            <Editor
              editorState={editorState}
              toolbarClassName="toolbarClassName"
              wrapperClassName="wrapperClassName"
              editorClassName="editorClassName"
              toolbarHidden
              readOnly
              editorStyle={{backgroundColor:"#FFFFFF",height:"auto"}}
            />
          
          </div>
        )
      } else if (item.type === "variable" ) {
        
        if ((item.variableType === "text") ) {
          
          return (
  
            <TextField
    
              style={{width: 500, margin: 2, marginTop: 10}}
    
              inputProps={{
                style: {fontSize: 15},
              }}
              color="primary"
              label={item.label}
    
              
              placeholder={item.description}

              value = {functionVariables[index]}
              key={index}
              
            />
          )
        } else if (item.variableType === "party") {
          
          return (
            
            <div>
              <TextField
                
                style={{width: 500, margin: 2, marginTop: 10}}
                
                inputProps={{
                  style: {fontSize: 15}
                }}
                color="secondary"
                
                placeholder={item.description}

                
                value = {functionVariables.index}
              
              />
              
            </div>
          )
          
          
        } else if ((item.variableType === "date") ||
          (item.variableType === "number")) {
          
          return (
            
            <TextField
              
              style={{width: 500, margin: 2, marginTop: 10}}
              
              inputProps={{
                style: {fontSize: 15}
              }}
              color="primary"
              
              placeholder={item.description}
              InputProps={{
                startAdornment: <InputAdornment position="start">{item.prefix}  :</InputAdornment>,
              }}
              
              value = {functionVariables.index}
              key={index}
              
            />
          
          )
        } else if (item.variableType === "signature") {
  
          return (
    
            <div style={{marginTop:50}} key = {index}>
              <Typography variant="h5" color="secondary">
                Signature
              </Typography>
      
              <div >
                <TextField
          
                  style={{width: 300, margin: 2, marginTop: 10}}
          
                  inputProps={{
                    style: {fontSize: 15}
                  }}
                  color="secondary"
          
                  label = "Signatory Name"
                  placeholder={item.description}

                  value = {functionVariables[index].name}
                  
        
                />
                <TextField
          
                  style={{marginLeft:30,width: 300, margin: 2, marginTop: 10}}
          
                  inputProps={{
                    style: {fontSize: 15}
                  }}
                  color="secondary"
          
                  label= "Signatory Title"
                  value = {functionVariables[index].title}
                  

                />
      
      
              </div>
      
              <div >
                <TextField
          
                  style={{width: 300, margin: 2, marginTop: 10}}
          
                  inputProps={{
                    style: {fontSize: 15}
                  }}
                  color="secondary"
          
                  label = "Signatory Home"
                  value = {functionVariables[index].home}
                  
                />
                <TextField
          
                  style={{marginLeft:10, width: 300, margin: 2, marginTop: 10}}
          
                  inputProps={{
                    style: {fontSize: 15}
                  }}
                  color="secondary"
          
                  label= "Signatory Account"
                  value = {functionVariables[index].account}
                  

                />
      
      
              </div>
      
              <div style={{marginTop:30}}>
                <Typography> Signatory Role</Typography>
        
                <FormGroup row>
                  {getRoles(item.roles, index)}
        
                </FormGroup>
      
      
      
              </div>
    
            </div>
          )
  
        }
        
      }
    })
    
  };
  
  
  return (
    
      
    <div style={{width:800,marginLeft:10}}>
      
      {getBody()}
      
    </div>
    
  );
}

