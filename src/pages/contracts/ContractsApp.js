
import React, {useEffect} from 'react';


import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import IconButton from '@material-ui/core/IconButton';
import SignIcon from '@material-ui/icons/Fingerprint';
import AddIcon from '@material-ui/icons/Add';
import ReferenceIcon from '@material-ui/icons/Dns';
import CollaborateIcon from '@material-ui/icons/GroupWork';

import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import Collapse from '@material-ui/core/Collapse';

import Grid from '@material-ui/core/Grid';

import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';

import Home from '@material-ui/icons/HomeWork';
import POC from '@material-ui/icons/Gavel';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';

import NetworkIcon from '@material-ui/icons/Business';
import FileIcon from '@material-ui/icons/FileCopy';

import ContractIcon from '@material-ui/icons/Gavel';
import TemplateIcon from '@material-ui/icons/BorderAll';
import FunctionIcon from '@material-ui/icons/Functions';

import axios from 'axios';

import NewTemplate from "./NewTemplate";
import SystemTemplates from "./SystemTemplates";
import NewContract from "./NewContract";
import NewFunction from "./NewCodeFunction";
import SystemCodeFunctions from "./SystemCodeFunctions";
import AllContracts from "./AllContracts";
import ContractsTable from "./ContractsTable";

import Signatures from './Signatures';
import NewControl from './NewControl';
import ControlsTable from './ControlsTable';




const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    marginTop:50,
    backgroundColor:"white"
  },
  nested: {
    paddingLeft: theme.spacing(11),
  },
  
  toolbar: theme.mixins.toolbar,
}));

export default function ContractsApp(props) {
  
  const {context} = props;
  const classes = useStyles();
  
  const [selectedIndex, setSelectedIndex] = React.useState(-1);
  
  const [snackMessage, setSnackMessage] = React.useState("");
  const [snackOpen, setSnackOpen] = React.useState(false);
  const [error, setError] = React.useState(false);
  
  const [contractOpen, setContractOpen] = React.useState(true); // expand the parent list item
  const [templateOpen, setTemplateOpen] = React.useState(false);
  const [codeOpen, setCodeOpen] = React.useState(false);
  const [policyOpen, setPolicyOpen] = React.useState(false);
  
  
  const [anchorEl, setAnchorEl] = React.useState(null);
  const anchorOpen = Boolean(anchorEl);
  const [tabValue, setTabValue] = React.useState(3);
  
  const [contractTypes, setContractTypes] = React.useState([]);
  const [showContractTypes, setShowContractTypes] = React.useState(true);
  const [selectedTemplateId, setSelectedTemplateId] = React.useState(0);
  const [selectedTemplateName, setSelectedTemplateName] = React.useState("");
  const [showContractsTable, setShowContractsTable] = React.useState(false);
  
  
  const onSelectContractType = (index) => {
    let template = contractTypes[index];
    setSelectedTemplateId(template._id);
    setSelectedTemplateName(JSON.parse(template.tx.body).heading.name);
    setShowContractsTable(true);
    setSelectedIndex(9);
  };
  
  
  const onAllContracts = () => {
    
    if (showContractTypes) {
      setShowContractTypes(false);
      return;
    }
    
    const url = "http://localhost:4000/contracts/templates/get?publisher=System";
  
    axios.get(url).then ((result) => {
      setContractTypes(result.data);
      setShowContractTypes(true);
      setSelectedIndex(9);
    
    }).catch ((error) => {
      console.log("Error in getting Internal Pending Invitations data  " + JSON.stringify(error));
    })
    
    
  };
  
  const getContractTypes = () => {
    return (
      contractTypes.map((type,index) => {
        const name = JSON.parse(type.tx.body).heading.name;
        return (
          <ListItem button key={index} className={classes.nested} onClick={event => onSelectContractType(index)} >
            <ListItemText primary={<Typography style={{ color: '#303f9f' }}>{name} </Typography>} />
          </ListItem>
        )
        
      })
      
    )
    
  }
  const handleTabChange = (event, newValue) => {
  
    if (newValue === 0 ) {
      props.setActiveScreenCallback(1); // Dashboard
    } else if (newValue === 1 ) {
      props.setActiveScreenCallback(7); // files
    } else if (newValue === 2 ) {
      props.setActiveScreenCallback(3); // Reference Objects
    } else if (newValue  === 3) {
      props.setActiveScreenCallback(4); // Contracts
    } else if (newValue  === 4) {
      props.setActiveScreenCallback(6); // Collaborate
    } else if (newValue  === 5) {
      props.setActiveScreenCallback(2); // Network
    }
    setTabValue(newValue);
  };
  
  
  
  const onLogout = () => {
    console.log("Logout called");
    props.logoutCallback();
    
  };
  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  
  
  
  const handleClick = (type) => {
    
    if (type === 0) {
      setContractOpen(!contractOpen);
    } else if (type === 1) {
      setTemplateOpen(!templateOpen);
    } else if (type === 2) {
      setCodeOpen(!codeOpen);
    } else {
      setPolicyOpen(!policyOpen);
    }
    
  };
  const handleSnackClose = ()=> {
    setSnackOpen(false);
  
  };
  
  const selectTab = (event, index) => {
    setSelectedIndex(index);
  };
  
  
  
  const setSnackbarMessageCallback = (isError,message) => {
    setError(isError);
    setSnackMessage(message);
    setSnackOpen(true);
  };
  
  
  
  
  const onNewTemplateCallback = (type,message) => {
  
    setSelectedIndex(4);
    if (type === "submit") {
    
      setSnackOpen(true);
      setSnackMessage("Successfully sent a new transaction to the network...");
    }
  };
  
  
  return (
    <div className={classes.root}>
      <CssBaseline/>
  
      <AppBar position="fixed" className={classes.appBar} style = {{backgroundColor:"white", color:"#303f9f"}}>
        <Toolbar>
          <Grid direction="row"  container>
  
            <Grid xs={1} item style={{marginTop:10}}>
              <Typography variant="h6" >
                ABBN
              </Typography>
            </Grid>
            
            <Grid xs={9} item>
              <Grid >
                <Tabs
                  value={tabValue}
                  onChange={handleTabChange}
                  variant="fullWidth"
                  indicatorColor="primary"
                  textColor="primary"
                  centered
                >
                  <Tab icon={<Home fontSize="small"/>} label="Home"/>
                  <Tab icon={<FileIcon  fontSize="small" />} label="Files"/>
                  <Tab icon={<ReferenceIcon  fontSize="small" />} label="Reference Data Objects"/>
                  <Tab icon={<POC  fontSize="small" />} label="Contracts"/>
                  <Tab icon={<CollaborateIcon fontSize="small"/>} label="Collaborate"/>
                  <Tab icon={<NetworkIcon fontSize="small"/>} label="Network"/>

                </Tabs>
              </Grid>
            </Grid>
            <Grid item xs={1} />
        
            <Grid item xs={1} >
              <Grid >
            
                <div>
                  <IconButton
                
                    aria-haspopup="true"
                    onClick={handleMenu}
                    color="inherit"
              
                  >
                    <AccountCircle fontSize="large"/>
                  </IconButton>
                  <Menu
                    id="menu-appbar"
                    anchorEl={anchorEl}
                    anchorOrigin={{
                      vertical: 'top',
                      horizontal: 'right',
                    }}
                    keepMounted
                    transformOrigin={{
                      vertical: 'top',
                      horizontal: 'right',
                    }}
                    open={anchorOpen}
                    onClose={handleClose}
                  >
                    <MenuItem >{context.homeId} / {context.user}</MenuItem>
                    <MenuItem onClick={onLogout}>logout</MenuItem>
                  </Menu>
                </div>
          
              </Grid>
            </Grid>
          </Grid>
    
        </Toolbar>
      </AppBar>
      
      <Drawer
        className={classes.drawer}
        variant="permanent"
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <div className={classes.toolbar}/>
        
        
        <List style = {{marginTop:10}}>
  
          <div style={{marginLeft:20,marginRight:20,width:"90%",fontSize:14,marginTop:30}} align="center">
            {context.homeId}/ {context.user}
          </div>
  
  
          <div style={{marginLeft:20,marginRight:20,backgroundColor:"#0091EA", color:"white", height:30, width:"90%",fontSize:20,marginTop:30}} align="center">
            Smart Contracts
          </div>
  
  
          <ListItem button key="nc" onClick={event => selectTab(event, 1)} >
            <ListItemIcon>
              <AddIcon/>
            </ListItemIcon>
            <ListItemText primary={<Typography style={{ color: '#303f9f' }}>New Contract</Typography>} />
            
          </ListItem>
          
          <Divider/>
  
          <ListItem key="contractTypes" button onClick={onAllContracts}>
            <ListItemIcon>
              <ContractIcon/>
            </ListItemIcon>
            <ListItemText primary={<Typography style={{ color: '#303f9f' }}>All Contracts </Typography>} />
            {showContractTypes ? <ExpandLess /> : <ExpandMore />}
          </ListItem>
  
  
          <Collapse in={showContractTypes} timeout="auto" unmountOnExit>
            <List component="div" disablePadding>
              {getContractTypes()}
    
            </List>
          </Collapse>
          
          <Divider/>
          
          <ListItem button key="templates" onClick={()=>{handleClick(1)}}>
            <ListItemIcon>
              <TemplateIcon/>
            </ListItemIcon>
            <ListItemText primary={<Typography style={{ color: '#303f9f' }}>Templates</Typography>} />
            {templateOpen ? <ExpandLess /> : <ExpandMore />}
          </ListItem>
          <Collapse in={templateOpen} timeout="auto" unmountOnExit>
            <List component="div" disablePadding>
              <ListItem button key="1" className={classes.nested} onClick={event => selectTab(event, 3)} >
                <ListItemText primary={<Typography style={{ color: '#303f9f' }}>New Template</Typography>} />
              </ListItem>
  
              <ListItem button key="2" className={classes.nested} onClick={event => selectTab(event, 4)}>
                <ListItemText primary={<Typography style={{ color: '#303f9f' }}>My Templates</Typography>} />
              </ListItem>
  
              <ListItem button key="3" className={classes.nested} onClick={event => selectTab(event, 5)}>
                <ListItemText primary={<Typography style={{ color: '#303f9f' }}>System Templates</Typography>} />
              </ListItem>
              <ListItem button key="4" className={classes.nested} onClick={event => selectTab(event, 10)}>
                <ListItemText primary={<Typography style={{ color: '#303f9f' }}>New Control</Typography>} />
              </ListItem>
  
              <ListItem button key="5" className={classes.nested} onClick={event => selectTab(event, 11)}>
                <ListItemText primary={<Typography style={{ color: '#303f9f' }}>All Controls</Typography>} />
              </ListItem>
              
            </List>
          </Collapse>
  
          <Divider/>
  
  
          <ListItem button key="code" onClick={()=>{handleClick(2)}}>
            <ListItemIcon>
              <FunctionIcon/>
            </ListItemIcon>
            <ListItemText primary={<Typography style={{ color: '#303f9f' }}>Contract Code</Typography>} />
            {codeOpen ? <ExpandLess /> : <ExpandMore />}
          </ListItem>
          <Collapse in={codeOpen} timeout="auto" unmountOnExit>
            <List component="div" disablePadding>
  
              <ListItem button key="newFunction"className={classes.nested} onClick={event => selectTab(event, 6)} >
                <ListItemText primary={<Typography style={{ color: '#303f9f' }}>Add Contract Code</Typography>} />
              </ListItem>
              <ListItem button key="systemFunction" className={classes.nested} onClick={event => selectTab(event, 7)} >
                <ListItemText primary={<Typography style={{ color: '#303f9f' }}>Contract Codes </Typography>} />
              </ListItem>
              
            </List>
          </Collapse>
  
          <Divider/>
  
          <div style={{marginLeft:20,marginRight:20,backgroundColor:"#0091EA", color:"white", height:30, width:"90%",fontSize:20,marginTop:30}} align="center">
            Signatures
          </div>
  
  
          <ListItem button key="sign" onClick={event => selectTab(event, 8)}>
            <ListItemIcon>
              <SignIcon/>
            </ListItemIcon>
            <ListItemText primary={<Typography style={{ color: '#303f9f' }}>Sign Contracts</Typography>} />
          </ListItem>
          
        </List>
        <Divider/>
        
        
      </Drawer>
      
      
      <main className={classes.content}>
  
  
        {selectedIndex === -1 &&
        <div>
          <IntoSection context={props.context} setSnackbarMessageCallback={setSnackbarMessageCallback}/>
        </div>
        }
  
  
        {selectedIndex === 1 &&
        <div>
          <NewContract context={props.context} />
        </div>
        }
  
        {selectedIndex === 2 &&
        <div>
          <AllContracts context={props.context}  />
        </div>
        }
        
        
        {selectedIndex === 3 &&
        <div>
          <NewTemplate context={props.context}  callback = {onNewTemplateCallback} />
        </div>
        }
  
        {selectedIndex === 5 &&
        <div>
          <SystemTemplates context={props.context}  />
        </div>
        }
  
        {selectedIndex === 6 &&
        <div>
          <NewFunction context={props.context}  callback = {onNewTemplateCallback} />
        </div>
        }
  
        {selectedIndex === 7 &&
        <div>
          <SystemCodeFunctions context={props.context} />
        </div>
        }
  
        {selectedIndex === 8 &&
  
        <div>
          <Signatures context={props.context}  />
        </div>
    
        }
  
        {selectedIndex === 9 && showContractsTable &&
  
        <div>
          <ContractsTable context={props.context} templateId = {selectedTemplateId} templateName = {selectedTemplateName} />
        </div>
    
        }
  
        {selectedIndex === 10 &&
  
        <div>
          <NewControl context={props.context}  />
        </div>
    
        }
  
        {selectedIndex === 11 &&
  
        <div>
          <ControlsTable context={props.context}  />
        </div>
    
        }
        
        
        <div>
          <Snackbar open={snackOpen} autoHideDuration={6000} onClose={handleSnackClose}>
            { !error &&
              <Alert onClose={handleSnackClose} severity={error?"error":"success"}>{snackMessage}</Alert>
            }
          </Snackbar>
        </div>
      </main>
    </div>
  );
}

function IntoSection (props) {
  
  const {context} = props;
  const classes = useStyles();
  
  return (
  
    <div style={{marginTop:5}}>
      
      <div style={{backgroundColor:"#0091EA", color:"white", height:100, width:"100%",fontSize:24}} align="center">
        Business Smart Contracts
      </div>
      
      <div style={{marginTop:20}}>
      
        <Grid container alignItems="center"
            justify="center" direction="row">
      
          <Grid item xs={6}>
          <Card className={classes.root} variant="outlined"
                style={{width: '100%',height:250,display: 'flex', flexDirection: 'column',align:"center"}}>
            <CardHeader style = {{backgroundColor:"white", color:"#0091EA", textAlign:"center"}}

                        title="Employment Contracts"
              
            />
            <CardContent >
              <Typography align="center" color = "primary">
                Employment Contracts
              </Typography>
              <Typography align="center" color = "primary">
                Strong Cryptographic Signatures
              </Typography>
              <Typography align="center" color = "primary">
                Verifiable on the blockchain
              </Typography>
    
            </CardContent>
  
          </Card>
          
        </Grid>
          <Grid item xs={6}>
            <Card className={classes.root} variant="outlined"
                  style={{width: '100%',height:250,display: 'flex', flexDirection: 'column',align:"center"}}>
              <CardHeader style = {{backgroundColor:"white", color:"#0091EA", textAlign:"center"}}
      
                          title="Sales Contracts"
      
              />
              <CardContent >
                <Typography align="center" color = "primary">
                  Sales Contracts
                </Typography>
                <Typography align="center" color = "primary">
                  Strong Cryptographic Signatures
                </Typography>
                <Typography align="center" color = "primary">
                  Verifiable on the blockchain
                </Typography>
      
              </CardContent>
    
            </Card>
  
          </Grid>
          <Grid item xs={6}>
            <Card className={classes.root} variant="outlined"
                  style={{width: '100%',height:250,display: 'flex', flexDirection: 'column',align:"center"}}>
              <CardHeader style = {{backgroundColor:"white", color:"#0091EA",textAlign:"center"}}
      
                          title="Vendor Contracts"
      
              />
              <CardContent >
                <Typography align="center" color = "primary">
                  Vendor Contracts
                </Typography>
                <Typography align="center" color = "primary">
                  Strong Cryptographic Signatures
                </Typography>
                <Typography align="center" color = "primary">
                  Verifiable on the blockchain
                </Typography>
      
              </CardContent>
    
            </Card>
  
          </Grid>
          <Grid item xs={6}>
            <Card className={classes.root} variant="outlined"
                  style={{width: '100%',height:250,display: 'flex', flexDirection: 'column',align:"center"}}>
              <CardHeader style = {{backgroundColor:"white", color:"#0091EA", textAlign:"center"}}
      
                          title="Custom Contracts"
      
              />
              <CardContent >
                <Typography align="center" color = "primary">
                  Custom Contracts
                </Typography>
                <Typography align="center" color = "primary">
                  Strong Cryptographic Signatures
                </Typography>
                <Typography align="center" color = "primary">
                  Verifiable on the blockchain
                </Typography>
      
              </CardContent>
    
            </Card>
  
          </Grid>
  
        
      
      </Grid>
      
      </div>
    </div>
    
  )
}


function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}
