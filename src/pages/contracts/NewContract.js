import React,{useEffect} from 'react';


import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

import InputAdornment from '@material-ui/core/InputAdornment';

import {Editor} from 'react-draft-wysiwyg';

import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { EditorState, convertFromRaw } from 'draft-js';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';

import ListItemText from '@material-ui/core/ListItemText';
import AttachmentIcon from '@material-ui/icons/Attachment';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';



import axios from 'axios';

import { makeStyles } from '@material-ui/core/styles';
import {GetSignature,GetHash} from '../../ec/CryptoUtils';
import CodeFunctionResult from './CodeFunctionResult';
import ControlEntry from "./ControlEntry";


const style = makeStyles((theme)=> ({
  dialog: {
    position: 'absolute',
    left: 10,
    top: 50
  },
  input: {
    display: 'none',
  },
  shape: {
    borderRadius: 25,
  },
  selectRoot: {
    '& .MuiTextField-root': {
      margin: theme.spacing(1),
      width: '40ch',
    },
  },
  
}));

export default  function NewContract(props) {
  
  
  const { context } = props;
  
  const classes = style();
  
  const [createNew, setCreateNew] = React.useState(false);
  
  const [chooseTemplate, setChooseTemplate] = React.useState(false);
  const [templateData, setTemplateData] = React.useState(false);
  const [renderContract, setRenderContract] = React.useState(false);
  const [contractData, setContractData] = React.useState({});
  const [showFinalizeComponent, setShowFinalizeComponent] = React.useState(false);
  const [showResultComponent, setShowResultComponent] = React.useState(false);
  const [result, setResult] = React.useState({});
  
  
  const templateCallback = (cancel,data) => {
  
    setChooseTemplate(false);
    
    if (!cancel) {
      setChooseTemplate(false);
      setTemplateData(data);
      setRenderContract(true);
    } else {
      setCreateNew(false);
    }
  };
  
  const contractCallback = (cancel,data) => {
  
    setRenderContract(false);
    
    if (cancel) {
      setCreateNew(false);
    } else {
      setContractData(data);
      setShowFinalizeComponent(true);
    }
  };
  
  const finalizeCallback = (cancel, password) => {
    
    if (cancel) {
      setShowFinalizeComponent(false);
      setCreateNew(false);
      return;
    }
  
    // send the template to
    const contractS = JSON.stringify(contractData);
    const hash = GetHash(contractS);
  
    const signature = GetSignature(context.encryptedPK,password,hash);
    const publicKey = context.publicKey;
  
   //TODO: get Type and Name...
  
    const tx = {
      originator:context.homeId,
      senderAccount:context.user,
      headerData:contractData.headerData,
      senderPublicKey: publicKey,
      hash:hash,
      signature:signature,
      body:contractS
    };
  
    // Send the request to the server...
    const url = "http://localhost:4000/contracts/contracts/add";
    
    
  
      return axios.post(url, tx).then((result) => {
    
        if (result.data.success) {
          // message posted successfully
          setResult({set: true, success: true})
        } else {
          setResult({set: true, success: false, error: result.data.error})
        }
        setShowFinalizeComponent(false);
        setShowResultComponent(true);
    
      }).catch (ex => {
        setShowResultComponent(true);
        setShowFinalizeComponent(false);
        setResult({set: true, success: false, error: ex.toString()});
        
    });
  };
  
  
  const onDone = () => {
    setShowResultComponent(false);
    setCreateNew(false);
  };
  
  const createNewContract = () => {
    setCreateNew(true);
    setChooseTemplate(true);
  };
  
  return (
    
    <div className={classes.content} >
        
      <div className={classes.margin}>
          <Typography color="primary" variant="h1" align="center">
            
            {!createNew &&

              <Button color="primary" onClick={createNewContract}> Create a New Contract</Button>
            }
            
            </Typography>
      </div>
      
      <div>
        {chooseTemplate &&
          <ChooseTemplate callback={templateCallback}/>
        }
        
        {renderContract &&
          <Contract data = {templateData} callback={ contractCallback }/>
        }
  
        {showFinalizeComponent &&
  
        <Finalize callback = {finalizeCallback} />
        
        }
  
        {showResultComponent &&
  
          <Result callback = {onDone} result={result} />
        }
        
        
      </div>
  
      
    </div>
    
  );
}


function ChooseTemplate(props) {
  
  const {callback} = props;
  
  const classes = style();
  
  const [fromSystemT, setFromSystemT] = React.useState(false);
  const [selectedIndex, setSelectedIndex] = React.useState(false);
  const [templates, setTemplates] = React.useState([]);
  
  
  const onSelectT = () => {
    callback(false,templates[selectedIndex]);
  };
  
  const onCancel = () => {
    callback(true,"");
  };
  
  const onSystemT = () => {
    setFromSystemT(true);
    
    const url = "http://localhost:4000/contracts/templates/get?publisher=System";
    
    axios.get(url).then ((result) => {
      setTemplates(result.data);
      
    }).catch ((error) => {
      console.log("Error in getting Internal Pending Invitations data  " + JSON.stringify(error));
    })
    
  };
  
  const handleListClick = (event, index) => {
    setSelectedIndex(index);
    
  };
  
  const getTemplateList = () => {
    
    return templates.map((item,index) => {
      
      const body = JSON.parse(item.tx.body);
      return (
        <ListItem
          button
          selected={selectedIndex === index}
          key={index}
          onClick={(event) => handleListClick(event, index)} >
          
          <ListItemText primary={body.heading.name} color="secondary" />
        </ListItem>
      
      )
    });
    
  };
  
  
  return (
    
    <div>
      {!fromSystemT &&
      <div align="center" style={{marginTop:100}}>
        <Button onClick={onSystemT} className={classes.shape} align="left" variant="contained" color="primary"
                style={{width: 300, height: 30}}>Select from System Templates </Button>
    
      </div>
      }
  
  
      { fromSystemT &&
      <div align="center" style={{marginTop:30, width:800}}>
    
        <List component="nav" align="center">
          {getTemplateList()}
    
        </List>
  
      </div>
    
      }
  
      {fromSystemT &&
      <div align="center" style={{marginTop:100}}>
        <Button onClick={onSelectT} className={classes.shape} align="left" variant="contained" color="primary"
                style={{width: 100, height: 30}}>Select Template </Button>
        <Button onClick={onCancel} className={classes.shape} align="left" variant="contained" color="primary"
                style={{marginLeft:30, width: 100, height: 30}}>Cancel </Button>
      </div>
      }
      
    </div>
    
  )
  
  
}

function Contract(props) {
  
  
  const { context, data,callback } = props;
  
  const[functionVariables, setFunctionVariables] = React.useState([]);
  const [partyValues, setPartyValues] = React.useState([]);
  const [name, setName] = React.useState("");
  const [startDate, setStartDate] = React.useState("");
  const [endDate, setEndDate] = React.useState("");
  const [codeFunction, setCodeFunction] = React.useState({});
  const [selectCF, setSelectCF] = React.useState(false);
  const [showAddCodeButtons, setShowAddCodeButtons] = React.useState(true);
  const [CFSelected, setCFSelected] = React.useState(false);
  
  const [controls, setControls] = React.useState([]);
  
  
  useEffect(()=> {
    
    const url = "http://localhost:4000/contracts/templates/controls/get?publisher=System";
    
    axios.get(url).then ((result) => {
      setControls(result.data);
      
    }).catch ((error) => {
      console.log("Error in getting Template Controls  " + JSON.stringify(error));
    })
    
  },[]);
  
  
  const body = JSON.parse(data.tx.body);
  const classes = style();
  
  
  
  const getArgs = () => {
    
    
    var result = [];
    result.push(startDate);
    result.push(endDate);
    
    for (var p =0; p<body.heading.parties.length; p++) {
      result.push(partyValues[p]);
    }
    
    for (var i =0; i < body.sections.length; i++) {
      const item = body.sections[i];
      if (item.variableType ==="party") continue;
      if (item.type === "variable") {
        result.push(functionVariables[i]);
      }
      
    }
    console.log("Arg Values  " + result);
    return result;
  };
  
  const onSelectCF = () => {
    setShowAddCodeButtons(false);
    setSelectCF(true);
  };
  
  const selectCodeFunctionCallback = (cancel, data) => {
    if (cancel) {
      setSelectCF(false);
      setShowAddCodeButtons(true);
    } else {
      setCodeFunction(data);
      setSelectCF(false);
      setCFSelected(true);
    }
  };
  
  const onSetPartyValue = (e,index) => {
    
    let pval = partyValues;
    let party = pval[index];
    if (!party) {
      party = {name: "", homeId: "", accountId: ""};
    }
    party.name = e.target.value;
    
    pval[index] = party;
    
    setPartyValues(pval);
    
  //  const newValue = partyValues;
  //  newValue[index] = e.target.value;
  //  setPartyValues(newValue);
    
  };
  
  const onSetPartyHome = (e,index) => {
    
    let pval = partyValues;
    let party = pval[index];
    if (!party) {
      party = {name: "", homeId: "", accountId: ""};
    }
    party.homeId = e.target.value;
    
    pval[index] = party;
    
    setPartyValues(pval);
    
  };
  
  const onSetPartyAccount = (e,index) => {
    
    let pval = partyValues;
    let party = pval[index];
    if (!party) {
      party = {name: "", homeId: "", accountId: ""};
    }
    party.accountId = e.target.value;
    
    pval[index] = party;
    
    setPartyValues(pval);
    
    
  };
  
  
  const setVariableValue = (e,index) =>  {
    const newValue = functionVariables;
     newValue[index] = e.target.value;
    setFunctionVariables(newValue);
  };
  
  const setDateVariable = (date,index) => {
    const newValue = functionVariables;
    newValue[index] = date.target.value;
    setFunctionVariables(newValue);
  };
  
  const getHeaderParties = () => {
    
    
    
    return body.heading.parties.map((party, index) => {
      
      return (
        <div key = {index}>
          <TextField
            style={{width: 250, margin: 2, marginTop: 10}}
            id = {party}
            key ={index}
            label={party}
            value = {partyValues.index}
            onChange={(e)=>{onSetPartyValue(e,index)}}
          />
          <TextField
            style={{width: 150, marginLeft: 5, marginTop: 10}}
            id = {party}
            
            label="homeId"
            onChange={(e)=>{onSetPartyHome(e,index)}}
          />
          <TextField
            style={{width: 250, marginLeft: 5, marginTop: 10}}
            id = {party}
    
            label="accountId"
            onChange={(e)=>{onSetPartyAccount(e,index)}}
          />
          
        </div>
        
      )
    })
  };
  
  const getHeader = () => {
    
    return (
      
      <div>
        <div className={classes.margin} style={{marginTop: 1, width: 500}}>
          <Typography variant="h5"> {body.heading.name} </Typography>
          <Typography variant="body1"> {body.heading.description} </Typography>
        </div>
  
        <div style={{marginTop:50}}>
          <Typography variant="h6"> Contract Schedule </Typography>
        </div>
  
        { body.heading.hasStartDate &&
  
        <div>
    
          <TextField
            style={{width: 500, margin: 2, marginTop: 10}}
      
            label="Contract Execution Date"
            value = {startDate}
            type="date"
            
            InputLabelProps={{
              shrink: true,
            }}
            onChange={(e)=>{setStartDate(e.target.value)}}
          />
  
        </div>
    
    
        }
  
        { body.heading.hasEndDate &&
  
        <div>
    
          <TextField
            style={{width: 500, margin: 2, marginTop: 10}}
      
            label="Contract Termination Date"
            value = {endDate}
            type="date"
            
            InputLabelProps={{
              shrink: true,
            }}
            onChange={(e) => {setEndDate(e.target.value)}}
          />
  
        </div>
    
        }
  
        <div style={{marginTop:50}}>
    
          <Typography variant="h6"> Contract Parties</Typography>
    
          {getHeaderParties()}
  
        </div>
        
        
      </div>
    )
  };
  
  const getBody = () => {
    
    return (
      
      <div>
        <div style={{marginTop:50}}>
          <Typography variant="h6" > Contract Body</Typography>
        </div>
        <div style={{marginTop:50}}>
          
          {getItems()}
        </div>
      
      
      </div>
    )
    
  };
  
  const getName = () => {
    
    return (
      
      <div>
        <div style={{marginTop:50}}>
          <Typography variant="h6" > Contract Name</Typography>
        </div>
        <div style={{marginTop:50}}>
  
          <TextField
            style={{width: 500, margin: 2}}
    
            label="Contract Name"
            value = {name}
            
            InputLabelProps={{
              shrink: true,
            }}
            onChange={(e)=>{setName(e.target.value)}}
          />
          
        </div>
      
      
      </div>
    )
    
  };
  
  const getItems = () => {
    
    return body.sections.map((item,index) => {
      if (item.type === "text") {
        
        const editorState = EditorState.createWithContent(convertFromRaw(item.value));
        
        return (
          
          
          <div className={classes.margin} style={{marginTop:1,width:500}} key = {index}>
            <Editor
              editorState={editorState}
              toolbarClassName="toolbarClassName"
              wrapperClassName="wrapperClassName"
              editorClassName="editorClassName"
              toolbarHidden
              readOnly
              editorStyle={{backgroundColor:"#FFFFFF",height:"auto"}}
            />
          
          </div>
        )
      } else if (item.type === "variable" ) {
        
        if ((item.variableType === "text") ) {
  
          return (
            
            <TextField
              
              style={{width: 500, margin: 2, marginTop: 10}}
              
              inputProps={{
                style: {fontSize: 15}
              }}
              color="primary"
              
              placeholder={item.description}
              
              value = {functionVariables[index]}
              onChange={(e)=>setVariableValue(e,index)}
              key = {index}
            
            />
          
          
          )
        } else if (item.variableType === "party") {
  
          
          return (
            
            <div>
              <TextField
                
                style={{width: 500, margin: 2, marginTop: 10}}
                
                inputProps={{
                  style: {fontSize: 15,color:"red"}
                }}
                color="secondary"
                placeholder={item.description}
                
                value = {functionVariables.index}
                onChange={(e)=>setVariableValue(e,index)}
                key = {index}
                
              />
              
            </div>
          )
        
        } else if (item.variableType === "number") {
          
          return (
            
            <TextField
              
              style={{width: 500, margin: 2, marginTop: 10}}
              type="number"
              
              inputProps={{
                style: {fontSize: 15}
              }}
              color="primary"
              
              placeholder={item.description}
              InputProps={{
                startAdornment: <InputAdornment position="start">{item.prefix}  :</InputAdornment>,
              }}

              value = {functionVariables.index}
              onChange={(e)=>setVariableValue(e,index)}
              key = {index}
              
            />
          
          )
        } else if (item.variableType === "date" ) {
          
          return (
            
            <div key = {index}>
  
              <TextField
                id="date"
                label={item.description}
                type="date"
                defaultValue="2021-01-01"
              
                InputLabelProps={{
                  shrink: true,
                }}

                value = {functionVariables.index}
                onChange={(e)=>setDateVariable(e,index)}
              />
            </div>
          )
        }
      } else if (item.type ==="control") {
        
        let control = getControlTemplate(item.name);
        let fv = functionVariables.index;
        if (control) {
          if (!fv) {
            let body = JSON.parse(control.tx.body);
            fv = [];
            body.sections.map((item, index) => {
              if (item.type === "variable") {
                if (item.variableType === "signature") {
                  fv[index] = {};
                  fv[index].roles  = [];
                } else {
                  fv[index] = "";
                }
              }
            });
          }
        }
        
        if (control) {
          return (
    
            <ControlEntry controlIndex={index} functionVariables={fv} template={control} key = {index}
                          callback={controlCallback}/>
          )
        } else {
          return "";
        }
      } else if (item.type === "controlFooter") {
  
        return (
          <div align="center" style={{marginTop:30, width:800}}>
      
            <Button className={classes.shape} align="left" variant="contained" color="primary"
                    style={{width: 200, height: 30, marginRight:30}}>Add Control </Button>
      
            <Button className={classes.shape} align="left" variant="contained" color="primary"
                    style={{width: 200, height: 30}}>Delete Control </Button>
    
          </div>
        )
  
      }
    })
    
  };
  
  const onSaveAndSubmit = () => {
    
    const hData = {
      name: name,
      startDate:startDate,
      endDate:endDate,
      partiesData: partyValues,
      templateType:body.heading.name,
      templateId:data._id,
      codeType: codeFunction.name,
      codeId:codeFunction._id
    };
  
    const contract = {
      contractTemplate:data._id,
      variableData: functionVariables,
      headerData: hData,
      codeFunction:codeFunction._id
    };
    
    callback(false,contract);
  };
  
  const getControlTemplate = (cid) => {
  
    let controlObject = "";
    
    for (let i =0; i <controls.length; i++) {
      if (controls[i]._id === cid) {
        controlObject = controls[i];
        break;
      }
    }
    return controlObject;
    
  };
  
  const controlCallback = (index,fv) => {
    let newFunctionVariable = functionVariables;
    newFunctionVariable[index] = fv;
    setFunctionVariables(newFunctionVariable);
  };
  
  
  return (
    
      
    <div className={classes.content} style={{width:800,align:"center",marginLeft:50}}>
      
      <div className={classes.margin}>
        <Typography color="primary" variant="h5" align="center"> Fill Contract values </Typography>
      </div>
      
      <div>
        
        {getHeader()}
        
        {getName() }
        
        {getBody()}
      
      </div>
  
      {showAddCodeButtons &&
      
      < div align="center" style={{marginTop:100}}>
    
          <Button onClick={onSelectCF} className={classes.shape} align="left" variant="contained" color="primary"
             style={{width: 300, height: 30}}>Add Code Function </Button>
    
          <Button onClick={() => {callback(true)}} className={classes.shape} align="left" variant="contained" color="primary"
             style={{width: 100, height: 30}}>Cancel </Button>
  
          <Button onClick={onSaveAndSubmit} className={classes.shape} align="left" variant="contained" color="primary"
                style={{width: 300, height: 30}}>Save and Submit </Button>
    
        </div>
    
      }
  
      {selectCF &&
  
      <div>
         <ChooseCodeFunction  callback = {selectCodeFunctionCallback } args = {getArgs()}/>
  
      </div>
      }
      
      {CFSelected &&
        
        <div style={{marginTop:30}}>
  
          <div >
            <Typography variant="h6" color="secondary" > Smart Contract Code  </Typography>
          </div>
        
          <div className={classes.margin} >
            <Typography variant="body1" color="secondary" > {codeFunction.name}  </Typography>
          </div>
        
          <div style ={{marginTop:20}}>
            
            <Button onClick={onSaveAndSubmit} className={classes.shape} align="left" variant="contained" color="primary"
                    style={{width: 300, height: 30}}>Save and Submit </Button>
  
             <Button onClick={() => {callback(true)}} className={classes.shape} align="left" variant="contained" color="primary"
                    style={{width: 100, height: 30}}>Cancel </Button>
          </div>
        
        
        </div>
      
      }
      
    </div>
    
  );
}

function ChooseCodeFunction(props) {
  
  const {callback, args} = props;
  
  const classes = style();
  
  const [chooseCodeFunction, setChooseCodeFunction] = React.useState(false);
  const [selectedIndex, setSelectedIndex] = React.useState(false);
  const [codeFunctions, setCodeFunctions] = React.useState([]);
  const [showCFList, setShowCFList] = React.useState(false);
  const [codeSelected, setCodeSelected] = React.useState(false);
  const [showCodeBox, setShowCodeBox] = React.useState(false);
  const [showResultBox, setShowResultBox] = React.useState(false);
  const [resultData, setResultData] = React.useState("");
  
  
  
  const onSave = () => {
    callback(false,codeFunctions[selectedIndex]);
  };
  
  const onSelectCF = () => {
    // callback(false,codeFunctions[selectedIndex]);
    setCodeSelected(true);
    setShowCFList(false);
  };
  
  const onCancel = () => {
    callback(true,"");
  };
  
  const onChooseCodeFunction = () => {
    setShowCFList(true);
    setChooseCodeFunction(true);
    
    const url = "http://localhost:4000/contracts/codes/get?publisher=System";
    
    axios.get(url).then ((result) => {
      setCodeFunctions(result.data);
      
    }).catch ((error) => {
      console.log("Error in getting Internal Pending Invitations data  " + JSON.stringify(error));
    })
    
  };
  
  const handleListClick = (event, index) => {
    setSelectedIndex(index);
    
  };
  
  const getCodeFunctionList = () => {
    
    return codeFunctions.map((item,index) => {
      return (
        <ListItem
          button
          selected={selectedIndex === index}
          key={index}
          onClick={(event) => handleListClick(event, index)} >
          
          <ListItemText primary={item.tx.name} color="secondary" />
        </ListItem>
      
      )
    });
    
  };
  
  
  const getCodeFunctionComponent = () => {
  
    const codeF = codeFunctions[selectedIndex];
    const codeBody = JSON.parse(codeF.tx.body);
    
  
    const editorState = EditorState.createWithContent(convertFromRaw(codeBody.data));
  
    return (
    
      <div align="center">
      
        
        <div align="center" className={classes.margin} style={{marginTop: 30, width: 800}}>
          <Editor
            editorState={editorState}
            toolbarClassName="toolbarClassName"
            wrapperClassName="wrapperClassName"
            editorClassName="editorClassName"
            toolbarHidden
            readOnly
            editorStyle={{backgroundColor: "#c4c4c4", height: "auto", fontFamily: "monospace", fontSize: 14}}
          />
      
        </div>
      </div>
    )
  
  };
  
  
  const onTest = () => {
    setShowResultBox(true);
  
    const codeF = codeFunctions[selectedIndex];
    const body = JSON.parse(codeF.tx.body);
  
    try {
      const raw = body.data;
      let text = "";
    
      raw.blocks.map((block,index) => {
        text = text +  block.text.trim();
      });
    
      const func = new Function("return " + text)();
      const result = func(...args);
      setResultData(result);
    } catch (exception) {
    
      setResultData(exception.toString())
    
    }
  };
  
  const showDialogCallback = () => {
    setShowResultBox(false);
  };
  
  
  
  return (
    
    <div>
      
      {!chooseCodeFunction &&
      
      <div align="center" style={{marginTop:100}}>
        <Button onClick={onChooseCodeFunction} className={classes.shape} align="left" variant="contained" color="primary"
                style={{width: 300, height: 30}}>Select a Code Function </Button>
      
      </div>
      }
      
      
      { showCFList &&
      <div align="center" style={{marginTop:30, width:800}}>
        
        <List component="nav" align="center" style={{align:"center",color:'blue'}}>
          {getCodeFunctionList()}
        
        </List>
      
      </div>
      
      }
      
      {showCFList &&
      <div align="center" style={{marginTop:100}}>
        <Button onClick={onSelectCF} className={classes.shape} align="left" variant="contained" color="primary"
                style={{width: 100, height: 30}}>Select</Button>
        <Button onClick={onCancel} className={classes.shape} align="left" variant="contained" color="primary"
                style={{marginLeft:30, width: 100, height: 30}}>Cancel </Button>
      
      </div>
      }
  
      {codeSelected &&
  
      <div style={{marginTop:30}}>
    
        <Typography variant="h5"> Contract Code</Typography>
    
    
        <div style={{marginTop:20}}>
          <Grid
            container
            direction="row"
            justify="flex-start"
            alignItems="flex-start"
          >
        
            <div className={classes.margin} align="center" >
              <Typography variant="h6" color="secondary" > {codeFunctions[selectedIndex].name}  </Typography>
            </div>
        
            <div align="center" style={{marginLeft:20}}>
          
              <IconButton size="medium" color="secondary" onClick = {()=> {setShowCodeBox(true)}}>
                <AttachmentIcon />
              </IconButton>
        
        
            </div>
          </Grid>
    
        </div>
  
  
  
      </div>
      }
      
      {showCodeBox &&
      
      <div>
     
        {getCodeFunctionComponent()}
  
        
        
      </div>
      
      
      }
  
  
      { showResultBox &&

      <CodeFunctionResult open = {showResultBox} data = {resultData} callback = {showDialogCallback}  />
    
      }
      
      {codeSelected &&

      <div align="center" style={{marginTop:100}}>
  
        <Button onClick={onTest} className={classes.shape} align="left" variant="contained" color="primary"
                style={{width: 100, height: 30}}>Test</Button>
        <Button onClick={onSave} className={classes.shape} align="left" variant="contained" color="primary"
                style={{marginLeft:10, width: 100, height: 30}}>Save </Button>
        <Button onClick={onCancel} className={classes.shape} align="left" variant="contained" color="secondary"
                style={{marginLeft:10, width: 100, height: 30}}>Cancel </Button>

      </div>
      
      }
      
      
    </div>
  
  )
  
  
}

function Finalize(props)  {
  
  const {callback} = props;
  const [password, setPassword] = React.useState("");
  const classes = style();
  
  const finalize = () => {
    
    callback(false,password);
  };
  
  const cancel = () => {
    callback(true);
  };
  
  return (
    
    <div align="center" style={{marginTop:100}}>
      
      <div align="center" style={{marginTop:100}}>
        <Typography variant="h3"> Finalize Template and Send to the Network </Typography>
      
      </div>
      
      <div align="center" style={{marginTop:100}}>
        
        <TextField
          
          style={{width: 400, marginTop: 30, backgroundColor: "#FFFFFF", height: 20}}
          placeholder="password for private key"
          value={password}
          id="password"
          onChange={(e) => {setPassword(e.target.value)}}
          color="secondary"
        />
      
      
      </div>
      
      <div align = "center" style={{marginTop:100}}>
        
        <Button onClick={finalize} className={classes.shape} variant="contained" align="right"
                style={{width: 250, height: 30}} color="secondary"> Send to Network  </Button>
        
        <Button onClick={cancel} className={classes.shape} variant="contained" align="right"
                style={{width: 120, height: 30, marginLeft: 50}} color="secondary"> Cancel </Button>
      
      </div>
    
    </div>
  
  )
}

function Result(props)  {
  
  const {callback, result} = props;
  const classes = style();
  
  const done = () => {
    callback();
  };
  
  
  
  return (
    
    <div align="center" style={{marginTop:100}}>
      
      {result.success &&
      <div align="center" style={{marginTop:100}}>
        
        <Typography variant="h3"> Transaction Submitted successfully </Typography>
      
      </div>
      
      }
      
      {!result.success &&
      
      <div align="center" style={{marginTop: 100}}>
        
        
        <Typography variant="h3"> Transaction submission failed with error: </Typography>
        <Typography variant="h5"> {result.error} </Typography>
      
      
      </div>
      }
      
      <div align = "center" style={{marginTop:100}}>
        
        <Button onClick={done} className={classes.shape} variant="contained" align="right"
                style={{width: 250, height: 30}} color="secondary"> Done </Button>
      
      
      </div>
    
    </div>
  
  )
}

