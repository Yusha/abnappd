
import React, { useEffect,useStates } from 'react';
import { makeStyles } from '@material-ui/core/styles';

import Typography from '@material-ui/core/Typography';


import MaterialTable from 'material-table';
import { forwardRef } from 'react';


import ContractDetails from './ContractDetails';
import IconDef from '../common/IconDef';

import axios from 'axios';
import ContractSignatures from "./ContractSignatures";


export default  function AllContracts(props) {
  
  const { context } = props;
  
  const classes = tableTheme();
  
  const [tableData, setTableData] = React.useState([]);
  const [indexData, setIndexData] = React.useState({});
  const [showContractDetail, setShowContractDetail] = React.useState(false);
  const [showSignatureDetail, setShowSignatureDetail] = React.useState(false);
  
  const originator = context.homeId;
  
  useEffect(()=> {
    
    const url = "http://localhost:4000/contracts/contracts/get?originator=" + originator;
    
     axios.get(url).then ((result) => {
      setTableData(result.data);
      
    }).catch ((error) => {
      console.log("Error in getting Internal Pending Invitations data  " + JSON.stringify(error));
    })
    
  },[]);
  
  
  
  
  const getDataSet = () => {
    const data = tableData.map((row, index) => {
      
      let status = row.signatures.length === row.header.partiesData.length? "complete" : "In Progress";
  
      return {startDate: row.header.startDate,endDate:row.header.endDate,templateType:row.header.templateType,codeType:row.header.codeType,
        parties:row.header.partiesData,status:status};
    });
    return data;
  };
  
  const showDetails = (e,row) => {
    
    setIndexData(tableData[row.tableData.id]);
    setShowContractDetail(true);
    
  };
  
  const showSignatures = (e,row) => {
    setIndexData(tableData[row.tableData.id]);
    setShowSignatureDetail(true);
  };
  
  
  
  const contractDetailCallback = (f) => {
    setShowContractDetail(false);
  };
  
  const contractSignatureCallback = (f) => {
    setShowSignatureDetail(false);
  };
  
  
  const getPartyList = (rowData) => {
    
    return rowData.parties.map((party, index) => {
      return <Typography >{party.homeId} / {party.accountId} </Typography>
    })
  };
  
  
  
  
  
  
  return (
    
    <div className={classes.table}>
      
      <div>
        {!showContractDetail &&
        <MaterialTable
          title="All Contracts"
          icons={IconDef}
    
    
          localization={{
            header: {
              actions: 'Actions'
            },
          }}
    
          columns={[
            {title: 'Start Date', field: 'startDate'},
            {title: 'End Date', field: 'endDate'},
            {title: 'Parties', field: 'parties',render:rowData => <div > {getPartyList(rowData)}  </div> },
            {title: 'Template', field: 'templateType'},
            {title: 'Code', field: 'codeType'},
            {title: 'Status', field: 'status'},
    
          ]}
          data={getDataSet()}
    
          actions={[
  
            {
              icon: IconDef.ViewEye,
              tooltip: 'View Details',
              onClick: (event, rowData) => {showDetails(event,rowData)}
            },
            {
              icon: IconDef.FingerPrint,
              tooltip: 'Signatures',
              onClick: (event, rowData) => {showSignatures(event,rowData)}
            },
            
            
          ]}
    
          options={{
            
            padding: "dense",
            headerStyle: {
              color: 'white',
              background: '#0091EA',
              fontSize: 16,
              fontFamily: "Roboto"
            },
            rowStyle: {
              color: 'black'
            }
      
          }}
    
          components={{}}
        />
        }
        
      </div>
  
      {showContractDetail &&
        <div>
          
          <ContractDetails callback={contractDetailCallback} data={indexData} open = {showContractDetail}/>
        </div>
    
      }
      
      {showSignatureDetail &&
        
        <ContractSignatures callback = {contractSignatureCallback} open = {showSignatureDetail} data = {indexData} />
        
      }
      
    </div>
  )
}



const tableTheme = makeStyles(theme => ({
  table: {
    '& tbody>.MuiTableRow-root:hover': {
      background: '#EEE',
    }
  },
  body: {
    textColor:"primary"
  }
}));
