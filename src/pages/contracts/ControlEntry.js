import React from 'react';

import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';


import {Editor} from 'react-draft-wysiwyg';

import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { EditorState, convertFromRaw } from 'draft-js';


export default function ControlEntry(props) {
  
  
  const {template,functionVariables,controlIndex, callback } = props;
  
  const body = JSON.parse(template.tx.body);
  
  
  
  const setVariableValue = (e,index) =>  {
    
    const newValue = functionVariables;
    newValue[index] = e.target.value;
    callback(controlIndex,newValue);
  };
  
  const setSignatoryName = (e,index) =>  {
    const newValue = functionVariables;
    let indexValue = newValue[index];
    
    if (!indexValue) {
      indexValue = {};
      indexValue.roles = [];
    }
    indexValue.name = e.target.value;
    
    newValue[index] = indexValue;
    callback(controlIndex,newValue);
  };
  
  const setSignatoryTitle = (e,index) =>  {
    const newValue = functionVariables;
    let indexValue = newValue[index];
    
    if (!indexValue) {
      indexValue = {};
      indexValue.roles = [];
    }
    indexValue.title = e.target.value;
    
    newValue[index] = indexValue;
    callback(controlIndex,newValue);
  };
  
  const setSignatoryRole = (e,indexRole, index) =>  {
    const newValue = functionVariables;
    let indexValue = newValue[indexRole];
    
    if (!indexValue) {
      indexValue = {};
      indexValue.roles = [];
    }
    
    indexValue.roles[index] = e.target.checked;
    newValue[indexRole] = indexValue;
    callback(controlIndex,newValue);
    
  };
  
  const setSignatoryHome = (e,index) =>  {
    const newValue = functionVariables;
    let indexValue = newValue[index];
    
    if (!indexValue) {
      indexValue = {};
    }
    indexValue.home = e.target.value;
    
    newValue[index] = indexValue;
    callback(controlIndex,newValue);
  };
  
  const setSignatoryAccount = (e,index) =>  {
    const newValue = functionVariables;
    let indexValue = newValue[index];
    
    if (!indexValue) {
      indexValue = {};
    }
    indexValue.account = e.target.value;
    
    newValue[index] = indexValue;
    callback(controlIndex,newValue);
  };
  
  
  
  const getBody = () => {
    
    return (
      
      <div>
        
        <div style={{marginTop:50}}>
          
          {getItems()}
        </div>
        
        
      </div>
    )
    
  };
  
  
  const getRoles = (signerRoles, indexRole) => {
    
    
    return signerRoles.map((role,index) => {
      return (
        
        <FormControlLabel
          control={
            <Checkbox checked= {functionVariables[indexRole].roles[index]} onChange={(e)=>setSignatoryRole(e,indexRole, index)} name = {role} />
          }
          label= {role}
          key = {index}
          
          
        />
      
      )
      
    })
    
  };
  
  const getItems = () => {
    
    return body.sections.map((item,index) => {
      if (item.type === "text") {
        
        const editorState = EditorState.createWithContent(convertFromRaw(item.value));
        
        return (
          <div style={{marginTop:1,width:500}} >
            <Editor
              editorState={editorState}
              toolbarClassName="toolbarClassName"
              wrapperClassName="wrapperClassName"
              editorClassName="editorClassName"
              toolbarHidden
              readOnly
              editorStyle={{backgroundColor:"#FFFFFF",height:"auto"}}
            />
          
          </div>
        )
      } else if (item.type === "variable" ) {
        
        if ((item.variableType === "text") ) {
          
          return (
  
            <TextField
    
              style={{width: 500, margin: 2, marginTop: 10}}
    
              inputProps={{
                style: {fontSize: 15},
              }}
              color="primary"
              label={item.label}
    
              
              placeholder={item.description}

              value = {functionVariables.index}
              
              onChange={(e)=>setVariableValue(e,index)}
              key={index}
              
            />
          )
        } else if (item.variableType === "party") {
          
          return (
            
            <div>
              <TextField
                
                style={{width: 500, margin: 2, marginTop: 10}}
                
                inputProps={{
                  style: {fontSize: 15}
                }}
                color="secondary"
                
                placeholder={item.description}

                
                value = {functionVariables.index}
                onChange={(e)=>setVariableValue(e,index)}
                
              
              />
              
            </div>
          )
          
          
        } else if ((item.variableType === "date") ||
          (item.variableType === "number")) {
          
          return (
            
            <TextField
              
              style={{width: 500, margin: 2, marginTop: 10}}
              
              inputProps={{
                style: {fontSize: 15}
              }}
              color="primary"
              
              placeholder={item.description}
              InputProps={{
                startAdornment: <InputAdornment position="start">{item.prefix}  :</InputAdornment>,
              }}
              
              value = {functionVariables.index}
              key={index}
              onChange={(e)=>setVariableValue(e,index)}
            />
          
          )
        } else if (item.variableType === "signature") {
  
          return (
    
            <div style={{marginTop:50}} key = {index}>
              <Typography variant="h5" color="secondary">
                Signature
              </Typography>
      
              <div >
                <TextField
          
                  style={{width: 300, margin: 2, marginTop: 10}}
          
                  inputProps={{
                    style: {fontSize: 15}
                  }}
                  color="secondary"
          
                  label = "Signatory Name"
                  placeholder={item.description}

                  value = {functionVariables[index].name}
                  onChange={(e)=>setSignatoryName(e,index)}
        
        
                />
                <TextField
          
                  style={{marginLeft:30,width: 300, margin: 2, marginTop: 10}}
          
                  inputProps={{
                    style: {fontSize: 15}
                  }}
                  color="secondary"
          
                  label= "Signatory Title"
                  value = {functionVariables[index].title}
                  onChange={(e)=>setSignatoryTitle(e,index)}


                />
      
      
              </div>
      
              <div >
                <TextField
          
                  style={{width: 300, margin: 2, marginTop: 10}}
          
                  inputProps={{
                    style: {fontSize: 15}
                  }}
                  color="secondary"
          
                  label = "Signatory Home"
                  value = {functionVariables[index].home}
                  onChange={(e)=>setSignatoryHome(e,index)}



                />
                <TextField
          
                  style={{marginLeft:10, width: 300, margin: 2, marginTop: 10}}
          
                  inputProps={{
                    style: {fontSize: 15}
                  }}
                  color="secondary"
          
                  label= "Signatory Account"
                  value = {functionVariables[index].account}
                  onChange={(e)=>setSignatoryAccount(e,index)}


                />
      
      
              </div>
      
              <div align = "center" style={{marginTop:30}}>
                <Typography> Signatory Role</Typography>
        
                <FormGroup row align= "center">
                  
                  {getRoles(item.roles, index)}
        
                </FormGroup>
      
      
      
              </div>
    
            </div>
          )
  
        }
        
      }
    })
    
  };
  
  
  return (
  
  
    <div align = "center" style={{marginTop:100, borderStyle:"solid", borderColor:"black", width:800}}>
      
      {getBody()}
      
    </div>
    
  );
}

