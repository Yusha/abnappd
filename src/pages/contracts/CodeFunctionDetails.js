import React from 'react';
import { makeStyles} from '@material-ui/core/styles';

import Dialog from '@material-ui/core/Dialog';
import Button from '@material-ui/core/Button';

import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';


import {Editor} from 'react-draft-wysiwyg';

import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { EditorState, convertFromRaw } from 'draft-js';


export default function CodeFunctionDetails(props) {
  
  
  const { context, data,callback,open } = props;
  const [test, setTest] = React.useState(false);
  const [run, setRun] = React.useState(false);
  const [testArgs, setTestArgs] = React.useState("");
  const [showResult, setShowResult] = React.useState(false);
  const [testResult, setTestResult] = React.useState("");
  
  
  const body = JSON.parse(data.tx.body);
  
  
  const classes = dialogStyles();
  
  const handleClose = (action) => {
    callback(action);
  };
  
  const onTest = () => {
    setTest(true);
    setRun(true);
    
  };
  
  const onRun = () => {
    setShowResult(true);
    try {
      const raw = body.data;
      let text = "";
      
      raw.blocks.map((block,index) => {
        text = text +  block.text.trim();
      });
      
      const func = new Function("return " + text)();
      const allArrays = testArgs.split(",");
      const result = func(...allArrays);
      
      setTestResult(JSON.stringify(result));
      
    } catch (exception) {
      
      setTestResult(exception.toString())
      
    }
    
  };
  
  
  
  const getContent = () => {
  
    const editorState = EditorState.createWithContent(convertFromRaw(body.data));
    
    return (
      
      <div align="center">
      
        <div align="center" className={classes.margin} style={{marginTop: 1, width: 800}}>
        
          <Typography variant="h5"> {body.name} </Typography>
        </div>
  
        <div align="center" className={classes.margin} style={{marginTop:30,width:800}} >
          <Editor
            editorState={editorState}
            toolbarClassName="toolbarClassName"
            wrapperClassName="wrapperClassName"
            editorClassName="editorClassName"
            toolbarHidden
            readOnly
            editorStyle={{backgroundColor:"#c4c4c4",height:"auto", fontFamily:"monospace",fontSize:14}}
          />
  
        </div>
      </div>
    )
    
    
    
  };
  
  
  
  
  
  return (
    <Dialog onClose={handleClose} open={open} fullWidth maxWidth="lg" classes={{paper: classes.dialog }} >
      
      <div className={classes.content} style={{width:800,align:"center",marginLeft:50}}>
        
        <div className={classes.margin}>
          <Typography color="primary" variant="h5" align="center"> Template Details</Typography>
        </div>
        
        <div>
          
          {getContent()}
        </div>
        
        <div>
  
          {test &&
  
          <div align="center" style={{marginTop: 10, marginBottom: 20, width: 800}}>
    
    
            <div>
      
              <TextField
                style={{width: 400}}
        
                value={testArgs}
                onChange={(e) => {
                  setTestArgs(e.target.value)
                }}
                placeholder="Please Enter Comma Separated Input Values"
                label="Contract Input Value"
              />
            </div>
  
          </div>
          }
          
          
        </div>
  
        { showResult &&
  
        <div style={{width: 800, align: "center", marginLeft: 50, marginTop: 100, marginBottom:20}}>
          <div align="center" style={{marginBottom:50}}>
            <Typography variant="body1">
              Contract Results
            </Typography>
          </div>
    
          {testResult}
  
        </div>
    
        }
        
        
  
        <div align="center" style={{width:800,marginTop:100, marginBottom:100}}>
          <Button onClick={()=>{callback(true)}} className={classes.shape} align="left" variant="contained" color="primary"
                  style={{width: 100, height: 30, marginBottom:100}}>Close </Button>
          
          {!test &&
          <Button onClick={onTest} className={classes.shape} align="left" variant="contained" color="primary"
                  style={{width: 100, height: 30, marginBottom: 100}}>Test </Button>
          }
  
          {run &&
          <Button onClick={onRun} className={classes.shape} align="left" variant="contained" color="primary"
                  style={{width: 100, height: 30, marginBottom: 100}}>Run </Button>
          }
          
          
          
          
          
        </div>
        
      </div>
    
    
    </Dialog>
  );
}

const dialogStyles = makeStyles({
  dialog: {
    position: 'absolute',
    left: 10,
    top: 50
  },
  table: {
    minWidth: 700,
  },
});