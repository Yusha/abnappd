import React from 'react';


import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import MenuItem from '@material-ui/core/MenuItem';
import InputAdornment from '@material-ui/core/InputAdornment';

import {Editor} from 'react-draft-wysiwyg';

import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { EditorState, convertToRaw, convertFromRaw } from 'draft-js';

import EditIcon from '@material-ui/icons/Edit';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';

import Grid from '@material-ui/core/Grid';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';




import axios from 'axios';

import { makeStyles } from '@material-ui/core/styles';
import {GetSignature,GetHash} from '../../ec/CryptoUtils';


const style = makeStyles((theme)=> ({
  dialog: {
    position: 'absolute',
    left: 10,
    top: 50
  },
  input: {
    display: 'none',
  },
  shape: {
    borderRadius: 25,
  },
  selectRoot: {
    '& .MuiTextField-root': {
      margin: theme.spacing(1),
      width: '40ch',
    },
  },
  
}));

export default  function NewControl(props) {
  
  
  const { context } = props;
  
  const classes = style();
  
  
  
  const [createNew, setCreateNew] = React.useState(false);
  
  const [isTemplate, setTemplate] = React.useState(false);
  const [templateData, setTemplateData] = React.useState("");
  const [finalize, setFinalize] = React.useState(false);
  const [result,setResult] = React.useState({set:false});
  
  
  
  const templateCallback = (cancel,data) => {
  
    setTemplate(false);
    
    if (!cancel) {
      setTemplate(false);
      
      setTemplateData(data);
      setFinalize(true);
    } else {
      setCreateNew(false);
    }
  };
  
  const createNewTemplate = () => {
    setCreateNew(true);
    setTemplate(true);
  };
  
  
  
  
  const onFinalize = (cancel,password) => {
    if (cancel) {
      setFinalize(false);
      setTemplate(true);
      return;
    }
    
    // send the template to
    const templateS = JSON.stringify(templateData);
    const hash = GetHash(templateS);
  
    const signature = GetSignature(context.encryptedPK,password,hash);
    const publicKey = context.publicKey;
    
  
    
    const tx = {
      publisher:"System",
      senderHome: context.homeId,
      senderAccount:context.user,
      senderPublicKey: publicKey,
      hash:hash,
      signature:signature,
      body:templateS
    };
    
    // Send the request to the server...
    const url = "http://localhost:4000/contracts/templates/controls/add";
    
    return axios.post(url,tx).then ((result) => {
      
      if (result.data.success) {
        // message posted successfully
        setResult({set:true,success:true})
      } else {
        setResult({set:true,success:false,error:result.data.error})
      }
      setFinalize(false);
      
    });
    
    
  };
  
  const onDone = () => {
    setCreateNew(false);
    setResult(false);
  };
  

  
  return (
    
    <div className={classes.content} >
        
      <div className={classes.margin}>
          <Typography color="primary" variant="h1" align="center">
            
            {!createNew &&

              <Button color="primary" onClick={createNewTemplate}> Create a Custom Control</Button>
            }
            
            </Typography>
      </div>
      
      <div>
        {isTemplate &&
          <Control callback={templateCallback}/>
        }
        
        {finalize &&
        
          <Finalize callback = {onFinalize} />
        }
        
        {result.set &&
        
          <Result callback = {onDone} result={result} />
        }
      </div>
  
      
    </div>
    
  );
}



function Control(props) {
  
  const {callback} = props;
  const classes = style();
  
  const [isHeaderSet, setIsHeaderSet] = React.useState(false);
  
  
  const [headerData,setHeaderData] = React.useState({});
  
  const [items, setItems] = React.useState([]);
  const [isInsert, setIsInsert] = React.useState(false);
  const [insertText, setInsertText] = React.useState(false);
  const [insertFunction, setInsertFunction] = React.useState(false);
  const [textEditorState, setTextEditorState] = React.useState(EditorState.createEmpty());
  
  
  // Function variables
  const [variableId, setVariableId] = React.useState("");
  const [variableDescription, setVariableDescription] = React.useState("");
  const [variableType, setVariableType] = React.useState("");
  const [variablePrefix, setVariablePrefix] = React.useState("");
  const [variableLabel, setVariableLabel] = React.useState("");
  
  
  const [signerRoles, setSignerRoles] = React.useState([]);
  
  
  const saveControl = () => {
    callback(false,{heading:headerData,sections:items});
  };
  
  const cancelControl = () => {
    callback(true);
  };
  
  const onInsertText = () => {
    setIsInsert(true);
    setInsertText(true);
  };
  
  const onInsertFunction = () => {
    setIsInsert(true);
    setInsertFunction(true);
    
  };
  
  const cancelItem = () => {
    setIsInsert(false);
    setInsertFunction(false);
    
  };
  
  const saveItem = () => {
    if (insertText) {
      const raw = convertToRaw(textEditorState.getCurrentContent());
      
      setTextEditorState(EditorState.createEmpty());
      setItems(items.concat({type:"text",value:raw}));
      setIsInsert(false);
      setInsertText(false);
      
    }
    if (insertFunction) {
      if (variableType ==="signature") {
  
        setItems(items => [...items,
          {
            type: "variable",
            variableType: "signature",
            label:variableLabel,
            roles:signerRoles
          }]);
  
        setIsInsert(false);
        setInsertFunction(false);
        
      } else {
        setItems(items => [...items,
          {
            type: "variable",
            variableType: variableType,
            id: variableId,
            description: variableDescription,
            label:variableLabel,
            prefix: variablePrefix
          }]);
  
        setIsInsert(false);
        setInsertFunction(false);
      }
      
    }
  };
  
  
  const onTextEditorStateChange = (s) => {
    setTextEditorState(s);
  };
  
  const setFieldValue = (e) => {
    
    const id = e.target.id;
    const value = e.target.value;
    
    if (id ==="vid") {
      setVariableId(value);
    
    } else if (id === "vdesc") {
      setVariableDescription(value)
      
    } else if (id === "prefix") {
      setVariablePrefix(value);
    
    } else if (id === "role1") {
      let newSignerRoles = signerRoles;
        newSignerRoles[0] = value;
        setSignerRoles(newSignerRoles);
    
    } else if (id === "role2") {
      let newSignerRoles = signerRoles;
      newSignerRoles[1] = value;
      setSignerRoles(newSignerRoles);
    
    }else if (id === "role3") {
      let newSignerRoles = signerRoles;
      newSignerRoles[2] = value;
      setSignerRoles(newSignerRoles);
    
    }else if (id === "role4") {
      let newSignerRoles = signerRoles;
      newSignerRoles[3] = value;
      setSignerRoles(newSignerRoles);
    } else if (id === "label") {
      setVariableLabel(value);
    }
    
    else {
      console.log("id  is  " + id);
    }
    
  };
  
  
  const controlHeaderCallback = (cancel,data) => {
    
    if (cancel) {
      setIsHeaderSet(false);
      callback(true);
      
    } else {
      setHeaderData(data);
      setIsHeaderSet(true);
    }
  };
  
  
  const getHeader = () => {
    
    
    
    if (isHeaderSet) {
      return (
        
        <div>
  
          <div className={classes.margin} style={{marginTop: 20, width: 1000}}>
            <Typography variant="h4"> {headerData.name} </Typography>
            <Typography variant="body1"> {headerData.description} </Typography>
          </div>
          
        </div>
        
      
      
      );

    }
    
  };
  
  const getBody = () => {
    
    return (
      
      <div>
        
        <div style={{marginTop:50}}>
          
          {getItems()}
        </div>
      
      
      </div>
    )
    
  };
  
  const getRoles = () => {
    
    return signerRoles.map((role,index) => {
      return (
        
        <FormControlLabel
          control={
            <Checkbox  />
          }
          label= {role}
        />
        
      )
      
    })
    
  };
  
  
  const getItems = () => {
    
    return items.map((item,index) => {
      if (item.type === "text") {
        
        const editorState = EditorState.createWithContent(convertFromRaw(item.value));
        
        return (
  
          <Grid
            container
            direction="row"
            justify="flex-start"
            alignItems="flex-start"
          >
  
            <div className={classes.margin} style={{marginTop:1,width:500}} >
              <Editor
                editorState={editorState}
                toolbarClassName="toolbarClassName"
                wrapperClassName="wrapperClassName"
                editorClassName="editorClassName"
                toolbarHidden
                readOnly
                editorStyle={{backgroundColor:"#FFFFFF",height:"auto"}}
              />
            
            </div>
            
            <div align="center">
              
                <IconButton aria-label="edit" size="small">
                  <EditIcon />
                </IconButton>
                <IconButton aria-label="delete" size="small">
                  <DeleteIcon />
                </IconButton>
              
            </div>
          </Grid>
          
        )
      } else if (item.type === "variable" ) {
  
        if ((item.variableType === "text")) {
    
          return (
      
            <TextField
        
              style={{width: 500, margin: 2, marginTop: 10}}
        
              inputProps={{
                style: {fontSize: 15},
                readOnly: true,
              }}
              color="primary"
              label={item.label}

              helperText={item.description}
      
      
            />
    
    
          )
        } else if (item.variableType === "party") {
    
          return (
      
            <div>
              <TextField
          
                style={{width: 500, margin: 2, marginTop: 10}}
          
                inputProps={{
                  style: {fontSize: 15}
                }}
                color="secondary"
          
                placeholder={item.description}
          
                InputProps={{
                  startAdornment: <InputAdornment position="start">{item.prefix}  </InputAdornment>,
                  readOnly: true,
                }}
        
        
              />
      
            </div>
          )
    
    
        } else if ((item.variableType === "date") ||
          (item.variableType === "number")) {
    
          return (
      
            <TextField
        
              style={{width: 500, margin: 2, marginTop: 10}}
        
              inputProps={{
                style: {fontSize: 15}
              }}
              color="primary"
        
              placeholder={item.description}
              InputProps={{
                startAdornment: <InputAdornment position="start">{item.prefix}  </InputAdornment>,
                readOnly: true,
              }}
      
            />
    
    
          )
        } else if (item.variableType === "signature") {
    
          return (
      
            <div style={{marginTop:50}}>
              <Typography variant="h5" color="secondary">
                Signature
              </Typography>
              
              <div >
                <TextField
          
                  style={{width: 300, margin: 2, marginTop: 10}}
          
                  inputProps={{
                    style: {fontSize: 15}
                  }}
                  color="secondary"
          
                  label = "Signatory Name"
        
        
                />
                <TextField
    
                  style={{marginLeft:30,width: 300, margin: 2, marginTop: 10}}
    
                  inputProps={{
                    style: {fontSize: 15}
                  }}
                  color="secondary"
    
                  label= "Signatory Title"
                  
                />
                
                
              </div>
  
              <div >
                <TextField
      
                  style={{width: 300, margin: 2, marginTop: 10}}
      
                  inputProps={{
                    style: {fontSize: 15}
                  }}
                  color="secondary"
      
                  label = "Signatory Home"
    
    
                />
                <TextField
      
                  style={{marginLeft:10, width: 300, margin: 2, marginTop: 10}}
      
                  inputProps={{
                    style: {fontSize: 15}
                  }}
                  color="secondary"
      
                  label= "Signatory Account"
    
                />
  
  
              </div>
  
              <div style={{marginTop:30}}>
                <Typography> Signatory Role</Typography>
  
                <FormGroup row>
                  {getRoles()}
                  
                </FormGroup>
                
                
  
              </div>
      
            </div>
          )
          
        }
      }
    })
    
  };
  
  const onSelectFunction = (e) => {
    const vType = e.target.value;
    setVariableType(vType);
  };
  
  
  
  
  return (
  
  <div style={{width: "80%", align: "center", marginLeft: 50}}>
  
  
    {!isHeaderSet &&
    <div>
      <ControlHeader callback = {controlHeaderCallback}/>
      
    </div>
    
    }
  
    <div>
  
      {getHeader()}
      
      {getBody()}
  
    </div>
  
    <div align="center" style={{marginTop: 100, marginBottom: 100}}>
    
    
      {!isInsert && isHeaderSet &&
      <div>
      
        <Typography variant="h5"> Add Control Items </Typography>
      
        <div style={{marginTop:30, marginLeft:100}}>
        
          <Button onClick={onInsertText} className={classes.shape} align="left" variant="contained" color="primary"
                  style={{width: 200, height: 30}}>Insert Text </Button>
        
          <Button onClick={onInsertFunction} className={classes.shape} variant="contained" align="right"
                  style={{width: 200, height: 30, marginLeft: 20}} color="secondary"> Insert Function </Button>
        
        </div>
  
        <div style={{marginTop:30,marginLeft:100}}>
    
          <Button onClick={saveControl} className={classes.shape} variant="contained" align="right"
                  style={{width: 200, height: 30, marginLeft: 20}} color="secondary"> Save Control </Button>
    
          <Button onClick={cancelControl} className={classes.shape} variant="contained" align="right"
                  style={{width: 200, height: 30, marginLeft: 20}} color="secondary"> Cancel </Button>
        </div>
    
      </div>
      
      }
    
      {insertText &&
    
      <div className={classes.margin} style={{marginTop:30,width:800}}>
        <Editor
          editorState={textEditorState}
          toolbarClassName="toolbarClassName"
          wrapperClassName="wrapperClassName"
          editorClassName="editorClassName"
          placeholder = "Note for read request..."
          onEditorStateChange={onTextEditorStateChange}
          editorStyle={{backgroundColor:"#FFFFFF",height:"auto"}}
        />
    
      </div>
      
      
      }
    
      { insertFunction &&
      <div>
      
        {variableType ==="text" &&
        <div style={{marginTop:30}} align="center">
  
  
          <TextField
            style={{width: 200, margin: 2, marginTop: 10}}
            value = {variableLabel}
            id = "label"
            onChange={setFieldValue }
            color = "secondary"
    
            label="Field Label"
          />
  
          <TextField
            style={{width: 150, margin: 2, marginTop: 10}}
            value = {variableId}
            id = "vid"
            onChange={setFieldValue }
    
            label="Variable Id"
          />
  
          <TextField
            style={{width: 400, margin: 2, marginTop: 10}}
            value = {variableDescription}
            id = "vdesc"
            onChange={setFieldValue }
            label="Variable description"
          />



        </div>
        }
  
        {variableType ==="number" &&
        <div style={{marginTop:30}} align="center">
    
    
          <TextField
            style={{width: 70, margin: 2, marginTop: 10}}
            value = {variablePrefix}
            id = "prefix"
            onChange={setFieldValue }
            color = "secondary"
      
            label="prefix"
          />
    
          <TextField
            style={{width: 200, margin: 2, marginTop: 10}}
            value = {variableId}
            id = "vid"
            onChange={setFieldValue }
      
            label="Variable Id"
          />
    
          <TextField
            style={{width: 400, margin: 2, marginTop: 10}}
            value = {variableDescription}
            id = "vdesc"
            onChange={setFieldValue }
            label="Variable description"
          />
  
  
  
        </div>
        }
  
        {variableType ==="party" &&
        <div style={{marginTop:30}} align="center">
    
    
          <TextField
            style={{width: 70, margin: 2, marginTop: 10}}
            value = {variablePrefix}
            id = "prefix"
            onChange={setFieldValue }
            color = "secondary"
      
            label="prefix"
          />
    
          <TextField
            style={{width: 200, margin: 2, marginTop: 10}}
            value = {variableId}
            id = "vid"
            onChange={setFieldValue }
      
            label="Variable Id"
          />
    
          <TextField
            style={{width: 400, margin: 2, marginTop: 10}}
            value = {variableDescription}
            id = "vdesc"
            onChange={setFieldValue }
            label="Variable description"
          />
  
  
  
        </div>
        }
  
        {variableType ==="date" &&
        <div style={{marginTop:30}} align="center">
    
    
          <TextField
            style={{width: 70, margin: 2, marginTop: 10}}
            value = {variablePrefix}
            id = "prefix"
            onChange={setFieldValue }
            color = "secondary"
      
            label="prefix"
          />
    
          <TextField
            style={{width: 200, margin: 2, marginTop: 10}}
            value = {variableId}
            id = "vid"
            onChange={setFieldValue }
      
            label="Variable Id"
          />
    
          <TextField
            style={{width: 400, margin: 2, marginTop: 10}}
            value = {variableDescription}
            id = "vdesc"
            onChange={setFieldValue }
            label="Variable description"
          />
  
  
  
        </div>
        }
  
        {variableType ==="signature" &&
        <div style={{marginTop:30}} align="center">
          
          <div>
            Signatory Role Options
            
          </div>
    
          <div>
    
            <TextField
              style={{width: 200, margin: 2, marginTop: 10}}
              value = {signerRoles[0]}
              id = "role1"
              onChange={setFieldValue }
              color = "secondary"
      
              label="Role Option 1"
          />
    
            <TextField
            style={{width: 200, margin: 2, marginTop: 10}}
            value = {signerRoles[1]}
            id = "role2"
            onChange={setFieldValue }
      
            label="Role Option 2"
          />
    
          </div>
          <div>
    
            <TextField
              style={{width: 200, margin: 2, marginTop: 10}}
              value = {signerRoles[2]}
              id = "role3"
              onChange={setFieldValue }
              color = "secondary"
      
              label="Role Option 3"
            />
    
            <TextField
              style={{width: 200, margin: 2, marginTop: 10}}
              value = {signerRoles[3]}
              id = "role4"
              onChange={setFieldValue }
      
              label="Role Option 4"
            />
  
          </div>
          
  
        </div>
        }
        
        
        <div  style={{marginTop:10,width:900}} >
        
          <TextField
          
            select
            value={variableType}
            onChange={onSelectFunction}
            helperText="Please select variable type"
          >
          
            <MenuItem key="1" value="text">
              Text
            </MenuItem>
          
            <MenuItem key="2" value="number">
              Number
            </MenuItem>
            
            <MenuItem key="3" value="date">
              Date
            </MenuItem>
            
            <MenuItem key="4" value="party">
              Party
            </MenuItem>
            
            <MenuItem key="5" value="signature">
              Signature
            </MenuItem>
            
        
          </TextField>
      
        </div>
    
    
      </div>
      
      }
    
      {isInsert &&
      <div>
      
        <Button onClick={saveItem} className={classes.shape} variant="contained" align="right"
                style={{width: 120, height: 30, marginLeft: 20}} color="secondary"> Save </Button>
      
        <Button onClick={cancelItem} className={classes.shape} variant="contained" align="right"
                style={{width: 120, height: 30, marginLeft: 20}} color="secondary"> Cancel </Button>
    
      </div>
      }
  
  
    </div>
    
  
    
    
  </div>
  
  
  );
}

function ControlHeader(props)  {
  
  const {callback} = props;
  
  const classes = style();
  
  const [name, setName] = React.useState("");
  const [description, setDescription] = React.useState("");
  
  const saveHeader = () => {
    callback(false,{name:name,description:description});
  };
  
  const cancelHeader = () => {
    callback(true);
    
  };
  
  
  
  return (
    
    <div style={{width: "80%", marginLeft: 50}}>
      
      <div>
        
        <div align="center">
          <Typography variant="h5"> Control Header</Typography>
        
        </div>
        <div className={classes.margin} align="center" style={{marginTop: 30}}>
          <TextField
            
            style={{width: 800, margin: 2, marginTop: 10}}
            
            inputProps={{
              style: {fontSize: 25}
            }}
            color="primary"
            value={name}
            label={
              <Typography variant="h5"> Control Name </Typography>
            }
            
            id="name"
            
            fullWidth
            
            onChange={(e)=>{setName(e.target.value)}}
          
          />
          
          <TextField
            
            style={{width: 800, margin: 2, marginTop: 10}}
            
            label="Control Description"
            color="primary"
            value={description}
            id="description"
            onChange={(e)=>{setDescription(e.target.value)}}
            multiline
            rows="2"
            
            inputProps={{
              style: {fontSize: 16, color: "blue"}
            }}
          
          
          />
        </div>
        
        
        
        
        <div align="center" style={{marginTop:50}}>
          
          
          
          
          
          <div>
            
            
            <Button onClick={saveHeader} className={classes.shape} align="left" variant="contained" color="primary"
                    style={{width: 200, height: 30}}>Save Header </Button>
            
            
            <Button onClick={cancelHeader} className={classes.shape} align="left" variant="contained" color="secondary"
                    style={{width: 100, height: 30}}>Cancel </Button>
          </div>
          
        </div>
      
      </div>
    
    
    </div>
  
    
  
  );
  
  
  
}

function Finalize(props)  {
  
  const {callback} = props;
  const [password, setPassword] = React.useState("");
  const classes = style();
  
  const finalize = () => {
    
    callback(false,password);
  };
  
  const cancel = () => {
    callback(true);
  };
  
  return (
    
    <div align="center" style={{marginTop:100}}>
      
      <div align="center" style={{marginTop:100}}>
        <Typography variant="h3"> Finalize Template and Send to the Network </Typography>
        
      </div>
      
      <div align="center" style={{marginTop:100}}>
  
        <TextField
    
          style={{width: 400, marginTop: 30, backgroundColor: "#FFFFFF", height: 20}}
          placeholder="password for private key"
          value={password}
          id="password"
          onChange={(e) => {setPassword(e.target.value)}}
          color="secondary"
        />
        
        
      </div>
  
      <div align = "center" style={{marginTop:100}}>
    
        <Button onClick={finalize} className={classes.shape} variant="contained" align="right"
                style={{width: 250, height: 30}} color="secondary"> Send to Network  </Button>
    
        <Button onClick={cancel} className={classes.shape} variant="contained" align="right"
                style={{width: 120, height: 30, marginLeft: 50}} color="secondary"> Cancel </Button>
  
      </div>
      
    </div>
    
  )
}

function Result(props)  {
  
  const {callback, result} = props;
  const classes = style();
  
  const done = () => {
    callback();
  };
  

  
  return (
    
    <div align="center" style={{marginTop:100}}>
  
      {result.success &&
      <div align="center" style={{marginTop:100}}>
        
          <Typography variant="h3"> Transaction Submitted successfully </Typography>
        
      </div>
      
      }
  
      {!result.success &&
  
      <div align="center" style={{marginTop: 100}}>
    
    
        <Typography variant="h3"> Transaction submission failed with error: </Typography>
        <Typography variant="h5"> {result.error} </Typography>
  
  
      </div>
      }
      
      <div align = "center" style={{marginTop:100}}>
        
        <Button onClick={done} className={classes.shape} variant="contained" align="right"
                style={{width: 250, height: 30}} color="secondary"> Done </Button>
        
        
      </div>
    
    </div>
  
  )
}



