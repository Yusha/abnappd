import React,{useEffect} from 'react';


import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import MenuItem from '@material-ui/core/MenuItem';
import InputAdornment from '@material-ui/core/InputAdornment';

import {Editor} from 'react-draft-wysiwyg';

import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { EditorState, convertToRaw, convertFromRaw } from 'draft-js';

import EditIcon from '@material-ui/icons/Edit';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';

import Grid from '@material-ui/core/Grid';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';





import axios from 'axios';

import { makeStyles } from '@material-ui/core/styles';
import {GetSignature,GetHash} from '../../ec/CryptoUtils';


const style = makeStyles((theme)=> ({
  dialog: {
    position: 'absolute',
    left: 10,
    top: 50
  },
  input: {
    display: 'none',
  },
  shape: {
    borderRadius: 25,
  },
  selectRoot: {
    '& .MuiTextField-root': {
      margin: theme.spacing(1),
      width: '40ch',
    },
  },
  
}));

export default  function NewTemplate(props) {
  
  
  const { context } = props;
  
  const classes = style();
  
  
  
  const [createNew, setCreateNew] = React.useState(false);
  
  const [isTemplate, setTemplate] = React.useState(false);
  const [templateData, setTemplateData] = React.useState(false);
  const [finalize, setFinalize] = React.useState(false);
  const [result,setResult] = React.useState({set:false});
  
  
  
  const templateCallback = (cancel,data) => {
  
    setTemplate(false);
    
    if (!cancel) {
      setTemplate(false);
      
      setTemplateData(data);
      setFinalize(true);
    } else {
      setCreateNew(false);
    }
  };
  
  const createNewTemplate = () => {
    setCreateNew(true);
    setTemplate(true);
  };
  
  
  
  
  const onFinalize = (cancel,password) => {
    if (cancel) {
      setFinalize(false);
      setTemplate(true);
      return;
    }
    
    // send the template to
    const templateS = JSON.stringify(templateData);
    const hash = GetHash(templateS);
  
    const signature = GetSignature(context.encryptedPK,password,hash);
    const publicKey = context.publicKey;
    
  
    
    const tx = {
      publisher:"System",
      senderHome: context.homeId,
      senderAccount:context.user,
      senderPublicKey: publicKey,
      hash:hash,
      signature:signature,
      body:templateS
    };
    
    // Send the request to the server...
    const url = "http://localhost:4000/contracts/templates/add";
    
    return axios.post(url,tx).then ((result) => {
      
      if (result.data.success) {
        // message posted successfully
        setResult({set:true,success:true})
      } else {
        setResult({set:true,success:false,error:result.data.error})
      }
      setFinalize(false);
      
    });
    
    
  };
  
  const onDone = () => {
    setCreateNew(false);
    setResult(false);
  };
  

  
  return (
    
    <div className={classes.content} >
        
      <div className={classes.margin}>
          <Typography color="primary" variant="h1" align="center">
            
            {!createNew &&

              <Button color="primary" onClick={createNewTemplate}> Create a New Smart Template</Button>
            }
            
            </Typography>
      </div>
      
      <div>
        {isTemplate &&
          <Template callback={templateCallback}/>
        }
        
        {finalize &&
        
          <Finalize callback = {onFinalize} />
        }
        
        {result.set &&
        
          <Result callback = {onDone} result={result} />
        }
      </div>
  
      
    </div>
    
  );
}



function Template(props) {
  
  const {callback} = props;
  const classes = style();
  
  const [isHeaderSet, setIsHeaderSet] = React.useState(false);
  
  
  const [headerData, setHeaderData] = React.useState({});
  
  const [items, setItems] = React.useState([]);
  const [isInsert, setIsInsert] = React.useState(false);
  const [insertText, setInsertText] = React.useState(false);
  const [insertFunction, setInsertFunction] = React.useState(false);
  const [insertControl, setInsertControl] = React.useState(false);
  const [insertControlFooter, setInsertControlFooter] = React.useState(false);
  const [controlId, setControlId] = React.useState(0);
  const [textEditorState, setTextEditorState] = React.useState(EditorState.createEmpty());
  
  
  // Function variables
  const [variableId, setVariableId] = React.useState("");
  const [variableDescription, setVariableDescription] = React.useState("");
  const [variableType, setVariableType] = React.useState("");
  const [variablePrefix, setVariablePrefix] = React.useState("");
  const [controls, setControls] = React.useState([]);
  
  
  useEffect(()=> {
    
    const url = "http://localhost:4000/contracts/templates/controls/get?publisher=System" ;
    
    axios.get(url).then ((result) => {
      setControls(result.data);
      
    }).catch ((error) => {
      console.log("Error in getting Template Controls  " + JSON.stringify(error));
    })
    
  },[]);
  
  
  const saveTemplate = () => {
    callback(false,{heading:headerData,sections:items});
  };
  
  const cancelTemplate = () => {
    callback(true);
  };
  
  const onInsertText = () => {
    setIsInsert(true);
    setInsertText(true);
  };
  
  const onInsertFunction = () => {
    setIsInsert(true);
    setInsertFunction(true);
    
  };
  
  const onInsertControl = () => {
    setInsertControl(true);
    
  };
  
  const cancelItem = () => {
    setIsInsert(false);
    setInsertFunction(false);
    
  };
  const saveItem = () => {
    if (insertText) {
      const raw = convertToRaw(textEditorState.getCurrentContent());
      
      setTextEditorState(EditorState.createEmpty());
      setItems(items.concat({type:"text",value:raw}));
      setIsInsert(false);
      setInsertText(false);
      
    }
    if (insertFunction) {
      setItems(items => [...items,
        {type:"variable",variableType:variableType, id:variableId,description:variableDescription,prefix:variablePrefix}]);
      
      setIsInsert(false);
      setInsertFunction(false);
      
    }
  };
  
  const saveControl = () => {
    let cid = controls[controlId]._id;
    setItems(items => [...items,
      {type:"control",name:cid}]);
    setControlId(0);
    setInsertControl(false);
    setInsertControlFooter(true);
  };
  
  const onAddControlFooter = () => {
    let cid = controls[controlId]._id;
    setItems(items => [...items,
      {type:"controlFooter",name:cid}]);
    
    setInsertControlFooter(false);
    
  };
  
  const onNoControlFooter = () => {
    setInsertControlFooter(false);
  };
  
  
  const cancelControl = () => {
    setInsertControl(false);
  };
  
  const handleListClick = (event, index) => {
    setControlId(index);
    
  };
  
  const getControlList = () => {
    
    return controls.map((item,index) => {
      
      const body = JSON.parse(item.tx.body);
      return (
        <ListItem
          button
          selected={controlId === index}
          key={index}
          onClick={(event) => handleListClick(event, index)} >
          
          <ListItemText primary={body.heading.name} color="secondary" />
        </ListItem>
      
      )
    });
    
  };
  
  
  const onTextEditorStateChange = (s) => {
    setTextEditorState(s);
  };
  
  const setFieldValue = (e) => {
    
    const id = e.target.id;
    const value = e.target.value;
    
    if (id ==="vid") {
      setVariableId(value);
    } else if (id === "vdesc") {
      setVariableDescription(value)
      
    } else if (id === "prefix") {
      setVariablePrefix(value);
    }
    else {
      console.log("id  is  " + id);
    }
    
    
  };
  
  
  const templateHeaderCallback = (cancel,data) => {
    
    if (cancel) {
      setIsHeaderSet(false);
      callback(true);
      
    } else {
      setHeaderData(data);
      setIsHeaderSet(true);
    }
  };
  
  
  const getHeaderParties = () => {
    
    return headerData.parties.map((party, index) => {
      
      return (
  
        <TextField
          style={{width: 500, margin: 2, marginTop: 10}}

          inputProps={{
            readOnly: true,
          }}
          label={party}
        />
      )
    })
  };
  
  const getHeader = () => {
    
    
    
    if (isHeaderSet) {
      return (
        
        <div>
  
          <div className={classes.margin} style={{marginTop: 20, width: 500}}>
            <Typography variant="h4"> {headerData.name} </Typography>
            <Typography variant="body1"> {headerData.description} </Typography>
          </div>
          
          <div style={{marginTop:50}}>
            <Typography variant="h6"> Contract Schedule </Typography>
          </div>
  
          { headerData.hasStartDate &&
          
          <div>
              
              <TextField
                style={{width: 500, margin: 2, marginTop: 10}}

                inputProps={{
                  readOnly: true,
                }}
                label="Contract Execution Date"
              />
              
            </div>
    
    
          }
  
          { headerData.hasEndDate &&
  
          <div>
    
            <TextField
              style={{width: 500, margin: 2, marginTop: 10}}

              inputProps={{
                readOnly: true,
              }}
              label="Contract Termination Date"
            />
  
          </div>
    
    
          }
  
          
          
          <div style={{marginTop:50}}>
            
            <Typography variant="h6"> Contract Parties</Typography>
            
            {getHeaderParties()}
            
          </div>
          
         
        
        </div>
        
      
      
      );

    }
    
  };
  
  const getBody = () => {
    
    return (
      
      <div>
        {isHeaderSet &&
          <div style={{marginTop: 50}}>
            <Typography variant="h6"> Contract Body</Typography>
          </div>
        }
        <div style={{marginTop:50}}>
          
          {getItems()}
        </div>
      
      
      </div>
    )
    
  };
  
  const getItems = () => {
    
    return items.map((item,index) => {
      if (item.type === "text") {
        
        const editorState = EditorState.createWithContent(convertFromRaw(item.value));
        
        return (
  
          <Grid
            container
            direction="row"
            justify="flex-start"
            alignItems="flex-start"
          >
  
            <div className={classes.margin} style={{marginTop:1,width:500}} >
              <Editor
                editorState={editorState}
                toolbarClassName="toolbarClassName"
                wrapperClassName="wrapperClassName"
                editorClassName="editorClassName"
                toolbarHidden
                readOnly
                editorStyle={{backgroundColor:"#FFFFFF",height:"auto"}}
              />
            
            </div>
            
            <div align="center">
              
                <IconButton aria-label="edit" size="small">
                  <EditIcon />
                </IconButton>
                <IconButton aria-label="delete" size="small">
                  <DeleteIcon />
                </IconButton>
              
            </div>
          </Grid>
          
        )
      } else if (item.type === "variable" ) {
  
        if ((item.variableType === "text") ) {
    
          return (
      
            <TextField
        
              style={{width: 500, margin: 2, marginTop: 10}}
        
              inputProps={{
                style: {fontSize: 15},
                readOnly: true,
              }}
              color="primary"
              
              placeholder={item.description}
              
      
            />
    
    
          )
        } else if (item.variableType === "party") {
    
          return (
      
            <div>
              <TextField
          
                style={{width: 500, margin: 2, marginTop: 10}}
          
                inputProps={{
                  style: {fontSize: 15}
                }}
                color="secondary"
          
                placeholder={item.description}
                
                InputProps={{
                  startAdornment: <InputAdornment position="start">{item.prefix}  :</InputAdornment>,
                  readOnly: true,
                }}
                
        
              />
              
            </div>
          )
    
    
        } else if ((item.variableType === "date") ||
                  (item.variableType === "number")) {
  
          return (
    
            <TextField
      
              style={{width: 500, margin: 2, marginTop: 10}}
      
              inputProps={{
                style: {fontSize: 15}
              }}
              color="primary"
              
              placeholder={item.description}
              InputProps={{
                startAdornment: <InputAdornment position="start">{item.prefix}  :</InputAdornment>,
                readOnly: true,
              }}
      
            />
  
  
          )
        }
  
      } else if (item.type ==="control") {
        
        return (
          <div>
            {getControlBody(item.name)}
            
          </div>
          
        )
      } else if (item.type === "controlFooter") {
        return (
          <div align="center" style={{marginTop:30, width:800}}>
  
            <Button className={classes.shape} align="left" variant="contained" color="primary"
                    style={{width: 200, height: 30, marginRight:30}}>Add Control </Button>
  
            <Button className={classes.shape} align="left" variant="contained" color="primary"
                    style={{width: 200, height: 30}}>Delete Control </Button>
            
          </div>
          
        )
        
      }
    })
    
  };
  
  
  const getControlBody = (cid) => {
    
    let controlObject = "";
    for (let i =0; i <controls.length; i++) {
      if (controls[i]._id === cid) {
        controlObject = controls[i];
        break;
      }
    }
    let items = JSON.parse(controlObject.tx.body).sections;
    
    
    return (
      
      <div style={{marginTop:100, borderStyle:"solid", borderColor:"black", width:800}}>
        <div style={{marginTop:50}}>
          <Typography variant="h6" > </Typography>
        </div>
        <div style={{marginLeft:50,marginTop:50}}>
          
          {getControlItems(items)}
        </div>
      
      
      </div>
    )
    
  };
  const getControlItems = (items) => {
    
    return items.map((item,index) => {
      if (item.type === "text") {
        
        const editorState = EditorState.createWithContent(convertFromRaw(item.value));
        
        return (
          
          
          <div className={classes.margin} style={{marginTop:1,width:500}} >
            <Editor
              editorState={editorState}
              toolbarClassName="toolbarClassName"
              wrapperClassName="wrapperClassName"
              editorClassName="editorClassName"
              toolbarHidden
              readOnly
              editorStyle={{backgroundColor:"#FFFFFF",height:"auto"}}
            />
          
          </div>
        
        
        
        )
      } else if (item.type === "variable" ) {
        
        if ((item.variableType === "text") ) {
          
          return (
            
            <TextField
              
              style={{width: 500, margin: 2, marginTop: 10}}
              
              inputProps={{
                style: {fontSize: 15},
                readOnly: true,
              }}
              color="primary"
              label={item.label}
              
              helperText={item.description}
              
              readOnly
            
            
            />
          
          
          )
        } else if (item.variableType === "party") {
          
          return (
            
            <div>
              <TextField
                
                style={{width: 500, margin: 2, marginTop: 10}}
                
                inputProps={{
                  style: {fontSize: 15}
                }}
                color="secondary"
                
                placeholder={item.description}
                
                readOnly
              
              
              />
            
            </div>
          )
          
          
        } else if ((item.variableType === "date") ||
          (item.variableType === "number")) {
          
          return (
            
            <TextField
              
              style={{width: 500, margin: 2, marginTop: 10}}
              
              inputProps={{
                style: {fontSize: 15}
              }}
              color="primary"
              
              placeholder={item.description}
              InputProps={{
                startAdornment: <InputAdornment position="start">{item.prefix}  :</InputAdornment>,
              }}
              
              readOnly
            
            
            />
          
          
          )
        }else if (item.variableType === "signature") {
          
          return (
            
            <div style={{marginTop:50}}>
              <Typography variant="h5" color="secondary">
                Signature
              </Typography>
              
              <div >
                <TextField
                  
                  style={{width: 300, margin: 2, marginTop: 10}}
                  
                  inputProps={{
                    style: {fontSize: 15}
                  }}
                  color="secondary"
                  
                  label = "Signatory Name"
                
                
                />
                <TextField
                  
                  style={{marginLeft:30,width: 300, margin: 2, marginTop: 10}}
                  
                  inputProps={{
                    style: {fontSize: 15}
                  }}
                  color="secondary"
                  
                  label= "Signatory Title"
                
                />
              
              
              </div>
              
              <div >
                <TextField
                  
                  style={{width: 300, margin: 2, marginTop: 10}}
                  
                  inputProps={{
                    style: {fontSize: 15}
                  }}
                  color="secondary"
                  
                  label = "Signatory Home"
                
                
                />
                <TextField
                  
                  style={{marginLeft:10, width: 300, margin: 2, marginTop: 10}}
                  
                  inputProps={{
                    style: {fontSize: 15}
                  }}
                  color="secondary"
                  
                  label= "Signatory Account"
                
                />
              
              
              </div>
              
              <div style={{marginTop:30}}>
                <Typography> Signatory Role</Typography>
                
                <FormGroup row>
                  {getControlRoles(item.roles)}
                
                </FormGroup>
              
              
              
              </div>
            
            </div>
          )
          
        }
        
      }
    })
    
  };
  const getControlRoles = (signerRoles) => {
    
    return signerRoles.map((role,index) => {
      return (
        
        <FormControlLabel
          control={
            <Checkbox  />
          }
          label= {role}
        />
      
      )
      
    })
    
  };
  
  
  const onSelectFunction = (e) => {
    const vType = e.target.value;
    setVariableType(vType);
  };
  
  
  return (
  
  <div style={{width: "80%", align: "center", marginLeft: 50}}>
  
  
    {!isHeaderSet &&
    <div>
      <TemplateHeader callback = {templateHeaderCallback}/>
      
    </div>
    
    }
  
    <div>
  
      {getHeader()}
      
      {getBody()}
  
    </div>
  
    <div align="center" style={{marginTop: 100, marginBottom: 100}}>
    
    
      {!isInsert && isHeaderSet && !insertControl && !insertControlFooter&&
      <div>
      
        <Typography variant="h5"> Add Contract Body Items </Typography>
      
        <div style={{marginTop:100}}>
        
          <Button onClick={onInsertText} className={classes.shape} align="left" variant="contained" color="primary"
                  style={{width: 200, height: 30}}>Insert Text </Button>
        
          <Button onClick={onInsertFunction} className={classes.shape} variant="contained" align="right"
                  style={{width: 200, height: 30, marginLeft: 20}} color="secondary"> Insert Function </Button>
        
          <Button onClick={onInsertControl} className={classes.shape} variant="contained" align="right"
                  style={{width: 200, height: 30, marginLeft: 20}} color="secondary"> Insert Control </Button>
        
          
        </div>
  
        <div style={{marginTop:50}}>
    
          <Button onClick={saveTemplate} className={classes.shape} variant="contained" align="right"
                  style={{width: 200, height: 30, marginLeft: 20}} color="secondary"> Save Template </Button>
    
          <Button onClick={cancelTemplate} className={classes.shape} variant="contained" align="right"
                  style={{width: 120, height: 30, marginLeft: 20}} color="secondary"> Cancel </Button>
        </div>
    
      </div>
      
      }
    
      {insertText &&
    
      <div className={classes.margin} style={{marginTop:30,width:800}}>
        <Editor
          editorState={textEditorState}
          toolbarClassName="toolbarClassName"
          wrapperClassName="wrapperClassName"
          editorClassName="editorClassName"
          placeholder = "Note for read request..."
          onEditorStateChange={onTextEditorStateChange}
          editorStyle={{backgroundColor:"#FFFFFF",height:"auto"}}
        />
    
      </div>
      
      
      }
    
      { insertFunction &&
      <div>
      
        <div style={{marginTop:30}} align="center">
        
        
          <TextField
            style={{width: 70, margin: 2, marginTop: 10}}
            value = {variablePrefix}
            id = "prefix"
            onChange={setFieldValue }
            color = "secondary"
          
            label="prefix"
          />
        
          <TextField
            style={{width: 200, margin: 2, marginTop: 10}}
            value = {variableId}
            id = "vid"
            onChange={setFieldValue }
          
            label="Variable Id"
          />
        
          <TextField
            style={{width: 400, margin: 2, marginTop: 10}}
            value = {variableDescription}
            id = "vdesc"
            onChange={setFieldValue }
            label="Variable description"
          />
          
        </div>
      
        <div  style={{marginTop:10,width:800}} >
        
          <TextField
          
            select
            value={variableType}
            onChange={onSelectFunction}
            helperText="Please select variable type"
          >
          
            <MenuItem key="1" value="text">
              Text Function
            </MenuItem>
          
            <MenuItem key="2" value="number">
              Number Function
            </MenuItem>
            <MenuItem key="3" value="date">
              Date Function
            </MenuItem>
            <MenuItem key="4" value="party">
              Party Function
            </MenuItem>
        
          </TextField>
      
        </div>
    
    
      </div>
      
      }
    
      {isInsert &&
      <div>
      
        <Button onClick={saveItem} className={classes.shape} variant="contained" align="right"
                style={{width: 120, height: 30, marginLeft: 20}} color="secondary"> Save </Button>
      
        <Button onClick={cancelItem} className={classes.shape} variant="contained" align="right"
                style={{width: 120, height: 30, marginLeft: 20}} color="secondary"> Cancel </Button>
    
      </div>
      }
  
      { insertControl &&
      <div align="center" style={{marginTop:30, width:800}}>
    
        <List component="nav" align="center">
          {getControlList()}
    
        </List>
  
      </div>
    
      }
  
      {insertControl &&
      <div>
    
        <Button onClick={saveControl} className={classes.shape} variant="contained" align="right"
                style={{width: 120, height: 30, marginLeft: 20}} color="secondary"> Save Control </Button>
    
        <Button onClick={cancelControl} className={classes.shape} variant="contained" align="right"
                style={{width: 120, height: 30, marginLeft: 20}} color="secondary"> Cancel </Button>
  
      </div>
      }
  
      {insertControlFooter &&
      <div>
    
        <Button onClick={onAddControlFooter} className={classes.shape} variant="contained" align="right"
                style={{width: 200, height: 30, marginLeft: 20}} color="secondary"> Add Control Footer </Button>
    
        <Button onClick={onNoControlFooter} className={classes.shape} variant="contained" align="right"
                style={{width: 200, height: 30, marginLeft: 20}} color="secondary"> Continue </Button>
  
      </div>
      }
  
  
    </div>
    
  
    
    
  </div>
  
  
  );
}

function TemplateHeader(props)  {
  
  const {callback} = props;
  
  const classes = style();
  
  const [name, setName] = React.useState("");
  const [description, setDescription] = React.useState("");
  const [parties, setParties] = React.useState([]);
  const [hasStartDate, setHasStartDate] = React.useState(true);
  const [hasEndDate, setHasEndDate] = React.useState(true);
  const [isBreakable, setIsBreakable] = React.useState(true);
  
  const [isAddParty, setIsAddParty] = React.useState(false);
  
  const [partyName,setPartyName] = React.useState("");
  
  const saveHeader = () => {
    callback(false,{name:name,description:description,parties:parties,hasStartDate:hasStartDate, hasEndDate:hasEndDate,isBreakable:isBreakable});
  };
  
  const cancelHeader = () => {
    callback(true);
    
  };
  
  const addParty = () => {
    setIsAddParty(false);
    setParties(parties => [...parties,partyName]);
    
  };
  
  const getParties = () => {
    
    return parties.map((party,index) => {
      
      return (
        
        <TextField
          
          style={{width: 500, margin: 2, marginTop: 10}}
          
          inputProps={{
            style: {fontSize: 15},
            readOnly: true
          }}
          color="primary"
          
          placeholder={party}
          readOnly
        
        />
      
      
      )
      
    });
    
  };
  
  
  
  return (
    
    <div style={{width: "80%", marginLeft: 50}}>
      
      <div>
        
        <div align="center">
          <Typography variant="h5"> Contract Header</Typography>
        
        </div>
        <div className={classes.margin} align="center" style={{marginTop: 30}}>
          <TextField
            
            style={{width: 500, margin: 2, marginTop: 10}}
            
            inputProps={{
              style: {fontSize: 25}
            }}
            color="primary"
            value={name}
            label={
              <Typography variant="h5"> Name of the smart template </Typography>
            }
            
            id="name"
            
            fullWidth
            
            onChange={(e)=>{setName(e.target.value)}}
          
          />
          
          <TextField
            
            style={{width: 500, margin: 2, marginTop: 10}}
            
            label="Description of the template"
            color="primary"
            value={description}
            id="description"
            onChange={(e)=>{setDescription(e.target.value)}}
            multiline
            rows="2"
            
            inputProps={{
              style: {fontSize: 16, color: "blue"}
            }}
          
          
          />
        </div>
        
        <div align="center" style={{width:500,marginTop: 10}}>
          
          <Typography variant="h6"> Contract Options </Typography>
          
          <div className={classes.margin}>
            <Grid container spacing={1} alignItems="flex-end">
              <FormControlLabel
                control={
                  <Checkbox checked={hasStartDate} onChange={(e)=>{setHasStartDate(e.target.checked)}} />
                }
                label="has Start Date"
              />
            </Grid>
          </div>
          <div className={classes.margin}>
            <Grid container spacing={1} alignItems="flex-end">
              <FormControlLabel
                control={
                  <Checkbox checked={hasEndDate} onChange={(e)=>{setHasEndDate(e.target.checked)}} />
                }
                label="has End Date"
              />
            </Grid>
          </div>
          
          <div className={classes.margin}>
            <Grid container spacing={1} alignItems="flex-end">
              <FormControlLabel
                control={
                  <Checkbox checked={isBreakable} onChange={(e)=>{setIsBreakable(e.target.checked)}} />
                }
                label="Contract is Breakable"
              />
            </Grid>
          </div>
        
        
        </div>
        
        <div className={classes.margin} align="center" style={{width:500,marginTop: 10}}>
          <Typography variant="h6"> Contract Parties </Typography>
          
          <div style={{marginTop:10}}>
            
            {getParties()}
          
          </div>
          
          { isAddParty &&
          
          <div style={{marginTop:20}} align="center">
            
            <TextField
              style={{width: 500, margin: 2, marginTop: 10}}
              value = {partyName}
              
              onChange={(e)=>{ setPartyName(e.target.value)}}
              label="Party Name"
            />
          
          </div>
          
          }
        
        </div>
        
        
        
        <div align="center" style={{marginTop:50}}>
          
          {isAddParty &&
          
          <div>
            <Button onClick={addParty} className={classes.shape} align="left" variant="contained" color="primary"
                    style={{width: 100, height: 30}}>Save Party </Button>
            
            <Button onClick={()=>{setIsAddParty(false)}} className={classes.shape} align="left" variant="contained" color="primary"
                    style={{width: 100, height: 30}}>Cancel </Button>
          
          </div>
          
          }
          
          {!isAddParty &&
          
          <div>
            
            <Button onClick={()=>{setIsAddParty(true)}} className={classes.shape} align="left" variant="contained" color="primary"
                    style={{width: 200, height: 30}}>Add party </Button>
            
            <Button onClick={saveHeader} className={classes.shape} align="left" variant="contained" color="primary"
                    style={{width: 200, height: 30}}>Save Header </Button>
            
            
            <Button onClick={cancelHeader} className={classes.shape} align="left" variant="contained" color="secondary"
                    style={{width: 100, height: 30}}>Cancel </Button>
          </div>
          }
        </div>
      
      </div>
    
    
    </div>
  
  
  
  
  );
  
  
  
}

function Finalize(props)  {
  
  const {callback} = props;
  const [password, setPassword] = React.useState("");
  const classes = style();
  
  const finalize = () => {
    
    callback(false,password);
  };
  
  const cancel = () => {
    callback(true);
  };
  
  return (
    
    <div align="center" style={{marginTop:100}}>
      
      <div align="center" style={{marginTop:100}}>
        <Typography variant="h3"> Finalize Template and Send to the Network </Typography>
        
      </div>
      
      <div align="center" style={{marginTop:100}}>
  
        <TextField
    
          style={{width: 400, marginTop: 30, backgroundColor: "#FFFFFF", height: 20}}
          placeholder="password for private key"
          value={password}
          id="password"
          onChange={(e) => {setPassword(e.target.value)}}
          color="secondary"
        />
        
        
      </div>
  
      <div align = "center" style={{marginTop:100}}>
    
        <Button onClick={finalize} className={classes.shape} variant="contained" align="right"
                style={{width: 250, height: 30}} color="secondary"> Send to Network  </Button>
    
        <Button onClick={cancel} className={classes.shape} variant="contained" align="right"
                style={{width: 120, height: 30, marginLeft: 50}} color="secondary"> Cancel </Button>
  
      </div>
      
    </div>
    
  )
}

function Result(props)  {
  
  const {callback, result} = props;
  const classes = style();
  
  const done = () => {
    callback();
  };
  

  
  return (
    
    <div align="center" style={{marginTop:100}}>
  
      {result.success &&
      <div align="center" style={{marginTop:100}}>
        
          <Typography variant="h3"> Transaction Submitted successfully </Typography>
        
      </div>
      
      }
  
      {!result.success &&
  
      <div align="center" style={{marginTop: 100}}>
    
    
        <Typography variant="h3"> Transaction submission failed with error: </Typography>
        <Typography variant="h5"> {result.error} </Typography>
  
  
      </div>
      }
      
      <div align = "center" style={{marginTop:100}}>
        
        <Button onClick={done} className={classes.shape} variant="contained" align="right"
                style={{width: 250, height: 30}} color="secondary"> Done </Button>
        
        
      </div>
    
    </div>
  
  )
}



