
import React, { useEffect,useStates } from 'react';
import { makeStyles } from '@material-ui/core/styles';


import MaterialTable from 'material-table';
import { forwardRef } from 'react';


import axios from 'axios';
import CodeFunctionDetails from "./CodeFunctionDetails";
import tableIcons from '../common/IconDef';

export default  function SystemCodeFunctions(props) {
  
  const { context } = props;
  
  const classes = tableTheme();
  
  const [tableData, setTableData] = React.useState([]);
  const [indexData, setIndexData] = React.useState({});
  const [openDetailsDialog, setOpenDetailsDialog] = React.useState(false);
  
  
  
  useEffect(()=> {
    
    const url = "http://localhost:4000/contracts/codes/get?publisher=System";
    
    axios.get(url).then ((result) => {
      setTableData(result.data);
      
    }).catch ((error) => {
      console.log("Error in getting Internal Pending Invitations data  " + JSON.stringify(error));
    })
    
  },[]);
  
  
  
  
  const getDataSet = () => {
    const data = tableData.map((row, index) => {
      
      return {name: row.name,templateName:row.templateName,templateId:row.templateId,publisher:row.publisher};
    });
    return data;
  };
  
  const rowClicked = (e,row) => {
    
    setIndexData(tableData[row.tableData.id]);
    
    setOpenDetailsDialog(true);
    
  };
  
  const detailsDialogCallback = (f) => {
    setOpenDetailsDialog(false);
  };
  
  
  
  return (
    
    <div className={classes.table}>
      <MaterialTable
        title="System Code Functions"
        icons={tableIcons}
        
        
        localization={{
          header: {
            actions: 'View'
          },
        }}
        
        columns={[
          { title: 'Publisher', field: 'publisher' },
          { title: 'Name', field: 'name' },
          { title: 'Template Name', field: 'templateName' },
          { title: 'Template Id', field: 'templateId' },
          
        ]}
        data={getDataSet()}
        
        actions={[
          rowData => ({
            icon: tableIcons.ViewEye,
            tooltip: 'View Code Function',
            onClick: rowClicked
          })
        ]}
        
        options={{
          actionsColumnIndex: 4,
          padding:"dense",
          
          headerStyle: {
            color: 'white',
            background:'#0091EA',
            fontSize:20,
            fontFamily:"Roboto"
          },
          rowStyle: {
            color: 'black'
          }
          
        }}
        
        components={{
        
        }}
      />
  
      {openDetailsDialog &&
  
        <CodeFunctionDetails open={openDetailsDialog} context = {context} callback={detailsDialogCallback} data={indexData}/>
    
      }
      
    </div>
  )
}



const tableTheme = makeStyles(theme => ({
  table: {
    '& tbody>.MuiTableRow-root:hover': {
      background: '#EEE',
    }
  },
  body: {
    textColor:"primary"
  }
}));
