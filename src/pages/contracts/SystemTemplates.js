
import React, { useEffect,useStates } from 'react';
import { makeStyles } from '@material-ui/core/styles';


import MaterialTable from 'material-table';
import { forwardRef } from 'react';

import TemplateDetails from './TemplateDetails';


import axios from 'axios';
import tableIcons from '../common/IconDef';


export default  function SystemTemplates(props) {
  
  const { context } = props;
  
  const classes = tableTheme();
  
  const [tableData, setTableData] = React.useState([]);
  const [indexData, setIndexData] = React.useState({});
  const [openDetailsDialog, setOpenDetailsDialog] = React.useState(false);
  
  
  
  useEffect(()=> {
    
    const url = "http://localhost:4000/contracts/templates/get?publisher=System";
    
    axios.get(url).then ((result) => {
      setTableData(result.data);
      
    }).catch ((error) => {
      console.log("Error in getting Internal Pending Invitations data  " + JSON.stringify(error));
    })
    
  },[]);
  
  
  
  
  const getDataSet = () => {
    const data = tableData.map((row, index) => {
      
      const body = JSON.parse(row.tx.body);
      return {publisher: "System", name: body.heading.name,description:body.heading.description};
    });
    return data;
  };
  
  const rowClicked = (e,row) => {
    
    setIndexData(tableData[row.tableData.id]);
    
    setOpenDetailsDialog(true);
    
  };
  
  const detailsDialogCallback = (f) => {
    setOpenDetailsDialog(false);
  };
  
  
  
  return (
    
    <div className={classes.table}>
      <MaterialTable
        title="System Templates"
        icons={tableIcons}
        
        
        localization={{
          header: {
            actions: 'View'
          },
        }}
        
        columns={[
          { title: 'Publisher', field: 'publisher' },
          { title: 'Name', field: 'name' },
          { title: 'Description', field: 'description' },
          
        ]}
        data={getDataSet()}
        
        actions={[
          rowData => ({
            icon: tableIcons.ViewEye,
            tooltip: 'View Template',
            onClick: rowClicked
          })
        ]}
        
        options={{
          actionsColumnIndex: 4,
          padding:"dense",
          
          headerStyle: {
            color: 'white',
            background:'#0091EA',
            fontSize:20,
            fontFamily:"Roboto"
          },
          rowStyle: {
            color: 'black'
          }
          
        }}
        
        components={{
        
        }}
      />
  
      {openDetailsDialog &&
  
        <TemplateDetails open={openDetailsDialog} context = {context} callback={detailsDialogCallback} data={indexData}/>
    
      }
      
    </div>
  )
}


const tableTheme = makeStyles(theme => ({
  table: {
    '& tbody>.MuiTableRow-root:hover': {
      background: '#EEE',
    }
  },
  body: {
    textColor:"primary"
  }
}));
