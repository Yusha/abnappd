
import React from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';

import Dialog from '@material-ui/core/Dialog';
import Typography from '@material-ui/core/Typography';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';


export default function CodeFunctionResult(props) {
  
  const {data,callback,open } = props;
  const classes = dialogStyles();
  
  
  const handleClose = (action) => {
    callback(action);
  };
  
  
  
  
  const StyledTableCell = withStyles((theme) => ({
    head: {
      backgroundColor: "#0091EA",
      color: "white",
      fontSize:20
    },
    body: {
      fontSize: 14,
    },
  }))(TableCell);
  
  const StyledTableRow = withStyles((theme) => ({
    root: {
      '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
      },
    },
  }))(TableRow);
  
  
  const getProperties = () => {
    
    
    return data.result.map((item,index) => {
      
      return (
        <StyledTableRow key={index}>
          <StyledTableCell >
            {item.name}
          </StyledTableCell>
          <StyledTableCell >
            {item.liability}
          </StyledTableCell>
          <StyledTableCell>
            {item.liabilityParty.name}
          </StyledTableCell>
        </StyledTableRow>
      )
    })
  };
  
  
  
  return (
    <Dialog onClose={handleClose} open={open} fullWidth maxWidth="lg" classes={{paper: classes.dialog }} style={{marginTop:50, marginLeft:50}}>
      
      <div className={classes.content} style={{width:800,marginLeft:50}}>
        
        <div style={{marginTop:50}}>
          <Typography color="primary" variant="h5" align="center"> {data.name} </Typography>
        </div>
        
        <div style={{marginTop:50}}>
          
          <div>
            <Typography variant="h6"> Contract Maturation Result </Typography>
          </div>
          
         
         
        </div>
        
        <Divider/>
        
        
        <div className={classes.margin} style={{marginTop:30}}>
          
          <div>
            <div>
              <Typography variant="h6"> Contract Result </Typography>
            </div>
          </div>
          
          <TableContainer component={Paper}>
            <Table className={classes.table} aria-label="customized table">
              <TableHead>
                <TableRow>
                  <StyledTableCell>Description</StyledTableCell>
                  <StyledTableCell >Liability Amount </StyledTableCell>
                  <StyledTableCell >Party </StyledTableCell>
                
                </TableRow>
              </TableHead>
              <TableBody>
                {getProperties()}
              </TableBody>
            </Table>
          </TableContainer>
  
          <Divider/>
          
          
          <div align="center" style={{marginTop:50,marginBottom:50}}>
            
            <Button onClick={handleClose} className={classes.shape} align="left" variant="contained" color="primary"
                    style={{width: 100, height: 30}}>Close</Button>
            
          </div>
        </div>
      </div>
    
    
    </Dialog>
  );
}


const dialogStyles = makeStyles({
  dialog: {
    position: 'absolute',
    left: 50,
    top: 50
  },
  table: {
    minWidth: 700,
  },
});

